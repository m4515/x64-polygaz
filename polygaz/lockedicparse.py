
# coding=utf-8

""" Module for reading and parsing a locking configuration .xml file
for an INSTRUCTION CACHE

Example of xml file:
<?xml version="1.0"?>
<lockedic_config>
  <loadpoint id="0" pc="0">
     <line>0x10eac</line>
     <line>0x102e0</line>
     <line>0x10c44</line>
     <line>0x10c40</line>
     <line>0x10fb8</line>
     <line>0x11128</line>
     <line>0x110f0</line>
     <line>0x110bc</line>
  </loadpoint>
</lockedic_config>
"""

import xml.etree.ElementTree as ET
import sys


lockedic_set = {}

def parse(filename):
    """ Parsing function """

    tree = ET.parse(filename)
    root = tree.getroot()

#    for child in root:
#        print(child.tag, child.attrib)
#        #drps.add(int(child.attrib["pc"], 0)) # 0=guess base
#        #lockedic_set.add(int(child.find("pc").text, 0)) # 0=guess base
#    print("Memory lines locked in IC (%d): %s" % (len(lockedic_set), lockedic_set))

    for load_point in root.findall('loadpoint'):
        lockedic_point = int(load_point.attrib["pc"], 0)
        lockedic_set[lockedic_point] = set()
        for line in load_point.findall('line'):
            #print("  %s" % line.text)
            lockedic_set[lockedic_point].add(int(line.text, 0))
        print("IC lines to load at 0x%x (%d):" % (lockedic_point, len(load_point.findall('line'))), lockedic_set[lockedic_point], file = sys.stderr)

def ic_locking_points():
    return lockedic_set.keys()

def is_locked(line, locking_point):
    if line in lockedic_set[locking_point]:
        return True
    else:
        return False

def locked_lines(locking_point):
    return lockedic_set[locking_point]

def main():
    """ Simply parses, for testing purposes """
    if len(sys.argv) != 2:
        print("Usage: %s lockedic_config.xml" % sys.argv[0], file = sys.stderr)
        sys.exit()
    filename = sys.argv[1]

    parse(filename)
    #print(lockedic_set)

if __name__ == "__main__":
    sys.exit(main())
