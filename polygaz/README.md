# Obtention of data memory access patterns and reuse information, and its integration into the WCET analysis of data caches

If you use the code to obtain the data access patterns and reuse information, please provide the following reference:

J. Segarra, J. Cortadella, R. Gran Tejero and V. Viñals-Yúfera, "Automatic Safe Data Reuse Detection for the WCET Analysis of Systems With Data Caches," in IEEE Access, vol. 8, pp. 192379-192392, 2020, doi: 10.1109/ACCESS.2020.3032145.

```
@article{DBLP:journals/access/SegarraCTY20,
  author    = {Juan Segarra and
               Jordi Cortadella and
               Ruben Gran Tejero and
               V{\'{\i}}ctor Vi{\~{n}}als Y{\'{u}}fera},
  title     = {Automatic Safe Data Reuse Detection for the {WCET} Analysis of Systems With Data Caches},
  journal   = {{IEEE} Access},
  volume    = {8},
  pages     = {192379--192392},
  year      = {2020},
  url       = {https://doi.org/10.1109/ACCESS.2020.3032145},
  doi       = {10.1109/ACCESS.2020.3032145},
  timestamp = {Sat, 14 Nov 2020 00:55:58 +0100},
  biburl    = {https://dblp.org/rec/journals/access/SegarraCTY20.bib},
  bibsource = {dblp computer science bibliography, https://dblp.org}
}
```

If you use the code to perform an IPET analysis based on the previous data reuse information, please provide the following reference:

---to be published---

## License

This code is licensed under *GNU General Public License* (version 3) **WITH THE FOLLOWING RESTRICTION**:

*The use, investigation or development, in a direct or indirect way, of any of the scientific contributions of the authors contained in this work by any army or armed group in the world, for military purposes and for any other use which is against human rights or the environment, is strictly prohibited unless written consent is obtained from all the authors of this work, or all the people in the world.*

## Development links
-   [APRON](https://antoinemine.github.io/Apron/doc/) ([fuentes GitHub](https://github.com/antoinemine/apron), [fuentes Debian](http://deb.debian.org/debian/pool/main/a/apron/apron_0.9.10.orig.tar.gz))
-   [ELINA](https://elina.ethz.ch/) (por ahora no lo usamos)
-   [angr](http://angr.io/)
-   [angr-management](https://github.com/angr/angr-management) (interfaz gráfica angr)
-   [PyVex](https://github.com/angr/pyvex) (ya incluido en angr)
-   [Radare](https://www.radare.org/r/) (por ahora no lo usamos)
-   [Manual básico python](https://www.tutorialspoint.com/python/index.htm)
-   [Pylint](https://www.pylint.org/) (test de legibilidad de código python: `pylint3 analysis1.py`)

## Instalación

### en Debian 10

1.  `git clone https://gitlab.com/uz-gaz/polygaz.git polygaz` e ir al directorio base
2.  `apt install virtualenv python3-venv python3-virtualenv build-essential python3-dev python3-wheel libapron libapron-dev parallel`
3.  `python3 -m venv venv` crea un entorno virtual para trabajar en el subdirectorio *venv*.
En `venv/bin/` se creará el ejecutable de python a usar.
4.  `./venv/bin/pip3 install angr` busca, descarga, e instala para el usuario (no para el sistema) todo lo necesario. Para instalar una versión particular: `./venv/bin/pip3 install angr==9.0.4663`.
5.  `make`

Los archivos locales están en ./venv/lib/python3.7/site-packages/* (e.g. stmt.py en ./venv/lib/python3.7/site-packages/pyvex/stmt.py)

### en CentOS 7 (funciona con python3.6, no funciona con 3.4)

1.  `yum install epel-release python36 python36-pip python36-devel gmp-devel mpfr-devel parallel`
2.  `git clone https://gitlab.com/uz-gaz/polygaz.git polygaz` e ir al directorio base
3.  `virtualenv --python=$(which python3) venv` crea un entorno virtual para trabajar en el subdirectorio *venv*. El comando virtualenv equivale a `python3 -m venv venv`.
En `venv/bin/` se creará el ejecutable de python a usar.
4.  `./venv/bin/pip3 install angr` busca, descarga, e instala para el usuario (no para el sistema) todo lo necesario.
5.  Bajar/compilar/instalar apron
6.  No instala bien, copiar manualmente en ~/apron:
    1.  `find . -name lib* -exec cp \{\} ~/apron/lib \;`
    2.  `find . -name *.h -exec cp \{\} ~/apron/include/ \;`
7.  Editar Makefile de polygaz.
Añadir -I/home/jsegarra/apron/include en el comando de compilación y -L/home/jsegarra/apron/lib en el de enlazado.
Si no tiene Parma Polyhedra, quitar -lap_ppl y comentar líneas en apron_interface.c.
8.  Antes de ejecutar, ajustar LD_LIBRARY_PATH: `export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/apron/lib`
9.  `make`

## Mantenimiento

Para comprobar si existe algún paquete actualizado: ```./venv/bin/pip3 list --outdated```

Para actualizar un paquete: ```./venv/bin/pip3 install --upgrade <paquete>```

Para instalar una versión específica: ```./venv/bin/pip3 install angr==8.19.7.25```

## Ejecución

### Vía scripts
-   `make && ./scripts/reuse_detection.sh` lanza un análisis de reuso binarios en paralelo. Hay que editarlo para establecer rutas, tipo de CFG (fast/emulated) a usar en el análisis y número de ejecuciones en paralelo. Genera ficheros de reuso XML.
-   `./scripts/gen_lockms_solve.sh` lanza análisis de WCET usando los XML anteriores. Incluye obtención heurística de DRPs (vía análisis always-miss), y análisis de WCET para LRU asociativa por conjuntos, LRU simulando persistencia (usa solo direcciones constantes), ACDC, always-hit, unlimited-size. Genera y resuelve ficheros LP para lp_solve en el proceso. Genera CSV con datos finales.
-   `./scripts/gen_acdc_config2.sh` genera un fichero de configuración XML para la ACDC (DRPs) a partir de solución de modelo lp_solve IPET always-miss.
-   `./scripts/gen_acdc_config.sh` genera un fichero de configuración XML para la ACDC (DRPs) a partir de solución de modelo lp_solve Lock-MS single-path ACDC.
-   `mkff.sh` genera plantilla para rellenar iteraciones máximas de bucles sobre los benchmarks del directorio actual.

-   OBSOLETO `make && ./scripts/reuse_analysis.sh` lanza el análisis de los binarios en la ruta que tiene definida, 2 análisis en paralelo
-   OBSOLETO `./scripts/gen_static_results.sh` genera el fichero results.csv a partir de los resultados del análisis

### Vía intérprete python
-   `reuse-detection.py` lanza el análisis de reuso. Hay que especificarle tipo de CFG. Llama a `analysis1.py`. Con prints de depuración: `make && ./venv/bin/python3 -u reuse-detection.py [fast|emulated] <binario-a-analizar>`; sin depuración: `make && ./venv/bin/python3 -O reuse-detection.py [fast|emulated] <binario-a-analizar>`.
-   `ipet.py` lanza un análisis IPET para la cache especificada.
-   `lockMS.py` lanza un análisis LockMS para la cache de instrucciones bloqueable y ACDC (single path). CON ACDC PUEDE SUBESTIMAR EL WCET.
-   `mkff.py` genera plantilla para rellenar iteraciones máximas de bucles.
-   `printdot.py` genera una figura DOT del CFG o árbol.

-   OBSOLETO `analysis1.py` lanza un análisis de reuso, pero está obsoleto. Se debería lanzar desde `reuse-detection.py`.


### Análisis de rendimiento
Parece que lo que más le cuesta son las operaciones matemáticas por detrás de *apron*: aritmética entera de precisión (gmpz).
1.  Instalar [google-perftools](https://github.com/gperftools/gperftools): `apt install google-perftools`
2.  Instalar [yep](https://pypi.org/project/yep/) `./venv/bin/easy_install -U yep`
3.  Lanzar análisis vía módulo yep `./venv/bin/python -u -O -m yep -v -- analysis1.py test/ludcmp_O3_arm > ludcmp_O3_arm.reuse`
4.  Analizar fichero generado con pperf `google-pprof libpolygaz.so analysis1.py.prof` (en modo interactivo, el comando `web` abre un .svg en el navegador)
