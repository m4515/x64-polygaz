# bibliotecas
library(ggplot2) # para gráficas
library(tikzDevice) # para exportar a LaTeX

# path
setwd("C:/Users/emore/Desktop/TFG/graficas")
temporal<-FALSE
if(temporal){
# datos reuso temporal
file1<-read.csv(file="temporal_O2.csv",sep=",") # sep indica el caracter separador en el CSV, por defecto la coma
file2<-read.csv(file="temporal_O0.csv",sep=",") # sep indica el caracter separador en el CSV, por defecto la coma
total <- rbind( file1, file2)
total$OLevel[total$OLevel == 2] <- "O2"
total$OLevel[total$OLevel == 0] <- "O0"
a<-total[total$Binario<"cover" & total$Arch_compiler=="amd64_gcc",]
b<-total[total$Binario>="cover" & total$Binario<="g723_enc"& total$Arch_compiler=="amd64_gcc",]
c<-total[total$Binario>="g723_enc" & total$Binario<="matrix1" & total$Arch_compiler=="amd64_gcc",]
d<-total[total$Binario>"matrix1"& total$Binario<"susan"& total$Arch_compiler=="amd64_gcc",]
#View(a)
#b<-read.csv(file="results_pkeq.csv",sep=" ")
# gráficas
tikz(file="Gcc_comparative.tikz",width=7,height=3) ;  
pdf(file="Gcc_Comparative.pdf",width=9,height=5) ;
if(FALSE){
print(ggplot(data=a, aes(x=Arch_compiler)) + geom_boxplot(aes(y=LongChain))+facet_grid(.~Binario)+theme(strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), axis.text.x = element_text(angle=90,size = rel(0.75),vjust=0.1), panel.spacing.x=unit(0.25, "lines")) +scale_y_continuous(trans="log10") +labs(x="Compiler", y="Temporal Reuse Chain") +guides(colour="none") )
print(ggplot(data=b, aes(x=Arch_compiler)) + geom_boxplot(aes(y=LongChain))+facet_grid(.~Binario)+theme(strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), axis.text.x = element_text(angle=90,size = rel(0.75),vjust=0.1), panel.spacing.x=unit(0.25, "lines")) +scale_y_continuous(trans="log10") +labs(x="Compiler", y="Temporal Reuse Chain") +guides(colour="none") )
print(ggplot(data=c, aes(x=Arch_compiler)) + geom_boxplot(aes(y=LongChain))+facet_grid(.~Binario)+theme(strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), axis.text.x = element_text(angle=90,size = rel(0.75),vjust=0.1), panel.spacing.x=unit(0.25, "lines")) +scale_y_continuous(trans="log10") +labs(x="Compiler", y="Temporal Reuse Chain") +guides(colour="none") )
print(ggplot(data=d, aes(x=Arch_compiler)) + geom_boxplot(aes(y=LongChain))+facet_grid(.~Binario)+theme(strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), axis.text.x = element_text(angle=90,size = rel(0.75),vjust=0.1), panel.spacing.x=unit(0.25, "lines")) +scale_y_continuous(trans="log10") +labs(x="Compiler", y="Temporal Reuse Chain") +guides(colour="none") )
}
print(ggplot(data=a, aes(x=OLevel)) + geom_boxplot(aes(y=LongChain))+facet_grid(.~Binario)+theme(strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), axis.text.x = element_text(angle=90,size = rel(0.75),vjust=0.1), panel.spacing.x=unit(0.25, "lines")) +scale_y_continuous(trans="log10") +labs(x="Optimization Level", y="Temporal Reuse Chain") +guides(colour="none") )
print(ggplot(data=b, aes(x=OLevel)) + geom_boxplot(aes(y=LongChain))+facet_grid(.~Binario)+theme(strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), axis.text.x = element_text(angle=90,size = rel(0.75),vjust=0.1), panel.spacing.x=unit(0.25, "lines")) +scale_y_continuous(trans="log10") +labs(x="Optimization Level", y="Temporal Reuse Chain") +guides(colour="none") )
print(ggplot(data=c, aes(x=OLevel)) + geom_boxplot(aes(y=LongChain))+facet_grid(.~Binario)+theme(strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), axis.text.x = element_text(angle=90,size = rel(0.75),vjust=0.1), panel.spacing.x=unit(0.25, "lines")) +scale_y_continuous(trans="log10") +labs(x="Optimization Level", y="Temporal Reuse Chain") +guides(colour="none") )
print(ggplot(data=d, aes(x=OLevel)) + geom_boxplot(aes(y=LongChain))+facet_grid(.~Binario)+theme(strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), axis.text.x = element_text(angle=90,size = rel(0.75),vjust=0.1), panel.spacing.x=unit(0.25, "lines")) +scale_y_continuous(trans="log10") +labs(x="Optimization Level", y="Temporal Reuse Chain") +guides(colour="none") )
dev.off()

# datos reuso espacial
}else{
total<-read.csv(file="spatial_O2.csv",sep=",")
file1<-read.csv(file="dots_O2.csv",sep=",")
file2 <- merge(total,file1,by=c("Binario","Arch_compiler","OLevel"))
e<-file2[file2$Binario<"cover",]
f<-file2[file2$Binario>="cover" & file2$Binario<="g723_enc",]
g<-file2[file2$Binario>="g723_enc" & file2$Binario<="matrix1",]
h<-file2[file2$Binario>"matrix1",]

tikz(file="degrade_O2.tikz",width=7,height=3) ;  
pdf(file="degrade_O2.pdf",width=9,height=5) ; 

print(ggplot(data=e, aes(x=Arch_compiler)) + geom_boxplot(aes(y=LongChain,fill=NPuntos))+facet_grid(.~Binario)+theme(strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), axis.text.x = element_text(angle=90,size = rel(0.75),vjust=0.1), panel.spacing.x=unit(0.25, "lines")) +scale_y_continuous(trans="log10") +labs(x="Compiler", y="Spatial Reuse Chain") +guides(colour="none",))
print(ggplot(data=f, aes(x=Arch_compiler)) + geom_boxplot(aes(y=LongChain,fill=NPuntos))+facet_grid(.~Binario)+theme(strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), axis.text.x = element_text(angle=90,size = rel(0.75),vjust=0.1), panel.spacing.x=unit(0.25, "lines")) +scale_y_continuous(trans="log10") +labs(x="Compiler", y="Spatial Reuse Chain") +guides(colour="none") )
print(ggplot(data=g, aes(x=Arch_compiler)) + geom_boxplot(aes(y=LongChain,fill=NPuntos))+facet_grid(.~Binario)+theme(strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), axis.text.x = element_text(angle=90,size = rel(0.75),vjust=0.1), panel.spacing.x=unit(0.25, "lines")) +scale_y_continuous(trans="log10") +labs(x="Compiler", y="Spatial Reuse Chain") +guides(colour="none") )
print(ggplot(data=h, aes(x=Arch_compiler)) + geom_boxplot(aes(y=LongChain,fill=NPuntos))+facet_grid(.~Binario)+theme(strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), axis.text.x = element_text(angle=90,size = rel(0.75),vjust=0.1), panel.spacing.x=unit(0.25, "lines")) +scale_y_continuous(trans="log10") +labs(x="Compiler", y="Spatial Reuse Chain") +guides(colour="none") )
dev.off()
}











# exportar a pdf o tikz
#tikz(file="temporal.tikz",width=7,height=3) ; last_plot() ; dev.off()
#pdf(file="dominants.pdf",width=9,height=5) ; last_plot() ; dev.off()

###################################### Ejemplos de uso ##############################################################################
# otro ejemplo
#print(ggplot(data=b, aes(x=OLevel)) +geom_bar(aes(y=Dominant),stat="identity",position="stack",fill="#3182bd",colour="black") +facet_grid(.~Name) +theme(strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), axis.text.x = element_text(size = rel(0.75)), legend.position="bottom",panel.spacing.x=unit(0.25, "lines")) +scale_y_continuous(trans="log10") +labs(x="Optimization level (-O)", y="Dominant refereces") +guides(colour="none"))
#print(ggplot(data=b, aes(x=LongChain,fill=Arch_compiler),) +geom_bar(aes(y=NRepetitions),stat="identity",position=position_dodge(),colour="black") +facet_grid(.~Binario) +theme(strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), axis.text.x = element_text(size = rel(0.75)), legend.position="bottom",panel.spacing.x=unit(0.25, "lines")) +scale_y_continuous(trans="log10") +labs(x="Spatial Reuse Chains Longitude", y="Number of repetitions") +guides(colour="none"))
#print(ggplot(data=b,aes(x=OLevel,y=(initT+memrefT+indvarT+reusegenT+reusecalT),colour=OLevel)) +geom_boxplot(outlier.size=0.4) +facet_grid(.~Name,margins=TRUE) +scale_y_continuous(trans="log10") +theme(strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), legend.position="bottom") +geom_hline(yintercept=60) +geom_hline(yintercept=60*60) +geom_hline(yintercept=60*60*24) +guides(colour="none") +labs(x="Optimization level (-O)",y="Analysis time (s)") )

#dev.off()

#g <- ggplot(data=b,aes(x=LongChain,y=NRepetitions,fill=Arch_compiler))
#g + geom_bar(stat="identity") + facet_grid(.~Binario)
#c <- g + geom_bar(stat="identity",position=position_dodge())
#print(c)
#print(ggplot(data=b, aes(x=Arch_compiler),) +geom_violin(aes(y=LongChain),scale="area",colour="black") +facet_grid(.~Binario) +theme(strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), axis.text.x = element_text(size = rel(0.75)), panel.spacing.x=unit(0.25, "lines")) +scale_y_continuous(trans="log10") +labs(x="Compiler", y="Temporal Reuse Chain") +guides(colour="none"))
#print(ggplot(data=b, aes(x=Arch_compiler),) +geom_dotplot(aes(y=LongChain),binaxis="y",stackdir="center",colour="black") +facet_grid(.~Binario) +theme(strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), axis.text.x = element_text(size = rel(0.75)), panel.spacing.x=unit(0.25, "lines")) +scale_y_continuous(trans="log10") +labs(x="Compiler", y="Temporal Reuse Chain") +guides(colour="none"))
#print(ggplot(data=b, aes(x=Arch_compiler),) +geom_point(aes(y=LongChain,colour=factor(NRepetitions),size=NRepetitions))+geom_violin(aes(y=LongChain,alpha = 0,weight=NRepetitions),scale="area",colour="black")+facet_grid(.~Binario) +theme(strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), axis.text.x = element_text(angle=90,size = rel(0.75),vjust=0.1), panel.spacing.x=unit(0.25, "lines")) +scale_y_continuous(trans="log10") +labs(x="Compiler", y="Temporal Reuse Chain") +guides(colour="none",alpha="none"))
