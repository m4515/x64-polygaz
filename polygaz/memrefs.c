#include "memrefs.h"

// apron_interface.c
extern ap_manager_t* man;
extern ap_environment_t* env;
extern ap_linexpr1_t expr;
extern ap_abstract1_t aistate;
extern ap_lincons1_array_t array; // array for the widen threshold
extern ap_lincons1_t cons;

/**
 * Array of static memory accesses
 */
memref_t memrefs[MAXREFS];
unsigned int nextemptyref=0;


// array of loop induction variables
loopIndVar_t nestinfo[MAXNESTDEPTH];
int nestdepth=0;

// array of explored constraints in an abstract state
#define MAXCONSINARRAY 100
size_t explored[MAXCONSINARRAY];
int indexp=0;


////////////////////////// MEMORY REFERENCES ///////////////////////////
void addMemConstRef(int cfgnodeid, int bbaddr, int addr, int dest, int bytes, int isload, int isconditional) {
//void addMemConstRef(int addr, int dest, int bytes, int isload) {
    for (int ref=0; ref<nextemptyref; ref++) {
        if (memrefs[ref].cfgnodeid==cfgnodeid && memrefs[ref].addr==addr) {
#ifdef DEBUG_MEM
            printMemRef(&memrefs[ref]);
#endif
            if (memrefs[ref].dest!=dest) {
                printf("addMemConstRef: instr 0x%x previously accessing constant 0x%x now accesses constant 0x%x; continuing...\n",addr,memrefs[ref].dest,dest);
            } else {
                ap_abstract1_clear(man,&memrefs[ref].aistate);
                memrefs[ref].aistate=ap_abstract1_copy(man,&aistate);
                return;
            }
        }
    }
    if (nextemptyref<MAXREFS-1) {
        memrefs[nextemptyref].cfgnodeid=cfgnodeid;
        memrefs[nextemptyref].bbaddr=bbaddr;
        memrefs[nextemptyref].loopaddr=0;
        memrefs[nextemptyref].addr=addr;
        memrefs[nextemptyref].dest=dest;
        memrefs[nextemptyref].indvar[0]=-1;
        memrefs[nextemptyref].firsttarget=dest;
        strcpy(memrefs[nextemptyref].regname,"e");
        memrefs[nextemptyref].isload=isload;
        memrefs[nextemptyref].isconditional=isconditional;
        memrefs[nextemptyref].bytes=bytes;
        memrefs[nextemptyref].aistate=ap_abstract1_copy(man,&aistate);
        //memrefs[nextemptyref].firstGR=&memrefs[nextemptyref];
        memrefs[nextemptyref].firstGR=NULL;
        memrefs[nextemptyref].lastGR=NULL;
#ifdef DEBUG_MEM
        printMemRef(&memrefs[nextemptyref]);
#endif
        nextemptyref++;
    } else {
        printf("addMemConstRef: too many references (%d)\n",MAXREFS);
        exit(-1);
    }
}

void addMemRef(int cfgnodeid, int bbaddr, int addr, char* registerName, int ftarget, int bytes, int isload, int isconditional, int loopaddr) {
//void addMemRef(int addr, char* registerName, int bytes, int isload) {
    for (int ref=0; ref<nextemptyref; ref++) {
        if (memrefs[ref].cfgnodeid==cfgnodeid && memrefs[ref].addr==addr && !strcmp(memrefs[ref].regname,registerName)) {
            ap_abstract1_clear(man,&memrefs[ref].aistate);
            memrefs[ref].aistate=ap_abstract1_copy(man,&aistate);
            //memrefs[ref].dest=0; // not constant for sure
#ifdef DEBUG_MEM
            printMemRef(&memrefs[ref]);
#endif
            return;
        }
    }
    if (nextemptyref<MAXREFS-1) {
        memrefs[nextemptyref].cfgnodeid=cfgnodeid;
        memrefs[nextemptyref].bbaddr=bbaddr;
        memrefs[nextemptyref].loopaddr=loopaddr;
        memrefs[nextemptyref].addr=addr;
        memrefs[nextemptyref].dest=0;
        memrefs[nextemptyref].firsttarget=ftarget;
        memrefs[nextemptyref].indvar[0]=-1;
        memrefs[nextemptyref].isload=isload;
        memrefs[nextemptyref].isconditional=isconditional;
        memrefs[nextemptyref].bytes=bytes;
        strcpy(memrefs[nextemptyref].regname,registerName);
        memrefs[nextemptyref].aistate=ap_abstract1_copy(man,&aistate);
        //memrefs[nextemptyref].firstGR=&memrefs[nextemptyref];
        memrefs[nextemptyref].firstGR=NULL;
        memrefs[nextemptyref].lastGR=NULL;
#ifdef DEBUG_MEM
        printMemRef(&memrefs[nextemptyref]);
#endif
        nextemptyref++;
    } else {
        printf("addMemRef: too many references\n");
        exit(-1);
    }
}

void addMemRefOffset(int cfgnodeid, int addr, int loop, char* registerName, int reg, int offset) {
    for (int ref=0; ref<nextemptyref; ref++) {
        if (memrefs[ref].cfgnodeid==cfgnodeid && memrefs[ref].addr==addr && !strcmp(memrefs[ref].regname,registerName)) {
#ifdef DEBUG_MEM
            printMemRef(&memrefs[ref]);
#endif
            int iv=0;
            while (iv<MAXINDVARS && memrefs[ref].indvar[iv]!=-1 && (memrefs[ref].indvar[iv]!=reg || memrefs[ref].loop[iv]!=loop)) {
                iv++;
            }
            if (memrefs[ref].indvar[iv]==reg && memrefs[ref].loop[iv]==loop) {
                if (memrefs[ref].offset[iv]!=offset) {
                    printf("addMemRefOffset: error in 0x%x (loop 0x%x): setting r%d + %d but already set r%d + %d\n",addr,loop,memrefs[ref].indvar[iv],memrefs[ref].offset[iv],reg,offset);
                    printf("This is probably caused by a CFG with instanced functions (angr CFGEmulated) where the context_sensitivity_level has a value too low for this benchmark. Try increasing it or use CFGFast.\n");
                    exit(-1);
                }
            } else if (iv<=(MAXINDVARS-2)) {
                memrefs[ref].indvar[iv]=reg;
                memrefs[ref].loop[iv]=loop;
                memrefs[ref].offset[iv]=offset;
                memrefs[ref].indvar[iv+1]=-1;
                //printf("addMemRefOffset: 0x%x at loop 0x%x (%d): r%d + %d\n",addr,loop,iv+1,reg,offset);
            } else {
                printf("addMemRefOffset: too many induction vars (MAXINDVARS=%d)\n",MAXINDVARS);
                exit(-1);
            }
            return;
        }
    }
    printf("addMemRefOffset: memory reference at 0x%x (%s) in loop 0x%x not found. Induction variable analysis is beyond scope.\n",addr,registerName,loop);
    exit(2);
}

///////////////////// ANALYSIS OF INDUCTION VARIABLES //////////////////////
int intcoef;
int solveVarGetConstant(int reg) {
    return intcoef;
}
int solveVarGetRegister(int reg) {
    //printf("Trying to explore the init value of r%d\n",reg);
    ap_lincons1_t cons;
    ap_coeff_t *coeff=ap_coeff_alloc(AP_COEFF_SCALAR);
    ap_coeff_t *coeffvar;
    ap_coeff_t *coeffconst=ap_coeff_alloc(AP_COEFF_SCALAR);
    ap_lincons1_array_t array=ap_abstract1_to_lincons_array(man,&aistate);
    size_t numelements=ap_lincons1_array_size(&array);
    double coeffRegname, coeffdbl;
    //int firstvar=1;
    //int notfound=1;
    char regname[MAX_VAR_CHARS];
    sprintf(regname,"r%d",reg);
    int result=-1;
    int varcoef=0;
    intcoef=-1;

    //ap_abstract1_fprint(stdout,man,&aistate);
    for (size_t i=0; i<numelements; i++) {
        cons=ap_lincons1_array_get(&array,i);
        //ap_lincons1_fprint(stdout,&cons);

        if (!ap_lincons1_get_coeff(coeff,&cons,regname)) {
            if (!ap_coeff_zero(coeff)) {
                //notfound=0;
                if (coeff->discr==AP_COEFF_SCALAR) {
                    //ap_scalar_t *pscalar=coeff->val.scalar;
                    //ap_double_set_scalar(&coeffRegname,pscalar,0);
                    ap_double_set_scalar(&coeffRegname,coeff->val.scalar,0);
                    //printf("coeff: %f\n",coeffRegname);
                } else {
                    printf("not scalar\n");
                    exit(-1);
                }
                //printf("%s =\n", regname);
                //printf(" = ");
                ap_constyp_t* constype=ap_lincons1_constypref(&cons);
                if (*constype==AP_CONS_EQ) {
                    //printf("\n"); ap_lincons1_fprint(stdout,&cons); printf("\n");
                    ap_var_t var;
                    size_t j;
                    ap_linexpr1_t e=ap_lincons1_linexpr1ref(&cons);
                    // INTERESTING if: single var and var is register and coeff is 1
                    int numvars=0;
                    int notemps=1;
                    ap_linexpr1_ForeachLinterm1(&e, j, var, coeffvar) {
                        if (!ap_coeff_zero(coeffvar) && strcmp(var,regname)) {

                            numvars++;
                            if (numvars==1) {
                                if (*((char*)var)=='r') {
                                    ap_double_set_scalar(&coeffdbl,coeffvar->val.scalar,0);
                                    varcoef=(int)(coeffdbl/(-coeffRegname));
                                    //if (varcoef==1) {
                                        result=atoi((char*)(var)+1);
                                    //}
                                } else {
                                    notemps=0;
                                    break;
                                }
                            } else {
                                break;
                            }
                        }
                    };
                    if (numvars==1 && notemps && varcoef==1) {
                        ap_linexpr1_get_cst(coeffconst,&e);
                        ap_double_set_scalar(&coeffdbl,coeffconst->val.scalar,0);
                        intcoef=(long long int)(coeffdbl/-coeffRegname)%0x100000000;
                        //fprintf(stdout,"found r%d = r%d + %d\n",reg,result,intcoef);
                        break;
                        //return result;
                    } else {
                        //fprintf(stdout,"solveVarGetRegister ignored: coefficient of r%d should be 1 but is %d:\n",result,varcoef);
                        //ap_lincons1_fprint(stdout,&cons); printf("\n");
                        result=-1;
                        intcoef=-1;
                    }
                //} else { printf("desigualdad\n");
                }
            //} else { printf("cero\n");
            }
        //} else { printf("no existe\n");
        }
    }
    //if (notfound)
    //    fprintf(stdout,"\n");
    if (result==-1) {
        //fprintf(stdout," register not found, trying harder\n");

        char varname[MAX_VAR_CHARS+1];
        double inf,sup;
        env=ap_abstract1_environment(man,&aistate);
        for (int reg=1; reg<15; reg++) {
            sprintf(varname,"r%d",(reg+2)*4);
            initExpr();
            addRegToExpr(1, regname);
            addRegToExpr(-1, varname);
            aistate=ap_abstract1_change_environment(man,true,&aistate,env,false);
            ap_interval_t* varinterval=ap_abstract1_bound_linexpr(man,&aistate,&expr);
            //printf("%s - %s = ",regname,varname);
            //ap_interval_fprint(stdout,varinterval);
            //printf("\n");
            ap_double_set_scalar(&inf,varinterval->inf,0);
            ap_double_set_scalar(&sup,varinterval->sup,0);
            if (inf==sup) {
                //printf("%s + %d\n",varname,(int)inf);
                //result=(reg+2)*4;
                result=atoi((char*)(varname)+1);
                intcoef=inf;
                //fprintf(stdout,"found r%d = r%d + %d\n",reg,result,intcoef);
                break; // TODO May be useful several coincidences???
            }
        }
    }

    ap_coeff_free(coeff);
    ap_coeff_free(coeffconst);
    return result;
}


///////////////////// PRINTING OF MEMORY REFERENCES //////////////////////
void solveAndPrintVar(ap_abstract1_t *aistate, char * regname, int bbaddr) {
    ap_lincons1_t cons;
    ap_coeff_t *coeff=ap_coeff_alloc(AP_COEFF_SCALAR);
    ap_coeff_t *coeffvar;
    ap_coeff_t *coeffconst=ap_coeff_alloc(AP_COEFF_SCALAR);
    ap_lincons1_array_t array=ap_abstract1_to_lincons_array(man,aistate);
    size_t numelements=ap_lincons1_array_size(&array);
    double coeffRegname, coeffdbl;
    //int firstvar=1;
    //int notfound=1;
    int result=-1;
    int varcoef=0;
    intcoef=-1;

    //ap_abstract1_fprint(stdout,man,aistate);
    for (size_t i=0; i<numelements; i++) {
        cons=ap_lincons1_array_get(&array,i);
        //ap_lincons1_fprint(stdout,&cons);

        if (!ap_lincons1_get_coeff(coeff,&cons,regname)) {
            if (!ap_coeff_zero(coeff)) {
                //notfound=0;
                if (coeff->discr==AP_COEFF_SCALAR) {
                    //ap_scalar_t *pscalar=coeff->val.scalar;
                    //ap_double_set_scalar(&coeffRegname,pscalar,0);
                    ap_double_set_scalar(&coeffRegname,coeff->val.scalar,0);
                    //printf("coeff: %f\n",coeffRegname);
                } else {
                    printf("not scalar\n");
                    exit(-1);
                }
                //printf("%s =\n", regname);
                printf(" = ");
                ap_constyp_t* constype=ap_lincons1_constypref(&cons);
                if (*constype==AP_CONS_EQ) {
                    //printf("\n"); ap_lincons1_fprint(stdout,&cons); printf("\n");
                    ap_var_t var;
                    size_t j;
                    ap_linexpr1_t e=ap_lincons1_linexpr1ref(&cons);
// nou
                    // INTERESTING if: single var and var is register and coeff is 1
                    int numvars=0;
                    int notemps=1;
                    ap_linexpr1_ForeachLinterm1(&e, j, var, coeffvar) {
                        if (!ap_coeff_zero(coeffvar) && strcmp(var,regname)) {

                            numvars++;
                            if (numvars==1) {
                                if (*((char*)var)=='r') {
                                    ap_double_set_scalar(&coeffdbl,coeffvar->val.scalar,0);
                                    varcoef=(int)(coeffdbl)/(-(int)coeffRegname);
                                    //if (varcoef==1) {
                                        result=atoi((char*)(var)+1);
                                    //}
                                } else {
                                    notemps=0;
                                    break;
                                }
                            } else {
                                break;
                            }
                        }
                    };
                    if (numvars==1 && notemps && varcoef==1) {
                        ap_linexpr1_get_cst(coeffconst,&e);
                        ap_double_set_scalar(&coeffdbl,coeffconst->val.scalar,0);
                        intcoef=(long long int)(coeffdbl/-coeffRegname)%0x100000000;
                        //fprintf(stdout,"r%d ",result);
                        char varname[MAX_VAR_CHARS+1];
                        sprintf(varname,"r%d",result);
                        fprintf(stdout,"%s ",varname);
                            ap_interval_t* varinterval= ap_abstract1_bound_variable(man,aistate,varname);
                            ap_interval_fprint(stdout,varinterval);
                        if (intcoef) {
                            fprintf(stdout," + %d",intcoef);
                        }
                        //printf("\n"); ap_lincons1_fprint(stdout,&cons);
                        break;
                    } else {
                        //fprintf(stdout,"solveAndPrintVar: more than 1 regs, temps, or coefficient!=1:\n");
                        //ap_lincons1_fprint(stdout,&cons); printf("\n");
                        result=-1;
                        intcoef=-1;
                    }

// finou

/* VELL
                    ap_linexpr1_ForeachLinterm1(&e, j, var, coeffvar) {
                        if (!ap_coeff_zero(coeffvar) && strcmp(var,regname)) {
                            ap_double_set_scalar(&coeffdbl,coeffvar->val.scalar,0);
                            int intcoef=(int)(coeffdbl)/(-(int)coeffRegname);
                            if (firstvar)
                                firstvar=0;
                            else
                                fprintf(stdout," + ");
                            fprintf(stdout,"%d %s ",intcoef,(char*)var);
                            ap_interval_t* varinterval= ap_abstract1_bound_variable(man,aistate,var);
                            ap_interval_fprint(stdout,varinterval);
                       }
                    };
                    ap_linexpr1_get_cst(coeffconst,&e);
                    ap_double_set_scalar(&coeffdbl,coeffconst->val.scalar,0);
                    //int intcoef=(int)(coeffdbl)/(-((int)coeffRegname));
                    int intcoef=(long long int)(coeffdbl/-coeffRegname)%0x100000000;
                    fprintf(stdout," + %d",intcoef);
                //} else { printf("desigualdad\n");
FI VELL */
                }
            //} else { printf("cero\n");
            }
        //} else { printf("no existe\n");
        }
    }
// nou
    if (result==-1) {
        fprintf(stdout,"trying... ");

        char varname[MAX_VAR_CHARS+1];
        double inf,sup;
        env=ap_abstract1_environment(man,aistate);
        for (int reg=1; reg<15; reg++) {
            sprintf(varname,"r%d",(reg+2)*4);
            initExpr();
            addRegToExpr(1, regname);
            addRegToExpr(-1, varname); // environment may have grown
            //ap_linexpr1_fprint(stdout,&expr); fprintf(stdout,"\n");
            *aistate=ap_abstract1_change_environment(man,true,aistate,env,false);
            ap_interval_t* varinterval=ap_abstract1_bound_linexpr(man,aistate,&expr);
            //printf("%s - %s = ",regname,varname);
            //ap_interval_fprint(stdout,varinterval);
            //printf("\n");
            ap_double_set_scalar(&inf,varinterval->inf,0);
            ap_double_set_scalar(&sup,varinterval->sup,0);
            if (inf==sup) {
                printf("%s + %d",varname,(int)inf);
                result=(reg+2)*4;
                break;
            }
        }
        if (result==-1) {
            fprintf(stdout," non-linear!");
        }
    }
// finou
    //if (notfound)
    //    fprintf(stdout,"\n");
    ap_coeff_free(coeff);
    ap_coeff_free(coeffconst);
}


void printMemRef(memref_t * memref) {
    if (memref->dest) { // Constant explicit address
        if (memref->isload)
            printf("Inst @: 0x%08x, Load  @: 0x%08x\n",memref->addr,memref->dest);
        else
            printf("Inst @: 0x%08x, Store @: 0x%08x\n",memref->addr,memref->dest);
    } else {
        env=ap_abstract1_environment(man,&(memref->aistate));
        if (varExists(memref->regname)) {
            ap_interval_t* varinterval= ap_abstract1_bound_variable(man,&(memref->aistate),memref->regname);
            if (!ap_scalar_cmp(varinterval->inf,varinterval->sup)) {
                // Constant address
                //ap_scalar_fprint(stdout,varinterval->inf);
                double addrdbl;
                ap_double_set_scalar(&addrdbl,varinterval->inf,0);
                long long int aux=addrdbl;
                int32_t addr32=aux%(0x100000000);
                memref->dest=addr32;
                if (memref->isload)
                    printf("Inst @: 0x%08x, Load  @: 0x%08x\n",memref->addr,addr32);
                else
                    printf("Inst @: 0x%08x, Store @: 0x%08x\n",memref->addr,addr32);
            } else {
                // Unknown or variable address
                if (memref->isload)
                    printf("Inst @: 0x%08x, Load  @: %s ",memref->addr,memref->regname);
                else
                    printf("Inst @: 0x%08x, Store @: %s ",memref->addr,memref->regname);
                ap_interval_fprint(stdout,varinterval);

                //solveAndPrintVar(&(memref->aistate),memref->regname,memref->bbaddr);
                solveAndPrintVar(&(memref->aistate),memref->regname,memref->addr);
                printf("\n");
            }
        } else {
            // Predicated (conditional) load/store
            if (memref->isload)
                printf("Inst @: 0x%08x, Load  @: %s ",memref->addr,memref->regname);
            else
                printf("Inst @: 0x%08x, Store @: %s ",memref->addr,memref->regname);
            printf("(predicated)");
        }
        if (memref->isconditional) {
            printf(" (CONDITIONAL)\n");
        }
    }
}


void printAbstractMemRefs() {
    printf("\n\n %d memory references\n----------------------\n",nextemptyref);
    for (int ref=0; ref<nextemptyref; ref++) {
        printf("%d) ",ref);
        printMemRef(&memrefs[ref]);
    }
}


///////////////////// GENERATION OF MEMORY PATTERNS //////////////////////
void printReuseTheoryMemRefs() {
    printf("\n\n %d reuse theory memory references\n----------------------\n",nextemptyref);
    for (int ref=0; ref<nextemptyref; ref++) {
        if (memrefs[ref].isload)
            printf("%d) Inst @: 0x%08x, Load  @: ",ref,memrefs[ref].addr);
        else
            printf("%d) Inst @: 0x%08x, Store @: ",ref,memrefs[ref].addr);
        if (memrefs[ref].loopcoef!=NULL) {
            rtvectorPrint(memrefs[ref].loopcoef);
        } else {
            printf("Non-linear");
        }
        if (memrefs[ref].isconditional) {
            printf(" (CONDITIONAL)");
        }
        printf("\n");
        if (memrefs[ref].lastGR!=NULL) {
            //printf("  First Group Reuse: 0x%08x\n",memrefs[ref].firstGR->addr);
            //printf("  Last Group Reuse: 0x%08x\n",memrefs[ref].lastGR->addr);
            printf("  Last Group Reuse: %d)\n",(int)(memrefs[ref].lastGR-memrefs));
        }
    }
}


void resetNestInfo() {
    nestdepth=0;
}

//void addNestInfo(int addr, int reg, int step, int base, int encladdr, int enclreg, int enclregconfirmed) {
void addNestInfo(int cfgnodeid, int loopid, int addr, int reg, int step, int base, int baseknown) {
    if (nestdepth<MAXNESTDEPTH && addr!=0) {
        for (int i=0; i<nestdepth; i++) {
            if (nestinfo[i].cfgnodeid==cfgnodeid && nestinfo[i].loopid==loopid && nestinfo[i].reg==reg) {
                printf("addNestInfo WARNING: info already exists\n");
                if (nestinfo[i].step!=step || nestinfo[i].baseknown!=baseknown || nestinfo[i].base!=base) {
                    printf("addNestInfo ERROR: trying to add new info:\n");
                    printf(" old: id %d, addr %x, r%d, step %d, base %d\n",nestinfo[i].cfgnodeid,nestinfo[i].addr,nestinfo[i].reg,nestinfo[i].step,nestinfo[i].base);
                    printf(" new: id %d, addr %x, r%d, step %d, base %d\n",cfgnodeid,addr,reg,step,base);
                    exit(-1);
                }
                return;
            }
        }
        nestinfo[nestdepth].cfgnodeid=cfgnodeid;
        nestinfo[nestdepth].loopid=loopid;
        nestinfo[nestdepth].addr=addr;
        nestinfo[nestdepth].reg=reg;
        nestinfo[nestdepth].step=step;
        nestinfo[nestdepth].base=base;
        nestinfo[nestdepth].baseknown=baseknown;
        //nestinfo[nestdepth].encladdr=encladdr;
        //nestinfo[nestdepth].enclreg=enclreg;
        //nestinfo[nestdepth].enclregconfirmed=enclregconfirmed;
        //printf("addNestInfo: added CFGid %d, loop addr %x, induction r%d, step %d, base %d, baseknown %d\n",cfgnodeid,addr,reg,step,base,baseknown);
        nestdepth++;
    } else if (addr!=0) {
        printf("addNestInfo error: %d reached\n",MAXNESTDEPTH);
        exit(-1);
    } else {
        printf("addNestInfo error: trying to add a loop with addr 0x0\n");
        exit(-1);
    }
}

/*
int addIndVar(reuseth_vector_t **rv, int addr, int reg, double coeffRegname) {
    int found=0;
    int i=0;
    while (!found && i<nestdepth) {
        //printf("is r%d indvar of 0x%x (r%d)?\n",reg,nestinfo[i].addr,nestinfo[i].reg);
        if (nestinfo[i].reg==reg && nestinfo[i].addr==addr) {
            //printf("addstep %d r%d loop 0x%08x + %d\n",nestinfo[i].step, reg, addr, nestinfo[i].base);
            *rv=rtvectorAddLoop(*rv, nestinfo[i].step, nestinfo[i].addr);
            *rv=rtvectorAddBase(*rv,nestinfo[i].base); //TODO base? or base/-coeffRegname?
            if (nestinfo[i].encladdr) {
                if (nestinfo[i].encladdr!=-1) {
                    if (addr==nestinfo[i].encladdr) {
                        if (reg==nestinfo[i].enclreg) {
                            printf("addIndVar: infinite loop (cause: loop 0x%x, reg %d), aborting.\n",addr,reg);
                            exit(-1);
                        }
                        printf("addIndVar: possible infinite loop analyzing loop 0x%x and reg %d...\n",addr,reg);
                    }
                    addIndVar(rv,nestinfo[i].encladdr, nestinfo[i].enclreg, coeffRegname);
                } else { //if (nestinfo[i].enclreg==-1) {
                    printf("no hay enclosing loop\n");
                    //printf("add unknown base\n");
                    *rv=rtvectorAddUnkBase(*rv);
                }
            //} else { printf("All info collected\n");
            }
            found=1;
        }
        i++;
    }
    if (!found) {
        printf("Top? loop addr 0x%x\n",addr);
        // *rv=rtvectorAddUnkLoop(*rv,addr);
        // *rv=rtvectorAddUnkBase(*rv);
        // TODO free rv
        *rv=NULL;
    }
    return found;
}
*/

/*
 int addIndVar(reuseth_vector_t **rv, int addr, int reg, double coeffRegname, int basehint) {
     reuseth_vector_t *rvcopy, *rvcopy1, *rvcopy2;
    int found=0;
    int i=0;
    int optsameinduction=0,optdiffinduction=0;
    int newreg,newaddr;
    while (!found && i<nestdepth) {
        if (nestinfo[i].reg==reg && nestinfo[i].addr==addr) {
            //printf("addstep %d r%d loop 0x%08x + %d\n",nestinfo[i].step, reg, addr, nestinfo[i].base);
            *rv=rtvectorAddLoop(*rv, nestinfo[i].step, nestinfo[i].addr);
            *rv=rtvectorAddBase(*rv,nestinfo[i].base); //TODO base? or base/-coeffRegname?

            if (nestinfo[i].encladdr) { // nested loop
                if (nestinfo[i].encladdr!=-1) { // nested loop
                    newaddr=nestinfo[i].encladdr;
                    // two options to explore:
                    // 1) assume that enclosing loop has THE SAME induction variable (reg)
                    // 2) assume that enclosing loop has ANOTHER induction variable (nestinfo[i].enclreg)
                    if (addr!=newaddr) {
                        optsameinduction=1; // opt 1) is possible
                    }
                    if (addr!=newaddr || reg!=nestinfo[i].enclreg) {
                        optdiffinduction=1; // opt 2) is possible
                        //// begin infinite loop watchdog
                        for (int wdi=0; wdi<nestdepth; wdi++) {
                            if (nestinfo[wdi].addr==newaddr &&
                                nestinfo[wdi].reg==nestinfo[i].enclreg &&
                                nestinfo[wdi].enclreg==reg) {
                            // if enclosing info (newaddr,nestinfo[i].enclreg)
                            // in its own enclosing info
                            // (nestinfo[wdi].encl,nestinfo[wdi].enclreg)
//                            if (nestinfo[wdi].encladdr==addr && nestinfo[wdi].enclreg==reg) {
                                printf("addIndVar: interdependence relation (loop 0x%x, regs %d,%d). Impossible induction var.\n",addr,reg,nestinfo[i].enclreg);
                                optdiffinduction=0; // opt 2) is not possible
                                break;
                            }
                        }
                        //// end infinite loop watchdog
                    }

                    // assignment reg <- enclreg has been confirmed
                    if (nestinfo[i].enclregconfirmed) {
                        optsameinduction=0;
                        optdiffinduction=1;
                    }

                    if (!optsameinduction && !optdiffinduction) {
                        printf("addIndVar: infinite loop (cause: loop 0x%x, reg %d), aborting.\n",addr,reg);
                        free(*rv);
                        *rv=NULL;
                        return 0;
                    }
                    if (optsameinduction) {
                        // test option 1)
                        newreg=reg;
                        rvcopy=rtvectorCopy(*rv);
                        if (addIndVar(&rvcopy,newaddr,newreg,0,basehint)) {
                            //printf("addIndVar success assuming THE SAME induction variable (r%d) at 0x%x:\n",newreg,newaddr);
                            //rtvectorPrint(rvcopy);
                            //printf("\n");
                            rvcopy1=rvcopy;
                        } else {
                            //printf("addIndVar failed assuming THE SAME induction variable (r%d) at 0x%x\n",newreg,newaddr);
                            optsameinduction=0;
                            free(rvcopy);
                        }
                    }
                    if (optdiffinduction) {
                        // test option 2
                        //printf("addIndVar: possible infinite loop analyzing loop 0x%x and reg %d...\n",addr,reg);
                        newreg=nestinfo[i].enclreg;
                        rvcopy=rtvectorCopy(*rv);
                        if (addIndVar(&rvcopy,newaddr,newreg,coeffRegname,basehint)) {
                            //printf("addIndVar success assuming ANOTHER induction variable (r%d) at 0x%x:\n",newreg,newaddr);
                            //rtvectorPrint(rvcopy);
                            //printf("\n");
                            rvcopy2=rvcopy;
                        } else {
                            //printf("addIndVar failed assuming ANOTHER induction variable (r%d) at 0x%x\n",newreg,newaddr);
                            optdiffinduction=0;
                            free(rvcopy);
                        }
                    }
                    if (optsameinduction && optdiffinduction) {
                        printf("addIndVar: two possible induction variables found (r%d and r%d) for loop 0x%x:\n",reg,nestinfo[i].enclreg,addr);
                        rtvectorPrint(rvcopy1);
                        printf("\n");
                        rtvectorPrint(rvcopy2);
                        int preferred=0;
                        int base1=rtvectorGetBase(rvcopy1);
                        int base2=rtvectorGetBase(rvcopy2);
                        if (base1==basehint) {
                            preferred=1;
                        } else if (base2==basehint) {
                            preferred=2;
                        } else { // base does not match first target
                            int stride1=rtvectorGetCoefSum(rvcopy1);
                            int stride2=rtvectorGetCoefSum(rvcopy2);
                            if (base1+stride1==basehint) {
                                preferred=1;
                            } else if (base2+stride2==basehint) {
                                preferred=2;
                            } else {
                                // TODO test linear combination here
                                //if basehint-base1 is linear comb of strides rvcopy1...
                                // last test, basehint between base and base+std
                                if (base1<=basehint && base1+stride1>=basehint) {
                                    preferred=1;
                                } else if (base2<=basehint && base2+stride2>=basehint) {
                                    preferred=2;
                                }
                            }
                        }

                        printf("\nAssuming %d (base addr hint 0x%x):\n", preferred, basehint);
                        free(*rv);
                        if (preferred==1) {
                            *rv=rvcopy1;
                        } else { // preferred==2 or no preference
                            *rv=rvcopy2;
                        }
                        rtvectorPrint(*rv);
                        printf("\nVERIFY MANUALLY!!!\n");
                        found=1;
                    } else if (optsameinduction) {
                        free(*rv);
                        *rv=rvcopy1;
                        found=1;
                    } else if (optdiffinduction) {
                        free(*rv);
                        *rv=rvcopy2;
                        found=1;
                    } else {
                        //printf("all options failed\n");
                        //exit(-1);
                    }
                } else { //if (nestinfo[i].enclreg==-1) {
                    printf("no hay enclosing loop\n");
                    //printf("add unknown base\n");
                    *rv=rtvectorAddUnkBase(*rv);
                    found=1;
                }
            } else { //printf("All info collected\n"); }
                found=1;
            }
        }
        i++;
    }
    if (!found) {
        printf("Top? loop addr 0x%x\n",addr);
        free(*rv);
        *rv=NULL;
    }
    return found;
}
*/

//reuseth_vector_t * NEWgenReuseTheoryMemRefs(reuseth_vector_t *rv,ap_abstract1_t *aistate,int loopaddr, char* regname, int indvar[], int loop[], int despl[]);

/*
void genReuseTheoryMemRef(int i) {
    if (memrefs[i].dest) { // Constant explicit address
        memrefs[i].loopcoef=rtvectorInitMemRef(memrefs[i].dest);
        //printf("0x%08x: init const\n",memrefs[i].addr);
    } else {
        //printf("0x%08x: %s\n",memrefs[i].addr,memrefs[i].regname);
        //ap_abstract1_fprint(stdout,man,&memrefs[i].aistate);
        memrefs[i].loopcoef=rtvectorInitMemRef(0);
        indexp=0;
        // TEST
        //printf("0x%08x: %s\n",memrefs[i].addr,memrefs[i].regname);
        //memrefs[i].loopcoef=NEWgenReuseTheoryMemRefs(memrefs[i].loopcoef,&memrefs[i].aistate,memrefs[i].loopaddr,memrefs[i].regname,memrefs[i].firsttarget);
        memrefs[i].loopcoef=NEWgenReuseTheoryMemRefs(memrefs[i].loopcoef,&memrefs[i].aistate,memrefs[i].loopaddr,memrefs[i].regname,memrefs[i].indvar,memrefs[i].loop,memrefs[i].offset);
        // END TEST
        //memrefs[i].loopcoef=genReuseTheoryMemRefs(memrefs[i].loopcoef,&memrefs[i].aistate,memrefs[i].loopaddr,memrefs[i].regname,memrefs[i].firsttarget);
        //printf("vec="); rtvectorPrint(memrefs[i].loopcoef); printf("\n");
    }
}
*/

/*
reuseth_vector_t * genReuseTheoryMemRefs(reuseth_vector_t *rv,ap_abstract1_t *aistate,int loopaddr, char* regname, int basehint) {
    ap_coeff_t *coeff=ap_coeff_alloc(AP_COEFF_SCALAR);
    ap_coeff_t *coeffvar;
    ap_coeff_t *coeffconst=ap_coeff_alloc(AP_COEFF_SCALAR);
    ap_lincons1_array_t array=ap_abstract1_to_lincons_array(man,aistate);
    size_t numelements=ap_lincons1_array_size(&array);
    double coeffRegname, coeffdbl;

    int result=-1;
    int varcoef=0;
    int intcoef=-1;

    // for each constraint in abstract state (look for one with regname)
    for (size_t i=0; i<numelements; i++) {
        int i2=0;
        while (i2<indexp && explored[i2]!=i) {
            i2++;
        }
        // if constraint already explored, explore next
        if (i2<indexp && explored[i2]==i) {
            continue;
        }
        ap_lincons1_t cons=ap_lincons1_array_get(&array,i);
        // if contains regname and its coefficient is not 0
        if (!ap_lincons1_get_coeff(coeff,&cons,regname)) {
            if (!ap_coeff_zero(coeff)) {
                if (coeff->discr==AP_COEFF_SCALAR) {
                    ap_double_set_scalar(&coeffRegname,coeff->val.scalar,0);
                } else {
                    printf("not scalar\n");
                    exit(-1);
                }
                if (coeffRegname!=1) {
                    printf("genReuseTheoryMemRefs WARNING: coefficient is not 1:\n");
                    ap_lincons1_fprint(stdout,&cons); printf("\n");
                }
                // assert const type is equality
                ap_constyp_t* constype=ap_lincons1_constypref(&cons);
                if (*constype==AP_CONS_EQ) {
                    // explore equality
                    //printf("solving %s in lincons %d (addr 0x%x)\n", regname,(int)i,loopaddr);
                    //ap_lincons1_fprint(stdout,&cons); printf("\n");
                    ap_var_t var;
                    size_t j;
                    ap_linexpr1_t e=ap_lincons1_linexpr1ref(&cons);

//nou
                    // INTERESTING if: single var and var is register and coeff is 1
                    int numvars=0;
                    int notemps=1;
                    ap_linexpr1_ForeachLinterm1(&e, j, var, coeffvar) {
                        if (!ap_coeff_zero(coeffvar) && strcmp(var,regname)) {

                            numvars++;
                            if (numvars==1) {
                                if (*((char*)var)=='r') {
                                    ap_double_set_scalar(&coeffdbl,coeffvar->val.scalar,0);
                                    varcoef=(int)(coeffdbl)/(-(int)coeffRegname);
                                    //if (varcoef==1) {
                                        result=atoi((char*)(var)+1);
                                    //}
                                } else {
                                    notemps=0;
                                    break;
                                }
                            } else {
                                break;
                            }
                        }
                    };
//                    if (numvars==1 && notemps) {
//                        if (varcoef==1) {
                    if (numvars==1 && notemps && varcoef==1) {
                            ap_linexpr1_get_cst(coeffconst,&e);
                            ap_double_set_scalar(&coeffdbl,coeffconst->val.scalar,0);
                            intcoef=(long long int)(coeffdbl/-coeffRegname)%0x100000000;
                            //fprintf(stdout,"r%d = r%d + %d\n",reg,result,intcoef);
                    rv=rtvectorAddBase(rv,intcoef);
                                    //int found=
                                    addIndVar(&rv, loopaddr, result, coeffRegname,basehint);
                            break;
                            //return result;
                    } else {
                            //fprintf(stdout,"solveVarGetRegister ignored: coefficient of r%d should be 1 but is %d:\n",result,varcoef);
                            //ap_lincons1_fprint(stdout,&cons); printf("\n");
                            result=-1;
                            intcoef=-1;
                    }
// finou

                } else { printf("disequality\n");
                }
                //} else { printf("coefficient for regname is cero");
            }
        } else {
            printf("regname is not in this constraint\n");
        }
    }
    if (result==-1) {
        //printf("Register not found. One last try...\n");

        char varname[MAX_VAR_CHARS+1];
        double inf,sup;
        env=ap_abstract1_environment(man,aistate);
        for (int reg=1; reg<15; reg++) {
            sprintf(varname,"r%d",(reg+2)*4);
            initExpr();
            addRegToExpr(1, regname);
            addRegToExpr(-1, varname);
            *aistate=ap_abstract1_change_environment(man,true,aistate,env,false);
            ap_interval_t* varinterval=ap_abstract1_bound_linexpr(man,aistate,&expr);
            //printf("%s - %s = ",regname,varname);
            //ap_interval_fprint(stdout,varinterval);
            //printf("\n");
            ap_double_set_scalar(&inf,varinterval->inf,0);
            ap_double_set_scalar(&sup,varinterval->sup,0);
            if (inf==sup) {
                //printf("%s + %d\n",varname,(int)inf);
                result=(reg+2)*4;
                rv=rtvectorAddBase(rv,inf);
                addIndVar(&rv, loopaddr, result, 1, basehint);
                break; // TODO May be useful several coincidences???
            }
        }
        if (result==-1) {
            //printf("Non-linear!!!!!\n");
            // TODO: free rv
            rv=NULL;
        }
    }
    ap_coeff_free(coeff);
    ap_coeff_free(coeffconst);
    //printf("vec="); rtvectorPrint(rv); printf("\n");
    return rv;
}
*/

int adjustOffset(int loopaddr, int reg, int indvar[], int loop[], int despl[], int *unknown) {
    int iv=0;
    while (iv<MAXINDVARS && indvar[iv]!=-1 && (indvar[iv]!=reg || loop[iv]!=loopaddr)) {
        iv++;
    }
    if (indvar[iv]==reg && loop[iv]==loopaddr) {
        //printf("adjustOffset: %d\n",despl[iv]);
        *unknown=0;
        return despl[iv];
    }
    if (indvar[iv]==-1) {
        printf("adjustOffset warning: not enough information to adjust array indexed by reg=%d in loop=0x%x\n",reg,loopaddr);
        // This means that the starting value of the induction variable is unknown
    } else {
        printf("adjustOffset error: MAXINDVARS limit (%d) reached\n",iv);
        exit(-1);
    }
    *unknown=1;
    return 0;
}

int test1var(ap_abstract1_t *aistate, char* regname, int var, int* offset) {
    char varname[MAX_VAR_CHARS+1];
    double inf,sup;

    initExpr();
    sprintf(varname,"r%d",var);
    addRegToExpr(1, regname); // target address register
    //addRegToExpr(-4, varname);
    addRegToExpr(-1, varname);

    *aistate=ap_abstract1_change_environment(man,true,aistate,env,false); //destructive
    ap_interval_t* varinterval=ap_abstract1_bound_linexpr(man,aistate,&expr);
    //ap_linexpr1_fprint(stdout,&expr); fprintf(stdout," in ");
    //ap_interval_fprint(stdout,varinterval); printf("\n");
    ap_double_set_scalar(&inf,varinterval->inf,0);
    ap_double_set_scalar(&sup,varinterval->sup,0);
    if (inf==sup) {
        *offset=(int)inf;
        return 1;
    }
    return 0;
}

int test2var(ap_abstract1_t *aistate, char* regname, int var1, int var2, int* offset) {
    char varname1[MAX_VAR_CHARS+1];
    char varname2[MAX_VAR_CHARS+1];
    double inf,sup;

    sprintf(varname1,"r%d",var1);
    sprintf(varname2,"r%d",var2);
    initExpr();
    addRegToExpr(1, regname); // target address register
    addRegToExpr(-1, varname1); // loop induction var l1
    addRegToExpr(-1, varname2); // loop induction var l2

    *aistate=ap_abstract1_change_environment(man,true,aistate,env,false);
    ap_interval_t* varinterval=ap_abstract1_bound_linexpr(man,aistate,&expr);
    //ap_linexpr1_fprint(stdout,&expr); fprintf(stdout," in ");
    //ap_interval_fprint(stdout,varinterval); printf("\n");
    ap_double_set_scalar(&inf,varinterval->inf,0);
    ap_double_set_scalar(&sup,varinterval->sup,0);
    if (inf==sup) {
        *offset=(int)inf;
        return 1;
    }
    return 0;
}

int test3var(ap_abstract1_t *aistate, char* regname, int var1, int var2, int var3, int* offset) {
    char varname1[MAX_VAR_CHARS+1];
    char varname2[MAX_VAR_CHARS+1];
    char varname3[MAX_VAR_CHARS+1];
    double inf,sup;

    sprintf(varname1,"r%d",var1);
    sprintf(varname2,"r%d",var2);
    sprintf(varname3,"r%d",var3);
    initExpr();
    addRegToExpr(1, regname); // target address register
    addRegToExpr(-1, varname1); // loop induction var l1
    addRegToExpr(-1, varname2); // loop induction var l2
    addRegToExpr(-1, varname3); // loop induction var l3

    *aistate=ap_abstract1_change_environment(man,true,aistate,env,false);
    ap_interval_t* varinterval=ap_abstract1_bound_linexpr(man,aistate,&expr);
    //ap_linexpr1_fprint(stdout,&expr); fprintf(stdout," in ");
    //ap_interval_fprint(stdout,varinterval); printf("\n");
    ap_double_set_scalar(&inf,varinterval->inf,0);
    ap_double_set_scalar(&sup,varinterval->sup,0);
    if (inf==sup) {
        *offset=(int)inf;
        return 1;
    }
    return 0;
}


reuseth_vector_t* test1loop(memref_t* memref, int addr1, int l1) {
    int found=0;
    int offset=0;
    int unk=0;
    reuseth_vector_t *rv=NULL;

    env=ap_abstract1_environment(man,&memref->aistate);
    // while not found and some nestinfo remains with addr
    while (!found && nestinfo[l1].addr==addr1 && l1<nestdepth) {
        //printf("test: r%d\n",nestinfo[l1].reg);
        if (test1var(&(memref->aistate), memref->regname, nestinfo[l1].reg, &offset)) {
            offset+=adjustOffset(memref->loopaddr,nestinfo[l1].reg,memref->indvar,memref->loop,memref->offset,&unk);
            rv=rtvectorInitMemRef(0);
            if (unk || nestinfo[l1].baseknown==0) {
                rv=rtvectorAddUnkBase(rv);
            }
            rv=rtvectorAddBase(rv,nestinfo[l1].base+offset);
            rv=rtvectorAddLoop(rv, nestinfo[l1].step, nestinfo[l1].addr);
            printf("found: "); rtvectorPrint(rv); printf("\n");
            found=1;
        }
        l1++;
    }
    return rv;
}

reuseth_vector_t* test2loop(memref_t* memref, int addr1, int l1, int addr2, int l2) {
    int found=0;
    int offset=0;
    int unk=0;
    reuseth_vector_t *rv=NULL;

    env=ap_abstract1_environment(man,&memref->aistate);
    while (!found && nestinfo[l1].addr==addr1 && l1<nestdepth) {
        //int l2=index[1];
        while (!found && nestinfo[l1].reg!=nestinfo[l2].reg && nestinfo[l2].addr==addr2 && l2<nestdepth) {
            if (test2var(&memref->aistate, memref->regname, nestinfo[l1].reg, nestinfo[l2].reg, &offset)) {
                offset+=adjustOffset(memref->loopaddr,nestinfo[l1].reg,memref->indvar,memref->loop,memref->offset,&unk);
                rv=rtvectorInitMemRef(0);
                if (unk || nestinfo[l1].baseknown==0) {
                    rv=rtvectorAddUnkBase(rv);
                }
                offset+=adjustOffset(memref->loopaddr,nestinfo[l2].reg,memref->indvar,memref->loop,memref->offset,&unk);
                if (unk || nestinfo[l2].baseknown==0) {
                    rv=rtvectorAddUnkBase(rv);
                }
                rv=rtvectorAddBase(rv,nestinfo[l1].base+offset);
                rv=rtvectorAddLoop(rv, nestinfo[l1].step, nestinfo[l1].addr);
                rv=rtvectorAddLoop(rv, nestinfo[l2].step, nestinfo[l2].addr);
                printf("found: "); rtvectorPrint(rv); printf("\n");
                found=1;
            }
            l2++;
        }
        l1++;
    }
    return rv;
}

reuseth_vector_t* test3loop(memref_t* memref, int addr1, int l1, int addr2, int l2, int addr3, int l3) {
    int found=0;
    int offset=0;
    int unk=0;
    reuseth_vector_t *rv=NULL;

    env=ap_abstract1_environment(man,&memref->aistate);
    while (!found && nestinfo[l1].addr==addr1 && l1<nestdepth) {
        //int l2=index[1];
        while (!found && nestinfo[l1].reg!=nestinfo[l2].reg && nestinfo[l2].addr==addr2 && l2<nestdepth) {
            //int l3=index[2];
            while (!found && nestinfo[l1].reg!=nestinfo[l2].reg && nestinfo[l1].reg!=nestinfo[l3].reg && nestinfo[l2].reg!=nestinfo[l3].reg && nestinfo[l3].addr==addr3 && l3<nestdepth) {
                if (test3var(&memref->aistate, memref->regname, nestinfo[l1].reg, nestinfo[l2].reg,  nestinfo[l3].reg, &offset)) {
                    offset+=adjustOffset(memref->loopaddr,nestinfo[l1].reg,memref->indvar,memref->loop,memref->offset,&unk);
                    rv=rtvectorInitMemRef(0);
                    if (unk || nestinfo[l1].baseknown==0) {
                        rv=rtvectorAddUnkBase(rv);
                    }
                    offset+=adjustOffset(memref->loopaddr,nestinfo[l2].reg,memref->indvar,memref->loop,memref->offset,&unk);
                    if (unk || nestinfo[l2].baseknown==0) {
                        rv=rtvectorAddUnkBase(rv);
                    }
                    offset+=adjustOffset(memref->loopaddr,nestinfo[l3].reg,memref->indvar,memref->loop,memref->offset,&unk);
                    if (unk || nestinfo[l3].baseknown==0) {
                        rv=rtvectorAddUnkBase(rv);
                    }
                    rv=rtvectorAddBase(rv,nestinfo[l1].base+offset);
                    rv=rtvectorAddLoop(rv, nestinfo[l1].step, nestinfo[l1].addr);
                    rv=rtvectorAddLoop(rv, nestinfo[l2].step, nestinfo[l2].addr);
                    rv=rtvectorAddLoop(rv, nestinfo[l3].step, nestinfo[l3].addr);
                    printf("found: "); rtvectorPrint(rv); printf("\n");
                    found=1;
                }
                l3++;
            }
            l2++;
        }
        l1++;
    }
    return rv;
}

void genReuseTheoryMemRef(int i) {
    int deepest, middle, outest;

    if (memrefs[i].dest) { // Constant explicit address
        memrefs[i].loopcoef=rtvectorInitMemRef(memrefs[i].dest);
        //printf("0x%08x: init const\n",memrefs[i].addr);
    } else if (memrefs[i].loopaddr==0) {
        //printf("0x%08x: access outside loops without known constant value: non-linear\n",memrefs[i].addr);
        //printMemRef(&memrefs[i]);
        memrefs[i].loopcoef=NULL;
    } else {
        int addr[MAXNESTEDLOOPS];
        int index[MAXNESTEDLOOPS];

        int numnestedloops=0;
        int cnest=0;

        // find first loop
        //printf("memref in nodeid %d\n",memrefs[i].cfgnodeid);
        while (cnest<nestdepth && nestinfo[cnest].cfgnodeid!=memrefs[i].cfgnodeid) {
            cnest++;
            //printf("nestifo in nodeid %d\n",nestinfo[cnest].cfgnodeid);
        }
        if (cnest<nestdepth) {
            addr[0]=nestinfo[0].addr;
            index[0]=0;
            numnestedloops=1;

            while (cnest<nestdepth) {
                //if (nestinfo[cnest].addr!=addr[numnestedloops-1]) {
                if (nestinfo[cnest].cfgnodeid==memrefs[i].cfgnodeid && nestinfo[cnest].addr!=addr[numnestedloops-1]) {
                    addr[numnestedloops]=nestinfo[cnest].addr;
                    index[numnestedloops]=cnest;
                    numnestedloops++;
                    if (numnestedloops==MAXNESTEDLOOPS) {
                        printf("Too many nested loops\n");
                        exit(-1);
                    }
                }
                cnest++;
            }
        }
        printf("Loops (nested): %d\n",numnestedloops);

        if (numnestedloops==0) { //////////// 0 LOOPS
            printf("No induction variables found\n");
        } else if (numnestedloops==1) { //////////// 1 LOOP
            memrefs[i].loopcoef=test1loop(&memrefs[i],addr[0],index[0]);
        } else if (numnestedloops==2) { /////// 2 NESTED LOOPS
            // TODO: order from deepest to outest
            if (addr[0]>addr[1]) { // simple nesting test
                deepest=0;
                outest=1;
            } else {
                deepest=1;
                outest=0;
            }
            // test for l2 only (deepest)
            //printf("test deepest: 0x%x\n",addr[deepest]);
            memrefs[i].loopcoef=test1loop(&memrefs[i],addr[deepest],index[deepest]);
            if (memrefs[i].loopcoef==NULL) {
                // test for l1 only (outest)
                //printf("test outest: 0x%x\n",addr[outest]);
                memrefs[i].loopcoef=test1loop(&memrefs[i],addr[outest],index[outest]);
            }
            if (memrefs[i].loopcoef==NULL) {
                // test for l1 and l2
                memrefs[i].loopcoef=test2loop(&memrefs[i],addr[0],index[0],addr[1],index[1]);
            }
        } else if (numnestedloops==3) { //////////// 3 NESTED LOOPS
            // TODO: order from deepest to outest
            if (addr[0]>addr[1]) { // 0 in 1
                if (addr[0]>addr[2]) { // 0 in 2, 0 in 1
                    deepest=0;
                    if (addr[1]>addr[2]) { // 1 in 2
                        middle=1;
                        outest=2;
                    } else { // 2 in 1
                        middle=2;
                        outest=1;
                    }
                } else { // 2 in 0, 0 in 1
                    deepest=2;
                    middle=0;
                    outest=1;
                }
            } else { // 1 in 0
                if (addr[0]>addr[2]) { // 1 in 0, 0 in 2
                    deepest=1;
                    middle=0;
                    outest=2;
                } else { // 1 in 0, 2 in 0
                    outest=0;
                    if (addr[1]>addr[2]) { // 1 in 2
                        deepest=1;
                        middle=2;
                    } else { // 2 in 1
                        deepest=2;
                        middle=1;
                    }
                }
            }
            // test for l3 only (deepest)
            //printf("test deepest: 0x%x\n",addr[deepest]);
            memrefs[i].loopcoef=test1loop(&memrefs[i],addr[deepest],index[deepest]);
            if (memrefs[i].loopcoef==NULL) {
                // test for l2 only (middle)
                //printf("test middle: 0x%x\n",addr[middle]);
                memrefs[i].loopcoef=test1loop(&memrefs[i],addr[middle],index[middle]);
            }
            if (memrefs[i].loopcoef==NULL) {
                // test for l1 only (outest)
                //printf("test outest: 0x%x\n",addr[outest]);
                memrefs[i].loopcoef=test1loop(&memrefs[i],addr[outest],index[outest]);
            }
            if (memrefs[i].loopcoef==NULL) {
                // test for l2 and l3 (deepest ones)
                memrefs[i].loopcoef=test2loop(&memrefs[i],addr[middle],index[middle],addr[deepest],index[deepest]);
            }
            if (memrefs[i].loopcoef==NULL) {
                // test for l1 and l3 (outest and deepest)
                memrefs[i].loopcoef=test2loop(&memrefs[i],addr[outest],index[outest],addr[deepest],index[deepest]);
            }
            if (memrefs[i].loopcoef==NULL) {
                // test for l1 and l2 (outer ones)
                memrefs[i].loopcoef=test2loop(&memrefs[i],addr[outest],index[outest],addr[middle],index[middle]);
            }
            if (memrefs[i].loopcoef==NULL) {
                // test for l1, l2 and l3
                memrefs[i].loopcoef=test3loop(&memrefs[i],addr[outest],index[outest],addr[middle],index[middle],addr[deepest],index[deepest]);
            }
        } else {
            printf("nests>3 not supported\n");
        }
    }
}

/*
reuseth_vector_t * NEWgenReuseTheoryMemRefs(reuseth_vector_t *rv,ap_abstract1_t *aistate,int loopaddr, char* regname, int indvar[], int loop[], int despl[]) {
    int addr[10]; // define MAX NESTS or something
    int index[10]; // define MAX NESTS or something
    int numnestedloops=1;
    int i=1;
    int found=0; // if found, try no more
    int offset;
    int unk=0;

    addr[0]=nestinfo[0].addr;
    index[0]=0;
    while (i<nestdepth) {
        //printf("loop 0x%x indvar %d \n",nestinfo[i].addr,nestinfo[i].reg);
        if (nestinfo[i].addr!=addr[numnestedloops-1]) {
            addr[numnestedloops]=nestinfo[i].addr;
            index[numnestedloops]=i;
            numnestedloops++;
        }
        i++;
    }
    printf("Loops (nested): %d\n",numnestedloops);
    if (numnestedloops>=10) {
        printf("Too many nested loops\n");
    }

    env=ap_abstract1_environment(man,aistate);
    //addLoopVariables(numnestedloops,aistate);
    if (numnestedloops==1) { //////////// 1 LOOP
        int l1=index[0];
        while (!found && nestinfo[l1].addr==addr[0] && l1<nestdepth) {
            if (test1var(aistate, regname, nestinfo[l1].reg, &offset)) {
                offset+=adjustOffset(loopaddr,nestinfo[l1].reg,indvar,loop,despl,&unk);
                if (unk || nestinfo[l1].baseknown==0) {
                    rv=rtvectorAddUnkBase(rv);
                }
                rv=rtvectorAddBase(rv,nestinfo[l1].base+offset);
                rv=rtvectorAddLoop(rv, nestinfo[l1].step, nestinfo[l1].addr);
                printf("found: "); rtvectorPrint(rv); printf("\n");
                found=1;
            }
            l1++;
        }
    } else if (numnestedloops==2) { //////////// 2 NESTED LOOPS
        int l1;
        // test for l2 only (deepest)
        l1=index[1];
        while (!found && nestinfo[l1].addr==addr[1] && l1<nestdepth) {
            if (test1var(aistate, regname, nestinfo[l1].reg, &offset)) {
                offset+=adjustOffset(loopaddr,nestinfo[l1].reg,indvar,loop,despl,&unk);
                if (unk || nestinfo[l1].baseknown==0) {
                    rv=rtvectorAddUnkBase(rv);
                }
                rv=rtvectorAddBase(rv,nestinfo[l1].base+offset);
                rv=rtvectorAddLoop(rv, nestinfo[l1].step, nestinfo[l1].addr);
                printf("found: "); rtvectorPrint(rv); printf("\n");
                found=1;
            }
            l1++;
        }

        // test for l1 only (outest)
        l1=index[0];
        while (!found && nestinfo[l1].addr==addr[0] && l1<nestdepth) {
            if (test1var(aistate, regname, nestinfo[l1].reg, &offset)) {
                offset+=adjustOffset(loopaddr,nestinfo[l1].reg,indvar,loop,despl,&unk);
                if (unk || nestinfo[l1].baseknown==0) {
                    rv=rtvectorAddUnkBase(rv);
                }
                rv=rtvectorAddBase(rv,nestinfo[l1].base+offset);
                rv=rtvectorAddLoop(rv, nestinfo[l1].step, nestinfo[l1].addr);
                printf("found: "); rtvectorPrint(rv); printf("\n");
                found=1;
            }
            l1++;
        }

        // test for l1 and l2
        l1=index[0];
        while (!found && nestinfo[l1].addr==addr[0] && l1<nestdepth) {
            int l2=index[1];
            while (!found && nestinfo[l1].reg!=nestinfo[l2].reg && nestinfo[l2].addr==addr[1] && l2<nestdepth) {
                if (test2var(aistate, regname, nestinfo[l1].reg, nestinfo[l2].reg, &offset)) {
                    offset+=adjustOffset(loopaddr,nestinfo[l1].reg,indvar,loop,despl,&unk);
                    if (unk || nestinfo[l1].baseknown==0) {
                        rv=rtvectorAddUnkBase(rv);
                    }
                    offset+=adjustOffset(loopaddr,nestinfo[l2].reg,indvar,loop,despl,&unk);
                    if (unk || nestinfo[l2].baseknown==0) {
                        rv=rtvectorAddUnkBase(rv);
                    }
                    rv=rtvectorAddBase(rv,nestinfo[l1].base+offset);
                    rv=rtvectorAddLoop(rv, nestinfo[l1].step, nestinfo[l1].addr);
                    rv=rtvectorAddLoop(rv, nestinfo[l2].step, nestinfo[l2].addr);
                    printf("found: "); rtvectorPrint(rv); printf("\n");
                    found=1;
                }
                l2++;
            }
            l1++;
        }
    } else if (numnestedloops==3) { //////////// 3 NESTED LOOPS
        int l1;
        // test for l3 only (deepest)
        l1=index[2];
        while (!found && nestinfo[l1].addr==addr[2] && l1<nestdepth) {
            if (test1var(aistate, regname, nestinfo[l1].reg, &offset)) {
                offset+=adjustOffset(loopaddr,nestinfo[l1].reg,indvar,loop,despl,&unk);
                if (unk || nestinfo[l1].baseknown==0) {
                    rv=rtvectorAddUnkBase(rv);
                }
                rv=rtvectorAddBase(rv,nestinfo[l1].base+offset);
                rv=rtvectorAddLoop(rv, nestinfo[l1].step, nestinfo[l1].addr);
                printf("found: "); rtvectorPrint(rv); printf("\n");
                found=1;
            }
            l1++;
        }

        // test for l2 only (middle)
        l1=index[1];
        while (!found && nestinfo[l1].addr==addr[1] && l1<nestdepth) {
            if (test1var(aistate, regname, nestinfo[l1].reg, &offset)) {
                offset+=adjustOffset(loopaddr,nestinfo[l1].reg,indvar,loop,despl,&unk);
                if (unk || nestinfo[l1].baseknown==0) {
                    rv=rtvectorAddUnkBase(rv);
                }
                rv=rtvectorAddBase(rv,nestinfo[l1].base+offset);
                rv=rtvectorAddLoop(rv, nestinfo[l1].step, nestinfo[l1].addr);
                printf("found: "); rtvectorPrint(rv); printf("\n");
                found=1;
            }
            l1++;
        }

        // test for l1 only (outest)
        l1=index[0];
        while (!found && nestinfo[l1].addr==addr[0] && l1<nestdepth) {
            if (test1var(aistate, regname, nestinfo[l1].reg, &offset)) {
                offset+=adjustOffset(loopaddr,nestinfo[l1].reg,indvar,loop,despl,&unk);
                if (unk || nestinfo[l1].baseknown==0) {
                    rv=rtvectorAddUnkBase(rv);
                }
                rv=rtvectorAddBase(rv,nestinfo[l1].base+offset);
                rv=rtvectorAddLoop(rv, nestinfo[l1].step, nestinfo[l1].addr);
                printf("found: "); rtvectorPrint(rv); printf("\n");
                found=1;
            }
            l1++;
        }

        // test for l2 and l3 (deepest ones)
        l1=index[1];
        while (!found && nestinfo[l1].addr==addr[1] && l1<nestdepth) {
            int l2=index[2];
            while (!found && nestinfo[l1].reg!=nestinfo[l2].reg && nestinfo[l2].addr==addr[2] && l2<nestdepth) {
                if (test2var(aistate, regname, nestinfo[l1].reg, nestinfo[l2].reg, &offset)) {
                    offset+=adjustOffset(loopaddr,nestinfo[l1].reg,indvar,loop,despl,&unk);
                    if (unk || nestinfo[l1].baseknown==0) {
                        rv=rtvectorAddUnkBase(rv);
                    }
                    offset+=adjustOffset(loopaddr,nestinfo[l2].reg,indvar,loop,despl,&unk);
                    if (unk || nestinfo[l2].baseknown==0) {
                        rv=rtvectorAddUnkBase(rv);
                    }
                    rv=rtvectorAddBase(rv,nestinfo[l1].base+offset);
                    rv=rtvectorAddLoop(rv, nestinfo[l1].step, nestinfo[l1].addr);
                    rv=rtvectorAddLoop(rv, nestinfo[l2].step, nestinfo[l2].addr);
                    printf("found: "); rtvectorPrint(rv); printf("\n");
                    found=1;
                }
                l2++;
            }
            l1++;
        }

        // test for l1 and l3 (outest and deepest)
        l1=index[0];
        while (!found && nestinfo[l1].addr==addr[0] && l1<nestdepth) {
            int l2=index[2];
            while (!found && nestinfo[l1].reg!=nestinfo[l2].reg && nestinfo[l2].addr==addr[2] && l2<nestdepth) {
                if (test2var(aistate, regname, nestinfo[l1].reg, nestinfo[l2].reg, &offset)) {
                    offset+=adjustOffset(loopaddr,nestinfo[l1].reg,indvar,loop,despl,&unk);
                    if (unk || nestinfo[l1].baseknown==0) {
                        rv=rtvectorAddUnkBase(rv);
                    }
                    offset+=adjustOffset(loopaddr,nestinfo[l2].reg,indvar,loop,despl,&unk);
                    if (unk || nestinfo[l2].baseknown==0) {
                        rv=rtvectorAddUnkBase(rv);
                    }
                    rv=rtvectorAddBase(rv,nestinfo[l1].base+offset);
                    rv=rtvectorAddLoop(rv, nestinfo[l1].step, nestinfo[l1].addr);
                    rv=rtvectorAddLoop(rv, nestinfo[l2].step, nestinfo[l2].addr);
                    printf("found: "); rtvectorPrint(rv); printf("\n");
                    found=1;
                }
                l2++;
            }
            l1++;
        }

        // test for l1 and l2 (outer ones)
        l1=index[0];
        while (!found && nestinfo[l1].addr==addr[0] && l1<nestdepth) {
            int l2=index[1];
            while (!found && nestinfo[l1].reg!=nestinfo[l2].reg && nestinfo[l2].addr==addr[1] && l2<nestdepth) {
                if (test2var(aistate, regname, nestinfo[l1].reg, nestinfo[l2].reg, &offset)) {
                    offset+=adjustOffset(loopaddr,nestinfo[l1].reg,indvar,loop,despl,&unk);
                    if (unk || nestinfo[l1].baseknown==0) {
                        rv=rtvectorAddUnkBase(rv);
                    }
                    offset+=adjustOffset(loopaddr,nestinfo[l2].reg,indvar,loop,despl,&unk);
                    if (unk || nestinfo[l2].baseknown==0) {
                        rv=rtvectorAddUnkBase(rv);
                    }
                    rv=rtvectorAddBase(rv,nestinfo[l1].base+offset);
                    rv=rtvectorAddLoop(rv, nestinfo[l1].step, nestinfo[l1].addr);
                    rv=rtvectorAddLoop(rv, nestinfo[l2].step, nestinfo[l2].addr);
                    printf("found: "); rtvectorPrint(rv); printf("\n");
                    found=1;
                }
                l2++;
            }
            l1++;
        }

        // test for l1, l2 and l3 (all 3 loops)
        l1=index[0];
        while (!found && nestinfo[l1].addr==addr[0] && l1<nestdepth) {
            int l2=index[1];
            while (!found && nestinfo[l1].reg!=nestinfo[l2].reg && nestinfo[l2].addr==addr[1] && l2<nestdepth) {
                int l3=index[2];
                while (!found && nestinfo[l1].reg!=nestinfo[l2].reg && nestinfo[l1].reg!=nestinfo[l3].reg && nestinfo[l2].reg!=nestinfo[l3].reg && nestinfo[l3].addr==addr[2] && l3<nestdepth) {
                    if (test3var(aistate, regname, nestinfo[l1].reg, nestinfo[l2].reg,  nestinfo[l3].reg, &offset)) {
                        offset+=adjustOffset(loopaddr,nestinfo[l1].reg,indvar,loop,despl,&unk);
                        if (unk || nestinfo[l1].baseknown==0) {
                            rv=rtvectorAddUnkBase(rv);
                        }
                        offset+=adjustOffset(loopaddr,nestinfo[l2].reg,indvar,loop,despl,&unk);
                        if (unk || nestinfo[l2].baseknown==0) {
                            rv=rtvectorAddUnkBase(rv);
                        }
                        offset+=adjustOffset(loopaddr,nestinfo[l3].reg,indvar,loop,despl,&unk);
                        if (unk || nestinfo[l3].baseknown==0) {
                            rv=rtvectorAddUnkBase(rv);
                        }
                        rv=rtvectorAddBase(rv,nestinfo[l1].base+offset);
                        rv=rtvectorAddLoop(rv, nestinfo[l1].step, nestinfo[l1].addr);
                        rv=rtvectorAddLoop(rv, nestinfo[l2].step, nestinfo[l2].addr);
                        rv=rtvectorAddLoop(rv, nestinfo[l3].step, nestinfo[l3].addr);
                        printf("found: "); rtvectorPrint(rv); printf("\n");
                        found=1;
                    }
                    l3++;
                }
                l2++;
            }
            l1++;
        }

    } else {
        printf("nests>3 not supported\n");
    }

    if (!found) {
        free(rv);
        rv=NULL;
    }
    return rv;
}
*/

void addLoopVariables(int nest, ap_abstract1_t *aistate) {
    char varname[MAX_VAR_CHARS+1];
    double inf,sup;
    if (nest==0) { // perform query
        *aistate=ap_abstract1_change_environment(man,true,aistate,env,false);
        ap_interval_t* varinterval=ap_abstract1_bound_linexpr(man,aistate,&expr);
        ap_linexpr1_fprint(stdout,&expr); fprintf(stdout,"\n");
        ap_interval_fprint(stdout,varinterval); printf("\n");
        ap_double_set_scalar(&inf,varinterval->inf,0);
        ap_double_set_scalar(&sup,varinterval->sup,0);
        if (inf==sup) {
            printf("found\n");
        }
    } else {
        int i=1;
        int loops=1;
        while (loops<nest && i<nestdepth) {
            if (nestinfo[i].addr!=nestinfo[i-1].addr) {
                loops++;
            }
            i++;
        }
        if (i<nestdepth) {
            i--;
        } else {
            exit(-2);
        }

        int addr=nestinfo[i].addr;
        while (nestinfo[i].addr==addr) {
            sprintf(varname,"r%d",nestinfo[i].reg);
            addRegToExpr(-1, varname);
            addLoopVariables(nest-1,aistate);
        }
        // one without variables for this loop
        addLoopVariables(nest-1,aistate);
    }
}

///////////////////////////// REUSE TESTING

int memRefIsConditional(int index) {
    if (index<nextemptyref) {
        return memrefs[index].isconditional;
    } else {
        return 0;
    }
}

int getMemRefPC(int index) {
    if (index<nextemptyref) {
        return memrefs[index].addr;
    } else {
        //printf("getMemRef: out of bounds\n");
        return 0;
    }
}

int getMemRefCFGid(int index) {
    if (index<nextemptyref) {
        return memrefs[index].cfgnodeid;
    } else {
        //printf("getMemRef: out of bounds\n");
        return 0;
    }
}

int getIndexMemRefFirst(int i) {
    if (i<nextemptyref) {
        return (memrefs[i].firstGR - memrefs);
    } else {
        printf("getIndexMemRefFirst: out of bounds\n");
        return 0;
    }
}

int getIndexMemRefLast(int i) {
    if (i<nextemptyref) {
        return (memrefs[i].lastGR - memrefs);
    } else {
        printf("getIndexMemRefLast: out of bounds\n");
        return 0;
    }
}

int orderedMemRefs(int i1, int i2) {
    if (i1<nextemptyref && i2<nextemptyref) {
        if (memrefs[i1].loopcoef && memrefs[i2].loopcoef) {
            if ((rtvectorGetBase(memrefs[i1].loopcoef)<rtvectorGetBase(memrefs[i2].loopcoef) && memrefs[i1].isload) ||
                (rtvectorGetBase(memrefs[i1].loopcoef)>rtvectorGetBase(memrefs[i2].loopcoef) && !memrefs[i1].isload)) {
                return 1;
                } else {
                    return 0;
                }
        } else {
            printf("orderedMemRefs: trying to order non-linear multiple access\n");
            return 0;
        }
    } else {
        printf("orderedMemRefs: out of bounds\n");
        return 0;
    }
}

int getMemRefBase(int i) {
    if (i<nextemptyref) {
        return rtvectorGetBase(memrefs[i].loopcoef);
    } else {
        printf("getMemRefBase: out of bounds\n");
        return 0;
    }
}

int getLastGR(int i) {
    if (i<nextemptyref) {
        if (memrefs[i].lastGR!=NULL) {
            return memrefs[i].lastGR->addr;
        } else {
            return 0;
        }
    } else {
        printf("getLastGR: out of bounds\n");
        return 0;
    }
}

/*
int getFirstGR(int i) {
    if (i<nextemptyref) {
        if (memrefs[i].firstGR!=NULL) {
            return memrefs[i].firstGR->addr;
        } else {
            return 0;
        }
    } else {
        printf("getFirstGR: out of bounds\n");
        return 0;
    }
}*/
 void setLastGR(int i1, int i2) {
    if (i1<nextemptyref && i2<nextemptyref && i1>=0 && i2>=0) {
        memrefs[i1].lastGR=&memrefs[i2];
    } else {
        printf("setLastGR: out of bounds (%d or %d)\n",i1,i2);
        exit(-1);
    }
}

/*
void setFirstGR(int i1, int i2) {
    if (i1<nextemptyref && i2<nextemptyref && i1>=0 && i2>=0) {
        memrefs[i1].firstGR=&memrefs[i2];
    } else {
        printf("setFirstGR: out of bounds (%d or %d)\n",i1,i2);
        exit(-1);
    }
}*/


memref_t* recursiveSetFirstGR(memref_t *memref) {
    if (memref->lastGR) {
        memref->firstGR=recursiveSetFirstGR(memref->lastGR);
        return memref->firstGR;
    } else {
        // for accesses without group reuse, let firstGR be NULL
        return memref;
    }
}

void calcFirstGR() {
    int dominants=0;
    for (int ref=0; ref<nextemptyref; ref++) {
        if (memrefs[ref].firstGR==NULL) {
            recursiveSetFirstGR(&memrefs[ref]);
        }
    }
    for (int ref=0; ref<nextemptyref; ref++) {
        if (memrefs[ref].firstGR==NULL) {
            int ref2=0;
            int found=0;
            while (ref2<nextemptyref && !found) {
                if (memrefs[ref2].firstGR==&memrefs[ref]) {
                    found=1;
                }
                ref2++;
            }
            if (found) {
                dominants++;
            }
        }
    }
    printf("\nReused dominant memory references: %d (of %d references)\n",dominants,nextemptyref);
}

/*
int haveGroupReuse(int index1, int index2, int linesize) {
    if (index1>=nextemptyref || index2>=nextemptyref) {
        printf("haveGroupReuse: out of bounds\n");
        return 0;
    } else if (memrefs[index1].loopcoef && memrefs[index2].loopcoef && !rtvectorEquals(memrefs[index1].loopcoef,memrefs[index2].loopcoef)) {
        // different target addresses
        if (rtvectorAllKnown(memrefs[index1].loopcoef) && rtvectorAllKnown(memrefs[index2].loopcoef)  && rtvectorIsConstant(memrefs[index1].loopcoef) && rtvectorIsConstant(memrefs[index2].loopcoef)) { // both known constant (scalar)
            if ((rtvectorGetBase(memrefs[index1].loopcoef)/linesize) == (rtvectorGetBase(memrefs[index2].loopcoef)/linesize)) { // same cache lines
                //printf("group reuse constant\n");
                // known scalar addresses, same cache line
                return 1;
            } else {
                // known scalar addresses, different cache line
                return 0;
            }
        } else {
            // different reuse vectors
            return 0;
        }
    } else { // reuse theory vectors are equal (may contain unknowns)
        if (memrefs[index1].loopcoef && memrefs[index2].loopcoef &&  rtvectorAllKnown(memrefs[index1].loopcoef) && rtvectorAllKnown(memrefs[index2].loopcoef)) {
            // both known and identical
            return 1;
        } else if (memrefs[index1].bbaddr==memrefs[index2].bbaddr) {
            // same basic block
            if (!strcmp(memrefs[index1].regname,memrefs[index2].regname)) {
                if (memrefs[index1].regname[0]=='t') {
                // same temporal variable in same basic block
                    return 1;
                } else {
                    // same register at same basic block
                    printf("WARNING: Assuming NO group reuse\n");
                    printf("Verify that %s has not been updated between memory instructions\n",memrefs[index1].regname);
                    printf("PC: 0x%x, Target: ",memrefs[index1].addr);
                    if (memrefs[index1].loopcoef) {
                        rtvectorPrint(memrefs[index1].loopcoef); printf("\n");
                    } else {
                        printf("Non-linear\n");
                    }
                    printf("PC: 0x%x, Target: ",memrefs[index2].addr);
                    if (memrefs[index2].loopcoef) {
                        rtvectorPrint(memrefs[index2].loopcoef); printf("\n");
                    } else {
                        printf("Non-linear\n");
                    }
                    return 0;
                }
            } else if ((memrefs[index1].addr>memrefs[index2].addr &&  satisfiesEquality(&memrefs[index1].aistate, memrefs[index1].regname, memrefs[index2].regname)) ||
                (memrefs[index1].addr<memrefs[index2].addr && satisfiesEquality(&memrefs[index2].aistate, memrefs[index1].regname, memrefs[index2].regname))) {
            //} else if (satisfiesEquality(&memrefs[index1].aistate, memrefs[index1].regname, memrefs[index2].regname) || satisfiesEquality(&memrefs[index2].aistate, memrefs[index1].regname, memrefs[index2].regname)) {
                // different register at same basic block, but seems equal
                printf("WARNING: Assuming group reuse\n");
                printf("Verify that %s at 0x%x has not been updated until  0x%x\n",memrefs[index1].regname,memrefs[index1].addr,memrefs[index2].addr);
                //printf("PC: 0x%x, Target: ",memrefs[index1].addr);
                //rtvectorPrint(memrefs[index1].loopcoef); printf("\n");
                //printf("PC: 0x%x, Target: ",memrefs[index2].addr);
                //rtvectorPrint(memrefs[index2].loopcoef); printf("\n");
                return 1;
            } else { // equality not satisfied
                return 0;
            }
        } else { // different basic blocks
            if (!strcmp(memrefs[index1].regname,memrefs[index2].regname)) {
                // same variable in different basic block
                printf("WARNING: Assuming NO group reuse\n");
                printf("Verify that %s has not been updated between memory instructions\n",memrefs[index1].regname);
                printf("PC: 0x%x, Target: ",memrefs[index1].addr);
                if (memrefs[index1].loopcoef) {
                    rtvectorPrint(memrefs[index1].loopcoef); printf("\n");
                } else {
                    printf("Non-linear\n");
                }
                printf("PC: 0x%x, Target: ",memrefs[index2].addr);
                if (memrefs[index2].loopcoef) {
                    rtvectorPrint(memrefs[index2].loopcoef); printf("\n");
                } else {
                    printf("Non-linear\n");
                }
                return 0;
            } else if (satisfiesEquality(&memrefs[index1].aistate, memrefs[index1].regname, memrefs[index2].regname) || satisfiesEquality(&memrefs[index2].aistate, memrefs[index1].regname, memrefs[index2].regname)) {
                // different register at same basic block, but seems equal
                printf("WARNING: Assuming NO group reuse\n");
                printf("Verify that %s at 0x%x has the same value as %s at 0x%x\n",memrefs[index1].regname,memrefs[index1].addr,memrefs[index2].regname,memrefs[index2].addr);
                printf("PC: 0x%x, Target: ",memrefs[index1].addr);
                if (memrefs[index1].loopcoef) {
                    rtvectorPrint(memrefs[index1].loopcoef); printf("\n");
                } else {
                    printf("Non-linear\n");
                }
                printf("PC: 0x%x, Target: ",memrefs[index2].addr);
                if (memrefs[index2].loopcoef) {
                    rtvectorPrint(memrefs[index2].loopcoef); printf("\n");
                } else {
                    printf("Non-linear\n");
                }
                 return 0;
            } else { // equality not satisfied
                return 0;
            }
        }
    }
} */

int haveGroupReuse(int index1, int index2, int linesize) {
    if (index1>=nextemptyref || index2>=nextemptyref) {
        printf("haveGroupReuse: out of bounds\n");
        return 0;
    }

    if (memrefs[index1].loopcoef && memrefs[index2].loopcoef) {
        // BOTH LINEAR PATTERNS
        if (!rtvectorEquals(memrefs[index1].loopcoef,memrefs[index2].loopcoef)) {
            // DIFFERENT LINEAR PATTERNS
            if (rtvectorAllKnown(memrefs[index1].loopcoef) && rtvectorAllKnown(memrefs[index2].loopcoef)  && rtvectorIsConstant(memrefs[index1].loopcoef) && rtvectorIsConstant(memrefs[index2].loopcoef) &&
            ((rtvectorGetBase(memrefs[index1].loopcoef)/linesize) == (rtvectorGetBase(memrefs[index2].loopcoef)/linesize))) {
                // BOTH SCALAR (known constant addr) and SAME CACHE LINES -> YES
                return 1;
            } else{
                // DIFFERENT REUSE VECTORS -> NO
                return 0;
            }
        } else {
            // EQUAL LINEAR PATTERNS
            if (rtvectorAllKnown(memrefs[index1].loopcoef) && rtvectorAllKnown(memrefs[index2].loopcoef)) {
                // BOTH KNOWN -> YES
                return 1;
            } else {
                // SOME UNKNOWN
                if (memrefs[index1].bbaddr==memrefs[index2].bbaddr) {
                    // SAME BASIC BLOCK
                    if (!strcmp(memrefs[index1].regname,memrefs[index2].regname)) {
                        // SAME VARIABLE
                        if (memrefs[index1].regname[0]=='t') {
                            // SAME TEMPORAL VARIABLE -> YES
                            return 1;
                        } else {
                            // SAME REGISTER -> NO
                            printf("WARNING: Assuming NO group reuse\n");
                            printf("Verify that %s has not been updated between memory instructions\n",memrefs[index1].regname);
                            printf("PC: 0x%x, Target: ",memrefs[index1].addr);
                            rtvectorPrint(memrefs[index1].loopcoef); printf("\n");
                            printf("PC: 0x%x, Target: ",memrefs[index2].addr);
                            rtvectorPrint(memrefs[index2].loopcoef); printf("\n");
                            return 0;
                        }
                    } else {
                        // DIFFERENT VARIABLES
                        if (
                            (memrefs[index1].addr>memrefs[index2].addr &&  satisfiesEquality(&memrefs[index1].aistate, memrefs[index1].regname, memrefs[index2].regname)) ||
                            (memrefs[index1].addr<memrefs[index2].addr && satisfiesEquality(&memrefs[index2].aistate, memrefs[index1].regname, memrefs[index2].regname))
                        ) {
                            // SATISFIES ABSTRACT EQUALITY -> YES
                            printf("WARNING: Assuming group reuse\n");
                            printf("Verify that %s at 0x%x has not been updated until  0x%x\n",memrefs[index1].regname,memrefs[index1].addr,memrefs[index2].addr);
                            //printf("PC: 0x%x, Target: ",memrefs[index1].addr);
                            //rtvectorPrint(memrefs[index1].loopcoef); printf("\n");
                            //printf("PC: 0x%x, Target: ",memrefs[index2].addr);
                            //rtvectorPrint(memrefs[index2].loopcoef); printf("\n");
                            return 1;
                        } else { // EQUALITY NOT SATISFIED -> NO
                            return 0;
                        }
                    }
                } else {
                    // DIFFERENT BASIC BLOCKS
                    if (!strcmp(memrefs[index1].regname,memrefs[index2].regname)) {
                        // SAME VARIABLE
                        if (memrefs[index1].regname[0]=='t') {
                            // SAME TEMPORAL VARIABLE -> YES
                            return 1;
                        } else {
                            // SAME REGISTER -> NO
                            printf("WARNING: Assuming NO group reuse\n");
                            printf("Verify that %s has not been updated between memory instructions\n",memrefs[index1].regname);
                            printf("PC: 0x%x, Target: ",memrefs[index1].addr);
                            rtvectorPrint(memrefs[index1].loopcoef); printf("\n");
                            printf("PC: 0x%x, Target: ",memrefs[index2].addr);
                            rtvectorPrint(memrefs[index2].loopcoef); printf("\n");
                            return 0;
                        }
                    }
                    return 0;
                }
            }
        }


    } else if (!memrefs[index1].loopcoef && !memrefs[index2].loopcoef) {
        // BOTH NON-LINEAR
        if (!strcmp(memrefs[index1].regname,memrefs[index2].regname)) {
            // SAME VARIABLE
            if (memrefs[index1].regname[0]=='t') {
                // SAME TEMPORAL VARIABLE -> YES
                printf("WARNING: Assuming group reuse!!!!!\n");
                printf("Verify that %s (non-linear) has not been updated between memory instructions at 0x%x and 0x%x\n",memrefs[index1].regname,memrefs[index1].addr,memrefs[index2].addr);
                return 1;
            } else {
                // SAME REGISTER -> NO
                printf("WARNING: Assuming NO group reuse!!!!!\n");
                printf("Verify that %s (non-linear) has not been updated between memory instructions at 0x%x and 0x%x\n",memrefs[index1].regname,memrefs[index1].addr,memrefs[index2].addr);
                return 0;
            }
        } else {
            // DIFFERENT VARIABLE
            if (memrefs[index1].addr==memrefs[index2].addr) {
                // MULTIREGISTER LOAD/STORE
                printf("WARNING: Assuming NO group reuse for multireg\n");
                printf("Memory accesses 0x%x probably present spatial reuse that it is not being accounted\n",memrefs[index1].addr);
                return 0;
            } else {
                // NORMAL LOAD/STORE
                if (satisfiesEquality(&memrefs[index1].aistate, memrefs[index1].regname, memrefs[index2].regname) || satisfiesEquality(&memrefs[index2].aistate, memrefs[index1].regname, memrefs[index2].regname)) {
                    // SATISFIES ABSTRACT EQUALITY -> YES
                    printf("WARNING: Assuming group reuse\n");
                    printf("Verify that %s at 0x%x and %s at 0x%x present group reuse\n",memrefs[index1].regname,memrefs[index1].addr,memrefs[index2].regname,memrefs[index2].addr);
                    //printf("PC: 0x%x, Target: Non-linear\n",memrefs[index1].addr);
                    //printf("PC: 0x%x, Target: Non-linear\n",memrefs[index2].addr);
                    return 1;
                } else {
                    return 0;
                }
            }
        }
    } else {
        // LINEAR AND NON-LINEAR
        return 0;
    }
}

/*
        } else if (!strcmp(memrefs[index1].regname,memrefs[index2].regname)) {
            if (memrefs[index1].regname[0]=='t' && memrefs[index1].bbaddr==memrefs[index2].bbaddr) {
                // same temporal variable in same basic block
                return 1;
            } else {
                // same register or different basic block
                printf("WARNING: Assming no group reuse\n");
                printf("Verify that %s has not been updated between memory instructions\n",memrefs[index1].regname);
                printf("PC: 0x%x, Target: ",memrefs[index1].addr);
                rtvectorPrint(memrefs[index1].loopcoef); printf("\n");
                printf("PC: 0x%x, Target: ",memrefs[index2].addr);
                rtvectorPrint(memrefs[index2].loopcoef); printf("\n");
                return 0;
            }
        } else if (satisfiesEquality(&memrefs[index1].aistate, memrefs[index1].regname, memrefs[index2].regname) || satisfiesEquality(&memrefs[index2].aistate, memrefs[index1].regname, memrefs[index2].regname)) {
            // variables are currently equal, but ¿have they been updated?
            printf("WARNING: Assming no group reuse\n");
            printf("Verify that %s at 0x%x has the same value as %s at 0x%x\n",memrefs[index1].regname,memrefs[index1].addr,memrefs[index2].regname,memrefs[index2].addr);
            printf("PC: 0x%x, Target: ",memrefs[index1].addr);
            rtvectorPrint(memrefs[index1].loopcoef); printf("\n");
            printf("PC: 0x%x, Target: ",memrefs[index2].addr);
            rtvectorPrint(memrefs[index2].loopcoef); printf("\n");
            return 0;
        } else {
            // variables are different
            return 0;
        }
    }
}
*/

// TODO: optimize
/*
int haveGroupReuse2(int index1, int index2, int linesize) {
    if (index1>=nextemptyref || index2>=nextemptyref) {
        printf("haveGroupReuse: out of bounds\n");
        return 0;
    } else {
        if (memrefs[index1].loopcoef && memrefs[index2].loopcoef && rtvectorAllKnown(memrefs[index1].loopcoef) && rtvectorAllKnown(memrefs[index2].loopcoef)) { // both known
            if (rtvectorIsConstant(memrefs[index1].loopcoef) && rtvectorIsConstant(memrefs[index2].loopcoef)) { // both constant
                if ((rtvectorGetBase(memrefs[index1].loopcoef)/linesize) == (rtvectorGetBase(memrefs[index2].loopcoef)/linesize)) { // same cache lines
                    //printf("group reuse constant\n");
                    return 1;
                } else {
                    return 0;
                }
            } else { // known, not constant
                if (rtvectorEquals(memrefs[index1].loopcoef,memrefs[index2].loopcoef)) {
                    // known, equals
                    //printf("group reuse pattern\n");
                    return 1;
                } else {
                    return 0;
                }
            }
        } else if (memrefs[index1].regname[0]=='t' && memrefs[index1].bbaddr==memrefs[index2].bbaddr && !strcmp(memrefs[index1].regname,memrefs[index2].regname)) {
            // same temporal variable in the same basic block: reuse
            //printf("same temp: reuse\n");
            return 1;
        } else if (memrefs[index1].loopcoef==NULL || memrefs[index2].loopcoef==NULL || (!rtvectorAllKnown(memrefs[index1].loopcoef) && !rtvectorAllKnown(memrefs[index2].loopcoef))) { // both unknown
        //} else { // unknown, test abstract state equality
            //printf("unknown: TODO abstract state satisfy equality\n");
            //rtvectorPrint(memrefs[index1].loopcoef); printf("\n");
            //rtvectorPrint(memrefs[index2].loopcoef); printf("\n");
            //return 0;
            // TODO; remove the previous return 0
            if (satisfiesEquality(&memrefs[index1].aistate, memrefs[index1].regname, memrefs[index2].regname) || satisfiesEquality(&memrefs[index2].aistate, memrefs[index1].regname, memrefs[index2].regname)) {
                if (memrefs[index1].bbaddr==memrefs[index2].bbaddr) {
                    return 1;
                } else {
                    printf("WARNING: guaranteed group reuse?\n");
                    printf("PC: 0x%x",memrefs[index1].addr);
                    if (memrefs[index1].loopcoef) {
                        rtvectorPrint(memrefs[index1].loopcoef); printf("\n");
                    } else {
                        printf("Top\n");
                    }
                    printf("PC: 0x%x",memrefs[index2].addr);
                    if (memrefs[index2].loopcoef) {
                        rtvectorPrint(memrefs[index2].loopcoef); printf("\n");
                    } else {
                        printf("Top\n");
                    }
                    return 1;
                }
            } else {
                //printf("not abstract group reuse\n");
                return 0;
            }
        } else {
            return 0;
        }
    }
}
*/

void printXMLinfo(char* filename, char* cfgtype) {
    FILE *fd;

    fd=fopen(filename,"w+");
    fprintf(fd,"<?xml version=\"1.0\"?>\n");
#ifdef POLYGAZ_VERSION
    fprintf(fd,"<memrefs polygaz=\"%s\"", POLYGAZ_VERSION);
#else
    fprintf(fd,"<memrefs");
#endif
    fprintf(fd," cfg=\"%s\">\n", cfgtype);

    int access=0;
    for (int ref=0; ref<nextemptyref; ref++) {
        fprintf(fd, "  <reference id=\"%d\">\n", ref);
        fprintf(fd, "    <cfg_node>%d</cfg_node>\n", memrefs[ref].cfgnodeid);
        fprintf(fd, "    <pc>%#x</pc>\n", memrefs[ref].addr);
        if (ref!=0 && memrefs[ref].cfgnodeid==memrefs[ref-1].cfgnodeid && memrefs[ref].addr==memrefs[ref-1].addr) {
            access++;
        } else {
            access=0;
        }
        fprintf(fd, "    <number>%d</number>\n", access);
        if (memrefs[ref].isload) {
            fprintf(fd, "    <type>load</type>\n");
        } else {
            fprintf(fd, "    <type>store</type>\n");
        }
        if (memrefs[ref].isconditional) {
            fprintf(fd, "    <predicated>yes</predicated>\n");
        } else {
            fprintf(fd, "    <predicated>no</predicated>\n");
        }
        fprintf(fd, "    <bytes>%d</bytes>\n", memrefs[ref].bytes);
        if (memrefs[ref].loopcoef) { // optional
            // constant or array
            //rtvectorPrint(memrefs[ref].loopcoef); printf("\n");
            int len=rtvectorGetLen(memrefs[ref].loopcoef);
            for (int i=0; i<len; i++) {
                int addr=rtvectorGetLoopAddr(memrefs[ref].loopcoef,i);
                int coef=rtvectorGetCoef(memrefs[ref].loopcoef,i);
                if (addr && coef) {
                    // array
                    fprintf(fd, "    <term i=\"%#x\" h=\"%d\"/>\n", addr,coef);
                } // if no printfs: constant
            }
            if (rtvectorIsBaseKnown(memrefs[ref].loopcoef)) {
                // constant / array+base
                fprintf(fd, "    <c>%#x</c>\n", rtvectorGetBase(memrefs[ref].loopcoef));
            } // else: array with unknown base address
        } // else: non-linear
        // id of first access dominating this reference, if any
        if (memrefs[ref].firstGR) {
            fprintf(fd, "    <firstreused>%d</firstreused>\n", (int)(memrefs[ref].firstGR-memrefs));
        }
        // id of last access dominating this reference, if any
        if (memrefs[ref].lastGR) {
            fprintf(fd, "    <lastreused>%d</lastreused>\n", (int)(memrefs[ref].lastGR-memrefs));
        }
        fprintf(fd,"  </reference>\n");
    }

    fprintf(fd,"</memrefs>\n");
    fclose(fd);
}

///////////////////////////// CATEGORIZATION ?

void addCount(int index, int count) {
    memrefs[index].maxcount=count;
}

void classify(int linesize) {
    long int notknown=0, array=0, scalar=0, total=0;
    long int //tempreuse=0, // temporal reuse (GSR; always hit)
    tempreusesca=0, // temporal reuse for scalars (GSR; always hit)
    tempreusearray=0, // temporal reuse for arrays (GSR; always hit)
    tempreusenotknown=0, // temporal reuse for non-linear (GSR; always hit)
    selftempreusesca=0, // self temporal reuse (first miss)
    tempnoreuse=0, // scalar accesses without reuse (the first miss / single miss)
    spareuse=0, // short stride spatial reuse (high hit ratio)
    toolongspareuse=0; // long stride spatial reuse (always miss)
    long int acdchit=0, // hits considering the reuse above
    acdcmiss=0; // misses considering the reuse above

    printf("\n\n categorization of memory references\n----------------------\n");
    for (int ref=0; ref<nextemptyref; ref++) {
        printf("%d) ",ref);
        if (memrefs[ref].lastGR) { // has group reuse
            if (memrefs[ref].dest) { // scalar
                printf("Group-temporal (scalar): %d accesses\n",memrefs[ref].maxcount);
                scalar+=memrefs[ref].maxcount;
                tempreusesca+=memrefs[ref].maxcount;
            } else if (memrefs[ref].loopcoef) { // array
                printf("Group-temporal (array): %d accesses\n",memrefs[ref].maxcount);
                array+=memrefs[ref].maxcount;
                tempreusearray+=memrefs[ref].maxcount;
            } else { // non-linear
                printf("Non-linear: %d accesses\n",memrefs[ref].maxcount);
                tempreusenotknown+=memrefs[ref].maxcount;
                notknown+=memrefs[ref].maxcount;
            }
            acdchit+=memrefs[ref].maxcount;
        } else { // without group reuse
            if (memrefs[ref].dest) { // scalar
                scalar+=memrefs[ref].maxcount;
                if (memrefs[ref].maxcount==1) { // Scalar, single time
                    printf("First-use (scalar): 1 access\n");
                    tempnoreuse+=1;
                } else { // scalar, several times
                    printf("Self-temporal (scalar): %d accesses\n",memrefs[ref].maxcount);
                    selftempreusesca+=memrefs[ref].maxcount;
                }
                acdcmiss+=1;
                acdchit+=memrefs[ref].maxcount-1;
            } else if (memrefs[ref].loopcoef) { // array
                array+=memrefs[ref].maxcount;
                if (rtvectorGetDeepestCoef(memrefs[ref].loopcoef)<linesize) {
                    printf("Short self-spatial (array): %d accesses TODO TODO TODO\n",memrefs[ref].maxcount);
                    spareuse+=memrefs[ref].maxcount;
                    // TODO acdchit+=memrefs[ref].maxcount;
                    // TODO acdcmiss+=memrefs[ref].maxcount;
                } else {
                    printf("Long self-spatial (array): %d accesses\n",memrefs[ref].maxcount);
                    toolongspareuse+=memrefs[ref].maxcount;
                    acdcmiss+=memrefs[ref].maxcount;
                }
            } else { // non-linear
                printf("Non-linear: %d accesses\n",memrefs[ref].maxcount);
                notknown+=memrefs[ref].maxcount;
                // TODO: si tiene self reuse sería hit?
                acdcmiss+=memrefs[ref].maxcount;
            }
        }
    }
/*
        // TODO: antes de nada mirar lastGR
        if (memrefs[ref].loopcoef==NULL) {
            printf("Non-linear: %d accesses\n",memrefs[ref].maxcount);
            notknown+=memrefs[ref].maxcount;
            acdcmiss+=memrefs[ref].maxcount;
        } else if (memrefs[ref].maxcount==1) { // Scalar, single time
            scalar+=memrefs[ref].maxcount;
            if (memrefs[ref].lastGR==NULL) {
                //printf("0h, 1m: Scalar Single miss (AM)\n");
                printf("First-use (scalar): 1 access\n");
                tempnoreuse+=1;
                acdcmiss+=1;
            } else {
                //printf("1h, 0m: Scalar Single hit (AH)\n");
                printf("Group-temporal (scalar): 1 access\n");
                tempreusesca+=memrefs[ref].maxcount;
                acdchit+=memrefs[ref].maxcount;
            }
        } else if (memrefs[ref].dest) { // scalar, several times
            scalar+=memrefs[ref].maxcount;
            if (memrefs[ref].lastGR==NULL) {
                //printf("%dh, 1m: Potential First miss (FM)\n",memrefs[ref].maxcount-1);
                printf("Self-temporal (scalar): %d accesses\n",memrefs[ref].maxcount);
                selftempreusesca+=memrefs[ref].maxcount;
                acdchit+=memrefs[ref].maxcount-1;
                acdcmiss+=1;
            } else {
                printf("Group-temporal (scalar): %d accesses\n",memrefs[ref].maxcount);
                tempreusesca+=memrefs[ref].maxcount;
                acdchit+=memrefs[ref].maxcount;
            }
        } else { // array or unknown TODO unknown
            array+=memrefs[ref].maxcount;
            if (memrefs[ref].lastGR==NULL) {
                //printf("Short/Long self-spatial (array): %d accesses\n",memrefs[ref].maxcount);
                spareuse+=memrefs[ref].maxcount;

                if (rtvectorGetDeepestCoef(memrefs[ref].loopcoef)<linesize) {
                    printf("Short self-spatial (array): %d accesses TODO TODO TODO\n",memrefs[ref].maxcount);
                    // TODO acdchit+=memrefs[ref].maxcount;
                    // TODO acdcmiss+=memrefs[ref].maxcount;
                } else {
                    printf("Long self-spatial (array): %d accesses\n",memrefs[ref].maxcount);
                    toolongspareuse+=memrefs[ref].maxcount;
                    acdcmiss+=memrefs[ref].maxcount;
                }
            } else {
                //printf("%dh, 0m: Potential Always hit (AH)\n",memrefs[ref].maxcount);
                printf("Group-temporal (array): %d accesses\n",memrefs[ref].maxcount);
                tempreusearray+=memrefs[ref].maxcount;
                acdchit+=memrefs[ref].maxcount;
            }
        }
    }
*/
    total=notknown+scalar+array;
    printf("\n\n summary of categorization\n----------------------\n");
    printf("Cache line size: %d Bytes\n",linesize);
    printf("Not Known: %ld (%ld%%)\n",notknown,notknown*100/total);
    printf("Scalar: %ld (%ld%%)\n",scalar,scalar*100/total);
    printf("Array: %ld (%ld%%)\n",array,array*100/total);
    printf("Scalar temporal reuse: %ld (%ld%%)\n",tempreusesca,tempreusesca*100/total);
    printf("Array temporal reuse: %ld (%ld%%)\n",tempreusearray,tempreusearray*100/total);
    printf("Non-linear temporal reuse: %ld (%ld%%)\n",tempreusenotknown,tempreusenotknown*100/total);
    printf("Scalar self reuse: %ld (%ld%%)\n",selftempreusesca,selftempreusesca*100/total);
    printf("Scalar not reused: %ld (%ld%%)\n",tempnoreuse,tempnoreuse*100/total);
    printf("Spatial non-temporal stride reuse: %ld (%ld%%)\n",spareuse,spareuse*100/total);
    printf("Spatial non-temporal longstrd reuse: %ld (%ld%%)\n",toolongspareuse,toolongspareuse*100/total);
    //printf("ACDC hits: %ld (%ld%%)\n",acdchit,acdchit*100/total);
    //printf("ACDC misses: %ld (%ld%%)\n",acdcmiss,acdcmiss*100/total);
}
