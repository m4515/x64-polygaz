#!angr/bin/python3
# coding=utf-8

""" Module for printing .dot files """

import sys
from lockMS import var_name, init, get_cfg, get_loop_headers, get_return_addrs, no_back_edge_successors, is_break_edge, get_loop_addr
import flowfacts


def print_dot_file(filename):
    """ Prints a .dot file with the CFG """
    global dot_file
    dot_file = open(filename + '.dot', 'w')
    dot_file.write("digraph main{\n")
    dot_file.write("node [shape=Mrecord, labeljust=l, fontsize=10];\n")
    for node in cfg.graph.nodes():
        dot_file.write(var_name("B", node)) # id
        dot_file.write(' [label="{') # [label={xxx|yyy|zzz}]
        dot_file.write(var_name("B", node)) # name
        if node.addr in cfg.kb.functions:
            func = cfg.kb.functions[node.addr]
            dot_file.write('| ' + func.name)
        if node in loop_headers:
            if node.addr in flowfacts.maxLoopIter:
                # loop iterations
                dot_file.write('| Loop iterations: %s' % str(flowfacts.maxLoopIter[node.addr]))
            else:
                dot_file.write('| Loop unbounded')
#        dot_file.write('| ASM code here') # + node.block.pp())
        dot_file.write('| ')
        try:
            for inst in node.block.capstone.insns: # for each instruction
                dot_file.write(inst.mnemonic + ' ' + remove_braces(inst.op_str) + '\\l')
        except AttributeError as error:
            # Unresolvable jump target
            print(error)
            print("print_dot_file error: CFG node without instructions")
            dot_file.write('error: block is None')
        dot_file.write('}"]\n')
    for src, dst in cfg.graph.edges():
        # src -> dst
        if dst in src.successors:
            if cfg.kb.functions.function(addr=dst.addr): # call
                dot_file.write('%s -> %s[style=dashed]' % (var_name("B", src), var_name("B", dst)))
            elif dst.addr in return_addrs: # return
                dot_file.write('%s -> %s[style=dashed]' % (var_name("B", src), var_name("B", dst)))
            else:
                dot_file.write('%s -> %s' % (var_name("B", src), var_name("B", dst)))
        #dot_file.write(' [label=xxx, style=dashed]') # label,style,weight
            dot_file.write(';\n')
    dot_file.write('}\n')
    dot_file.close()


def print_dot_tree(filename):
    """ Prints a .dot file with the CFG tree structure """
    global dot_file
    fmain = cfg.kb.functions.function(name="main") # main function
    main_node = cfg.model.get_any_node(fmain.addr) # main CFG node
    dot_file = open(filename + '.dot', 'w')
    dot_file.write("digraph main{\n")
    dot_file.write("node [shape=Mrecord, labeljust=l, fontsize=10];\n")
    explore.explored = []
    generate_dot_tree(dot_file, main_node, explore(main_node, [], 1))
    dot_file.write('}\n')
    dot_file.close()


def generate_dot_tree(dot_file, cfg_node, path):
    """
    Prints a .dot file with the structural tree of the CFG.
    For debugging purposes.
    """
    #dot_file.write(var_name("C", cfg_node) + ' -> ' + var_name("C", path[-1]) + ";\n")
    # prints consecutive block nodes
    #for node in path[:-1]:
    #    dot_file.write(var_name("C", cfg_node) + ' -> ' + var_name("B", node) + ";\n")
    #dot_file.write(var_name("C", cfg_node)) # id
    dot_file.write(var_name("C", cfg_node) + ' -> ' + var_name("C", path[-1]) + ' [label="') # [label={xxx|yyy|zzz}]
    for node in path[:-2]:
        dot_file.write(var_name("B", node) + "\\l")
    dot_file.write(var_name("B", path[-2]) + '"] ;\n')
    #dot_file.write('}"]\n')


"""
This function shoud be a copy of the one in the imported module,
substituting the generate_altpath_constraint() call by
generate_dot_tree() with the same parameters.
"""
def explore(cfg_node, path, maxexecs):
    """ Explores a CFG and builds a tree-structured ILP model """
    """ DOI: 10.1371/journal.pone.0229980 """
    global times
    #print(hex(cfg_node.addr), maxexecs)
    successors = no_back_edge_successors(cfg_node)
    #print("No back edge successors of", cfg_node, ":", successors)
    times[cfg_node] = maxexecs
    if len(successors) == 0:
        return path + [cfg_node]
    elif len(successors) == 1:
        if is_break_edge(cfg_node, successors[0]):
            maxexecs = int(maxexecs / flowfacts.maxLoopIter[get_loop_addr(cfg_node)])
        if successors[0] in loop_headers:
            maxexecs *= flowfacts.maxLoopIter[successors[0].addr]
            if maxexecs == 0:
                print("explore: reached loop 0x%x (0 iterations)" % successors[0].addr)
                return path + [cfg_node]
        return explore(successors[0], path + [cfg_node], maxexecs)
    elif len(successors) > 1 and cfg_node in explore.explored:
        return path + [cfg_node]
    else:
        explore.explored.append(cfg_node)
        for child_node in successors:
            alternative_path = None
            if child_node in loop_headers:
                if flowfacts.maxLoopIter[child_node.addr]:
                    alternative_path = explore(child_node, [cfg_node], maxexecs * flowfacts.maxLoopIter[child_node.addr])
                else:
                    print("explore: reached loop 0x%x (0 iterations)" % child_node.addr)
            elif is_break_edge(cfg_node, child_node):
                alternative_path = explore(child_node, [cfg_node], int(maxexecs / flowfacts.maxLoopIter[get_loop_addr(cfg_node)]))
            else:
                alternative_path = explore(child_node, [cfg_node], maxexecs)
            if alternative_path:
                generate_dot_tree(dot_file, cfg_node, alternative_path)
        return path + [cfg_node]
explore.explored = []


def remove_braces(string):
    """ Sanitizes string for .dot tags """
    output = ""
    for char in string:
        if char not in ('{', '}'):
            output += char
    return output


def main():
    global cfg, loop_headers, return_addrs, times
    if len(sys.argv) != 2:
        print("usage:", sys.argv[0], "<binaryfile>")
        sys.exit()
    filename = sys.argv[1]

    # cfg fast
    instance_functions = False
    init(filename, instance_functions)
    cfg = get_cfg()
    loop_headers = get_loop_headers()
    return_addrs = get_return_addrs()
    times = {}
    print_dot_file(filename + "_notinstanced_cfg")
    print(filename + "_notinstanced_cfg.dot generated")
    # tree
    print_dot_tree(filename + "_notinstanced_tree")
    print(filename + "_notinstanced_tree.dot generated")

    # cfg emulated
    instance_functions = True
    init(filename, instance_functions)
    cfg = get_cfg()
    loop_headers = get_loop_headers()
    return_addrs = get_return_addrs()
    times = {}
    print_dot_file(filename + "_instanced_cfg")
    print(filename + "_instanced_cfg.dot generated")
    # tree
    print_dot_tree(filename + "_instanced_tree")
    print(filename + "_instanced_tree.dot generated")


if __name__ == "__main__":
    sys.exit(main())
