#!venv/bin/python3
# coding=utf-8

""" Module with a set of functions to perform CFG/WCET analyses """


import sys
import resource # to set stack limit to unlimited (ulimit -s unlimited)
from math import ceil
import time
#import networkx
import angr # grafo
#import claripy # estado de memoria
import pyvex # irVEX

#import indvars
import flowfacts
import xmlparse

#import logging

DEBUG_COST = False

######################### INSTRUCTION CACHE #########################
FETCH_HIT_COST = 1
FETCH_MISS_COST = 14
# Always-hit and always-miss: FETCH_HIT_COST=FETCH_MISS_COST
INSTRUCTION_SIZE = 4 # bytes
INSTRUCTION_CACHE_TYPE = "locked"
INSTRUCTION_CACHE_SETS = 64 # intel/arm L1 instruction/data
INSTRUCTION_CACHE_WAYS = 8 # intel/arm L1 instruction/data
CACHE_LINE_SIZE = 64 # bytes, INSTRUCTION AND DATA
IC_PRELOAD_CALL_COST = 47 # cycles. Call + execute preloading + return
# The actual cost of preloading cache lines is not included; it is calculated
# based on the number of cache lines to preload.

############################ DATA CACHE ############################
DISABLE_ACDC_PENALTIES = True # True: UNSAFE but better DRP selection
DATA_HIT_COST = 1
DATA_MISS_COST = 14 # cache lookup (1) + memory latency (13)
# Micron Technology, Inc., Automotive DDR SDRAM MT46V32M8, MT46V16M16, https://media-www.micron.com/-/media/client/global/documents/products/data-sheet/dram/mobile-dram/low-power-dram/lpddr/256mb_x8x16_at_ddr_t66a.pdf
MEMORY_COST = DATA_MISS_COST - DATA_HIT_COST
COPYBACK_COST = MEMORY_COST
ACDC_PRELOAD_CALL_COST = IC_PRELOAD_CALL_COST


################################ INIT ################################

def no_back_edge_successors(cfg_node):
    """ Returns a list of successors not being back edges """
    result = []
    for dest_node in cfg_node.successors:
        is_back_edge = isBackEdge(dest_node, cfg_node)
        if not is_back_edge:
            # discard strange false single-node loop on fmref-O2 at 0x10528
            if dest_node == cfg_node:
                print("no_back_edge_successors warning: discarding a seemingly false single-node loop at 0x%x" % cfg_node.addr)
                is_back_edge = True
        if not is_back_edge:
            result.append(dest_node)
    return result


def init(filename, instance_functions):
    """initializes all"""
    global cfg, loop_info, apron, registers, main_node, cfg_nodes, loop_headers, return_addrs, ending_nodes, enclosing_loop_header, max_times

    # Set-up project from binary file
    proj = angr.Project(filename, auto_load_libs=False) # carga ejecutable
    main = proj.loader.main_object.get_symbol("main")
    #state = proj.factory.entry_state()
    state = proj.factory.blank_state(addr=main.rebased_addr)

    flowfacts.get_flowfacts(filename)
    #print("Flow facts:", flowfacts.maxLoopIter)

    # Build CFG and loop_info
    if instance_functions:
        # context_sensitivity_level=3 works except for fmref O2, md5, test3
        # md5_O0_arm (gcc-9.2.1) requires context_sensitivity_level=4
        # md5_O2_arm (gcc-9.2.1) requires context_sensitivity_level=6
        cfg = proj.analyses.CFGEmulated(normalize=True, context_sensitivity_level=3, starts=[main.rebased_addr], initial_state=state) # genera CFG
        print("CFG (emulated) has %d nodes and %d edges" % (len(cfg.graph.nodes()), len(cfg.graph.edges())))
    else:
        cfg = proj.analyses.CFG(normalize=True) # genera CFG (CFGFast)
        print("CFG (fast) has %d nodes and %d edges" % (len(cfg.graph.nodes()), len(cfg.graph.edges())))

    loop_info = proj.analyses.LoopFinder() # Set-up loop information
    # Locate main()
    fmain = cfg.kb.functions.function(name="main") # main function
    main_node = cfg.model.get_any_node(fmain.addr) # main CFG node
    #mainBlock = main_node.block # main basic block

    # set cfg_node indexes and headers for each loop
    # (a given loop may be represented by several emulated cfg node groups)
    #for loop in loop_info.loops:
    #    print("loop at", hex(loop.entry.addr))
    cfg_nodes = {}
    loop_headers = {}
    ending_nodes = []
    i = 0
    for node in cfg.model.nodes():
        cfg_nodes[node] = i
        for loop in loop_info.loops:
            if node.addr == loop.entry.addr:
                #print(hex(node.addr), "is header")
                loop_headers[node] = loop
                break
        #if not node.successors:
        if len(no_back_edge_successors(node)) == 0:
            ending_nodes.append(node) # CFG or loop ending nodes
        i += 1
    return_addrs = []
    for src, dst in cfg.graph.edges():
        if dst not in src.successors:
            return_addrs.append(dst.addr)

    #### Stack limits
    resource.setrlimit(resource.RLIMIT_STACK, (resource.RLIM_INFINITY, resource.RLIM_INFINITY)) # set system stack limit to unlimited
    print("System stack limit:", resource.getrlimit(resource.RLIMIT_STACK))
    # Print max stack depth (default: 1000)
    #sys.setrecursionlimit(20000) # works with default ulimit stack size (8192 KiB)
    sys.setrecursionlimit(1000000) # test: $ ulimit -s unlimited
    print("Python recursion limit (stack depth):", sys.getrecursionlimit())

    #### from ipet source code
    enclosing_loop_header = {}
    max_times = {}
    recursive_tag_loop(main_node, 1, [])

def recursive_tag_loop(cfg_node, max_execs, loop_nest_list):
    """ Explores CFG and builds ILP model """
    #global times, node_penalty
    #print(hex(cfg_node.addr), maxexecs)
    if cfg_node in enclosing_loop_header:
        # already explored
        return
    else:
        # set enclosing loop
        if len(loop_nest_list):
            enclosing_loop_header[cfg_node] = loop_nest_list[-1]
        else:
            enclosing_loop_header[cfg_node] = None
        max_times[cfg_node] = max_execs
        # explore successors
        successors = no_back_edge_successors(cfg_node)
        #print("No back edge successors of", cfg_node, ":", successors)
        if len(successors) == 0:
            return
        else:
            for child_node in successors:
                if child_node in loop_headers: # entering a loop
                #if child_node.addr in flowfacts.maxLoopIter: # entering a loop
                    if flowfacts.maxLoopIter[child_node.addr]:
                        #recursive_tag_loop(child_node, max_execs * flowfacts.maxLoopIter[child_node.addr], loop_nest_list + [loop_headers[child_node]])
                        recursive_tag_loop(child_node, max_execs * flowfacts.maxLoopIter[child_node.addr], loop_nest_list + [child_node])
                    else:
                        print("explore: reached loop 0x%x (0 iterations)" % child_node.addr)
                elif is_break_edge(cfg_node, child_node):
                    #recursive_tag_loop(child_node, int(max_execs / flowfacts.maxLoopIter[enclosing_loop[cfg_node].entry.addr]), loop_nest_list[:-1])
                    recursive_tag_loop(child_node, int(max_execs / flowfacts.maxLoopIter[enclosing_loop_header[cfg_node].addr]), loop_nest_list[:-1])
                else:
                    recursive_tag_loop(child_node, max_execs, loop_nest_list)
            return


def get_cfg():
    """ Returns the CFG created in init() """
    return cfg

def get_loop_headers():
    """ Returns the loop_headers structure created in init() """
    return loop_headers

def get_return_addrs():
    """ Returns the return_addrs structure created in init() """
    return return_addrs


############################ LOOP/NODE OPERATIONS ############################
def getDeepestLoop(node):
    """get deepest loop including CFGnode node"""
    loopnode = None
    for loop in loop_info.loops:
        if node.to_codenode() in loop.body_nodes:
            if loopnode is None:
                loopnode = loop
            else:
                if loopnode not in loop.subloops:
                    loopnode = loop
    if loopnode is None:
        return 0
    else:
        return loopnode.entry.addr

def set_node_id(node):
    """must fit in 32 bits"""
    return cfg_nodes[node]

def get_cfgid_node(nodeid):
    """ Gets node from identifier """
    for node in cfg_nodes:
        if cfg_nodes[node] == nodeid:
            return node
    print("get_cfgid_node: nodeID %d not found." % nodeid)
    sys.exit(-1)


def get_all_cfg_nodes(instaddr):
    """ Gets all nodes including instaddr """
    while (cfg.model.get_any_node(instaddr) is None) and (instaddr > 0):
        instaddr -= INSTRUCTION_SIZE
    return cfg.model.get_all_nodes(instaddr)


def get_any_cfg_node(instaddr):
    """ Gets any node including instaddr """
    while (cfg.model.get_any_node(instaddr) is None) and (instaddr > 0):
        instaddr -= INSTRUCTION_SIZE
    return cfg.model.get_any_node(instaddr)


def getLoop(nodeaddr):
    for loop in loop_info.loops:
        if nodeaddr == loop.entry.addr:
            return loop
    return None


## Test whether there is a back-edge from previous to CFGNode (loop header)
def isBackEdge(CFGNode, prevNode):
    """ Returns whether prevNode -> CFGNode is a back (continue) edge """
    for loop in loop_info.loops:
        if CFGNode.addr == loop.entry.addr:
            if (prevNode.to_codenode(), CFGNode.to_codenode()) in loop.continue_edges:
                return True
            # case of body basicblock ending with call, and header just in the next address
            node_above = get_any_cfg_node(CFGNode.addr - INSTRUCTION_SIZE)
            if node_above.to_codenode() in loop.body_nodes:
                if len(node_above.successors) == 1 and node_above.successors[0] != CFGNode:
                    called_function = cfg.kb.functions.function(addr=node_above.successors[0].addr)
                    if called_function and prevNode.to_codenode() in set(called_function.nodes):
                        #print("isBackEdge warning: Revise whether node", node_above, "calls a function reaching", prevNode, "and returns to the loop header", CFGNode)
                        return True
    return False


def get_node_set(func):
    #print("nodes of", func.name, ":", func.nodes)
    for node in set(func.nodes):
        if node not in get_node_set.nodeset:
            get_node_set.nodeset.append(node)
    for call in set(func.get_call_sites()):
        calledf = func.get_call_target(call)
        if calledf != func.addr: # do not follow recursive calls
            get_node_set(cfg.kb.functions[calledf])
    return get_node_set.nodeset
get_node_set.nodeset = []


def getLoopNodeSet(loop):
    loopnodeset = []
    for blocknode in loop.body_nodes:
        loopnodeset.append(blocknode.addr)
    if loop.has_calls:
        for blocknode in loop.body_nodes:
            #print(blocknode.addr)
            node = cfg.model.get_any_node(blocknode.addr)
            #for successor in blocknode.successors():
            # bug? blocknode.successors() fails sometimes
            for successor in node.successors:
                func = cfg.kb.functions.function(addr=successor.addr)
                if func:
                    #loopnodeset += getNodeSet(func)
                    for node2 in get_node_set(func):
                        loopnodeset.append(node2.addr)
    return loopnodeset



def classify_successors(cfg_node, loop):
    """ classifies, removing continue_edges to other loops and returns from body_nodes """
    internal_nodes = []
    break_nodes = []
    continue_nodes = []
    continue_to_other_loops = []
    #print(cfg_node)
    #print("Successors (and type of jump) of the entry point:", [ jumpkind + " to " + str(node.addr) for node,jumpkind in cfg.get_successors_and_jumpkind(cfg_node) ])
    #for dest_node in cfg_node.successors:
    for dest_node, jumpkind in cfg.model.get_successors_and_jumpkind(cfg_node):
        if dest_node.addr == loop.entry.addr: # continue_edge
            continue_nodes.append(dest_node)
        else: # no continue_edge
            is_break_node = False
            for (src, dst) in loop.break_edges:
                if dest_node.addr == dst.addr:
                    is_break_node = True
                    break
            if is_break_node:
                break_nodes.append(dest_node)
            else: # internal node or return node
                #print(dest_node, jumpkind)
                # discard continue_edges to other loops
                if not isBackEdge(dest_node, cfg_node):
                    # discard returns from body_nodes
                    #if not (cfg_node.to_codenode() in loop.body_nodes and jumpkind == "Ijk_Ret"):
                    if not (cfg_node.to_codenode() in loop.body_nodes and jumpkind == "Ijk_Ret"):
                        #print("-", dest_node, jumpkind)
                        internal_nodes.append(dest_node)
                    else: # return from body_nodes
                        break_nodes.append(dest_node)
                else:
                    continue_to_other_loops.append(dest_node)
    #if len(cfg.model.get_successors_and_jumpkind(cfg_node)) != (len(internal_nodes) + len(break_nodes) + len(continue_nodes)):
    if len(cfg.model.get_successors_and_jumpkind(cfg_node)) != (len(internal_nodes) + len(break_nodes) + len(continue_nodes) + len(continue_to_other_loops)):
        print("classify_successors: error: node lost")
        sys.exit(-1)
    return (internal_nodes, break_nodes, continue_nodes)


def get_successors_in_list(cfg_node, addr_list, node_list):
    for node in cfg_node.successors:
        if node.addr in addr_list and node not in node_list:
            node_list += get_successors_in_list(node, addr_list, node_list + [node])
    return list(set(node_list))


########################### WRITE ILP MODEL ###########################

def write_lockms_model(filename, main_node):
    """ Writes the Lock-MS ILP model """
    global lp_file
    lp_file = open("%s_%s_%s_%d_%d.lp" % (filename, INSTRUCTION_CACHE_TYPE, DATA_CACHE_TYPE, DATA_CACHE_SETS, DATA_CACHE_WAYS), 'w')
    # Header of LP file
    lp_file.write("\n/* Objective function */\n\n")
    generate_init_constraints(lp_file)
    # Body of LP file
    lp_file.write("\n/* Structural (tree) constraints */\n\n")
    generate_structural_constraints(lp_file)
    if DATA_CACHE_TYPE == "acdc" and not DISABLE_ACDC_PENALTIES:
        lp_file.write("\n/* ACDC: Loop penalty constraints */\n\n")
        generate_acdc_loop_penalty_constraints(lp_file)
#    times = {}
#    generate_altpath_constraint(lp_file, main_node, explore(main_node, [], 1))
#    lp_file.write("\n/* Ending nodes */\n\n")
#    for cfg_node in ending_nodes:
#        lp_file.write(var_name("C", cfg_node) + " = " + var_name("B", cfg_node) + ";\n")
    lp_file.write("\n/* Basic block constraints (Fetch/Execution/Memory) */\n\n")
    generate_basicblock_constraints(lp_file)
    lp_file.write("\n/* Lock-MS: Cached binary variables */\n\n")
    generate_cached_constraints(lp_file)
    if DATA_CACHE_TYPE == "acdc":
        lp_file.write("\n/* ACDC: DRP binary variables */\n\n")
        generate_drp_constraints(lp_file)
        #generate_multipoint_drp_constraints(lp_file)
    # Tail of LP file
    lp_file.write("\n/* Cached/locked lines binary variables */\n\n")
    generate_variable_type_constraints(lp_file)
    lp_file.close()


def var_name(pre, cfg_node):
    """ Generates a name for node: pre+cfg_node """
    return pre + str(cfg_nodes[cfg_node]).zfill(3) + "_" + str(hex(cfg_node.addr))


def generate_init_constraints(lp_file):
    """ Write objective for ILP """
    if INSTRUCTION_CACHE_TYPE == "locked":
        if DATA_CACHE_TYPE == "acdc":
            lp_file.write("min: IC_Load_Cost + ACDC_Load_Cost + WCET;\n")
        elif DATA_CACHE_TYPE in ["lru", "ah", "am"]:
            lp_file.write("min: IC_Load_Cost + WCET;\n")
        else:
            print("generate_init_constraint ERROR: unknown DATA_CACHE_TYPE")
            sys.exit(-1)
    else:
        print("generate_init_constraint ERROR: unknown INSTRUCTION_CACHE_TYPE")
        sys.exit(-1)


def generate_acdc_loop_penalty_constraints(lp_file):
    """
    For each loop, print its penalty as the maximum of alternative
    path penalties.
    """
    for loop in loop_info.loops:
        loop_addr_set = set(getLoopNodeSet(loop))
        for loop_node in cfg.model.get_all_nodes(loop.entry.addr):
            #explore_loop_paths(loop_node, loop_addr_set, loop_node, 0)
            set_alternative_path_penalties(loop_addr_set, loop_node, [])

#def explore_loop_paths(loop_node, loop_addr_set, cfg_node, path_penalty):
#    """ NOT USED """
#    path_penalty += node_penalty[cfg_node]
#    successors = no_back_edge_successors(cfg_node)
#    if len(successors) == 0:
#        lp_file.write("%s >= %d;\n" % (var_name("LP", loop_node), path_penalty*(DATA_MISS_COST-DATA_HIT_COST)))
#    else:
#        for child_node in successors:
#            if child_node.addr in loop_addr_set:
#                explore_loop_paths(loop_node, loop_addr_set, child_node, path_penalty)
#            else:
#                lp_file.write("%s >= %d;\n" % (var_name("LP", loop_node), path_penalty*(DATA_MISS_COST-DATA_HIT_COST)))

def set_alternative_path_penalties(loop_addr_set, cfg_node, path):
    successors = no_back_edge_successors(cfg_node)
    if len(successors) == 0:
        return path + [cfg_node]
    elif len(successors) == 1:
        return set_alternative_path_penalties(loop_addr_set, successors[0], path + [cfg_node])
    else:
        all_nodes = set({})
        replicated_nodes = set({})
        for child_node in successors:
            if child_node.addr in loop_addr_set:
                alternative_path = set_alternative_path_penalties(loop_addr_set, child_node, path + [cfg_node])
                if alternative_path:
                    for node in alternative_path:
                        if node in all_nodes:
                            # WARNING: overestimation if more than 2 paths
                            replicated_nodes.add(node)
                        else:
                            all_nodes.add(node)
        # Write penalty for alternative paths
        lp_file.write("%s >= 0" % var_name("AP", cfg_node))
        for node in list(set(all_nodes) - set(replicated_nodes)):
            lp_file.write(calc_acdc_node_penalty(node))
        lp_file.write(";\n")
        return list(replicated_nodes)
        #return all_nodes


def generate_variable_type_constraints(lp_file):
    """ Writes types of variables """
    if INSTRUCTION_CACHE_TYPE == "locked":
        lp_file.write("bin ")
        # lines_list calculated in generate_cached_constraints
        for cache_line in lines_list[:-1]:
            lp_file.write("cached_0x%x, " % cache_line)
        lp_file.write("cached_0x%x;\n" % lines_list[-1])
    if DATA_CACHE_TYPE == "acdc":
        lp_file.write("bin ")
        # drps calculated in generate_drp_constraints
        for addr in drps[:-1]:
            lp_file.write("DRP_0x%x, " % addr)
        lp_file.write("DRP_0x%x;\n" % drps[-1])


def generate_structural_constraints(lp_file):
    """ Generates structural (tree) constraints """
    """ DOI: 10.1371/journal.pone.0229980 """
    global times, node_penalty

    lp_file.write("WCET = %s;\n" % var_name("C", main_node))
    times = {} # filled in explore()
    node_penalty = {} # filled in explore()
    generate_altpath_constraint(lp_file, main_node, explore(main_node, [], 1), is_loop=False)
    # complete dead code
    for node in cfg.graph.nodes():
        if node not in times:
            times[node] = 0
    lp_file.write("/* Ending nodes */\n")
    for cfg_node in ending_nodes:
        lp_file.write(var_name("C", cfg_node) + " = " + var_name("B", cfg_node) + ";\n")


def generate_cached_constraints(lp_file):
    """ Write cache/line constraints """
    """ DOI: 10.1016/j.sysarc.2010.08.008 """
    global lines_list

    # explore all basic blocks and put their instruction cache line
    # addresses into lines_list
    lines_list = []
    for node in cfg.graph.nodes():
        if cache_line_addr(node.addr) not in lines_list:
            lines_list.append(cache_line_addr(node.addr))
        addr = int(cache_line_addr(node.addr) + CACHE_LINE_SIZE)
        if node.block:
            last_addr = node.addr + INSTRUCTION_SIZE * node.block.instructions
            while addr <= last_addr:
                if addr not in lines_list:
                    lines_list.append(addr)
                #addr += int(CACHE_LINE_SIZE / INSTRUCTION_SIZE)
                addr += CACHE_LINE_SIZE
        # else: node from unresolved jump target, without instructions

    for num_set in range(INSTRUCTION_CACHE_SETS):
        lines_found = False
        for cache_line in lines_list:
            if (cache_line/CACHE_LINE_SIZE) % INSTRUCTION_CACHE_SETS == num_set:
                if lines_found:
                    #lp_file.write(" + cached_" + str(hex(cache_line)))
                    lp_file.write(" + cached_0x%x" % cache_line)
                else:
                    lines_found = True
                    lp_file.write("cached_0x%x" % cache_line)
        if lines_found:
            #lp_file.write(" <= " + str(INSTRUCTION_CACHE_WAYS) + "; /* set " + str(num_set) + " */\n")
            lp_file.write(" <= %d; /* set %d */\n" % (INSTRUCTION_CACHE_WAYS, num_set))

    lp_file.write("Num_Cached_Lines = ")
    for cache_line in lines_list[:-1]:
        lp_file.write("cached_0x%x + " % cache_line)
    lp_file.write("cached_0x%x;\n" % lines_list[-1])

    lp_file.write("IC_Load_Cost = %d Num_Cached_Lines + %d;\n" % (FETCH_MISS_COST-FETCH_HIT_COST, IC_PRELOAD_CALL_COST))

def generate_multipoint_drp_constraints(lp_file):
    """
    Generate MULTIPLE LOADING POINTS for the DRP architectural constraints
    """
    global drps

    global_drp_string = ""
    local_drp_strings = {}

    drps = []
    # classify
    for ref in reference:
        if reference[ref].first_group_reuse:
            pc = reference[reference[ref].first_group_reuse].pc
        else:
            pc = reference[ref].pc
        if pc not in drps:
            loop_addr = get_most_external_loop_header_from_addr(pc)
            drps.append(pc)
            if loop_addr == 0:
                global_drp_string += " + DRP_0x%x" % pc
            elif loop_addr in local_drp_strings:
                local_drp_strings[loop_addr] += " + DRP_0x%x" % pc
            else:
                local_drp_strings[loop_addr] = " DRP_0x%x" % pc
    # print global
    lp_file.write("Global_DRPs =%s;\n" % global_drp_string)
    # print each local
    for loop_addr in local_drp_strings:
        lp_file.write("Loop0x%x_DRPs =%s;\n" % (loop_addr, local_drp_strings[loop_addr]))
    # summ all
    lp_file.write("Used_DRPs = Global_DRPs")
    for loop_addr in local_drp_strings:
        lp_file.write(" + Loop0x%x_DRPs" % loop_addr)
    lp_file.write(";\n")
    # limit each local
    # limit each local
    for loop_addr in local_drp_strings:
        lp_file.write("Loop0x%x_DRPs <= Loop_DRPs;\n" % loop_addr)
    # limit global + local
    lp_file.write("Global_DRPs + Loop_DRPs <= %d;\n" % ACDC_DC_LINES)
    lp_file.write("Global_DRPs + Loop_DRPs <= %d;\n" % ACDC_DC_LINES)
    lp_file.write("ACDC_Load_Cost = %d Used_DRPs + %d; /* ACDC_PRELOAD_CALL*(loops+1) */\n" % (MEMORY_COST, ACDC_PRELOAD_CALL_COST*(len(local_drp_strings)+1)))

def get_all_nodes_containing_addr(addr):
    """
    Returns a list with all nodes containing the instruction address addr
    """
    node_list = cfg.model.get_all_nodes(addr)
    if len(node_list) == 0:
        for node in cfg.model.nodes():
            if addr in node.instruction_addrs:
                node_list.append(node)
    return node_list


def get_most_external_loop_header_from_addr(addr):
    """
    Gets the most external loop header address.
    If addr is outside loops, returns 0.
    If addr is inside different external loops (function called in them), return 0
    Required for multiple locking points based on external loops.
    """
    global_external_loop_addr = None
    for node in get_all_nodes_containing_addr(addr):
        external_loop_addr = get_most_external_loop_header_from_node(node)
        if global_external_loop_addr is None:
            global_external_loop_addr = external_loop_addr
        elif global_external_loop_addr != external_loop_addr:
            # multiple parent loops: function called from loops
            return 0
    return global_external_loop_addr

def get_most_external_loop_header_from_node(node):
    """
    Gets the most external loop header address.
    If node is outside loops, returns 0.
    If node is inside different external loops (function called in them), return 0
    Required for multiple locking points based on external loops.
    """
    if node in enclosing_loop_header:
    #if times[node] > 1:
        # node in loop
        global_parent_loop_addr = None
        loop_header = enclosing_loop_header[node]
        if loop_header is None:
            return 0
        else:
            for parent in loop_header.predecessors:
                if not isBackEdge(loop_header, parent):
                    parent_loop_addr = get_most_external_loop_header_from_node(parent)
                    if parent_loop_addr == 0: # already at the most external loop
                        parent_loop_addr = loop_header.addr
                    if global_parent_loop_addr is None:
                        global_parent_loop_addr = parent_loop_addr
                    elif global_parent_loop_addr != parent_loop_addr:
                        # multiple parent loops: function called from loops
                        return 0
            return global_parent_loop_addr
    else:
        return 0


def generate_drp_constraints(lp_file):
    """ Generate DRP architectural constraints """
    """ DOI: 10.1109/RTAS.2012.11 """
    """ DOI: 10.1145/2677093 """
    """
        Assumes AC_LINES == DC_LINES
    """
    global drps

    lp_file.write("Used_DRPs =")
    drps = []
    for ref in reference:
        if reference[ref].first_group_reuse:
            if reference[reference[ref].first_group_reuse].pc not in drps:
                lp_file.write(" + DRP_0x%x" % reference[reference[ref].first_group_reuse].pc)
                drps.append(reference[reference[ref].first_group_reuse].pc)
        else:
            #node = get_cfgid_node(reference[ref].cfg_node_id)
            #if reference[ref].pc not in drps and times[node] > 1:
            if reference[ref].pc not in drps:
                lp_file.write(" + DRP_0x%x" % reference[ref].pc)
                drps.append(reference[ref].pc)
    lp_file.write(";\n")
    lp_file.write("Used_DRPs <= %d;\n" % ACDC_DC_LINES)
    lp_file.write("ACDC_Load_Cost = %d Used_DRPs;\n" % (MEMORY_COST))


def generate_basicblock_constraints(lp_file):
    """ Write basicblock constraints """

    for node in cfg.graph.nodes():

        try:
            #assert(node.block.instructions)
            node.block.instructions

            ######### Block cost = 1
            #lp_file.write(var_name("B", node) + " = 1;\n")

            ######### Block cost = number of instructions
            #lp_file.write(var_name("B", node) + " = " + str(node.block.instructions) + ";\n")

            ######### Block cost = Fetch + Exec + Memory
            lp_file.write("%s = %s + %s + %s;\n" % (var_name("B", node), var_name("F", node), var_name("E", node), var_name("M", node)))

            # Fetch: Lockable instruction cache with Line Buffer
            if INSTRUCTION_CACHE_TYPE == "locked":
                generate_fetch_constraints(lp_file, node)
            else:
                print("generate_basicblock_constraint ERROR: unknown INSTRUCTION_CACHE_TYPE")
                sys.exit(-1)

            # Execution: 1 cycle per instruction
            generate_execution_constraints(lp_file, node)

            # Data memory access
            if DATA_CACHE_TYPE == "acdc":
                # UNSAFE
                generate_acdc_constraints(lp_file, node)
#            elif DATA_CACHE_TYPE == "lru":
#                # UNSAFE
#                generate_lru_data_constraints(lp_file, node)
            elif DATA_CACHE_TYPE == "ah":
                generate_ah_data_constraints(lp_file, node)
            elif DATA_CACHE_TYPE == "am":
                generate_am_data_constraints(lp_file, node)
            else:
                print("generate_basicblock_constraint ERROR: unknown DATA_CACHE_TYPE")
                sys.exit(-1)
        except AttributeError as error:
        #except AssertionError as error:
            # Node from unresolvable jump target
            # If this occurs, .lp model may be infeasible
            print(error)
            print("CFG node without instructions")
            lp_file.write("%s = 0; /* Unresolvable jump target */" % (var_name("B", node)))


def calc_acdc_node_penalty(node):
    """
    In loops with alternative paths inside, the wost case in
    the ACDC may arise when the first one or two iterations go
    through an alternative path. No more iterations need to be
    considered, since two consecutive misses is the worst case
    for a self-spatial array traversal with DRP.
    If no iterations or single iterations, no penalty.

    Returns:
        the number of misses in the worst case in the first one
        or two iterations of the deepest loop.
    """
    """
    Overapproximation: no reference has DRP and misses
    TODO: estimate misses depending on DRP values
    """
    string = ""
    if times[node] < 2:
        return "0"
    if times[node] == 2:
        return "+" + str(len(get_refs_in_node(node))*(DATA_MISS_COST-DATA_HIT_COST))
    elif times[node] > 2:
        for ref_id in get_refs_in_node(node):
            first_gr = reference[ref_id].first_group_reuse
            if first_gr is not None:
                # group reuse without DRP: M M
                # group reuse with DRP: H H
                string += "+%d-%d DRP_0x%x" % (2*(DATA_MISS_COST-DATA_HIT_COST), 2*(DATA_MISS_COST-DATA_HIT_COST), reference[first_gr].pc)
            else:
                if is_constant(reference[ref_id]) or is_precise_array(reference[ref_id]):
                    # self reuse: M H
                    string += "+%d-%d DRP_0x%x" % (2*(DATA_MISS_COST-DATA_HIT_COST), (DATA_MISS_COST-DATA_HIT_COST), reference[first_gr].pc)
                else:
                    # imprecise array self reuse: M M
                    # nonlinear: M M
                    string += ("+%d" % (2*(DATA_MISS_COST-DATA_HIT_COST)))
        return string
        #return "+" + str(2 * len(get_refs_in_node(node))*(DATA_MISS_COST-DATA_HIT_COST))



def generate_fetch_constraints(lp_file, node):
    """ Generates fetch constraints for node in lp_file """
    """ DOI: 10.1016/j.sysarc.2010.08.008 """
    """
        Generates constraints for a lockable instruction cache
        and an instruction line buffer
    """

    # Lockable instruction cache with Line Buffer
    if node.addr % CACHE_LINE_SIZE:
        instruction_lines = node.block.instructions * INSTRUCTION_SIZE / CACHE_LINE_SIZE + 1
    else:
        instruction_lines = node.block.instructions * INSTRUCTION_SIZE / CACHE_LINE_SIZE
    line_addr = cache_line_addr(node.addr)
    instr_in_first_line = CACHE_LINE_SIZE - (node.addr % CACHE_LINE_SIZE) % INSTRUCTION_SIZE
    if instr_in_first_line > node.block.instructions:
        instr_in_first_line = node.block.instructions
    lp_file.write("%s = %d - %d cached_0x%x" % (var_name("F", node), times[node]*(instr_in_first_line*FETCH_MISS_COST), times[node]*(instr_in_first_line*(FETCH_MISS_COST-FETCH_HIT_COST)), line_addr))
    # remaining lines
    remaining_instr = node.block.instructions - instr_in_first_line
    line_addr += CACHE_LINE_SIZE
    while remaining_instr:
        if remaining_instr > (CACHE_LINE_SIZE / INSTRUCTION_SIZE):
            lp_file.write(" + %d - %d cached_0x%x" % (times[node]*((CACHE_LINE_SIZE/INSTRUCTION_SIZE)*FETCH_MISS_COST), times[node]*((CACHE_LINE_SIZE/INSTRUCTION_SIZE)*(FETCH_MISS_COST-FETCH_HIT_COST)), line_addr))
            remaining_instr -= (CACHE_LINE_SIZE / INSTRUCTION_SIZE)
            line_addr += CACHE_LINE_SIZE
        else:
            lp_file.write(" + %d - %d cached_0x%x" % (times[node]*(remaining_instr*FETCH_MISS_COST), times[node]*(remaining_instr*(FETCH_MISS_COST-FETCH_HIT_COST)), line_addr))
            remaining_instr = 0
    lp_file.write(";\n")


def generate_execution_constraints(lp_file, node):
    """ Generates execution constraints for node in lp_file """
    """
        Fixed cost: 1 cycle per instruction
    """

    lp_file.write("%s = %d; /* %d times %d instructions */\n" % (var_name("E", node), times[node]*node.block.instructions, times[node], node.block.instructions))


def get_refs_in_node(node):
    """
    Returns the list of memory references (from XML) in the specified node.
    For XML generated by CFGEmulated (virtual instances), node ids should coincide.
    For XML generated by CFGFast (no virtual instances), node address is used.
    """
    if xmlparse.get_cfg_type() in ('emulated', None):
        # XML with CFGEmulated
        refs_in_node = xmlparse.get_references_by_node()
        if set_node_id(node) in refs_in_node:
            return refs_in_node[set_node_id(node)]
        else:
            return []
    elif xmlparse.get_cfg_type() == 'fast':
        # XML with CFGFast
        refs_by_addr = xmlparse.get_references_by_addr()
        refs_in_codeblock = []
        for addr in node.block.instruction_addrs:
            if addr in refs_by_addr:
                refs_in_codeblock += refs_by_addr[addr]
        return refs_in_codeblock
    else:
        print("Unknown CFG type in XML:", xmlparse.get_cfg_type())
        sys.exit(-1)


def generate_ah_data_constraints(lp_file, node):
    """ Generates data access constraints for node in lp_file """
    """
        AlwaysHit
    """
    refs = get_refs_in_node(node)
    if refs:
        #lp_file.write("%s = %d;\n" % (var_name("M", node), DATA_HIT_COST*times[node]*len(refs_in_node[set_node_id(node)])))
        lp_file.write("%s = %d;\n" % (var_name("M", node), DATA_HIT_COST*times[node]*len(refs)))
    else:
        lp_file.write("%s = 0;\n" % (var_name("M", node)))


def generate_am_data_constraints(lp_file, node):
    """ Generates data access constraints for node in lp_file """
    """
        AlwaysMiss/DirectMemory (no cache lantency and no copybacks)
    """
    refs = get_refs_in_node(node)
    if refs:
        lp_file.write("%s = %d;\n" % (var_name("M", node), MEMORY_COST*times[node]*len(refs)))
    else:
        lp_file.write("%s = 0;\n" % (var_name("M", node)))


def calc_distance_and_reused_node(node, ref_id, last_gr):
    """
    Calculates the reuse distance and the reused node
    """
    distance = None
    last_gr_node = None
    for reused_node in get_all_cfg_nodes(reference[last_gr].pc):
        this_distance = max_conflicts_between(reused_node, last_gr, node, ref_id, ref_id, DATA_CACHE_WAYS)
        if distance is None:
            distance = this_distance
            last_gr_node = reused_node
        elif this_distance < distance:
            distance = this_distance
            last_gr_node = reused_node
    return (distance, last_gr_node)


def generate_lru_data_constraints(lp_file, node):
    """
    Generates data access constraints for node in lp_file.
    LRU hit/miss costs based on reuse distance.
    """
    total_mem_cost = 0
    enclosing_loop_addr = get_loop_addr(node)
    if enclosing_loop_addr: # loop
        loop_addr_list = list(set(getLoopNodeSet(loop_headers[cfg.model.get_any_node(enclosing_loop_addr)])))
        node_list = get_successors_in_list(node, loop_addr_list, [node])
    refs = get_refs_in_node(node)
    for ref_id in refs: # for each memory reference in the basic block
        misses = 0
        copybacks = 0
        # print load/store
        if reference[ref_id].is_load:
            lp_file.write("/* 0x%x ld: " % reference[ref_id].pc)
        else:
            lp_file.write("/* 0x%x st: " % reference[ref_id].pc)
        # calculate reuse distance and last group-reuse node
        last_gr = reference[ref_id].last_group_reuse

        #if enclosing_loop_addr and last_gr:
        if enclosing_loop_addr:
            if last_gr:
                ######## GROUP-REUSE in LOOP
                (distance, last_gr_node) = calc_distance_and_reused_node(node, ref_id, last_gr)
                if is_precise_array(reference[ref_id]) or is_imprecise_array(reference[ref_id]):
                    if is_precise_array(reference[ref_id]):
                        lp_file.write("precise array self+group-reuse, ")
                        offset = reference[ref_id].c
                    else: # imprecise array
                        lp_file.write("imprecise array self+group-reuse, ")
                        offset = None
                    misses = calculate_misses(node, reference[ref_id].term, offset)
                    if last_gr_node.addr in loop_addr_list:
                        # group reuse in loop
                        if distance > DATA_CACHE_WAYS: # always-miss
                            lp_file.write("dist >=%d in loop" % (distance))
                            misses = times[node]
                        else: # always hit
                            lp_file.write("dist %d in loop" % (distance))
                            misses = 0
                        if not reference[ref_id].is_load and all_loads_lru(last_gr):
                            copybacks = times[node]
                    else:
                        # group reuse outside loop
                        distance_before_loop = distance
                        # test self-reuse
                        distance_in_loop = max_conflicts_in_loop(node, loop_addr_list, ref_id, DATA_CACHE_WAYS)
                        if distance_in_loop > DATA_CACHE_WAYS:
                            if distance_before_loop > DATA_CACHE_WAYS: # always-miss
                                lp_file.write("dist >=%d in/out" % (DATA_CACHE_WAYS))
                                misses = times[node]
                                if not reference[ref_id].is_load:
                                    copybacks = times[node]
                            else: # first-hit
                                lp_file.write("dist >=%d in, %d before loop" % (distance_in_loop, distance_before_loop))
                                misses = times[node] - 1
                                if not reference[ref_id].is_load:
                                    if all_loads_lru(last_gr):
                                        copybacks = times[node]
                                    else:
                                        copybacks = times[node] - 1
                        else:
                            if distance_before_loop > DATA_CACHE_WAYS: # (first-miss +) ratio
                                lp_file.write("dist %d in, >=%d before loop" % (distance_in_loop, distance_before_loop))
                                if not reference[ref_id].is_load:
                                    copybacks = misses
                            else: # first-hit + ratio
                                lp_file.write("dist %d in, %d before loop" % (distance_in_loop, distance_before_loop))
                                misses -= 1
                                if not reference[ref_id].is_load:
                                    if all_loads_lru(last_gr):
                                        copybacks = times[node]
                                    else:
                                        copybacks = misses
                elif is_constant(reference[ref_id]):
                    lp_file.write("scalar self+group-reuse, ")
                    if last_gr_node.addr in loop_addr_list:
                        # group reuse in loop
                        if distance > DATA_CACHE_WAYS: # always-miss
                            lp_file.write("dist >=%d in loop" % (distance))
                            misses = times[node]
                        else: # always-hit
                            lp_file.write("dist %d in loop" % (distance))
                            misses = 0
                        if not reference[ref_id].is_load:
                            #copybacks = times[node]
                            if all_loads_lru(last_gr):
                                copybacks = max(misses, 1)
                            else:
                                copybacks = misses
                    else:
                        #print("Reusing outside the loop")
                        distance_before_loop = distance
                        # test self-reuse
                        distance_in_loop = max_conflicts_in_loop(node, loop_addr_list, ref_id, DATA_CACHE_WAYS)
                        #print("Distance in loop:", distance_in_loop)
                        if distance_in_loop > DATA_CACHE_WAYS:
                            if distance_before_loop > DATA_CACHE_WAYS: # always-miss
                                lp_file.write("dist >=%d in/out loop" % (DATA_CACHE_WAYS))
                                misses = times[node]
                                if not reference[ref_id].is_load:
                                    copybacks = misses
                            else: # first-hit
                                lp_file.write("dist >=%d in, %d out loop" % (distance_in_loop, distance_before_loop))
                                misses = times[node]-1
                                if not reference[ref_id].is_load:
                                    if all_loads_lru(last_gr):
                                        copybacks = times[node]
                                    else:
                                        copybacks = misses
                        else:
                            if distance_before_loop > DATA_CACHE_WAYS: #first-miss
                                lp_file.write("dist %d in, >=%d out loop" % (distance_in_loop, distance_before_loop))
                                misses = 1
                                if not reference[ref_id].is_load:
                                    copybacks = times[node]
                            else: # always-hit
                                lp_file.write("dist %d in, %d out loop" % (distance_in_loop, distance_before_loop))
                                misses = 0
                                if not reference[ref_id].is_load:
                                    if all_loads_lru(last_gr):
                                        copybacks = times[node]
                                    else:
                                        copybacks = times[node] - 1
                elif is_nonlinear(reference[ref_id]):
                    lp_file.write("non-linear group-reuse, ")
                    if last_gr_node.addr in loop_addr_list:
                        if distance > DATA_CACHE_WAYS: # always-miss
                            lp_file.write("dist >=%d in loop" % (distance))
                            misses = times[node]
                            if not reference[ref_id].is_load:
                                copybacks = misses
                        else: # always-hit
                            lp_file.write("dist %d in loop" % (distance))
                            misses = 0
                            if not reference[ref_id].is_load and all_loads_lru(last_gr):
                                copybacks = times[node]
                    else:
                        #print("Reusing outside the loop")
                        if distance > DATA_CACHE_WAYS: # always-miss
                            lp_file.write("dist %d before loop" % (distance))
                            misses = times[node]
                            if not reference[ref_id].is_load:
                                copybacks = misses
                        else: # first-hit
                            lp_file.write("dist %d before loop" % (distance))
                            misses = times[node] - 1
                            if not reference[ref_id].is_load and all_loads_lru(last_gr):
                                copybacks = times[node]
                else:
                    print("ERROR: unknown reuse type")
                    sys.exit(-1)

            #elif enclosing_loop_addr and last_gr is None:
            else:
                # NO GROUP-REUSE in loop
                if is_precise_array(reference[ref_id]) or is_imprecise_array(reference[ref_id]):
                    # get distance and sequential misses
                    distance = max_conflicts_in_loop(node, loop_addr_list, ref_id, DATA_CACHE_WAYS)
                    if is_precise_array(reference[ref_id]):
                        lp_file.write("precise array self-reuse, ")
                        misses = calculate_misses(node, reference[ref_id].term, reference[ref_id].c)
                    else: # imprecise array
                        lp_file.write("imprecise array self-reuse, ")
                        misses = calculate_misses(node, reference[ref_id].term, None)
                    # get stride
                    stride = 0
                    for (i, h) in reference[ref_id].term:
                        if i == enclosing_loop_addr:
                            stride = h
                            break
                    # if stride==0, we have temporal (not spatial) reuse
                    lp_file.write("stride %d, " % (stride))
                    if stride == 0 or int(CACHE_LINE_SIZE / stride) > 1:
                        # short self spatial reuse
                        if distance > DATA_CACHE_WAYS:
                            lp_file.write("dist >=%d, dim %d" % (distance, len(reference[ref_id].term)))
                            misses = times[node]
                            if not reference[ref_id].is_load:
                                copybacks = misses
                        else:
                            lp_file.write("dist %d" % (distance))
                            if not reference[ref_id].is_load:
                                copybacks = misses
                    else:
                        # long self spatial reuse
                        if distance > DATA_CACHE_WAYS:
                            lp_file.write("dist >=%d, stride %d dim %d" % (distance, stride, len(reference[ref_id].term)))
                            misses = times[node]
                            if not reference[ref_id].is_load:
                                copybacks = misses
                        else:
                            lp_file.write("dist %d, stride %d dim %d" % (distance, stride, len(reference[ref_id].term)))
                            misses = times[node]
                            if not reference[ref_id].is_load:
                                copybacks = misses
                elif is_constant(reference[ref_id]):
                    lp_file.write("scalar self-reuse, ")
                    distance = max_conflicts_in_loop(node, loop_addr_list, ref_id, DATA_CACHE_WAYS)
                    if distance > DATA_CACHE_WAYS: # always-miss
                        lp_file.write("dist >=%d" % (distance))
                        misses = times[node]
                        if not reference[ref_id].is_load:
                            copybacks = misses
                    else: # first-miss
                        lp_file.write("dist %d" % (distance))
                        misses = 1
                        if not reference[ref_id].is_load:
                            copybacks = misses
                elif is_nonlinear(reference[ref_id]):
                    lp_file.write("non-linear in loop, ")
                    misses = times[node]
                    if not reference[ref_id].is_load:
                        copybacks = misses
                else:
                    print("ERROR: unknown reuse type")
                    sys.exit(-1)

        else: # outside loop
            if last_gr:
                # GROUP-REUSE outside loop
                (distance, last_gr_node) = calc_distance_and_reused_node(node, ref_id, last_gr)
                if is_constant(reference[ref_id]):
                    lp_file.write("scalar group-reuse, ")
                elif is_nonlinear(reference[ref_id]):
                    lp_file.write("non-linear group-reuse, ")
                else:
                    print("ERROR: unknown reuse type at 0x%x" % reference[ref_id].pc)
                    sys.exit(-1)
                if distance > DATA_CACHE_WAYS: # no locality
                    lp_file.write("dist >=%d" % (distance))
                    misses = times[node] # = 0 or 1
                    if not reference[ref_id].is_load:
                        copybacks = misses
                else: # locality
                    lp_file.write("dist %d" % (distance))
                    misses = 0
                    if not reference[ref_id].is_load and all_loads_lru(last_gr):
                        copybacks = times[node] # = 0 or 1
            else:
                # NO GROUP-REUSE outside loop (scalar first-use, non-linear): MISS
                if is_constant(reference[ref_id]):
                    lp_file.write("scalar first-use")
                else:
                    lp_file.write("non-linear first-use")
                misses = times[node]
                if not reference[ref_id].is_load:
                    copybacks = times[node]

        ######################################
        if misses < 0:
            misses = 0
        if copybacks < 0:
            copybacks = 0
        hits = times[node] - misses
        lp_file.write(": %dH + %dM + %dCB */\n" % (hits, misses, copybacks))
        total_mem_cost += misses*DATA_MISS_COST + hits*DATA_HIT_COST + copybacks*COPYBACK_COST
        ######################################

    lp_file.write("%s = %d; /* %d times, %d refs */\n" % (var_name("M", node), total_mem_cost, times[node], len(refs)))
    lp_file.flush()
    # os.fsync(lp_file)


def generate_acdc_constraints(lp_file, node):
    """ Generates data access constraints for node in lp_file """
    """ DOI: 10.1109/RTAS.2012.11 """
    """ DOI: 10.1145/2677093 """
    """
        ACDC hit/miss costs based on reuse distance
    """

    # M = DCHITCOST*nDChit + DCMISSCOST*nDCmiss + DCMISSCOST*DCWriteBacks
    # nDChit = times - nDCmiss
    # nDCmiss = ACDCmisses*DRP + times*(1-DRP)
    # DCWriteBacks = EITHER 0 OR nDCmiss*DRP
    #
    # M = DCHITCOST*(times-nDCmiss) + DCMISSCOST*nDCmiss + DCWriteBacks
    # M = DCHITCOST*times + (DCMISSCOST-DCHITCOST)*nDCmiss +DCWriteBacks
    # M = DCHITCOST*times + (DCMISSCOST-DCHITCOST)*ACDCmisses*DRP
    #                     + (DCMISSCOST-DCHITCOST)*times*(1-DRP)
    #                     + DCMISSCOST*DCWriteBacks
    # M = DCHITCOST*times + (DCMISSCOST-DCHITCOST)*ACDCmisses*DRP
    #                     + (DCMISSCOST-DCHITCOST)*times
    #                     - (DCMISSCOST-DCHITCOST)*times*DRP)
    #                     + DCMISSCOST*DCWriteBacks
    # Without WriteBacks:
    # M = DCMISSCOST*times + (DCMISSCOST-DCHITCOST)*ACDCmisses*DRP
    #                     - (DCMISSCOST-DCHITCOST)*times*DRP
    # M=DCMISSCOST*times + (DCMISSCOST-DCHITCOST)*(ACDCmisses-times)*DRP
    # With Writebacks:
    # M=DCMISSCOST*times + (DCMISSCOST-DCHITCOST)*(ACDCmisses-times)*DRP
    #                    + DCMISSCOST*(ACDCmisses*DRP + times*(1-DRP))*DRP
    # M=DCMISSCOST*times + (DCMISSCOST-DCHITCOST)*(ACDCmisses-times)*DRP
    #                    + DCMISSCOST*ACDCmisses*DRP
    # M = DCMISSCOST*times
    #   + ((DCMISSCOST-DCHITCOST)*(ACDCmisses-times)+DCMISSCOST*ACDCmisses)*DRP


    #                     + ACDCmisses*DRP + times*(1-DRP)
    # M = (DCMISSCOST+1)*times
    #   + (DCMISSCOST-DCHITCOST+1)*(ACDCmisses-times)*DRP

    # Memory constraints
    refs = get_refs_in_node(node)
    if refs: # has loads/stores
        #lp_file.write("%s = " % var_name("M", node))
        lp_line = "%s =" % var_name("M", node)
        for ref_id in refs:
            if reference[ref_id].is_load:
                lp_file.write("/* 0x%x ld: " % reference[ref_id].pc)
            else:
                lp_file.write("/* 0x%x st: " % reference[ref_id].pc)
            first_gr = reference[ref_id].first_group_reuse
            if first_gr is not None:
                # GROUP-REUSE
                acdc_misses = 0
                if is_precise_array(reference[ref_id]):
                    #print("/* 0x%x: precise array group-reuse */" % reference[ref_id].pc)
                    lp_file.write("precise array group-reuse: ")
                elif is_imprecise_array(reference[ref_id]):
                    #print("0x%x: imprecise array group-reuse" % reference[ref_id].pc)
                    lp_file.write("imprecise array group-reuse: ")
                elif is_constant(reference[ref_id]):
                    #print("0x%x: scalar group-reuse" % reference[ref_id].pc)
                    if reference[first_gr].is_multiline:
                        lp_file.write("MULTILINE ")
#                    if reference[ref_id].max_number:
#                        if reference[ref_id].is_multiline:
#                            lp_file.write("%d/%d MULTILINE " % (reference[ref_id].number+1, reference[ref_id].max_number+1))
#                        else:
#                            lp_file.write("%d/%d single line " % (reference[ref_id].number+1, reference[ref_id].max_number+1))
                    lp_file.write("scalar group-reuse: ")
                elif is_nonlinear(reference[ref_id]):
                    #print("0x%x: non-linear group-reuse" % reference[ref_id].pc)
                    lp_file.write("non-linear group-reuse: ")
                else:
                    print("ERROR: unknown reuse type")
                    sys.exit(-1)
                #if is_constant(reference[ref_id]) or all_loads(first_gr):
                #if is_constant(reference[ref_id]) or (reference[ref_id].is_load and all_loads(ref_id)):
                #if is_constant(reference[ref_id]) or (reference[ref_id].is_load or not all_loads(ref_id)):
                if is_constant(reference[ref_id]) or all_loads(first_gr):
                    if is_constant(reference[ref_id]) and reference[first_gr].is_multiline:
                        # WARNING: a stack multiregister push/pop instruction cannot be analyzed as multiple constant accesses if they are not mapped to the same cache line
                        lp_file.write("%dM (discard caching; TODO) */\n" % (times[node]))
                        lp_line += " + %d" % (DATA_MISS_COST*times[node])
                    else:
                        lp_file.write("%dM | %dM + %dH */\n" % (times[node], acdc_misses, times[node]-acdc_misses))
                        lp_line += " + %d + %d DRP_0x%x" % (DATA_MISS_COST*times[node], (DATA_MISS_COST-DATA_HIT_COST)*(acdc_misses-times[node]), reference[first_gr].pc)
                else: # array store and all previous load
                    lp_file.write("%dM | %dM + %dH + %dCB */\n" % (times[node], acdc_misses, times[node]-acdc_misses, acdc_misses))
                    lp_line += " + %d + %d DRP_0x%x" % (DATA_MISS_COST*times[node], (DATA_MISS_COST-DATA_HIT_COST)*(acdc_misses-times[node])+COPYBACK_COST*acdc_misses, reference[first_gr].pc)
            else:
                # NO GROUP-REUSE
                if times[node] > 1: # loop
                    if is_nonlinear(reference[ref_id]): # always miss
                        #print("0x%x: non-linear" % reference[ref_id].pc)
                        lp_file.write("non-linear: %dM */\n" % times[node])
                        lp_line += " + %d" % (times[node] * DATA_MISS_COST)
                    else:
                        if is_precise_array(reference[ref_id]):
                            #print("0x%x: precise array self-reuse" % reference[ref_id].pc)
                            lp_file.write("precise array self-reuse: ")
                            acdc_misses = calculate_misses(node, reference[ref_id].term, reference[ref_id].c)
                        elif is_imprecise_array(reference[ref_id]):
                            #print("0x%x: imprecise array self-reuse" % reference[ref_id].pc)
                            lp_file.write("imprecise array self-reuse: ")
                            acdc_misses = calculate_misses(node, reference[ref_id].term, None)
                        elif is_constant(reference[ref_id]):
                            #print("0x%x: scalar self-reuse" % reference[ref_id].pc)
                            if reference[ref_id].max_number:
                                if reference[ref_id].is_multiline:
                                    lp_file.write("%d/%d MULTILINE " % (reference[ref_id].number+1, reference[ref_id].max_number+1))
                                else:
                                    lp_file.write("%d/%d single line " % (reference[ref_id].number+1, reference[ref_id].max_number+1))
                            lp_file.write("scalar self-reuse: ")
                            acdc_misses = 1
                        else:
                            print("ERROR: unknown reuse type")
                            sys.exit(-1)
                        # poner ecuación con su DRP
                        if is_constant(reference[ref_id]) or reference[ref_id].is_load:
                            #print("All loads (no copybacks)")
                            if not reference[ref_id].is_multiline:
                                lp_file.write("%dM | %dM + %dH */\n" % (times[node], acdc_misses, times[node]-acdc_misses))
                                lp_line += " + %d + %d DRP_0x%x" % (DATA_MISS_COST*times[node], (DATA_MISS_COST-DATA_HIT_COST)*(acdc_misses-times[node]), reference[ref_id].pc)
                            else:
                                lp_file.write("%dM (discarding cache; TODO) */\n" % (times[node]))
                                lp_line += " + %d " % (DATA_MISS_COST*times[node])
                        else:
                            #print("Some stores (%d copybacks)" % acdc_misses)
                            lp_file.write("%dM | %dM + %dH + %dCB*/\n" % (times[node], acdc_misses, times[node]-acdc_misses, acdc_misses))
                            lp_line += " + %d + %d DRP_0x%x" % (DATA_MISS_COST*times[node], (DATA_MISS_COST-DATA_HIT_COST)*(acdc_misses-times[node])+COPYBACK_COST*acdc_misses, reference[ref_id].pc)
                else: # no self/group reuse: always miss
                    if is_constant(reference[ref_id]):
                        if reference[ref_id].max_number:
                            if reference[ref_id].is_multiline:
                                lp_file.write("%d/%d MULTILINE " % (reference[ref_id].number+1, reference[ref_id].max_number+1))
                            else:
                                lp_file.write("%d/%d single line " % (reference[ref_id].number+1, reference[ref_id].max_number+1))
                        lp_file.write("scalar first-use: %dM */\n" % times[node])
                    else:
                        lp_file.write("non-linear first-use: %dM */\n" % times[node])
                    #lp_file.write("+ %d" % (times[node] * DATA_MISS_COST))
                    lp_line += " + %d" % (times[node] * DATA_MISS_COST)
        #lp_file.write(";\n")
        lp_file.write(lp_line + ";\n")
    else: # No memory instructions
        lp_file.write("%s = 0;\n" % var_name("M", node))


def all_loads(first_gr):
    """ Returns whether all instructions reusing first_gr are loads """
    for ref in reference:
        if reference[ref].first_group_reuse == first_gr and not reference[ref].is_load:
            return False
    return True

def all_loads_lru(last_gr):
    """ Returns whether all instructions reusing first_gr are loads """
    if reference[last_gr].is_load:
        if reference[last_gr].last_group_reuse:
            return all_loads_lru(reference[last_gr].last_group_reuse)
        else:
            return True
    else:
        return False


def calculate_misses(node, terms, base):
    """ Calculates safe number of misses considering nested loops """
    """
        TODO: Depending on the loop pattern, iterations reported in the
        source code may be one less than the actual number of executions
        of a basic block. Studying patterns and adding +1 is not implemented.
        See DOI: 10.1371/journal.pone.0229980
    """

    # get number of iterations in loop
    loop_addr = get_loop_addr(node)
    loop_iterations = flowfacts.maxLoopIter[loop_addr]

    # get number of executions of this node
    if times[node] == 0:
        # unfeasible path
        # print("calculate_misses WARNING: loop 0x%x has no iterations" % (loop_addr))
        # outer_loop_iterations = 0
        return 0
    elif times[node] == loop_iterations:
        # no nesting: simple computation of misses
        outer_loop_iterations = 1
    else:
        # nested loop: look for outside loop
        outer_loop_iterations = times[node] / loop_iterations

    # calculate stride for this loop
    access_stride = 0
    for (addr, stride) in terms:
        #print("%d i0x%x" % (stride, addr))
        if loop_addr == addr:
            access_stride = stride
            break
    # calculate misses just considering this loop depending on stride
    if access_stride:
        # stride found
        loop_misses = calculate_sequential_misses(loop_iterations, access_stride, base)
    else:
        # no stride found for this loop;
        # the same array position is accessed in each iteration
        print("calculate_misses: temporal (not spatial) reuse at loop 0x%x" % loop_addr)
        #print(node)
        for term in terms:
            print("%d i0x%x" % (term[1], term[0]))
        #print(terms)
        loop_misses = 1

    total_misses = loop_misses * outer_loop_iterations
    return total_misses


def calculate_misses_old(node, terms, base):
    """ Calculates misses considering nested loops """
    """
        TODO: Depending on the loop pattern, iterations reported in the
        source code may be one less than the actual number of executions
        of a basic block. Studying patterns and adding +1 is not implemented.
        See DOI: 10.1371/journal.pone.0229980
    """

    # calculate stride for this loop
    access_stride = 0
    loop_addr = get_loop_addr(node)
    loop_iterations = flowfacts.maxLoopIter[get_loop_addr(node)]
    #print("Node", node, "in loop 0x%x" % loop_addr)
    for (addr, stride) in terms:
        #print("%d i0x%x" % (stride, addr))
        if loop_addr == addr:
            access_stride = stride
            break

    # calculate misses just considering this loop depending on stride
    if access_stride:
        # stride found
        loop_misses = calculate_sequential_misses(loop_iterations, access_stride, base)
    else:
        # no stride found for this loop;
        # the same array position is accessed in each iteration
        print("calculate_misses: temporal (not spatial) reuse at loop 0x%x" % loop_addr)
        #print(node)
        for term in terms:
            print("%d i0x%x" % (term[1], term[0]))
        #print(terms)
        loop_misses = 1

    #outer_loop_iterations = times[node] / loop_iterations
    if times[node] == loop_iterations:
        if loop_iterations:
            outer_loop_iterations = 1
        else:
            outer_loop_iterations = 0
            loop_misses = 0
            print("calculate_misses WARNING: loop 0x%x has no iterations and %d misses" % (loop_addr, loop_misses))
    else:
        outer_node = None
        loop_node = cfg.model.get_any_node(loop_addr)
        for pred_node in loop_node.predecessors:
            if not isBackEdge(pred_node, loop_node):
                outer_node = pred_node
                break
        enclosing_loop_addr = get_loop_addr(outer_node)
        if enclosing_loop_addr:
            outer_loop_iterations = flowfacts.maxLoopIter[enclosing_loop_addr]
        else:
            outer_loop_iterations = 1
        #outer_loop_iterations = int(get_recursive_execs(node, fmain) / loop_iterations)
#    outer_loop_iterations = 1
#    outer_loop_addr = enclosingLoop(loop_addr)
#    while outer_loop_addr != -1:
#        outer_loop_iterations *= flowfacts.maxLoopIter[outer_loop_addr]
#        outer_loop_addr = enclosingLoop(outer_loop_addr)
    if outer_loop_iterations*loop_iterations:
        if loop_misses == 1 or times[node] != outer_loop_iterations*loop_iterations:
            print("calculate_misses error: node %s is executed %d times: %d outer, %d loop, %d inner iterations in loop 0x%x" % (var_name("B", node), times[node], outer_loop_iterations, loop_iterations, times[node]/(outer_loop_iterations*loop_iterations), loop_addr))
    else:
        print("calculate_misses: unfeasible loop 0x%d in node" % loop_addr, node)

#    if loop_misses == 1:
#        print("calculate_misses: %d times: %d outer, %d loop, %d inner iterations in loop 0x%x" % (times[node], outer_loop_iterations, loop_iterations, times[node]/(outer_loop_iterations*loop_iterations), loop_addr))
#    if times[node] != outer_loop_iterations*loop_iterations:
#        if outer_loop_iterations*loop_iterations != 0:
#            print("calculate_misses: %d times: %d outer, %d loop, %d inner iterations in loop 0x%x" % (times[node], outer_loop_iterations, loop_iterations, times[node]/(outer_loop_iterations*loop_iterations), loop_addr))
#        else:
#            print("calculate_misses: unfeasible loop 0x%d in node" % loop_addr, node)
#            #print("calculate_misses: %d times: %d outer, %d loop, +oo inner iterations in loop 0x%x" % (times[node], outer_loop_iterations, loop_iterations, loop_addr))
#        print(terms)

    #print("%d iterations (%d times) with 1/%d misses in loop 0x%x" % (loop_iterations, outer_loop_iterations, CACHE_LINE_SIZE/access_stride, loop_addr))
    total_misses = loop_misses * outer_loop_iterations
    return total_misses


def calculate_sequential_misses(times, stride, base):
    """ Calculates maximum misses in the sequence """
    elems_in_line = CACHE_LINE_SIZE / abs(stride)
    if elems_in_line <= 1:
        misses = times # always miss
    else:
        if base:
            if stride > 0:
                first_line_elems = (CACHE_LINE_SIZE - (base % CACHE_LINE_SIZE)) / stride
            else:
                first_line_elems = (base % CACHE_LINE_SIZE) / (-stride)
        else:
            first_line_elems = 1
        min_touched_lines = 1 + ceil((times-first_line_elems) / elems_in_line)
        misses = min_touched_lines
    return misses


# Make list of unique accesses between reusing accesses
def max_conflicts_in_loop(node, addr_list, target_ref, abort_at):
    """
    Obtain the number of unique accesses (except subloops) between a given load/store in different iterations, considering any alternative path between them.
    If the returned value is larger than <abort_at>, it is not exact.

    Returns:
        The number described above.
    """
    ref_list = recursive_access_list_in_loop(node, addr_list, [], [], target_ref, abort_at)
    #print("Conflicts for ld/st at", hex(reference[target_ref].pc), ref_list)
    num_refs = len(ref_list)
    if is_array(reference[target_ref]):
        num_refs -= 1 # discard conflict with itself
    return num_refs


def recursive_access_list_in_loop(node, addr_list, explored, ref_list, target_ref, abort_at):
    """
    Recursivelly explore successors of <node> belonging to <addr_list> to obtain a list of unique accesses (except subloops) between a given load/store in different iterations, considering any alternative path between them.
    Abort if the list gets larger than <abort_at>.

    Returns:
        A list of unique addresses (except for subloops and unknown address marks)
    """
    if node.addr not in explored and node.addr in addr_list:
        accesses = ref_list_in(ref_list, node, target_ref)
        explored.append(node.addr)
        if len(accesses) > abort_at:
            return accesses + [-1,-1,-1]
        #print("explored", explored)
        max_accesses = accesses
        for child in node.successors:
            # OPTION 1: Consider the path with target_ref and all alternative paths. For each ref, add it
            path_accesses = recursive_access_list_in_loop(child, addr_list, explored, max_accesses, target_ref, abort_at)
            if times[child] > times[node] and times[node]:
                # if child is a new loop, calculate extra pollution
                path_accesses = calc_pollution(child, int(times[child]/times[node]), path_accesses, target_ref, abort_at)
            max_accesses += path_accesses
            # OPTION 2: Consider just the path with target_ref and the worst alternative path. UNSAFE.
            # path_accesses = recursive_access_list_in_loop(child, addr_list, explored, accesses, target_ref, abort_at)
            # if times[child] > times[node] and times[node]:
            #     # if child is a new loop, calculate extra pollution
            #     path_accesses = calc_pollution(node, int(times[child]/times[node]), path_accesses, target_ref, abort_at)
            # if len(path_accesses) > len(max_accesses):
            #     max_accesses = path_accesses
            # OPTION 3: Consider just the path with target_ref. UNSAFE.
            # not implemented
        #print(added_accesses)
        return max_accesses
    else:
        return ref_list


def conflicts(ref1, ref2):
    """
    Returns whether two references conflict in cache.
    If they 1) present group reuse, or 2) are the same constant address, or 3) are constants mapped to different sets, they are assumed to conflict.
    """
    if reference[ref1].last_group_reuse == ref2 or reference[ref2].last_group_reuse == ref1:
        return False
    if is_constant(reference[ref1]) and is_constant(reference[ref2]):
        addr1 = reference[ref1].c
        addr2 = reference[ref2].c
        if addr1 == addr2 or data_cache_set(addr1) != data_cache_set(addr2):
            return False
    return True


def ref_list_in(ini_list, node, target_ref):
    """
    Returns a list of unique loads/stores in node conflicting with <target_ref>.
    If <target_ref> is None, assume conflict always.

    Returns:
        List of accessed reference ids
    """
    ref_list = ini_list
    refs = get_refs_in_node(node)
    for ref in refs:
        if reference[ref].last_group_reuse and reference[ref].last_group_reuse in ref_list:
            # if ref reuses anoter ref in ref_list):
            # update for the newer last group reuse reference
            ref_list.remove(reference[ref].last_group_reuse)
            ref_list.append(ref)
        elif conflicts(ref, target_ref):
            ref_list.append(ref)
    return ref_list

def print_pcs(target_ref, ref_list):
    if target_ref:
        sys.stdout.write("Conflicts for 0x%x:" % reference[target_ref].pc)
    for ref in ref_list:
        if ref != -1: # -1 is abort mark
            sys.stdout.write(" 0x%x," % reference[ref].pc)
    sys.stdout.write("\n")

def max_conflicts_between(ini_node, ini_ref, fin_node, fin_ref, target_ref, abort_at):
    """
    Returns the number of loads/stores between two loads/stores.
    If constant_addr, does not count references accessing other sets.
    If node_list (nodes in a loop), do not explore outside.
    """
    #print("Conflicts between 0x%x and 0x%x" % (reference[ini_ref].pc, reference[fin_ref].pc))
    ref_list = back_explore_conflicts(ini_node, ini_ref, fin_node, fin_ref, fin_node, [], target_ref, abort_at)
    #print_pcs(target_ref, ref_list)
    num_refs = len(ref_list)
    if is_array(reference[target_ref]):
        num_refs -= 1 # discard conflict with itself
    return num_refs


def ref_list_from_to(node, ini, fin, target_ref):
    """
    Returns a list of unique loads/stores in node, from <ini> to <fin> conflicting with <target_ref>.

    Returns:
        List of accessed reference ids
    """
    if ini < fin:
        ini_id = ini
        fin_id = fin
    else:
        fin_id = ini
        ini_id = fin
    ref_list = []
    counting = False
    refs = get_refs_in_node(node)
    for ref in refs:
        if ref == ini_id:
            counting = True
        if ref == fin_id and counting:
#            print(ref_list)
            return ref_list
        if counting:
            if reference[ref].last_group_reuse and reference[ref].last_group_reuse in ref_list:
                # if ref reuses anoter ref in ref_list):
                # update for the newer last group reuse reference
                ref_list.remove(reference[ref].last_group_reuse)
                ref_list.append(ref)
#                print("  + 0x%x - 0x%x" % (reference[ref].pc,reference[reference[ref].last_group_reuse].pc))
            elif conflicts(ref, target_ref):
                ref_list.append(ref)
#                print("  + 0x%x (%d)" % (reference[ref].pc, ref))
#            else:
#                print("  discarded 0x%x" % (reference[ref].pc))
    print("ref_list_from_to ERROR: delimiters", ini_id, fin_id, "not found")
    print(refs)
    sys.exit(-1)


def ref_list_to(ini_list, node, ref_id, target_ref):
    """
    Returns a list of unique loads/stores in node, until <fin_id> conflicting with <target_ref>.

    Returns:
        List of accessed reference ids
    """
    ref_list = ini_list
    refs = get_refs_in_node(node)
    for ref in refs:
        if ref == ref_id:
            break
        if reference[ref].last_group_reuse and reference[ref].last_group_reuse in ref_list:
            # if ref reuses anoter ref in ref_list):
            # update for the newer last group reuse reference
            ref_list.remove(reference[ref].last_group_reuse)
            ref_list.append(ref)
        elif conflicts(ref, target_ref):
            ref_list.append(ref)
#                print("  added", hex(reference[ref].pc))
#            else:
#                print("  discarded", hex(reference[ref].pc))
    return ref_list

def back_explore_conflicts(ini, ini_id, fin, fin_id, node, ref_list, target_ref, abort_at):
    """
    Performs a backwards exploration (from nodes fin to ini) to count memory references from ini_id to fin_id conflicting with target_ref.

    Returns:
        The longest list of references found between ini_id in ini and fin_id in fin, ending with a mark if aborted at abort_at
    """
    #print(var_name("B", node), ref_list)
    if ini == fin: # Same basic block, no CFG analysis needed
        return ref_list_from_to(ini, ini_id, fin_id, target_ref)
    elif ini == node: # First (ending) node reached, end CFG analysis
        return ref_list_from(ref_list, ini, ini_id, target_ref)
    elif fin == node: # Last (starting) node, start CFG analysis
        accesses = ref_list_to([], fin, fin_id, target_ref)
    else:
        accesses = ref_list_in_reverse(ref_list, node, target_ref)
    if len(accesses) > abort_at:
        return accesses + [-1, -1, -1]
    max_accesses = accesses
    #print(node.predecessors)
    for parent in node.predecessors:
        if not isBackEdge(node, parent): # may traverse loops
            path_accesses = back_explore_conflicts(ini, ini_id, fin, fin_id, parent, accesses, target_ref, abort_at)
            if times[parent] > times[node] and times[node]:
                path_accesses = calc_pollution(parent, int(times[parent]/times[node]), path_accesses, target_ref, abort_at)
            if len(path_accesses) > len(max_accesses):
                max_accesses = path_accesses
    return max_accesses


def calc_pollution(node, iterations, ref_list, target_ref, abort_at):
    """
    Given a list of references, for each array reference in the list, this function test how many ways in each set it might touch. Then, it replicates the reference as many times as ways it can touch in a set.

    Returns:
    The new list of references, with the described replications.
    """
    new_ref_list = ref_list.copy()
    loop_addr = get_loop_addr(node)
    for ref in ref_list:
        if ref != -1 and is_array(reference[ref]):
            access_stride = 0
            #print("Node", node, "in loop 0x%x" % loop_addr)
            for (addr, stride) in reference[ref].term:
                #print("%d i0x%x" % (stride, addr))
                if loop_addr == addr:
                    access_stride = stride
                    break
            if access_stride: # self-spatial reuse
                if reference[target_ref].c:
                    loop_misses = calculate_sequential_misses(iterations, access_stride, reference[target_ref].c)
                else:
                    loop_misses = calculate_sequential_misses(iterations, access_stride, None)
                polluted_ways = ceil(loop_misses / DATA_CACHE_SETS)
                if polluted_ways > 1:
                    print("%d polluted ways in %s" % (polluted_ways, var_name("M", node)))
                for way in range(1, polluted_ways):
                    new_ref_list.append(ref)
                    if len(new_ref_list) > abort_at:
                        return new_ref_list + [-1,-1,-1]
    return new_ref_list


def ref_list_in_reverse(ini_list, node, target_ref):
    """
    Returns a list of unique loads/stores in node conflicting with <target_ref>.
    If <target_ref> is None, assume conflict always.

    Returns:
        List of accessed reference ids
    """
    ref_list = ini_list.copy()
    refs = get_refs_in_node(node)
    for ref in reversed(refs):
        added = False
        for confl_ref in ref_list:
            if reference[confl_ref].last_group_reuse and reference[confl_ref].last_group_reuse == ref:
                # if ref reuses anoter ref in ref_list):
                # update for the newer last group reuse reference
                ref_list.remove(confl_ref)
                ref_list.append(ref)
#                    print("  + 0x%x (%d) - 0x%x (%d)" % (reference[ref].pc, ref, reference[confl_ref].pc, confl_ref))
                added = True
                break
        if not added and conflicts(ref, target_ref):
            ref_list.append(ref)
#                print("  + 0x%x (%d)" % (reference[ref].pc, ref))
#            else:
#                print("  discarded 0x%x" % (reference[ref].pc))
    return ref_list


def ref_list_from(ini_list, node, ref_id, target_ref):
    ref_list = ini_list
    refs = get_refs_in_node(node)
    counting = False
    for ref in refs:
        if ref == ref_id:
            counting = True
        if counting:
            if reference[ref].last_group_reuse and reference[ref].last_group_reuse in ref_list:
                # if ref reuses anoter ref in ref_list):
                # update for the newer last group reuse reference
                ref_list.remove(reference[ref].last_group_reuse)
                ref_list.append(ref)
#                    print("  + 0x%x - 0x%x" % (reference[ref].pc,reference[reference[ref].last_group_reuse].pc))
            elif conflicts(ref, target_ref):
                ref_list.append(ref)
#                    print("  + 0x%x " % (reference[ref].pc))
#                else:
#                    print("  discarded 0x%x" % (reference[ref].pc))
    return ref_list


# Count accesses between reusing accesses
def get_accesses_in_loop(node, addr_list, constant_addr=None):
    #print(addr_list)
    num_refs = get_recursive_accesses_in_loop(node, addr_list, [], constant_addr) - 1
    if constant_addr:
        num_refs += 1
    return num_refs
#    accesses = 0
#    for node in node_list:
#        if set_node_id(node) in refs_in_node:
#            accesses += len(refs_in_node[set_node_id(node)])
#    return accesses

def get_recursive_accesses_in_loop(node, addr_list, explored, constant_addr):
    """ improvement TODO: si constant_addr y hay varios, contar conflictivos desde el último acceso, no todos """
    if node.addr not in explored and node.addr in addr_list:
        accesses = 0
        explored.append(node.addr)
        #print("explored", explored)
        for child in node.successors:
            path_accesses = get_recursive_accesses_in_loop(child, addr_list, explored, constant_addr)
            #print("path", accesses)
            if path_accesses > accesses:
                accesses = path_accesses
        accesses += get_refs_in(node, constant_addr)
        #print(accesses)
        return accesses
    else:
        return 0


def conflicting(addr1, addr2):
    """ Returns whether addr1 and addr2 are different and may be
    mapped to the same cache set """
    if addr1 != addr2 and data_cache_set(addr1) == data_cache_set(addr2):
        return True
    else:
        return False

""" improvement TODO: several conflicting comparisons of addr against other addresses should not increase the count if the other addresses coincide. A LRU stack of WAYS is probably required for this count. """

def get_refs_from_to(node, ini_id, fin_id, constant_addr=None):
    """ Returns the number of loads/stores between ini_id and fin_id """
    """ If constant_addr, does not count references accessing other sets """
    if reference[ini_id].pc == reference[fin_id].pc:
        if constant_addr:
            return 1
        else:
            return reference[fin_id].number - reference[ini_id].number
    else:
        num_refs = 0
        counting = False
        refs = get_refs_in_node(node)
        for ref in refs:
            if ref == ini_id:
                counting = True
            if ref == fin_id and counting:
                return num_refs
            if counting:
                if constant_addr and reference[ref].c:
                    if conflicting(reference[ref].c, constant_addr):
                        #print(hex(reference[ref].pc))
                        num_refs += 1
                else:
                    #print(hex(reference[ref].pc))
                    num_refs += 1
        print("get_refs_from_to ERROR: delimiters", ini_id, fin_id, "not found")
        print(refs)
        sys.exit(-1)


def get_refs_to(node, ref_id, constant_addr=None):
    """ Returns the number of loads/stores until ref_id """
    """ If constant_addr, does not count references accessing other sets """
    num_refs = 0
    refs = get_refs_in_node(node)
    for ref in refs:
        if constant_addr and reference[ref].c:
            if conflicting(reference[ref].c, constant_addr):
                num_refs += 1
        else:
            num_refs += 1
        if ref == ref_id:
            break
    return num_refs

def get_refs_from(node, ref_id, constant_addr=None):
    """ Returns the number of loads/stores from ref_id """
    """ If constant_addr, does not count references accessing other sets """
    num_refs = 0
    refs = get_refs_in_node(node)
    counting = False
    for ref in refs:
        if ref == ref_id:
            counting = True
        if counting:
            if constant_addr and reference[ref].c:
                if conflicting(reference[ref].c, constant_addr):
                    num_refs += 1
            else:
                num_refs += 1
    return num_refs


def is_refs_in(node, constant_addr):
    """ Returns whether there is an access to constant_addr in node """
    refs = get_refs_in_node(node)
    for ref in refs:
        if reference[ref].c == constant_addr:
            return True
    return False


def get_refs_in(node, constant_addr=None):
    """ Returns the number of loads/stores in node """
    """ If constant_addr, does not count references accessing other sets """
    num_refs = 0
    refs = get_refs_in_node(node)
    if constant_addr:
        for ref in refs:
            if reference[ref].c:
                if conflicting(reference[ref].c, constant_addr):
                    #print(hex(reference[ref].pc))
                    num_refs += 1
            else:
                #print(hex(reference[ref].pc))
                num_refs += 1
    else:
        num_refs = len(refs)
    return num_refs


def get_accesses_between(ini, ini_id, fin, fin_id, constant_addr, abort_at, node_list=None):
    """ Returns the number of loads/stores between two loads/stores """
    """
        If constant_addr, does not count references accessing other sets.
        If node_list (nodes in a loop), do not explore outside.
    """
    if ini == fin: # Same node (basic block)
        num_refs = get_refs_from_to(ini, ini_id, fin_id, constant_addr)
    else:
        num_refs = get_refs_to(fin, fin_id, constant_addr)
        if node_list is None:
            num_refs = backexplore(ini, ini_id, fin, constant_addr, num_refs, abort_at)
        else:
            num_refs += backexplore_loop(ini, ini_id, fin, constant_addr, node_list)
    if constant_addr:
        num_refs += 1
    return num_refs


def backexplore_loop(ini, ini_id, node, constant_addr, node_list):
    if ini == node:
        return get_refs_from(node, constant_addr)
    else:
        max_accesses = 0
        for parent in node.predecessors:
            if parent in node_list and not isBackEdge(node, parent):
                accesses = backexplore_loop(ini, ini_id, parent, constant_addr, node_list)
                if accesses > max_accesses:
                    max_accesses = accesses
        return max_accesses + get_refs_in(node, constant_addr)


def backexplore(ini, ini_id, node, constant_addr, count, abort_at):
    """ Returns the number of loads/stores between ini_id and node """
    """
        If cache_set, does not count references accessing other sets.
        Recursive backwards exploration, from node to ini.
        Counts all refs in node, and those after ini_id in ini.
    """
    #print("nodo", hex(node.addr))
    if ini == node:
        #print("end node", hex(node.addr))
        return count + get_refs_from(ini, ini_id, constant_addr)
    else:
        accesses = count + get_refs_in(node, constant_addr)
        max_path_accesses = accesses
        if accesses <= abort_at:
            #print(node.predecessors)
            for parent in node.predecessors:
                if not isBackEdge(node, parent): # may traverse loops
                    path_accesses = backexplore(ini, ini_id, parent, constant_addr, accesses, abort_at)
                    if path_accesses > max_path_accesses:
                        max_path_accesses = path_accesses
        return max_path_accesses


def is_array(ref):
    return bool(ref.term)

def is_precise_array(ref):
    return bool(ref.c and ref.term)

def is_imprecise_array(ref):
    return bool(not ref.c and ref.term)

def is_constant(ref):
    return bool(ref.c and not ref.term)

def is_nonlinear(ref):
    return bool(not ref.c and not ref.term)

def cache_line_addr(addr):
    return addr - (addr % CACHE_LINE_SIZE)

def instrunction_cache_set(addr):
    return int(addr / CACHE_LINE_SIZE) % INSTRUCTION_CACHE_SETS

def data_cache_set(addr):
    return int(addr / CACHE_LINE_SIZE) % DATA_CACHE_SETS

def generate_altpath_constraint(lp_file, cfg_node, path, is_loop):
    """ Write alternative path constraint: cfg_node >= path """
    """ DOI: 10.1371/journal.pone.0229980 """
    global times
    #print(path)
    lp_file.write(var_name("C", cfg_node) + " >= ")
#    for (node, times) in path[:-1]:
#        lp_file.write("%d %s + " % (int(times), var_name("B", node)))
#    lp_file.write(var_name("C", path[-1][0]) + ";\n")

    # loop penalty for going through alternative paths
    if DATA_CACHE_TYPE == "acdc" and is_loop:
        lp_file.write("%s + " % var_name("AP", cfg_node))

    for node in path[:-1]:
        #lp_file.write("%d %s + " % (times[node], var_name("B", node)))
        lp_file.write("%s + " % var_name("B", node))
    lp_file.write(var_name("C", path[-1]) + ";\n")


def explore(cfg_node, path, maxexecs):
    """ Explores a CFG and builds a tree-structured ILP model """
    """ DOI: 10.1371/journal.pone.0229980 """
    #global times, node_penalty
    #print(hex(cfg_node.addr), maxexecs)
    successors = no_back_edge_successors(cfg_node)
    #print("No back edge successors of", cfg_node, ":", successors)
    times[cfg_node] = maxexecs
#    if DATA_CACHE_TYPE == "acdc":
#        node_penalty[cfg_node] = calc_acdc_node_penalty(cfg_node)
    #if cfg_node in loop_headers:
    if times[cfg_node] > 1:
        is_loop = True
    else:
        is_loop = False
    if len(successors) == 0:
        return path + [cfg_node]
    #elif len(successors) == 1 and successors[0] not in loop_headers:
    elif len(successors) == 1:
        if is_break_edge(cfg_node, successors[0]):
            maxexecs = int(maxexecs / flowfacts.maxLoopIter[get_loop_addr(cfg_node)])
        if successors[0] in loop_headers:
            ### TODO: loops should generate a new lp structure line with generate_altpath_constraint(..., is_loop)
#            print("ERROR TODO: loops should generate a new lp structure line")
#            lp_file.write("ERROR TODO: loops should generate a new lp structure line." + var_name("C", cfg_node))
#            sys.exit(-1)
            ### END TODO
            maxexecs *= flowfacts.maxLoopIter[successors[0].addr]
            if maxexecs == 0:
                print("explore: reached loop 0x%x (0 iterations)" % successors[0].addr)
                return path + [cfg_node]
        return explore(successors[0], path + [cfg_node], maxexecs)
    elif len(successors) > 1 and cfg_node in explore.explored:
        return path + [cfg_node]
    else:
        explore.explored.append(cfg_node)
        for child_node in successors:
            alternative_path = None
            if child_node in loop_headers: # entering a loop
                if flowfacts.maxLoopIter[child_node.addr]:
                    alternative_path = explore(child_node, [cfg_node], maxexecs * flowfacts.maxLoopIter[child_node.addr])
                else:
                    print("explore: reached loop 0x%x (0 iterations)" % child_node.addr)
            elif is_break_edge(cfg_node, child_node):
                alternative_path = explore(child_node, [cfg_node], int(maxexecs / flowfacts.maxLoopIter[get_loop_addr(cfg_node)]))
            else:
                alternative_path = explore(child_node, [cfg_node], maxexecs)
            if alternative_path:
                generate_altpath_constraint(lp_file, cfg_node, alternative_path, is_loop)
        return path + [cfg_node]
explore.explored = []


def is_break_edge(pre_node, suc_node):
    """ Returns whether the edge pre->suc is break node """
    for loop in loop_info.loops:
        if pre_node.to_codenode() in loop.body_nodes:
            (internal_nodes, break_nodes, continue_nodes) = classify_successors(pre_node, loop)
            if suc_node in break_nodes:
                return True
    return False


def get_loop_addr(cfg_node):
    """
    Returns the addr of the loop enclosing cfg_node.
    IMPORTANT: if cfg_node is inside no loop, the return value will be invalid.
    """
    # first try: look in loop body nodes
    loop_addr = getDeepestLoop(cfg_node)
    if loop_addr != 0:
        return loop_addr
    #print("second try for", cfg_node)
    # second try (functions called from loops): look for predecessor loop header
    for parent in cfg_node.predecessors:
        if parent in loop_headers and not is_break_edge(parent, cfg_node):
            return parent.addr
    for parent in cfg_node.predecessors:
        if not is_break_edge(parent, cfg_node):
            result = get_loop_addr(parent)
            if result:
                return result
        else: # loop above, jump it and continue testing
            loop_addr = get_loop_addr(parent)
            #print("found loop 0x%x above node" % (loop_addr))
            if loop_addr:
                loop = getLoop(loop_addr)
                for entry_node in cfg.model.get_all_nodes(loop_addr):
                    #print("entry node of loop 0x%x:" % (loop_addr), entry_node)
                    for parent2 in entry_node.predecessors:
                        result = get_loop_addr(parent2)
                        if result:
                            return result
    # no loop found
    #print("get_loop_addr: Error,", var_name("B", cfg_node), "is inside no loop")
    return None
    #sys.exit(-1)


def generate_lockms_model(filename, data_cache, sets, ways): #, cache_line_size, data_hit_cost, data_miss_cost):
    """
    Generates a Lock-MS ILP model and writes the corresponding
    lp-solve .lp file.
    Lock-MS requires instanced functions. Otherwise, call-return sequences may generate loops in the CFG and the generated model
    will be infeasible.
    """
    global reference #, CACHE_LINE_SIZE
    global DATA_CACHE_TYPE, DATA_CACHE_SETS, DATA_CACHE_WAYS, ACDC_DC_LINES #, DATA_HIT_COST, DATA_MISS_COST, MEMORY_COST, COPYBACK_COST
    DATA_CACHE_TYPE = data_cache
    DATA_CACHE_SETS = sets
    DATA_CACHE_WAYS = ways
    ACDC_DC_LINES = ways
    #CACHE_LINE_SIZE = cache_line_size
    #DATA_HIT_COST = data_hit_cost
    #DATA_MISS_COST = data_miss_cost
    #MEMORY_COST = data_miss_cost - data_hit_cost
    #COPYBACK_COST = MEMORY_COST


    part_time = time.process_time()
    xmlparse.parse(filename + ".xml", CACHE_LINE_SIZE)
    reference = xmlparse.get_references()
    #refs_in_node = xmlparse.get_references_by_node()
    #xmlparse.print_all()
    init(filename, instance_functions=True)
    print("--- Init time:", round(time.process_time() - part_time, 2), "seconds ---")
    part_time = time.process_time()

    write_lockms_model(filename, main_node)
    print("--- ILP model generation time:", round(time.process_time() - part_time, 2), "seconds ---")


def print_usage():
    print("usage:", sys.argv[0], "[acdc <ways> | lru <sets> <ways> | am | ah] <binaryfile>")
    sys.exit()

## MAIN
def main():
    if len(sys.argv) < 3 or len(sys.argv) > 5:
        print_usage()
    data_cache = sys.argv[1] # acdc or lru
    #line_size = 64
    #data_hit_cost = 1
    #data_miss_cost = 10
    # data_memory_access = data_copyback_cost = data_miss_cost - data_hit_cost
    if data_cache == "acdc":
        if len(sys.argv) != 4:
            print_usage()
        sets = 1
        ways = int(sys.argv[2])
        file_name = sys.argv[3]
    elif data_cache == "lru":
        if len(sys.argv) != 5:
            print_usage()
        sets = int(sys.argv[2])
        ways = int(sys.argv[3])
        file_name = sys.argv[4]
    elif data_cache in ["ah", "am"]:
        if len(sys.argv) != 3:
            print_usage()
        ways = 0
        sets = 0
        file_name = sys.argv[2]
    else:
        print_usage()

    generate_lockms_model(file_name, data_cache, sets, ways) #, line_size, data_hit_cost, data_miss_cost)

if __name__ == "__main__":
    sys.exit(main())
