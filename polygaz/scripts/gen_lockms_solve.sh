#!/bin/sh

# Launches generation and solving of ILP models
# Generates a .csv with single-space separated columns

#ANADIR="$HOME/investigacio/treal/analizadores/polygaz"
ANADIR="$HOME/polygaz/"
ANACMD="venv/bin/python3 -u -O ipet.py"
DRPCMD1="venv/bin/python3 -u -O lockMS.py"
DRPCMD2="scripts/gen_acdc_config.sh"
DRPCMD3="scripts/gen_acdc_config2.sh"

icache="unlimited"
ACDC_DC_LINES="4 8 16 32" # 256-2048 B netos
# 4*(64+4+4) - X; 288-2304 B brutos (data+tag+AC)
#LRU_WAYS="1 2 4 8" # 4-32 KB netos
LRU_WAYS="4 8 16 32" # 16-128 KB netos
# 64*(64+(4-6/8)) - 64*(64*8+(4-6/8)+8*3/8) ; 4304-33168 B brutos
# data+tag+lru

#BENCHDIR="$HOME/investigacio/treal/benchmarks/xmls"
BENCHDIR="$HOME/xmls/"

#SOLVER="lp_solve  -BB -Bc -Bd -Bg -Bo -R -time -timeout 60"
#SOLVER="lp_solve -presolve -R -time" # states powerwindow O0 is unbounded
SOLVER="lp_solve -R -time"
# mpeg2 O3 necesita timeout > 60

OUTFILE="lockms_results.csv"

cd "$ANADIR"

for fi in `find "$BENCHDIR" -name "*arm.xml"` ; do
  XML="$fi"
  ELF=`echo "$fi" | sed 's/\.xml$//g'`
  ########### DRP benefit estimation (Always Miss)
  LOG=`echo "$fi" | sed "s/\.xml/_am_0_0\.log/g"`
  LPFILE=`echo "$fi" | sed "s/\.xml/_${icache}_am_0_0\.lp/g"`
  LPRESULT="${LPFILE}.solved"
  if [ ! -e $LPFILE ] ; then
    echo "$ANADIR/$ANACMD am $ELF > $LOG 2>&1 && $SOLVER $LPFILE > $LPRESULT 2>&1"
  fi
done | parallel -j -2 # --memfree 2G # Computation server

 ######################## GENERATE AND SOLVE ILP MODELS
for fi in `find "$BENCHDIR" -name "*arm.xml"` ; do
  XML="$fi"
  ELF=`echo "$fi" | sed 's/\.xml$//g'`
  #ELF=`echo "$fi" | sed 's/_arm_.*/_arm/g'`
  ########### AlwaysHit
  LOG=`echo "$fi" | sed "s/\.xml/_ah_0_0\.log/g"`
  LPFILE=`echo "$fi" | sed "s/\.xml/_${icache}_ah_0_0\.lp/g"`
  LPRESULT="${LPFILE}.solved"
  if [ ! -e $LPFILE ] ; then
    echo "$ANADIR/$ANACMD ah $ELF > $LOG 2>&1 && $SOLVER $LPFILE > $LPRESULT 2>&1"
  fi
  ########### No cache (direct memory)
  LOG=`echo "$fi" | sed "s/\.xml/_dm_0_0\.log/g"`
  LPFILE=`echo "$fi" | sed "s/\.xml/_${icache}_dm_0_0\.lp/g"`
  LPRESULT="${LPFILE}.solved"
  if [ ! -e $LPFILE ] ; then
    echo "$ANADIR/$ANACMD dm $ELF > $LOG 2>&1 && $SOLVER $LPFILE > $LPRESULT 2>&1"
  fi
  ########### FirstMiss
  LOG=`echo "$fi" | sed "s/\.xml/_fm_0_0\.log/g"`
  LPFILE=`echo "$fi" | sed "s/\.xml/_${icache}_fm_0_0\.lp/g"`
  LPRESULT="${LPFILE}.solved"
  if [ ! -e $LPFILE ] ; then
    echo "$ANADIR/$ANACMD fm $ELF > $LOG 2>&1 && $SOLVER $LPFILE > $LPRESULT 2>&1"
  fi
   ########### ACDC
  for lines in $ACDC_DC_LINES ; do
    LOG=`echo "$fi" | sed "s/\.xml/_acdc_1_${lines}\.log/g"`
    DRPFILE=`echo "$fi" | sed "s/\.xml/_acdc_${lines}\.xml/g"`
    LPFILE=`echo "$fi" | sed "s/\.xml/_${icache}_acdc_1_${lines}\.lp/g"`
    #LPFILEDRP=`echo "$fi" | sed "s/\.xml/_locked_acdc_1_${lines}\.lp/g"`
    LPRESULT="${LPFILE}.solved"
    #LPRESULTDRP="${LPFILEDRP}.solved"
    LPRESULTDRP=`echo "$fi" | sed "s/\.xml/_${icache}_am_0_0\.lp\.solved/g"`
#    if [ ! -e $DRPFILE ] ; then
#      echo "$ANADIR/$DRPCMD1 acdc $lines $ELF > $LOG 2>&1 && $SOLVER $LPFILEDRP > $LPRESULTDRP 2>&1 && $ANADIR/$DRPCMD2 $LPRESULTDRP > $DRPFILE"
#    fi
    if [ ! -e $LPFILE ] ; then
      #echo "$ANADIR/$DRPCMD1 acdc $lines $ELF > $LOG 2>&1 && $SOLVER $LPFILEDRP > $LPRESULTDRP 2>&1 && $ANADIR/$DRPCMD2 $LPRESULTDRP > $DRPFILE && $ANADIR/$ANACMD acdc $lines $ELF > $LOG 2>&1 && $SOLVER $LPFILE > $LPRESULT 2>&1"
      echo "$ANADIR/$DRPCMD3 $LPRESULTDRP $lines > $DRPFILE && $ANADIR/$ANACMD acdc $lines $ELF > $LOG 2>&1 && $SOLVER $LPFILE > $LPRESULT 2>&1"
      #echo "$ANADIR/$ANACMD acdc $lines $ELF >> $LOG 2>&1 && $SOLVER $LPFILE > $LPRESULT 2>&1"
    fi
  done
  ########### LRU
  sets=64
  for ways in $LRU_WAYS ; do
    LOG=`echo "$fi" | sed "s/\.xml/_lru_${sets}_${ways}\.log/g"`
    LPFILE=`echo "$fi" | sed "s/\.xml/_${icache}_lru_${sets}_${ways}\.lp/g"`
    LPRESULT="${LPFILE}.solved"
    if [ ! -e $LPFILE ] ; then
      #echo "$ANADIR/$ANACMD lru $sets $ways $ELF > $LOG 2>&1 && $SOLVER $LPFILE > $LPRESULT 2>&1"
      # generate lp and solve OR (if solver fails) generate with NoFH and solve
      echo "$ANADIR/$ANACMD lru $sets $ways $ELF > $LOG 2>&1 && $SOLVER $LPFILE > $LPRESULT 2>&1 || $ANADIR/$ANACMD lru $sets $ways NoFH $ELF > $LOG 2>&1 && $SOLVER $LPFILE > $LPRESULT 2>&1"
    fi
  done
  ########### Constant addr only - LRU
  sets=64
  for ways in $LRU_WAYS ; do
    LOG=`echo "$fi" | sed "s/\.xml/_constlru_${sets}_${ways}\.log/g"`
    LPFILE=`echo "$fi" | sed "s/\.xml/_${icache}_constlru_${sets}_${ways}\.lp/g"`
    LPRESULT="${LPFILE}.solved"
    if [ ! -e $LPFILE ] ; then
      #echo "$ANADIR/$ANACMD lru $sets $ways $ELF > $LOG 2>&1 && $SOLVER $LPFILE > $LPRESULT 2>&1"
      # generate lp and solve OR (if solver fails) generate with NoFH and solve
      echo "$ANADIR/$ANACMD constlru $sets $ways $ELF > $LOG 2>&1 && $SOLVER $LPFILE > $LPRESULT 2>&1 || $ANADIR/$ANACMD constlru $sets $ways NoFH $ELF > $LOG 2>&1 && $SOLVER $LPFILE > $LPRESULT 2>&1"
    fi
  done
   ########### GENERATE fully-associative LRU ILP MODEL
#  sets=1
#  for ways in $ACDC_DC_LINES ; do
#    LOG=`echo "$fi" | sed "s/\.xml/_lru_${sets}_${ways}\.log/g"`
#    LPFILE=`echo "$fi" | sed "s/\.xml/_${icache}_lru_${sets}_${ways}\.lp/g"`
#    if [ ! -e $LPFILE ] ; then
#      echo "$ANADIR/$ANACMD lru $sets $ways $ELF > $LOG 2>&1 ;"
#    fi
#  done
  #$ANADIR/$ANACMD $ELF > $LOG ;
  #read -p "Press enter to continue" REPLY
done | parallel -j -2 # --memfree 2G # Computation server
#done | parallel -j 50% --eta #--memfree 16G # Desktop
#done

###################### GENERATE CSV FILE
cp "$BENCHDIR/$OUTFILE" "$BENCHDIR/${OUTFILE}.old"
echo "Name OLevel PCs PCsNGR DCache Sets Ways WCET UsedDRPs InitT MemRefT IndVarT ReuseGenT ReuseCalcT InitT2 ModelT FMILPT SolveT WCET.ah WCET.am WCET.fm" > "$BENCHDIR/$OUTFILE"
for fi in `find "$BENCHDIR" -name "*${icache}*.lp.solved"` ; do
  #echo $fi >&2
  XML_LOG_PATTERN=`echo "$fi" | sed "s/_arm_.*$/_arm_\*.reuse/g"`
  XML_LOG_FILE=`ls -1 $XML_LOG_PATTERN`
  # config
  name=`basename "$fi" | sed "s/_O.*//g"`
  olevel=`echo "$fi" | sed "s/^.*_O//g" | sed "s/_arm.*//g"`
  parameters=`echo "$fi" | sed "s/^.*arm_//g" | sed "s/.lp.solved//g"`
  #icache=`echo "$parameters" | cut -f1 -d_`
  dcache=`echo "$parameters" | cut -f2 -d_`
  sets=`echo "$parameters" | cut -f3 -d_`
  ways=`echo "$parameters" | cut -f4 -d_`
  # xml summary
  MODEL_LOG_FILE="${BENCHDIR}/${name}_O${olevel}_arm_${dcache}_${sets}_${ways}.log"
  pcs=`grep 'XML PCs:' "${MODEL_LOG_FILE}" | cut -f3 -d' '`
  pcsngr=`grep 'XML PCs without' "${MODEL_LOG_FILE}" | cut -f6 -d' '`
#  if [ -z "$pcs" ] ; then # old outputs did not contain these lines
#    pcs=`./venv/bin/python3 -O xmlparse.py $BENCHDIR/${name}_O${olevel}_arm.xml | grep 'XML PCs:' | cut -f3 -d' '`
#    pcsngr=`./venv/bin/python3 -O xmlparse.py $BENCHDIR/${name}_O${olevel}_arm.xml | grep 'XML PCs without' | cut -f6 -d' '`
#  fi
  # times xml generation from $XML_LOG_FILE
  if [ -z "$XML_LOG_FILE" ] ; then
    initt="NA"
    memreft="NA"
    indvart="NA"
    reusegent="NA"
    reusecalt="NA"
  else
    initt=`grep time "$XML_LOG_FILE" | cut -f2 -d: | cut -f2 -d' ' | sed -n 1p`
    memreft=`grep time "$XML_LOG_FILE" | cut -f2 -d: | cut -f2 -d' ' | sed -n 2p`
    indvart=`grep time "$XML_LOG_FILE" | cut -f2 -d: | cut -f2 -d' ' | sed -n 3p`
    reusegent=`grep time "$XML_LOG_FILE" | cut -f2 -d: | cut -f2 -d' ' | sed -n 4p`
    reusecalt=`grep time "$XML_LOG_FILE" | cut -f2 -d: | cut -f2 -d' ' | sed -n 5p`
  fi
  # times lockms
  initt2=`grep time: "${MODEL_LOG_FILE}" | cut -f2 -d: | cut -f2 -d' ' | sed -n 1p`
  modelt=`grep time: "${MODEL_LOG_FILE}" | cut -f2 -d: | cut -f2 -d' ' | sed -n 2p`
  if [ -z "$modelt" ] ; then # not required if all goes well
    modelt="NA"
	firstMILPt="NA"
	solvet="NA"
	wcet="NA"
	useddrps="NA"
  else
    # results and more times
    firstMILPt=`grep "First MILP" "$fi" | cut -f2 -dP | cut -f1 -ds`
    if [ -z "$firstMILPt" ] ; then
	  firstMILPt="NA"
	fi
    solvet=`grep "CPU Time for solving:" "$fi" | cut -f2 -d: | cut -f1 -ds`
    wcet=`grep "Value of objective function:" "$fi" | cut -f2 -d:`
    useddrps=`grep Used_DRPs "$fi" | cut -f3 -ds`
    if [ -z "$useddrps" ] ; then
      useddrps="NA"
    fi
  fi
  # log summary
#  dominant=`grep 'Reused dominant memory references:' "${BENCHDIR}/${name}_O${olevel}_arm.log" | cut -f5 -d' '`
#  refs=`grep 'Reused dominant memory references:' "${BENCHDIR}/${name}_O${olevel}_arm.log" | cut -f7 -d' '`
#  if [ -z "$dominant" ] ; then
#    dominant="NA"
#    refs="NA"
#  fi
  # baselines
  wcetah=`grep "Value of objective function:" "${BENCHDIR}/${name}_O${olevel}_arm_${icache}_ah_0_0.lp.solved" | cut -f2 -d:`
  wcetam=`grep "Value of objective function:" "${BENCHDIR}/${name}_O${olevel}_arm_${icache}_dm_0_0.lp.solved" | cut -f2 -d:`
  wcetfm=`grep "Value of objective function:" "${BENCHDIR}/${name}_O${olevel}_arm_${icache}_fm_0_0.lp.solved" | cut -f2 -d:`
  # print all
  echo "$name $olevel $pcs $pcsngr $dcache $sets $ways $wcet $useddrps $initt $memreft $indvart $reusegent $reusecalt $initt2 $modelt $firstMILPt $solvet $wcetah $wcetam $wcetfm"
done | sed 's/  */ /g' | sort >> "$BENCHDIR/$OUTFILE"
