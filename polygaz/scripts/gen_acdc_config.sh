#!/bin/sh

# Grabs DRP variables and values from lp_solve results and
# generates a .xml with them.

source=$1

if [ "$source" ] ; then
	echo '<?xml version="1.0"?>'
	echo '<acdc_config>'
	for addr in `grep '^DRP.*1$' "$source" | cut -f2 -d_ | cut -f1 -d' ' | sort` ; do
		echo "  <entry>"
		echo "    <pc>$addr</pc>"
		echo "  </entry>"
	done
	echo '</acdc_config>'
else
	echo Usage: $0 lpfile.solved
#	echo '<?xml version="1.0"?>'
#	echo '<acdc_config>'
#	grep '^DRP.*1$' | cut -f2 -d_ | cut -f1 -d' ' | sort |
#	while read addr ; do
#		echo "  <entry>"
#		echo "    <pc>$addr</pc>"
#		echo "  </entry>"
#	done
#   echo '</acdc_config>'
fi
