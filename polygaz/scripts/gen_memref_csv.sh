#!/bin/sh
#
# Grabs statistics from memory references (.xml files) and generates
# a .csv file with results
# Requires a parameter to indicate if memory has been tracked in the xmls
#

ANADIR="$HOME/polygaz/"
ANACMD="$ANADIR/venv/bin/python3 -O $ANADIR/xmlparse.py"
TMPFILE="tempfile"
OUTFILE=memrefdata.csv

mem=$1 # yes/no

echo "Name OLevel CFG MemState PCs PCsNGR Refs RefsNGR" > "$OUTFILE"
for XML in `ls -1 *arm.xml` ; do
  echo $XML

  $ANACMD $XML > $TMPFILE

  name=`basename "$XML" | sed "s/_O.*//g"`
  olevel=`echo "$XML" | sed "s/^.*_O//g" | sed "s/_arm.*//g"`
  
  cfg=`grep 'XML CFG type:' "${TMPFILE}" | cut -f4 -d' '`
  pcs=`grep 'XML PCs:' "${TMPFILE}" | cut -f3 -d' '`
  pcsngr=`grep 'XML PCs without' "${TMPFILE}" | cut -f6 -d' '`
  refs=`grep 'XML memory references:' "${TMPFILE}" | cut -f4 -d' '`
  refsngr=`grep 'XML memory references without' "${TMPFILE}" | cut -f7 -d' '`

  echo "$name $olevel $cfg $mem $pcs $pcsngr $refs $refsngr" >> "$OUTFILE"

  rm "$TMPFILE"
done
