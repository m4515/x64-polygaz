#!/bin/sh

# Generates ACDC config file (DRP PCs) from estimations from no-cache IPET
# The  configuration is STATIC (DRPs fixed for the whole execution)

source=$1
max=$2

echo '<?xml version="1.0"?>'
echo '<acdc_config>'
echo '  <loadpoint id="0" pc="0">'
for addr in `grep '^IMPR' "$source" | grep -v '-' | grep -v ' 0$' | sort -k 2gr | cut -f2 -d_ | cut -f1 -d' ' | head -n $max | sort` ; do
		echo "    <pc>$addr</pc>"
done
echo "  </loadpoint>"
echo '</acdc_config>'
