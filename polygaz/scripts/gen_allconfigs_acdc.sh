#!/bin/sh

# Generates multiple configuration files from a multilock configuration file

source=$1
max=$(echo $source | sed 's/.*acdc_//' | sed 's/_m\.xml//')

for num in $(seq 1 $(expr $max - 1)) ; do

    filename=$(echo $source | sed "s/\.xml/_$num\.xml/")
    awk -v maxpcs=$max -v maxcommonpcs=$num -v maxlooppcs=$(expr $max - $num) '
        /<loadpoint/ {
            idstr=$2;
            numpcs=0;
            print $0
            lastcommonpcline="    <pc>0x0</pc>"
        }
        /<pc>/ {
            numpcs++;
            if (idstr=="id=\"0\"") {
                if (numpcs<=maxcommonpcs) {
                    print $0
                    lastcommonpcline=$0
                }
            } else {
                if (numpcs<=maxlooppcs) {
                    print $0
                }
            }
        }
        $0 !~ /pc/ {
            if (idstr=="id=\"0\"") {
                while (numpcs<maxcommonpcs) {
                    print lastcommonpcline
                    numpcs++;
                }
            } else if (numpcs==0 && lastcommonpcline!="") {
                    print lastcommonpcline
            }
            print $0
        }
    ' $source > $filename

done
