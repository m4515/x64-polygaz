#!/bin/sh

# Launches the analysis and solves ILP models of multiple ACDC configs

#set -x
ACDC_CONFIG_FILE="$1" # acdc_config_file
ACDC_DC_LINES="$2" # acdc_ways

# Parallel jobs to launch
JOBS=-2  # 1 per processor, except for 2 processors

ANADIR="$HOME/polygaz/"
IPET="venv/bin/python3 -u -O ipet.py"
SOLVER="lp_solve -R -time"

icache="locked"
IC_SETS=64
IC_WAYS=8

BENCHDIR="$HOME/data/"

echo "Starting WCET analysis of $ACDC_CONFIG_FILE ($ACDC_DC_LINES ways)"
#echo ":$FILE:"
#echo ":$ACDC_DC_LINES:"

#ORIGDIR=`pwd`
cd "$ANADIR" # required to find the python virtual environment

DRP_FILEDIR=`echo "$ACDC_CONFIG_FILE" | sed 's/_$//g' | sed 's/\.xml.*/\.xml/g'`
DRP_FILE=`basename $DRP_FILEDIR` # name_Ox_arm_acdc_x.xml
NAME=`echo "$DRP_FILE" | sed 's/_acdc_.*//g'` # name_Ox_arm
ELF=`echo "$BENCHDIR/${NAME}"`
#MAX_CONFIG=`echo "$ACDC_CONFIG_FILE" | sed 's/_$//g' | sed 's/^.*_//g'`
RESULTFILE=`echo "$BENCHDIR/$DRP_FILE" | sed 's/\.xml.*/\.xml_wcet/g'` # name_Ox_arm
# CANCELADOR DE EXPERIMENTOS
# ITERFILE=`echo "$DRP_FILE" | sed "s/\.xml/\.iter/g"`
# if [ "`cat \"$ITERFILE\"`" = "200" ] ; then
#     echo Reached iteration 200. Aborting.
#     echo Invalid file > "$RESULTFILE"
#     exit
# else
#     expr `cat "$ITERFILE"` + 1 > "$ITERFILE"
# fi


ind=0
#MAX_CONFIG=98
#while [ $ind -le $MAX_CONFIG ] ; do
while [ -f "$BENCHDIR/${DRP_FILE}_${ind}" ] ; do
    DRP_TARGET_FILE=`echo "$BENCHDIR/${NAME}_acdc_${ACDC_DC_LINES}.xml"`
    LPFILE=`echo "$BENCHDIR/${NAME}_locked_64_8_acdc_1_${ACDC_DC_LINES}_${ind}.lp"`
    LOG=`echo "$LPFILE" | sed "s/\.lp/\.log/g"`
    LPRESULT="${LPFILE}.solved"
    #echo "$ANADIR/$IPET $icache $IC_SETS $IC_WAYS acdc $ACDC_DC_LINES $ELF > $LOG 2>&1 && \
    #$SOLVER $LPFILE > ${LPRESULT}_${ind} 2>&1" >> paralelized
#    echo "cp $BENCHDIR/${DRP_FILE}_${ind} $BENCHDIR/$DRP_FILE && \
#    $ANADIR/$IPET $icache $IC_SETS $IC_WAYS acdc $ACDC_DC_LINES $ELF > $LPFILE 2> $LOG && \
#    $SOLVER $LPFILE > ${LPRESULT} 2>&1"
    #cp $BENCHDIR/${DRP_FILE}_${ind} $BENCHDIR/$DRP_FILE && \
#    cp $BENCHDIR/${DRP_FILE}_${ind} $DRP_TARGET_FILE && \
#    $ANADIR/$IPET $icache $IC_SETS $IC_WAYS acdc $ACDC_DC_LINES $ELF > $LPFILE 2> $LOG && \
#    $SOLVER $LPFILE > ${LPRESULT} 2>&1
    cp $BENCHDIR/${DRP_FILE}_${ind} $DRP_TARGET_FILE
    $ANADIR/$IPET $icache $IC_SETS $IC_WAYS acdc $ACDC_DC_LINES $ELF > $LPFILE 2> $LOG
    $SOLVER $LPFILE > ${LPRESULT} 2>&1
    ind=`expr $ind + 1`
done #| parallel -j $JOBS --delay 1 #--bar # OJO DELAY: todos los jobs usan el mismo fichero de configuración de DRPs

# Generate resulting xml file
#RESULTFILE=`echo "$BENCHDIR/$DRP_FILE" | sed 's/\.xml/_results\.xm/g'`
echo '<?xml version="1.0"?>' > $RESULTFILE
echo '  <individuals_wcet>' >> $RESULTFILE
for fi in $BENCHDIR/${NAME}_locked_64_8_acdc_1_${ACDC_DC_LINES}_*.lp.solved ; do
    #id=`echo $fi | sed 's/^.*_//g'`
    id=`echo $fi | sed 's/^.*_\([^_]*\)\.lp\.solved/\1/g'`
    wcet=`grep "Value of objective function:" $fi | cut -f2 -d: | sed 's/ //g' | sed 's/\.0*$//g'`
    echo '    <entry>' >> $RESULTFILE
    echo "      <id>$id</id>" >> $RESULTFILE
    echo "      <value>$wcet</value>" >> $RESULTFILE
    echo '    </entry>' >> $RESULTFILE
#    echo "    <entry>\n      <id>$id</id>\n      <value>$wcet</value>\n    </entry>" >> $RESULTFILE
done
echo '  <individuals_wcet>' >> $RESULTFILE
