#!/bin/sh

#ANADIR="$HOME/investigacio/treal/analizadores/polygaz"
ANADIR="$HOME/polygaz/"
IPET="venv/bin/python3 -u -O ipet.py"
LOCKMS="venv/bin/python3 -u -O lockMS.py"

icache="locked"
IC_SETS=64
IC_WAYS=8
ACDC_DC_LINES="4 8 16 32" # 256-2048 B netos
# 4*(64+4+4) - X; 288-2304 B brutos (data+tag+AC)
#LRU_WAYS="1 2 4 8" # 4-32 KB netos
dcache="acdc"

BENCHDIR="$HOME/data/"

OUTFILE="genetic_results.csv"

###################### GENERATE CSV FILE
cp "$BENCHDIR/$OUTFILE" "$BENCHDIR/${OUTFILE}.old"
echo "Name,OLevel,PCs,PCsNGR,DCache,Sets,Ways,Algor,Iter,DRPs,WCET,LoopsL1" > "$BENCHDIR/$OUTFILE"
# faltaria Iter, BestWCET, UsedDRPs, temps
for fi in `find "$BENCHDIR" -name "*.xml_best_value"` ; do
  #echo $fi >&2
  #XML_LOG_PATTERN=`echo "$fi" | sed "s/_arm_.*$/_arm_\*.reuse/g"`
  #XML_LOG_FILE=`ls -1 $XML_LOG_PATTERN`
  # config
#  if [ `dirname $fi` = "$BENCHDIR" ] ; then
#      algor="none"
#  else
#      algor=`dirname $fi | sed 's/\/.*\///g'`
#  fi
  #solved="$fi"
  solved=`echo $fi | sed "s/_acdc_\([0-9][0-9]*\).*/_${icache}_${IC_SETS}_${IC_WAYS}_${dcache}_1_\1_0\.lp\.solved/g"`
  algor="genetic"
  name=`basename "$solved" | sed "s/_O.*//g"`
  olevel=`echo "$solved" | sed "s/^.*_O//g" | sed "s/_arm.*//g"`
  parameters=`echo "$solved" | sed "s/^.*arm_//g" | sed "s/.lp.solved//g"`
  icache=`echo "$parameters" | cut -f1 -d_`
  isets=`echo "$parameters" | cut -f2 -d_`
  iways=`echo "$parameters" | cut -f3 -d_`
  dcache=`echo "$parameters" | cut -f4 -d_`
  sets=`echo "$parameters" | cut -f5 -d_`
  ways=`echo "$parameters" | cut -f6 -d_`
  # xml summary
  #MODEL_LOG_FILE="${BENCHDIR}/${name}_O${olevel}_arm_${dcache}_${sets}_${ways}.log"
  #MODEL_LOG_FILE=`echo "$fi" | sed 's/.lp.solved$/.log/g'`
  GENETIC_LOG_FILE=`echo "$fi" | sed 's/\.xml_best_.*$/\.log/g'`
  MODEL_LOG_FILE=`echo "$solved" | sed 's/\.lp.solved$/\.log/g'`
  pcs=`grep 'XML PCs:' "${MODEL_LOG_FILE}" | head -n 1 | cut -f3 -d' '`
  pcsngr=`grep 'XML PCs without' "${MODEL_LOG_FILE}" | head -n 1 | cut -f6 -d' '`
  # flowfacts
  FLOW_FACTS_FILE="${name}_O${olevel}_arm.ff"
  loops=$(grep '^loop' "$FLOW_FACTS_FILE" | wc -l)
  # times lockms
#  initt=`grep time: "${MODEL_LOG_FILE}" | cut -f2 -d: | cut -f2 -d' ' | sed -n 1p`
#  modelt=`grep time: "${MODEL_LOG_FILE}" | cut -f2 -d: | cut -f2 -d' ' | sed -n 2p`
#  if [ -z "$modelt" ] ; then # not required if all goes well
#    modelt="NA"
#	firstMILPt="NA"
#	solvet="NA"
#	wcet="NA"
#	useddrps="NA"
#  else
#    # results and more times
#    firstMILPt=`grep "First MILP" "$fi" | cut -f2 -dP | cut -f1 -ds`
#    if [ -z "$firstMILPt" ] ; then
#	  firstMILPt="NA"
#	fi
#    solvet=`grep "CPU Time for solving:" "$fi" | cut -f2 -d: | cut -f1 -ds`

    #wcet=`grep "Value of objective function:" "$solved" | cut -f2 -d:`
    if [ "$ways" -eq 0 ] ; then
        useddrps=0
    else
        ACDC_XML_FILE=`dirname $fi`"/${name}_O${olevel}_arm_acdc_${ways}.xml"
        useddrps=`grep '<pc>' "$ACDC_XML_FILE" | wc -l`
    fi
#    useddrps=`grep Used_DRPs "$fi" | cut -f3 -ds`
    if [ -z "$useddrps" ] ; then
      useddrps="NA"
    fi
  # log summary
  # baselines
#  wcetah=`grep "Value of objective function:" "${BENCHDIR}/${name}_O${olevel}_arm_${icache}_ah_0_0.lp.solved" | cut -f2 -d:`
#  wcetam=`grep "Value of objective function:" "${BENCHDIR}/${name}_O${olevel}_arm_${icache}_dm_0_0.lp.solved" | cut -f2 -d:`
#  wcetfm=`grep "Value of objective function:" "${BENCHDIR}/${name}_O${olevel}_arm_${icache}_fm_0_0.lp.solved" | cut -f2 -d:`
  # print all
#  echo "$name $olevel $pcs $pcsngr $dcache $sets $ways $algor $wcet $useddrps $initt $modelt $firstMILPt $solvet" # $wcetah $wcetam $wcetfm"

  while read line ; do
    wcet=`echo $line | cut -f1 -d' '`
    iter=`echo $line | cut -f2 -d' '`
    useddrps=$(grep '<pc>' $(echo $fi | sed "s/best_value/best_$iter/") | sort | uniq | wc -l)
    echo "$name,$olevel,$pcs,$pcsngr,$dcache,$sets,$ways,$algor,$iter,$useddrps,$wcet,$loops"
  done < $fi

done | sed 's/  */ /g' | sort >> "$BENCHDIR/$OUTFILE"
