#!/bin/sh

#ANADIR="$HOME/investigacio/treal/analizadores/polygaz"
ANADIR="/home/alvaro/TFG/polygaz/"
#ANACMD="venv/bin/python3 -u -O reuse_detection.py"
ANACMD="venv/bin/python3 -u reuse_detection.py"
#CFGTYPE="fast"
CFGTYPE="emulated"

#BENCHDIR="$HOME/investigacio/treal/benchmarks/binarios_datos/"
#BENCHDIR="$HOME/TFG/Benchmarks"
BENCHDIR="/home/alvaro/TFG/Benchmarks/tacle-bench/bench/sequential/cjpeg_wrbmp/bin/O2"
#BENCHDIR="$HOME/TFG/Benchmarks/tacle-bench/bench/debug/custom"
# lanza experimentos
cd "$ANADIR"
for fi in `find "$BENCHDIR" -name "*.ff"` ; do
  #echo "$fi" >&2 # print name on stderr
  FF="$fi"
  #ELF=`echo "$fi" | sed 's/\.ff$/\.elf/g'`
  ELF=`echo "$fi" | sed 's/\.ff$//g'`
  LOG=`echo "$fi" | sed "s/\.ff\$/_${CFGTYPE}\.reuse/g"`
  XML=`echo "$fi" | sed 's/\.ff$/\.xml/g'`
  #XML=`echo "$fi" | sed 's/\.ff$/_${CFGTYPE}\.xml/g'`
  if [ ! -e $XML ] ; then
    echo "$ANADIR/$ANACMD $CFGTYPE $ELF > $LOG 2>&1"
  fi

done | parallel -j 50% # --memfree 16G
