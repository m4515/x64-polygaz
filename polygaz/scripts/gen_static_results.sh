#!/bin/sh

ANADIR="/home/jsegarra/investigacio/treal/analizadores/polygaz"
ANACMD="venv/bin/python3 -u -O analysis1.py" # -u sincroniza output C/python; -O optimiza python (asume __debug__=FALSE)

OUTFILE=./results.csv
OUTFILEOLD=./results_old.csv

#BENCHDIR="/home/jsegarra/investigacio/treal/benchmarks/binarios_datos"
BENCHDIR="."

echo Benchmark directory: "$BENCHDIR"
# recopila (sobreescribe) resultados en OUTFILE
  mv "$OUTFILE" "$OUTFILEOLD" 2>/dev/null
#  printf "Name OLevel LineSize NotKnown Array Scalar Reuse ReuseType Type Dominant Refs TimeT Time\n" > "$OUTFILE"
#  for fi in `find "$BENCHDIR" -name "*.reuse"` ; do
#    #name=`echo "$fi" | sed "s/^.\///g" | sed "s/\/.*$//g"`
#    name=`basename "$fi" | sed "s/_O.*//g"`
#    olevel=`echo "$fi" | sed "s/^.*_O//g" | sed "s/_arm*//g" | sed "s/\.reuse//g"`
#    linesize=`grep "line size:" "$fi" | cut -f4 -d' '`
#    notknown=`grep Known: "$fi" | cut -f3 -d' '`
#    array=`grep Array: "$fi" | cut -f2 -d' '`
#    scalar=`grep Scalar: "$fi" | cut -f2 -d' '`
#    temporal=`grep "Scalar temporal reuse" "$fi" | cut -f4 -d' '`
#    arraytemporal=`grep "Array temporal reuse" "$fi" | cut -f4 -d' '`
#    notknowntemporal=`grep "Non-linear temporal reuse" "$fi" | cut -f4 -d' '`
#    selfsca=`grep "Scalar self reuse" "$fi" | cut -f4 -d' '`
#    notreused=`grep "not reused" "$fi" | cut -f4 -d' '`
#    spatial=`grep "tride reuse" "$fi" | cut -f5 -d' '`
#    longspatial=`grep "longstrd reuse" "$fi" | cut -f5 -d' '`
#    #hits=`grep "ACDC hits" "$fi" | cut -f3 -d' '`
#    #misses=`grep "ACDC misses" "$fi" | cut -f3 -d' '`
#    #aclines=`grep 'Lines:' "$fi" | cut -f4 -d' '`
#    dominant=`grep 'Reused dominant memory references:' "$fi" | cut -f5 -d' '`
#    refs=`grep 'Reused dominant memory references:' "$fi" | cut -f7 -d' '`
#    initt=`grep time "$fi" | cut -f2 -d: | cut -f2 -d' ' | sed -n 1p`
#    memreft=`grep time "$fi" | cut -f2 -d: | cut -f2 -d' ' | sed -n 2p`
#    indvart=`grep time "$fi" | cut -f2 -d: | cut -f2 -d' ' | sed -n 3p`
#    reusegent=`grep time "$fi" | cut -f2 -d: | cut -f2 -d' ' | sed -n 4p`
#    reusecalt=`grep time "$fi" | cut -f2 -d: | cut -f2 -d' ' | sed -n 5p`
#    #scalines=`grep 'Lines (scalar)' "$fi" | cut -f5 -d' '`
#    #arrlines=`grep 'Lines (array)' "$fi" | cut -f5 -d' '`
#
#	printf "$name $olevel $linesize $notknown $array $scalar $notreused NotReused scalar $dominant $refs init $initt\n"
#	printf "$name $olevel $linesize $notknown $array $scalar $selfsca ScaSelf scalar $dominant $refs memref $memreft\n"
#	printf "$name $olevel $linesize $notknown $array $scalar $temporal ScaTemp scalar $dominant $refs indvar $indvart\n"
#	printf "$name $olevel $linesize $notknown $array $scalar $arraytemporal ArrayTemp array $dominant $refs reusegen $reusegent\n"
#	printf "$name $olevel $linesize $notknown $array $scalar $spatial ShortSpa array $dominant $refs reusecal $reusecalt\n"
#	printf "$name $olevel $linesize $notknown $array $scalar $longspatial LongSpa array $dominant $refs init 0\n"
#	printf "$name $olevel $linesize $notknown $array $scalar `expr $notknown - $notknowntemporal` NonLinear nonlinear $dominant $refs init 0\n"
#	printf "$name $olevel $linesize $notknown $array $scalar $notknowntemporal NLTemp nonlinear $dominant $refs init 0\n"


  # 1 fila por experimento
  printf "Name OLevel LineSize NotKnown Array Scalar NLNotReused NLTemp ShortSpa LongSpa ArrayTemp ScaNotReused ScaSelf ScaTemp Dominant Refs initT memrefT indvarT reusegenT reusecalT\n" > "$OUTFILE"
  for fi in `find "$BENCHDIR" -name "*.reuse"` ; do
    #name=`echo "$fi" | sed "s/^.\///g" | sed "s/\/.*$//g"`
    name=`basename "$fi" | sed "s/_O.*//g"`
    olevel=`echo "$fi" | sed "s/^.*_O//g" | sed "s/_arm*//g" | sed "s/\.reuse//g"`
    linesize=`grep "line size:" "$fi" | cut -f4 -d' '`
    notknown=`grep Known: "$fi" | cut -f3 -d' '`
    array=`grep Array: "$fi" | cut -f2 -d' '`
    scalar=`grep Scalar: "$fi" | cut -f2 -d' '`
    temporal=`grep "Scalar temporal reuse" "$fi" | cut -f4 -d' '`
    arraytemporal=`grep "Array temporal reuse" "$fi" | cut -f4 -d' '`
    notknowntemporal=`grep "Non-linear temporal reuse" "$fi" | cut -f4 -d' '`
    selfsca=`grep "Scalar self reuse" "$fi" | cut -f4 -d' '`
    notreused=`grep "not reused" "$fi" | cut -f4 -d' '`
    spatial=`grep "tride reuse" "$fi" | cut -f5 -d' '`
    longspatial=`grep "longstrd reuse" "$fi" | cut -f5 -d' '`
    #hits=`grep "ACDC hits" "$fi" | cut -f3 -d' '`
    #misses=`grep "ACDC misses" "$fi" | cut -f3 -d' '`
    #aclines=`grep 'Lines:' "$fi" | cut -f4 -d' '`
    dominant=`grep 'Reused dominant memory references:' "$fi" | cut -f5 -d' '`
    refs=`grep 'Reused dominant memory references:' "$fi" | cut -f7 -d' '`
    initt=`grep time "$fi" | cut -f2 -d: | cut -f2 -d' ' | sed -n 1p`
    memreft=`grep time "$fi" | cut -f2 -d: | cut -f2 -d' ' | sed -n 2p`
    indvart=`grep time "$fi" | cut -f2 -d: | cut -f2 -d' ' | sed -n 3p`
    reusegent=`grep time "$fi" | cut -f2 -d: | cut -f2 -d' ' | sed -n 4p`
    reusecalt=`grep time "$fi" | cut -f2 -d: | cut -f2 -d' ' | sed -n 5p`
    #scalines=`grep 'Lines (scalar)' "$fi" | cut -f5 -d' '`
    #arrlines=`grep 'Lines (array)' "$fi" | cut -f5 -d' '`

	printf "$name $olevel $linesize $notknown $array $scalar `expr $notknown - $notknowntemporal` $notknowntemporal $spatial $longspatial $arraytemporal $notreused $selfsca $temporal $dominant $refs $initt $memreft $indvart $reusegent $reusecalt\n"


  done | sort >> "$OUTFILE"
