##
##
## FORMATO n LINEAS
##
##

library(ggplot2)
library(tikzDevice)


#setwd("/home/jsegarra/alba/binariosPruebasPhases_gcc6_data/")
#setwd("/home/jsegarra/alba/binariosFP_gcc6/")
#setwd("/home/jsegarra/alba/binarios_datos/")
setwd("/home/jsegarra/investigacio/treal/analizadores/polygaz/")

b<-read.csv(file="results.csv",sep=" ");
#b$X<-NULL



#b$OLevel<-factor(b$OLevel,levels=c("0","s","1","2","3"))
b$OLevel<-as.factor(b$OLevel)

#b$Name<-droplevels(b$Name)
# ordena por el número de entradas en AC para -O0
#b$Name<-factor(b$Name,levels=levels(reorder(levels(b$Name),b$ScaLines[b$OLevel=="0" & b$ReuseType=="ScaTemp"]+b$ArrayLines[b$OLevel=="0" & b$ReuseType=="ScaTemp"],FUN=min)))

# quitar benchmarks sin accesos a datos
#b<-b[b$Name!="janne_complex",]
# quitar benchmarks repetidos
# quitar los que fallan por assert de otawa
#b<-b[b$Name!="qsort-exam",] ----> versión entera funciona
# quitar resto

# factorizar ReuseType
#factor(b$ReuseType,levels=c("NonLinear","NotReused","LongSpa","ShortSpa","ScaSelf","ArrayTemp","ScaTemp","NLTemp"))
factor(b$ReuseType,levels=c("ScaTemp","ArrayTemp","NLTemp","ScaSelf","ShortSpa","LongSpa","NotReused","NonLinear"))

bcharac<-b

b2<-b
#b2$ReuseType<-factor(b2$ReuseType,levels=c("ScaTemp","ArrayTemp","NLTemp","ScaSelf","ShortSpa","LongSpa","NotReused","NonLinear"))
# orden por tipo de hits
b2$ReuseType<-factor(b2$ReuseType,levels=c("NonLinear","NotReused","LongSpa","ShortSpa","ScaSelf","NLTemp","ArrayTemp","ScaTemp"))
# orden por tipo de acceso
#b2$ReuseType<- factor(b2$ReuseType,levels=c("ScaTemp","ScaSelf","NotReused","ArrayTemp","ShortSpa","LongSpa","NLTemp","NonLinear"))
#levels(b2$ReuseType)[levels(b2$ReuseType)=="ScaTemp"]<-"Group-temporal (scalar)"
#levels(b2$ReuseType)[levels(b2$ReuseType)=="NonLinear"]<-"Not reusing (non-linear)"
#levels(b2$ReuseType)[levels(b2$ReuseType)=="NotReused"]<-"Not reusing (scalar)"
#levels(b2$ReuseType)[levels(b2$ReuseType)=="LongSpa"]<-"Long self-spatial (array)"
#levels(b2$ReuseType)[levels(b2$ReuseType)=="ShortSpa"]<-"Short self-spatial (array)"
#levels(b2$ReuseType)[levels(b2$ReuseType)=="ScaSelf"]<-"Self-temporal (scalar)"
#levels(b2$ReuseType)[levels(b2$ReuseType)=="ArrayTemp"]<-"Group-temporal (array)"
#levels(b2$ReuseType)[levels(b2$ReuseType)=="NLTemp"]<-"Group-temporal (non-linear)"

# para latex, quitar nombres con _
#levels(b$Name)[levels(b$Name)=="janne_complex"]<-"janne\\_complex"


#b$ACLines<-b$ScaLines+b$ArrayLines


## (1) TIPO REUSO LINEAL + AVERAGE

# # # # GreenPink, diverging
library(RColorBrewer)
#my_orange = brewer.pal(n = 9, "OrRd")[3:9] #there are 9, I exluded the two lighter hues
#my_orange = brewer.pal(n = 6, "PiYG")[c(2,3,5,6)] #there are 9, I exluded the two lighter hues
#my_orange = brewer.pal(n = 7, "PiYG")[c(1,4,5,6,7)] #there are 9, I exluded the two lighter hues
#my_orange = brewer.pal(n = 9, "PiYG")[c(1,5,6,7,8,9)]
#my_orange = brewer.pal(n = 10, "PiYG")[c(1,5,6,7,8,9,10)]
#my_orange = brewer.pal(n = 10, "PiYG")[c(10,9,8,7,6,5,1)]

#b2$ReuseType<-factor(b2$ReuseType,levels=c("NonLinear","NotReused","LongSpa","ShortSpa","ScaSelf","ArrayTemp","ScaTemp","NLTemp"))
#b3<-b2[b2$ReuseType!="Non-linear",]
#b3<-b2[b2$ReuseType!="NonLinear",]

b3<-b2
# añadir media a datos, por OLevel por benchmark
#b3$Percent<-100*b3$Reuse/(b3$Hits+b3$Misses)
b3$Percent<-100*b3$Reuse/(b3$NotKnown+b3$Array+b3$Scalar)
b3$Avg<-ave(b3$Percent,b3$OLevel,b3$ReuseType,FUN=mean)

b3<-rbind(b3,c(NA,"0",32,0,0,0,0,"ScaSelf","scalar",0,0,head(b3$Avg[b3$OLevel=="0" & b3$ReuseType=="ScaSelf"],n=1),0))
b3<-rbind(b3,c(NA,"1",32,0,0,0,0,"ScaSelf","scalar",0,0,head(b3$Avg[b3$OLevel=="1" & b3$ReuseType=="ScaSelf"],n=1),0))
b3<-rbind(b3,c(NA,"2",32,0,0,0,0,"ScaSelf","scalar",0,0,head(b3$Avg[b3$OLevel=="2" & b3$ReuseType=="ScaSelf"],n=1),0))
b3<-rbind(b3,c(NA,"3",32,0,0,0,0,"ScaSelf","scalar",0,0,head(b3$Avg[b3$OLevel=="3" & b3$ReuseType=="ScaSelf"],n=1),0))
b3<-rbind(b3,c(NA,"0",32,0,0,0,0,"ScaTemp","scalar",0,0,head(b3$Avg[b3$OLevel=="0" & b3$ReuseType=="ScaTemp"],n=1),0))
b3<-rbind(b3,c(NA,"1",32,0,0,0,0,"ScaTemp","scalar",0,0,head(b3$Avg[b3$OLevel=="1" & b3$ReuseType=="ScaTemp"],n=1),0))
b3<-rbind(b3,c(NA,"2",32,0,0,0,0,"ScaTemp","scalar",0,0,head(b3$Avg[b3$OLevel=="2" & b3$ReuseType=="ScaTemp"],n=1),0))
b3<-rbind(b3,c(NA,"3",32,0,0,0,0,"ScaTemp","scalar",0,0,head(b3$Avg[b3$OLevel=="3" & b3$ReuseType=="ScaTemp"],n=1),0))
b3<-rbind(b3,c(NA,"0",32,0,0,0,0,"ArrayTemp","array",0,0,head(b3$Avg[b3$OLevel=="0" & b3$ReuseType=="ArrayTemp"],n=1),0))
b3<-rbind(b3,c(NA,"1",32,0,0,0,0,"ArrayTemp","array",0,0,head(b3$Avg[b3$OLevel=="1" & b3$ReuseType=="ArrayTemp"],n=1),0))
b3<-rbind(b3,c(NA,"2",32,0,0,0,0,"ArrayTemp","array",0,0,head(b3$Avg[b3$OLevel=="2" & b3$ReuseType=="ArrayTemp"],n=1),0))
b3<-rbind(b3,c(NA,"3",32,0,0,0,0,"ArrayTemp","array",0,0,head(b3$Avg[b3$OLevel=="3" & b3$ReuseType=="ArrayTemp"],n=1),0))
b3<-rbind(b3,c(NA,"0",32,0,0,0,0,"LongSpa","array",0,0,head(b3$Avg[b3$OLevel=="0" & b3$ReuseType=="LongSpa"],n=1),0))
b3<-rbind(b3,c(NA,"1",32,0,0,0,0,"LongSpa","array",0,0,head(b3$Avg[b3$OLevel=="1" & b3$ReuseType=="LongSpa"],n=1),0))
b3<-rbind(b3,c(NA,"2",32,0,0,0,0,"LongSpa","array",0,0,head(b3$Avg[b3$OLevel=="2" & b3$ReuseType=="LongSpa"],n=1),0))
b3<-rbind(b3,c(NA,"3",32,0,0,0,0,"LongSpa","array",0,0,head(b3$Avg[b3$OLevel=="3" & b3$ReuseType=="LongSpa"],n=1),0))
b3<-rbind(b3,c(NA,"0",32,0,0,0,0,"ShortSpa","array",0,0,head(b3$Avg[b3$OLevel=="0" & b3$ReuseType=="ShortSpa"],n=1),0))
b3<-rbind(b3,c(NA,"1",32,0,0,0,0,"ShortSpa","array",0,0,head(b3$Avg[b3$OLevel=="1" & b3$ReuseType=="ShortSpa"],n=1),0))
b3<-rbind(b3,c(NA,"2",32,0,0,0,0,"ShortSpa","array",0,0,head(b3$Avg[b3$OLevel=="2" & b3$ReuseType=="ShortSpa"],n=1),0))
b3<-rbind(b3,c(NA,"3",32,0,0,0,0,"ShortSpa","array",0,0,head(b3$Avg[b3$OLevel=="3" & b3$ReuseType=="ShortSpa"],n=1),0))
b3<-rbind(b3,c(NA,"0",32,0,0,0,0,"NotReused","scalar",0,0,head(b3$Avg[b3$OLevel=="0" & b3$ReuseType=="NotReused"],n=1),0))
b3<-rbind(b3,c(NA,"1",32,0,0,0,0,"NotReused","scalar",0,0,head(b3$Avg[b3$OLevel=="1" & b3$ReuseType=="NotReused"],n=1),0))
b3<-rbind(b3,c(NA,"2",32,0,0,0,0,"NotReused","scalar",0,0,head(b3$Avg[b3$OLevel=="2" & b3$ReuseType=="NotReused"],n=1),0))
b3<-rbind(b3,c(NA,"3",32,0,0,0,0,"NotReused","scalar",0,0,head(b3$Avg[b3$OLevel=="3" & b3$ReuseType=="NotReused"],n=1),0))
b3<-rbind(b3,c(NA,"0",32,0,0,0,0,"NLTemp","nonlinear",0,0,head(b3$Avg[b3$OLevel=="0" & b3$ReuseType=="NLTemp"],n=1),0))
b3<-rbind(b3,c(NA,"1",32,0,0,0,0,"NLTemp","nonlinear",0,0,head(b3$Avg[b3$OLevel=="1" & b3$ReuseType=="NLTemp"],n=1),0))
b3<-rbind(b3,c(NA,"2",32,0,0,0,0,"NLTemp","nonlinear",0,0,head(b3$Avg[b3$OLevel=="2" & b3$ReuseType=="NLTemp"],n=1),0))
b3<-rbind(b3,c(NA,"3",32,0,0,0,0,"NLTemp","nonlinear",0,0,head(b3$Avg[b3$OLevel=="3" & b3$ReuseType=="NLTemp"],n=1),0))
b3<-rbind(b3,c(NA,"0",32,0,0,0,0,"NonLinear","nonlinear",0,0,head(b3$Avg[b3$OLevel=="0" & b3$ReuseType=="NonLinear"],n=1),0))
b3<-rbind(b3,c(NA,"1",32,0,0,0,0,"NonLinear","nonlinear",0,0,head(b3$Avg[b3$OLevel=="1" & b3$ReuseType=="NonLinear"],n=1),0))
b3<-rbind(b3,c(NA,"2",32,0,0,0,0,"NonLinear","nonlinear",0,0,head(b3$Avg[b3$OLevel=="2" & b3$ReuseType=="NonLinear"],n=1),0))
b3<-rbind(b3,c(NA,"3",32,0,0,0,0,"NonLinear","nonlinear",0,0,head(b3$Avg[b3$OLevel=="3" & b3$ReuseType=="NonLinear"],n=1),0))

b3$Percent<-as.double(b3$Percent)
b3$Avg<-as.double(b3$Avg)
b3$Name<-as.character(b3$Name)
b3$Name[is.na(b3$Name)]<-"(average)"
b3$Name<-as.factor(b3$Name)

b3$Name<-factor(b3$Name,levels=c("binarysearch","bsort","countnegative","matrix1","dijkstra","iir","fft","jfdctint","minver","ludcmp","petrinet","lift","g723_enc","(average)",
"complex_updates","cosf","deg2rad","filterbank","fir2dim","huff_dec","insertsort","isqrt","rad2deg","rijndael_dec","rijndael_enc"))

# orden por tipo de reuso y hits
b3$ReuseType<-factor(b3$ReuseType,levels=c("NonLinear","NLTemp","LongSpa","ShortSpa","ArrayTemp","NotReused","ScaSelf","ScaTemp"))
green_aux = brewer.pal(n=3, "Greens")
my_green = green_aux[c(1,3,1,2,3,1,2,3)]

b3$Type<-factor(b3$Type,levels=c("scalar","array","nonlinear"))
levels(b3$Type)[levels(b3$Type)=="scalar"]<-"Scalar"
levels(b3$Type)[levels(b3$Type)=="array"]<-"Array"
levels(b3$Type)[levels(b3$Type)=="nonlinear"]<-"Non-linear"

levels(b3$ReuseType)[levels(b3$ReuseType)=="ScaTemp"]<-"Group-temporal (scalar)"
levels(b3$ReuseType)[levels(b3$ReuseType)=="NonLinear"]<-"Not reusing (non-linear)"
levels(b3$ReuseType)[levels(b3$ReuseType)=="NotReused"]<-"Not reusing (scalar)"
levels(b3$ReuseType)[levels(b3$ReuseType)=="LongSpa"]<-"Long self-spatial (array)"
levels(b3$ReuseType)[levels(b3$ReuseType)=="ShortSpa"]<-"Short self-spatial (array)"
levels(b3$ReuseType)[levels(b3$ReuseType)=="ScaSelf"]<-"Self-temporal (scalar)"
levels(b3$ReuseType)[levels(b3$ReuseType)=="ArrayTemp"]<-"Group-temporal (array)"
levels(b3$ReuseType)[levels(b3$ReuseType)=="NLTemp"]<-"Group-temporal (non-linear)"


# grafica
ggplot(data=b3,aes(x=OLevel,fill=ReuseType)) +geom_bar(aes(y=Percent),stat="identity",position="stack",color="black") +facet_grid(Type~Name) +labs(x="GCC optimization level (-O)",y="Aggregated data accesses (\\%)") +theme(strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), legend.position="bottom") +scale_fill_manual(values=my_green)


## gráfica accesos totales y accesos con reuso aprovechable

b2<-b
# añadir media a datos, por OLevel por benchmark
#b3$Percent<-100*b3$Reuse/(b3$Hits+b3$Misses)
b2$Accesses<-b2$NotKnown+b2$Array+b2$Scalar
b2$MaxAcc<-ave(b2$Accesses,b2$Name,FUN=max)
b2$RelAcc<-b2$Accesses/b2$MaxAcc

b3<-b2[b2$ReuseType!="NotReused" & b2$ReuseType!="LongSpa" & b2$ReuseType!="NonLinear",]
b3$MostlyReuse<-ave(b3$Reuse,b3$Name,b3$OLevel,FUN=sum)
b3$RelMost<-b3$MostlyReuse/b3$MaxAcc

#ggplot(data=b3,aes(x=OLevel,y=RelAcc,group=1)) +geom_line() +geom_point() +facet_grid(.~Name) +geom_point(aes(y=RelMost),color="blue")

ggplot(data=b3,aes(x=OLevel,y=RelAcc,group=1)) +geom_point(size=0.75) +facet_grid(.~Name) +geom_bar(stat="identity",aes(y=RelMost),color="black",fill="darkgreen",position="dodge") +labs(x="GCC optimization level (-O)",y="Number of accesses (relative to O0)") +theme(strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")))

# por tipo reuso
ggplot(data=b3,aes(x=OLevel,y=RelAcc,group=1)) +geom_point(size=0.75) +facet_grid(.~Name) +geom_bar(stat="identity",aes(y=Reuse/MaxAcc,fill=ReuseType),color="black") +labs(x="GCC optimization level (-O)",y="Number of accesses (relative to O0)") +theme(strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), legend.position="bottom")

#ggplot(data=b3,aes(x=OLevel,fill=ReuseType)) +geom_bar(aes(y=Percent),stat="identity",position="stack",color="black",size=0.1) +facet_grid(.~Name) +labs(x="GCC optimization level (-O)",y="Aggregated data accesses (\\%)") +theme(strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), legend.position="bottom") +scale_fill_manual(values=my_orange,breaks=rev(levels(b3$ReuseType)))

#+scale_fill_manual(values=my_orange,breaks=rev(levels(b2$ReuseType)),name="Linear reuse type:",labels=c("Group-temporal (scalar) ","Group-temporal (array) ","Self-temporal (scalar) ","Short self-spatial (array) ","Long self-spatial (array) ","First use (scalar) ","NL-Temp", "Non-Linear"))
#ggplot(data=b3,aes(x=OLevel,fill=ReuseType)) +geom_bar(aes(y=Percent),stat="identity",position="stack",color="black",size=0.1) +facet_grid(.~Name) +labs(x="GCC optimization level (-O)",y="Aggregated data accesses (\\%)") +theme(text=element_text(size=10), strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), legend.position="bottom") +scale_fill_manual(values=my_orange,breaks=rev(levels(b2$ReuseType)),name="Linear reuse type:",labels=c("Self/Group-temporal (scalar)","Group-temporal (array)","Short self-spatial (array)","Long self-spatial (array)","First use (scalar)","Non-Linear"))
ggplot(data=b3,aes(x=OLevel,fill=ReuseType)) +geom_bar(aes(y=Percent),stat="identity",position="stack",color="black",size=0.1) +facet_grid(.~Name) +labs(x="GCC optimization level (-O)",y="Aggregated data accesses (\\%)") +theme(strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), legend.position="bottom") +scale_fill_manual(values=my_orange,breaks=rev(levels(b2$ReuseType)),name="Linear reuse type:",labels=c("Group-temporal (scalar) ","Group-temporal (array) ","Self-temporal (scalar) ","Short self-spatial (array) ","Long self-spatial (array) ","First use (scalar) ","Non-Linear"))
# con etiquetas abreviadas:
#ggplot(data=b3,aes(x=OLevel,fill=ReuseType)) +geom_bar(aes(y=Percent),stat="identity",position="stack",color="black",size=0.1) +facet_grid(.~Name) +labs(x="GCC optimization level (-O)",y="Aggregated data accesses (\\%)") +theme(strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), legend.position="bottom") +scale_fill_manual(values=my_orange,breaks=rev(levels(b2$ReuseType)),name="Linear reuse type:",labels=c("Group-temp (scalar) ","Group-temp (array) ","Self-temp (scalar) ","Short self-spa (array) ","Long self-spa (array) ","First use (scalar) ","Non-Linear"))

tikz(file="ReuseTypeG.tex",width=9,height=5) ; last_plot() ; dev.off() # RTS

tikz(file="ReuseTypeG.tex",width=7,height=4) ; last_plot() ; dev.off() # ECRTS
#tikz(file="ReuseTypeG.tex",width=6,height=3.5) ; last_plot() ; dev.off() # ECRTS con etiqu abreviadas


## Dominant / Refs
# orden por tipo de reuso y hits
b$Name<-factor(b$Name,levels=c(
"filterbank",      #  3
"bsort",           #  5
"deg2rad",         #  7
"rad2deg",         #  8
"iir",             #  9
"matrix1",         # 11
"jfdctint",        # 14
"fir2dim",         # 15
"binarysearch",    # 19
"complex_updates", # 21
"insertsort",      # 25
"countnegative",   # 26
"isqrt",           # 29
"dijkstra",        # 34
"cosf",            # 38
"ludcmp",          # 44
"fft",             # 53
"huff_dec",        # 54
"minver",          # 64
"lift",            # 91 
"g723_enc",        #118 
"rijndael_dec",    #122 
"rijndael_enc"     # ? 
))

ggplot(data=b, aes(x=OLevel,y=100*Dominant/Refs)) +geom_point() +facet_grid(.~Name)

ggplot(data=b, aes(x=OLevel,y=100*Dominant/Refs,colour=OLevel)) +geom_boxplot() +facet_grid(.~Name,margins=TRUE) +theme(strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), legend.position="bottom")


ggplot(data=b, aes(x=OLevel)) +geom_point(aes(y=Dominant,colour=OLevel)) +geom_point(aes(y=Refs)) +facet_grid(.~Name) +theme(strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), legend.position="bottom") +scale_y_continuous(trans="log10")


## TABLA

b2<-b
b2$Accesses<-b2$NotKnown+b2$Array+b2$Scalar
lt1<-b2[b2$ReuseType=="NonLinear",c("Name","OLevel","Dominant","Refs","Accesses")]

lt1$DomRel<-round(lt1$Dominant*100/lt1$Refs)
lt1$Refs=NULL # borra columna Refs
lt1$AccNorm<-ave(lt1$Accesses,lt1$Name,FUN=function(x){x[1]}) # first number (O0) of accesses
lt1$AccRel<-round(lt1$Accesses*100/lt1$AccNorm)
lt1$AccAbsRel<-lt1$AccRel
lt1$AccAbsRel[lt1$OLevel=="0"]<-lt1$Accesses[lt1$OLevel=="0"]
lt1$Accesses<-NULL
lt1$AccNorm<-NULL
lt1$AccRel<-NULL


tabla1<-reshape(lt1,direction="wide",idvar="Name",timevar="OLevel")
tabla2<-tabla1[,c("Name","Dominant.0","DomRel.0","Dominant.1","DomRel.1","Dominant.2","DomRel.2","Dominant.3","DomRel.3","AccAbsRel.0","AccAbsRel.1","AccAbsRel.2","AccAbsRel.3")]


## VIEJO A PARTIR DE AQUI





##1 ACLines por benchmark -> en tabla?
#ggplot(data=b, aes(x=Name,y=ACLines)) +geom_point(aes(color=OLevel)) +theme(axis.text.x = element_text(angle = 90, hjust = 1,vjust=0.5))+geom_hline(yintercept=16, color="green") +geom_hline(yintercept=32, color="orange") +geom_hline(yintercept=64, color="red") +scale_y_continuous(trans="log2") #+scale_color_brewer(palette="Oranges")
#ggplot(data=b, aes(x=OLevel,y=ACLines,group=Name)) +geom_point() +facet_grid(.~Name) +geom_hline(yintercept=16, color="green") +geom_hline(yintercept=32, color="orange") +geom_hline(yintercept=64, color="red") +scale_y_continuous(trans="log2") +labs(x="GCC optimization level (-O)",y="Memory instructions accessing to reusable content") +theme(text=element_text(size=10), strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), legend.position="none")


ggplot(data=b, aes(x=OLevel,group=Name)) +geom_point(aes(y=ScaLines),colour="red") +geom_point(aes(y=ArrayLines),colour="blue") +facet_grid(.~Name) +geom_hline(yintercept=16, color="green") +geom_hline(yintercept=32, color="orange") +geom_hline(yintercept=64, color="red") +scale_y_continuous(trans="log2") +labs(x="GCC optimization level (-O)",y="Memory instructions accessing to reusable content") +theme(text=element_text(size=10), strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), legend.position="none")


pdf(file="ACLines.pdf",width=9,height=6) ; last_plot() ; dev.off()
tikz(file="ACLines.tex",width=7,height=4) ; last_plot() ; dev.off()


## Accesos por benchmark
ggplot(data=b, aes(x=Name,y=(Hits+Misses)))+geom_point(aes(color=OLevel))+theme(axis.text.x = element_text(angle = 90, hjust = 1,vjust=0.5))+scale_y_continuous(trans="log10")


## Tipo reuso LINEAL

b2<-b
b2$ReuseType<-factor(b2$ReuseType,levels=c("NonLinear","NotReused","LongSpa","ShortSpa","Temp"))
b2<-b2[b2$ReuseType!="NonLinear",]
#ggplot(data=b2,aes(x=OLevel,fill=ReuseType)) +geom_bar(aes(y=100*Reuse/(Hits+Misses)),stat="identity",position="stack",color="black",size=0.1) +facet_grid(.~Name) +labs(x="GCC optimization level (-O)",y="Aggregated data accesses (\\%)") +theme(text=element_text(size=10), strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), legend.position="bottom") +scale_fill_brewer(palette="PiYG",breaks=rev(levels(b2$ReuseType)),name="Linear reuse type:",labels=c("Temporal","Short spatial","Long spatial","Not reused","Non-Linear"))

# # # # GreenPink, diverging
library(RColorBrewer)
#my_orange = brewer.pal(n = 9, "OrRd")[3:9] #there are 9, I exluded the two lighter hues
#my_orange = brewer.pal(n = 6, "PiYG")[c(2,3,5,6)] #there are 9, I exluded the two lighter hues
my_orange = brewer.pal(n = 7, "PiYG")[c(3,4,6,7)] #there are 9, I exluded the two lighter hues

ggplot(data=b2,aes(x=OLevel,fill=ReuseType)) +geom_bar(aes(y=100*Reuse/(Hits+Misses)),stat="identity",position="stack",color="black",size=0.1) +facet_grid(.~Name) +labs(x="GCC optimization level (-O)",y="Aggregated data accesses (\\%)") +theme(text=element_text(size=10), strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), legend.position="bottom") +scale_fill_manual(values=my_orange,breaks=rev(levels(b2$ReuseType)),name="Linear reuse type:",labels=c("Temporal","Short spatial","Long spatial","Not reused","Non-Linear"))


tikz(file="ReuseTypeG.tex",width=9,height=5) ; last_plot() ; dev.off() # RTS


## Tipo reuso LINEAL (temp spa y temp array)
b2<-b
b2$ReuseType<-factor(b2$ReuseType,levels=c("NonLinear","NotReused","LongSpa","ShortSpa","ArrayTemp","ScaSelf","ScaTemp"))
b2<-b2[b2$ReuseType!="NonLinear",]
b2$Reuse<-as.double(b2$Reuse)

# # # # GreenPink, diverging
library(RColorBrewer)
#my_orange = brewer.pal(n = 9, "OrRd")[3:9] #there are 9, I exluded the two lighter hues
#my_orange = brewer.pal(n = 6, "PiYG")[c(2,3,5,6)] #there are 9, I exluded the two lighter hues
my_orange = brewer.pal(n = 7, "PiYG")[c(1,3,4,5,6,7)] #there are 9, I exluded the two lighter hues

ggplot(data=b2,aes(x=OLevel,fill=ReuseType)) +geom_bar(aes(y=100*Reuse/(Hits+Misses)),stat="identity",position="stack",color="black",size=0.1) +facet_grid(.~Name) +labs(x="GCC optimization level (-O)",y="Aggregated data accesses (\\%)") +theme(text=element_text(size=10), strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), legend.position="bottom") +scale_fill_manual(values=my_orange,breaks=rev(levels(b2$ReuseType)),name="Linear reuse type:",labels=c("Temporal (scalar)","S-Temporal (scalar)","Temporal (array)","Short spatial (array)","Long spatial (array)","First use (scalar)","Non-Linear"))

tikz(file="ReuseTypeG.tex",width=9,height=5) ; last_plot() ; dev.off() # RTS

# media abajo

## Tipo reuso

#ggplot(data=b,aes(x=Name,fill=ReuseType)) +geom_bar(aes(y=100*Reuse/(Hits+Misses)),stat="identity",position="stack") +facet_grid(OLevel~.) +theme(axis.text.x = element_text(angle = 90, hjust = 1,vjust=0.5))

#ggplot(data=b,aes(x=OLevel,fill=ReuseType)) +geom_bar(aes(y=100*Reuse/(Hits+Misses)),stat="identity",position="stack") +facet_grid(.~Name) +labs(x="GCC ptimization level (-O)",y="Reuse Type (\\%)") +theme(text=element_text(size=8), strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), legend.position="bottom") #+scale_fill_brewer()

#b<-b[order(b$ReuseType,decreasing=FALSE),]
b$ReuseType<-factor(b$ReuseType,levels=c("NonLinear","NotReused","LongSpa","ShortSpa","Temp"))
#b$ReuseType<-factor(b$ReuseType,levels=c("NonLinear","NotReused","LongSpa","ShortSpa","ArrayTemp","ScaTemp"))
#ggplot(data=b,aes(x=OLevel,fill=ReuseType)) +geom_bar(aes(y=100*Reuse/(Hits+Misses)),stat="identity",position="stack") +facet_grid(.~Name) +labs(x="GCC optimization level (-O)",y="Aggregated data accesses (\\%)") +theme(text=element_text(size=10), strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), legend.position="bottom") +scale_fill_brewer(palette="Spectral",breaks=rev(levels(b$ReuseType)),name="Reuse type",labels=c("Temporal","Short spatial","Long spatial","Not reused"))
ggplot(data=b,aes(x=OLevel,fill=ReuseType)) +geom_bar(aes(y=100*Reuse/(Hits+Misses)),stat="identity",position="stack",color="black",size=0.1) +facet_grid(.~Name) +labs(x="GCC optimization level (-O)",y="Aggregated data accesses (\\%)") +theme(text=element_text(size=10), strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), legend.position="bottom") +scale_fill_brewer(palette="PiYG",breaks=rev(levels(b$ReuseType)),name="Reuse type",labels=c("Temporal","Short spatial","Long spatial","Not reused","Non-Linear"))
#ggplot(data=b,aes(x=OLevel,fill=ReuseType)) +geom_bar(aes(y=100*Reuse/(Hits+Misses)),stat="identity",position="stack",color="black",size=0.1) +facet_grid(.~Name) +labs(x="GCC optimization level (-O)",y="Aggregated data accesses (\\%)") +theme(text=element_text(size=10), strip.text.x = element_text(angle = 90, margin = margin(.1, 0, .1, 0, "cm")), legend.position="bottom") +scale_fill_brewer(palette="PiYG",breaks=rev(levels(b$ReuseType)),name="Reuse type",labels=c("Temporal","Short spatial","Long spatial","Not reused","Non-Linear access pattern"))

#pdf(file="ReuseType.pdf",width=6.5,height=3.5) ; last_plot() ; dev.off()
pdf(file="ReuseTypeG.pdf",width=9,height=6) ; last_plot() ; dev.off()
#tikz(file="ReuseTypeG.tex",width=7,height=4.5) ; last_plot() ; dev.off() # ECRTS
tikz(file="ReuseTypeG.tex",width=9,height=5) ; last_plot() ; dev.off() # RTS

# media
ave(b$Reuse/(b$Hits+b$Misses),b$ReuseType,FUN=mean)

# añadir media a b2
# head(ave(b2[b2$OLevel=="0"& b2$ReuseType=="ArrayTemp", c("Reuse")],FUN=mean), n=1)
# colMeans(b2[b2$OLevel=="0"& b2$ReuseType=="ArrayTemp", c(7,9,10)])
b3<-b2
media<-c(NA,"0",32,0,0,0,head(ave(b2[b2$OLevel=="0"& b2$ReuseType=="ArrayTemp", c("Reuse")],FUN=mean), n=1),"ArrayTemp",0,0,0,0,0)
b3<-rbind(b3,media)
media<-c(NA,"0",32,0,0,0,head(ave(b2[b2$OLevel=="1"& b2$ReuseType=="ArrayTemp", c("Reuse")],FUN=mean), n=1),"ArrayTemp",0,0,0,0,0)
b3<-rbind(b3,media)
media<-c(NA,"0",32,0,0,0,head(ave(b2[b2$OLevel=="2"& b2$ReuseType=="ArrayTemp", c("Reuse")],FUN=mean), n=1),"ArrayTemp",0,0,0,0,0)
b3<-rbind(b3,media)
media<-c(NA,"0",32,0,0,0,head(ave(b2[b2$OLevel=="3"& b2$ReuseType=="ArrayTemp", c("Reuse")],FUN=mean), n=1),"ArrayTemp",0,0,0,0,0)
b3<-rbind(b3,media)
media<-c(NA,"0",32,0,0,0,head(ave(b2[b2$OLevel=="0"& b2$ReuseType=="LongSpa", c("Reuse")],FUN=mean), n=1),"LongSpa",0,0,0,0,0)
b3<-rbind(b3,media)
media<-c(NA,"0",32,0,0,0,head(ave(b2[b2$OLevel=="1"& b2$ReuseType=="LongSpa", c("Reuse")],FUN=mean), n=1),"LongSpa",0,0,0,0,0)
b3<-rbind(b3,media)
media<-c(NA,"0",32,0,0,0,head(ave(b2[b2$OLevel=="2"& b2$ReuseType=="LongSpa", c("Reuse")],FUN=mean), n=1),"LongSpa",0,0,0,0,0)
b3<-rbind(b3,media)
media<-c(NA,"0",32,0,0,0,head(ave(b2[b2$OLevel=="3"& b2$ReuseType=="LongSpa", c("Reuse")],FUN=mean), n=1),"LongSpa",0,0,0,0,0)
b3<-rbind(b3,media)
media<-c(NA,"0",32,0,0,0,head(ave(b2[b2$OLevel=="0"& b2$ReuseType=="ShortSpa", c("Reuse")],FUN=mean), n=1),"ShortSpa",0,0,0,0,0)
b3<-rbind(b3,media)
media<-c(NA,"0",32,0,0,0,head(ave(b2[b2$OLevel=="1"& b2$ReuseType=="ShortSpa", c("Reuse")],FUN=mean), n=1),"ShortSpa",0,0,0,0,0)
b3<-rbind(b3,media)
media<-c(NA,"0",32,0,0,0,head(ave(b2[b2$OLevel=="2"& b2$ReuseType=="ShortSpa", c("Reuse")],FUN=mean), n=1),"ShortSpa",0,0,0,0,0)
b3<-rbind(b3,media)
media<-c(NA,"0",32,0,0,0,head(ave(b2[b2$OLevel=="3"& b2$ReuseType=="ShortSpa", c("Reuse")],FUN=mean), n=1),"ShortSpa",0,0,0,0,0)
b3<-rbind(b3,media)
media<-c(NA,"0",32,0,0,0,head(ave(b2[b2$OLevel=="0"& b2$ReuseType=="NotReused", c("Reuse")],FUN=mean), n=1),"NotReused",0,0,0,0,0)
b3<-rbind(b3,media)
media<-c(NA,"0",32,0,0,0,head(ave(b2[b2$OLevel=="1"& b2$ReuseType=="NotReused", c("Reuse")],FUN=mean), n=1),"NotReused",0,0,0,0,0)
b3<-rbind(b3,media)
media<-c(NA,"0",32,0,0,0,head(ave(b2[b2$OLevel=="2"& b2$ReuseType=="NotReused", c("Reuse")],FUN=mean), n=1),"NotReused",0,0,0,0,0)
b3<-rbind(b3,media)
media<-c(NA,"0",32,0,0,0,head(ave(b2[b2$OLevel=="3"& b2$ReuseType=="NotReused", c("Reuse")],FUN=mean), n=1),"NotReused",0,0,0,0,0)
b3<-rbind(b3,media)
media<-c(NA,"0",32,0,0,0,head(ave(b2[b2$OLevel=="0"& b2$ReuseType=="ScaTemp", c("Reuse")],FUN=mean), n=1),"ScaTemp",0,0,0,0,0)
b3<-rbind(b3,media)
media<-c(NA,"0",32,0,0,0,head(ave(b2[b2$OLevel=="1"& b2$ReuseType=="ScaTemp", c("Reuse")],FUN=mean), n=1),"ScaTemp",0,0,0,0,0)
b3<-rbind(b3,media)
media<-c(NA,"0",32,0,0,0,head(ave(b2[b2$OLevel=="2"& b2$ReuseType=="ScaTemp", c("Reuse")],FUN=mean), n=1),"ScaTemp",0,0,0,0,0)
b3<-rbind(b3,media)
media<-c(NA,"0",32,0,0,0,head(ave(b2[b2$OLevel=="3"& b2$ReuseType=="ScaTemp", c("Reuse")],FUN=mean), n=1),"ScaTemp",0,0,0,0,0)
b3<-rbind(b3,media)

# mal, saca media por acceso, y solo se ve reflejado quien más accesos tiene (dijkstra)
b3<-b2
media<-colMeans(b2[b2$OLevel=="0"& b2$ReuseType=="ArrayTemp", c(7,9,10)])
b3<-rbind(b3,c(NA,"0",32,0,0,0,media[1],"ArrayTemp",media[2],media[3],0,0,0))
media<-colMeans(b2[b2$OLevel=="1"& b2$ReuseType=="ArrayTemp", c(7,9,10)])
b3<-rbind(b3,c(NA,"1",32,0,0,0,media[1],"ArrayTemp",media[2],media[3],0,0,0))
media<-colMeans(b2[b2$OLevel=="2"& b2$ReuseType=="ArrayTemp", c(7,9,10)])
b3<-rbind(b3,c(NA,"2",32,0,0,0,media[1],"ArrayTemp",media[2],media[3],0,0,0))
media<-colMeans(b2[b2$OLevel=="3"& b2$ReuseType=="ArrayTemp", c(7,9,10)])
b3<-rbind(b3,c(NA,"3",32,0,0,0,media[1],"ArrayTemp",media[2],media[3],0,0,0))
media<-colMeans(b2[b2$OLevel=="0"& b2$ReuseType=="LongSpa", c(7,9,10)])
b3<-rbind(b3,c(NA,"0",32,0,0,0,media[1],"LongSpa",media[2],media[3],0,0,0))
media<-colMeans(b2[b2$OLevel=="1"& b2$ReuseType=="LongSpa", c(7,9,10)])
b3<-rbind(b3,c(NA,"1",32,0,0,0,media[1],"LongSpa",media[2],media[3],0,0,0))
media<-colMeans(b2[b2$OLevel=="2"& b2$ReuseType=="LongSpa", c(7,9,10)])
b3<-rbind(b3,c(NA,"2",32,0,0,0,media[1],"LongSpa",media[2],media[3],0,0,0))
media<-colMeans(b2[b2$OLevel=="3"& b2$ReuseType=="LongSpa", c(7,9,10)])
b3<-rbind(b3,c(NA,"3",32,0,0,0,media[1],"LongSpa",media[2],media[3],0,0,0))
media<-colMeans(b2[b2$OLevel=="0"& b2$ReuseType=="ShortSpa", c(7,9,10)])
b3<-rbind(b3,c(NA,"0",32,0,0,0,media[1],"ShortSpa",media[2],media[3],0,0,0))
media<-colMeans(b2[b2$OLevel=="1"& b2$ReuseType=="ShortSpa", c(7,9,10)])
b3<-rbind(b3,c(NA,"1",32,0,0,0,media[1],"ShortSpa",media[2],media[3],0,0,0))
media<-colMeans(b2[b2$OLevel=="2"& b2$ReuseType=="ShortSpa", c(7,9,10)])
b3<-rbind(b3,c(NA,"2",32,0,0,0,media[1],"ShortSpa",media[2],media[3],0,0,0))
media<-colMeans(b2[b2$OLevel=="3"& b2$ReuseType=="ShortSpa", c(7,9,10)])
b3<-rbind(b3,c(NA,"3",32,0,0,0,media[1],"ShortSpa",media[2],media[3],0,0,0))
media<-colMeans(b2[b2$OLevel=="0"& b2$ReuseType=="NotReused", c(7,9,10)])
b3<-rbind(b3,c(NA,"0",32,0,0,0,media[1],"NotReused",media[2],media[3],0,0,0))
media<-colMeans(b2[b2$OLevel=="1"& b2$ReuseType=="NotReused", c(7,9,10)])
b3<-rbind(b3,c(NA,"1",32,0,0,0,media[1],"NotReused",media[2],media[3],0,0,0))
media<-colMeans(b2[b2$OLevel=="2"& b2$ReuseType=="NotReused", c(7,9,10)])
b3<-rbind(b3,c(NA,"2",32,0,0,0,media[1],"NotReused",media[2],media[3],0,0,0))
media<-colMeans(b2[b2$OLevel=="3"& b2$ReuseType=="NotReused", c(7,9,10)])
b3<-rbind(b3,c(NA,"3",32,0,0,0,media[1],"NotReused",media[2],media[3],0,0,0))
media<-colMeans(b2[b2$OLevel=="0"& b2$ReuseType=="ScaTemp", c(7,9,10)])
b3<-rbind(b3,c(NA,"0",32,0,0,0,media[1],"ScaTemp",media[2],media[3],0,0,0))
media<-colMeans(b2[b2$OLevel=="1"& b2$ReuseType=="ScaTemp", c(7,9,10)])
b3<-rbind(b3,c(NA,"1",32,0,0,0,media[1],"ScaTemp",media[2],media[3],0,0,0))
media<-colMeans(b2[b2$OLevel=="2"& b2$ReuseType=="ScaTemp", c(7,9,10)])
b3<-rbind(b3,c(NA,"2",32,0,0,0,media[1],"ScaTemp",media[2],media[3],0,0,0))
media<-colMeans(b2[b2$OLevel=="3"& b2$ReuseType=="ScaTemp", c(7,9,10)])
b3<-rbind(b3,c(NA,"3",32,0,0,0,media[1],"ScaTemp",media[2],media[3],0,0,0))
b3$Reuse<-as.double(b3$Reuse)
b3$Hits<-as.double(b3$Hits)
b3$Misses<-as.double(b3$Misses)
# mal, saca media por acceso, y solo se ve reflejado quien más accesos tiene (dijkstra)


b3<-b2
b3$Percent<-100*b3$Reuse/(b3$Hits+b3$Misses)
b3$Avg<-ave(b3$Percent,b3$OLevel,b3$ReuseType,FUN=mean)

b3<-rbind(b3,c(NA,"0",32,0,0,0,0,"ScaTemp",1,0,0,0,0,head(b3$Avg[b3$OLevel=="0" & b3$ReuseType=="ScaTemp"],n=1),0))
b3<-rbind(b3,c(NA,"1",32,0,0,0,0,"ScaTemp",1,0,0,0,0,head(b3$Avg[b3$OLevel=="1" & b3$ReuseType=="ScaTemp"],n=1),0))
b3<-rbind(b3,c(NA,"2",32,0,0,0,0,"ScaTemp",1,0,0,0,0,head(b3$Avg[b3$OLevel=="2" & b3$ReuseType=="ScaTemp"],n=1),0))
b3<-rbind(b3,c(NA,"3",32,0,0,0,0,"ScaTemp",1,0,0,0,0,head(b3$Avg[b3$OLevel=="3" & b3$ReuseType=="ScaTemp"],n=1),0))
b3<-rbind(b3,c(NA,"0",32,0,0,0,0,"ArrayTemp",1,0,0,0,0,head(b3$Avg[b3$OLevel=="0" & b3$ReuseType=="ArrayTemp"],n=1),0))
b3<-rbind(b3,c(NA,"1",32,0,0,0,0,"ArrayTemp",1,0,0,0,0,head(b3$Avg[b3$OLevel=="1" & b3$ReuseType=="ArrayTemp"],n=1),0))
b3<-rbind(b3,c(NA,"2",32,0,0,0,0,"ArrayTemp",1,0,0,0,0,head(b3$Avg[b3$OLevel=="2" & b3$ReuseType=="ArrayTemp"],n=1),0))
b3<-rbind(b3,c(NA,"3",32,0,0,0,0,"ArrayTemp",1,0,0,0,0,head(b3$Avg[b3$OLevel=="3" & b3$ReuseType=="ArrayTemp"],n=1),0))
b3<-rbind(b3,c(NA,"0",32,0,0,0,0,"LongSpa",1,0,0,0,0,head(b3$Avg[b3$OLevel=="0" & b3$ReuseType=="LongSpa"],n=1),0))
b3<-rbind(b3,c(NA,"1",32,0,0,0,0,"LongSpa",1,0,0,0,0,head(b3$Avg[b3$OLevel=="1" & b3$ReuseType=="LongSpa"],n=1),0))
b3<-rbind(b3,c(NA,"2",32,0,0,0,0,"LongSpa",1,0,0,0,0,head(b3$Avg[b3$OLevel=="2" & b3$ReuseType=="LongSpa"],n=1),0))
b3<-rbind(b3,c(NA,"3",32,0,0,0,0,"LongSpa",1,0,0,0,0,head(b3$Avg[b3$OLevel=="3" & b3$ReuseType=="LongSpa"],n=1),0))
b3<-rbind(b3,c(NA,"0",32,0,0,0,0,"ShortSpa",1,0,0,0,0,head(b3$Avg[b3$OLevel=="0" & b3$ReuseType=="ShortSpa"],n=1),0))
b3<-rbind(b3,c(NA,"1",32,0,0,0,0,"ShortSpa",1,0,0,0,0,head(b3$Avg[b3$OLevel=="1" & b3$ReuseType=="ShortSpa"],n=1),0))
b3<-rbind(b3,c(NA,"2",32,0,0,0,0,"ShortSpa",1,0,0,0,0,head(b3$Avg[b3$OLevel=="2" & b3$ReuseType=="ShortSpa"],n=1),0))
b3<-rbind(b3,c(NA,"3",32,0,0,0,0,"ShortSpa",1,0,0,0,0,head(b3$Avg[b3$OLevel=="3" & b3$ReuseType=="ShortSpa"],n=1),0))
b3<-rbind(b3,c(NA,"0",32,0,0,0,0,"NotReused",1,0,0,0,0,head(b3$Avg[b3$OLevel=="0" & b3$ReuseType=="NotReused"],n=1),0))
b3<-rbind(b3,c(NA,"1",32,0,0,0,0,"NotReused",1,0,0,0,0,head(b3$Avg[b3$OLevel=="1" & b3$ReuseType=="NotReused"],n=1),0))
b3<-rbind(b3,c(NA,"2",32,0,0,0,0,"NotReused",1,0,0,0,0,head(b3$Avg[b3$OLevel=="2" & b3$ReuseType=="NotReused"],n=1),0))
b3<-rbind(b3,c(NA,"3",32,0,0,0,0,"NotReused",1,0,0,0,0,head(b3$Avg[b3$OLevel=="3" & b3$ReuseType=="NotReused"],n=1),0))

b3$Percent<-as.double(b3$Percent)
b3$Name<-as.character(b3$Name)
b3$Name[is.na(b3$Name)]<-"(average)"
b3$Name<-as.factor(b3$Name)

b3$Name<-factor(b3$Name,levels=c("qsort-exam","binarysearch","bsort","countnegative","matrix1","matmult","dijkstra","iir","crc","fft","jfdctint","minver","ludcmp","qurt","petrinet","lift","md5","g723\\_enc","ndes","statemate","basicmath","(average)"))

## FORMATO VIEJO; benchmark por linea

#setwd("/home/jsegarra/alba/binariosPruebasPhases/")
#setwd("/home/jsegarra/alba/binariosPruebasPhases_gcc6_data/")

b<-read.csv(file="results.csv",sep=" ");


library(ggplot2)


#b$OLevel<-factor(b$OLevel,levels=c("0","s","1","2","3"))
b$OLevel<-as.factor(b$OLevel)

# quitar basicmath, que no tiene O0
#b<-b[b$Name!="basicmath",]
#b$Name<-droplevels(b$Name)
# ordena por el número de entradas en AC para -O0
b$Name<-factor(b$Name,levels=levels(reorder(levels(b$Name),b$ACLines[b$OLevel==0],FUN=min)))

# quitar benchmarks sin accesos a datos
b<-b[b$Name!="janne_complex",]
# quitar benchmarks repetidos
b<-b[b$Name!="bs",] # bs=binarysearch
#b<-b[b$Name!="cnt",] # cnt=countnegative
b<-b[b$Name!="nsichneu",] # nsichneu=petrinet
b<-b[b$Name!="fdct",] # fdct=jfdctint


# para latex, quitar nombres con _
#levels(b$Name)[levels(b$Name)=="janne_complex"]<-"janne\\_complex"
#levels(b$Name)[levels(b$Name)=="complex_updates"]<-"comples\\_updates"
levels(b$Name)[levels(b$Name)=="g723_enc"]<-"g723\\_enc"
#levels(b$Name)[levels(b$Name)=="huff_dec"]<-"huff\\_dec"
levels(b$Name)[levels(b$Name)=="matmult_opti"]<-"matmult\\_opti"

# hit ratio
#ggplot(data=b, aes(x=OLevel,y=Hits/(Hits+Misses)))+geom_boxplot()

#ggplot(data=b, aes(x=Name,y=Hits/(Hits+Misses)))+geom_boxplot()+geom_point()+theme(axis.text.x = element_text(angle = 90, hjust = 1,vjust=0.5))

##1 ACLines por benchmark
#ggplot(data=b, aes(x=Name,y=ACLines))+geom_point(aes(color=OLevel))+theme(axis.text.x = element_text(angle = 90, hjust = 1,vjust=0.5))
ggplot(data=b, aes(x=Name,y=ACLines)) +geom_point(aes(color=OLevel)) +theme(axis.text.x = element_text(angle = 90, hjust = 1,vjust=0.5))+geom_hline(yintercept=16, color="green") +geom_hline(yintercept=32, color="orange") +geom_hline(yintercept=64, color="red") +scale_y_continuous(trans="log2") #+scale_color_brewer(palette="Oranges")

pdf(file="ACLines.pdf",width=9,height=6) ; last_plot() ; dev.off()

##2 Accesos por benchmark
ggplot(data=b, aes(x=Name,y=(Hits+Misses)))+geom_point(aes(color=OLevel))+theme(axis.text.x = element_text(angle = 90, hjust = 1,vjust=0.5))+scale_y_continuous(trans="log10")
pdf(file="Accesses.pdf",width=6,height=3) ; last_plot() ; dev.off()

##3 Tipo reuso por benchmark
#ggplot(data=b, aes(x=Name))+geom_bar(aes(y=(NotKnown+Temporal+Spatial)/(Hits+Misses)),fill="black",stat="identity")+geom_bar(aes(y=(Temporal+Spatial)/(Hits+Misses)),fill="red",stat="identity")+geom_bar(aes(y=Spatial/(Hits+Misses)),fill="blue",stat="identity")+facet_grid(OLevel~.)+theme(axis.text.x = element_text(angle = 90, hjust = 1,vjust=0.5))
# Negro: NotKnown, Rojo: Temporal, Azul: Stride-1, Resto: Sin datos ó (sin reuso + stride no 1)
#ggplot(data=b,aes(x=Name))+geom_bar(aes(y=1),fill="black",stat="identity")+geom_bar(aes(y=1-(NotKnown)/(Hits+Misses)),fill="blue",stat="identity")+facet_grid(OLevel~.)+geom_bar(aes(y=(Temporal+Spatial)/(Hits+Misses)),fill="cyan",stat="identity")+geom_bar(aes(y=Temporal/(Hits+Misses)),fill="red",stat="identity")+theme(axis.text.x = element_text(angle = 90, hjust = 1,vjust=0.5))+ylab("Rojo=%TempR, Cyan=%Strd1, Azul=%NoStrd1, Negro=%NotKnown")
#ggplot(data=b,aes(x=Name))+geom_bar(aes(y=1),fill="black",stat="identity")+geom_bar(aes(y=1-(NotKnown)/(Hits+Misses)),fill="blue",stat="identity")+facet_grid(OLevel~.)+geom_bar(aes(y=(Temporal+Spatial)/(Hits+Misses)),fill="cyan",stat="identity")+geom_bar(aes(y=Temporal/(Hits+Misses)),fill="red",stat="identity")+theme(axis.text.x = element_text(angle = 90, hjust = 1,vjust=0.5))+xlab("Rojo=%TempR, Cyan=%Strd1, Azul=%(NoStrd1+NoTempR), Negro=%NotKnown")+ylab("% tipo reuso")

# colores semaforo
#ggplot(data=b,aes(x=Name))+geom_bar(aes(y=1),fill="black",stat="identity")+geom_bar(aes(y=1-(NotKnown)/(Hits+Misses)),fill="red",stat="identity")+facet_grid(OLevel~.)+geom_bar(aes(y=(Temporal+Spatial)/(Hits+Misses)),fill="orange",stat="identity")+geom_bar(aes(y=Temporal/(Hits+Misses)),fill="darkgreen",stat="identity")+theme(axis.text.x = element_text(angle = 90, hjust = 1,vjust=0.5))+xlab("Verde=Temp, Naranja=Stride1, Rojo=(NoStride1+NoTemp), Negro=NotFound")+ylab("Tipo reuso (\\%)")

#ggplot(data=b,aes(x=Name)) +geom_bar(aes(y=100),fill="black",stat="identity") +geom_bar(aes(y=100*(1-(NotKnown)/(Hits+Misses))),fill="red",stat="identity") +facet_grid(OLevel~.) +geom_bar(aes(y=100*(Temporal+Spatial)/(Hits+Misses)),fill="orange",stat="identity") +geom_bar(aes(y=100*Temporal/(Hits+Misses)),fill="darkgreen",stat="identity") +theme(axis.text.x = element_text(angle = 90, hjust = 1,vjust=0.5))+xlab("Green=Temporal, Orange=Spatial (short stride), Red=(Long stride spatial + No temporal), Black=Unknown") +ylab("Reuse type (\\%)")

ggplot(data=b,aes(x=Name)) +geom_bar(aes(y=100*(1-(NotKnown)/(Hits+Misses))),fill="red",stat="identity") +facet_grid(OLevel~.) +geom_bar(aes(y=100*(Temporal+Spatial)/(Hits+Misses)),fill="orange",stat="identity") +geom_bar(aes(y=100*Temporal/(Hits+Misses)),fill="darkgreen",stat="identity") +theme(axis.text.x = element_text(angle = 90, hjust = 1,vjust=0.5))+xlab("Green=Temporal, Orange=Short stride Spatial, Red=Long stride Spatial") +ylab("Reuse type (\\%)")

#pdf(file="ReuseType.pdf",width=6.5,height=3.5) ; last_plot() ; dev.off()
pdf(file="ReuseTypeG.pdf",width=9,height=6) ; last_plot() ; dev.off()

tikz(file="reusetypes.tex",width=7,height=4) ; last_plot() ; dev.off()

##4 Hits sobreimpresos
#ggplot(data=b, aes(x=Name))+geom_bar(aes(y=(NotKnown+Temporal+Spatial)/(Hits+Misses)),fill="black",stat="identity")+geom_bar(aes(y=(Temporal+Spatial)/(Hits+Misses)),fill="red",stat="identity")+geom_bar(aes(y=Spatial/(Hits+Misses)),fill="blue",stat="identity")+geom_bar(aes(y=Hits/(Hits+Misses)),fill="darkgreen",stat="identity")+facet_grid(OLevel~.)+theme(axis.text.x = element_text(angle = 90, hjust = 1,vjust=0.5))
# Verde: Hits
ggplot(data=b,aes(x=Name))+geom_bar(aes(y=1),fill="black",stat="identity")+geom_bar(aes(y=1-(NotKnown)/(Hits+Misses)),fill="blue",stat="identity")+facet_grid(OLevel~.)+geom_bar(aes(y=(Temporal+Spatial)/(Hits+Misses)),fill="cyan",stat="identity")+geom_bar(aes(y=Temporal/(Hits+Misses)),fill="red",stat="identity")+geom_bar(aes(y=Hits/(Hits+Misses)),fill="darkgreen",stat="identity")+theme(axis.text.x = element_text(angle = 90, hjust = 1,vjust=0.5))+xlab("Rojo=%TempR, Cyan=%Strd1, Azul=%(NoStrd1+NoTempR), Negro=%NotKnown")+ylab("% tipo reuso, VERDE %HITS ACDC")

#pdf(file="HitRatioG.pdf",width=6.5,height=3.5) ; last_plot() ; dev.off()
pdf(file="HitRatioG.pdf",width=6,height=4) ; last_plot() ; dev.off()

