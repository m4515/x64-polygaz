#!/bin/sh

# Grabs DRP variables and values from lp_solve results and
# generates a .xml with them.

source=$1

if [ "$source" ] ; then
	echo '<?xml version="1.0"?>'
	echo '<acdc_config>'
	if [ "`grep Loop_DRPs \"$source\"`" ] ; then
		# MULTIPOINT ACDC CONFIG
		lpfile=`echo $source | sed 's/\.lp.*/\.lp/g'`
		# set global drps
    	echo '  <loadpoint id="0" pc="0">'
		for drpvar in `grep "Global_DRPs\ =" "$lpfile" | cut -f2 -d'=' | sed 's/[;+]//g' | tr ' ' '\n' | sort` ; do
			if [ "`grep "^$drpvar.*1\$" $source`" ] ; then
				addr=`echo "$drpvar" | cut -f2 -d'_'`
				echo "    <pc>$addr</pc>"
			fi
		done
    	echo "  </loadpoint>"
		# set dynamic drps
		dynpoints=`grep "Loop0x[[:xdigit:]][[:xdigit:]]*_DRPs\ =" "$lpfile" | wc -l`
		point=1
		while [ $point -le $dynpoints ] ; do
			pointline=`grep "Loop0x[[:xdigit:]][[:xdigit:]]*_DRPs\ =" "$lpfile" | sort | sed -n ${point}p`
			pc=`echo $pointline | cut -f1 -d' ' | sed 's/Loop//g' | cut -f1 -d'_'`
			echo "  <loadpoint id=\"$point\" pc=\"$pc\">"
			for drpvar in `echo $pointline | cut -f2 -d'=' | sed 's/[;+]//g' | tr ' ' '\n' | sort` ; do
				if [ "`grep "^$drpvar.*1\$" $source`" ] ; then
					addr=`echo "$drpvar" | cut -f2 -d'_'`
					echo "    <pc>$addr</pc>"
				fi
			done
			echo "  </loadpoint>"
			point=`expr $point + 1`
		done
	else
		# SINGLE POINT ACDC CONFIG
		echo '  <loadpoint id="0" pc="0">'
    	for addr in `grep '^DRP.*1$' "$source" | cut -f2 -d_ | cut -f1 -d' ' | sort` ; do
    		echo "    <pc>$addr</pc>"
    	done
		echo "  </loadpoint>"
	fi
	echo '</acdc_config>'
else
	echo Usage: $0 lpfile.solved
#	echo '<?xml version="1.0"?>'
#	echo '<acdc_config>'
#	grep '^DRP.*1$' | cut -f2 -d_ | cut -f1 -d' ' | sort |
#	while read addr ; do
#		echo "  <entry>"
#		echo "    <pc>$addr</pc>"
#		echo "  </entry>"
#	done
#   echo '</acdc_config>'
fi
