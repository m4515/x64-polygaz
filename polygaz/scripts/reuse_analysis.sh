#!/bin/sh

#ANADIR="/home/jsegarra/investigacio/treal/analizadores/polygaz"
ANADIR="$HOME/polygaz"
ANACMD="venv/bin/python3 -u -O analysis1.py"

#BENCHDIR="/home/jsegarra/investigacio/treal/benchmarks/binarios_datos/"
BENCHDIR="$HOME/binarios_datos/"

#MAXTIME="5m"

# lanza experimentos
cd "$ANADIR"
for fi in `find "$BENCHDIR" -name "*.ff"` ; do
  #echo "$fi" >&2 # print name on stderr
  FF="$fi"
  #ELF=`echo "$fi" | sed 's/\.ff$/\.elf/g'`
  ELF=`echo "$fi" | sed 's/\.ff$//g'`
  REUSE=`echo "$fi" | sed 's/\.ff$/\.reuse/g'`
  OLDREUSE=`echo "$fi" | sed 's/\.ff$/\.reuse_old/g'`

  mv "$REUSE" "$OLDREUSE" 2>/dev/null
  #timeout "$MAXTIME" $ANADIR/$ANACMD "$ELF" > "$REUSE" || echo "Timeout vencido" >> "$REUSE" # 2>&1
  echo "$ANADIR/$ANACMD $ELF > $REUSE ;"

  # compara con resultado anterior para ver si hay diferencias
#  cmp "$REUSE" "$OLDREUSE"
#  true
#  if [ 0 -ne $? ] ; then
#	  # lanza el visor de diferencias. Ojo: puede abrir muchas ventanas
#	  kompare "$OLDREUSE" "$REUSE" 2>/dev/null &
#  fi
done | parallel -j 50% --memfree 16G
