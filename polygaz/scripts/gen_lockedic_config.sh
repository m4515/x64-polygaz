#!/bin/sh

# Grabs cached variables and values from lp_solve results and
# generates a .xml with them.

source=$1

echo '<?xml version="1.0"?>'
echo '<lockedic_config>'
echo '  <loadpoint id="0" pc="0">'
for addr in `grep '^cached.*1$' "$source" | cut -f2 -d_ | cut -f1 -d' ' | sort` ; do
		echo "    <line>$addr</line>"
done
echo '  </loadpoint>'
echo '</lockedic_config>'
