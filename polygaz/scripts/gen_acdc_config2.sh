#!/bin/sh

# Generates ACDC config file (DRP PCs) from estimations from no-cache IPET

source=$1
max=$2

echo '<?xml version="1.0"?>'
echo '<acdc_config>'
for addr in `grep '^IMPR' "$source" | grep -v '-' | grep -v ' 0$' | sort -k 2gr | cut -f2 -d_ | cut -f1 -d' ' | head -n $max | sort` ; do
		echo "  <entry>"
		echo "    <pc>$addr</pc>"
		echo "  </entry>"
done
echo '</acdc_config>'
