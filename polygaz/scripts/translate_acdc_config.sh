#!/bin/sh

# Translates old ACDC config file to new format

source=$1

if [ "$source" ] ; then
	echo '<?xml version="1.0"?>'
	echo '<acdc_config>'
	echo '  <loadpoint id="0" pc="0">'
	for addr_line in `grep 'pc' "$source" | sort` ; do
		echo "    $addr_line"
		#echo "    <pc>$addr</pc>"
	done
	echo '  </loadpoint>'
	echo '</acdc_config>'
else
	echo Usage: $0 lpfile.solved
#	echo '<?xml version="1.0"?>'
#	echo '<acdc_config>'
#	grep '^DRP.*1$' | cut -f2 -d_ | cut -f1 -d' ' | sort |
#	while read addr ; do
#		echo "  <entry>"
#		echo "    <pc>$addr</pc>"
#		echo "  </entry>"
#	done
#   echo '</acdc_config>'
fi
