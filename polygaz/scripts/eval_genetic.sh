#!/bin/sh

# Launches generation and solving of ILP models
# Generates a .csv with single-space separated columns

# Parallel jobs to launch
JOBS=-2  # 1 per processor, except for 2 processors

#ANADIR="$HOME/investigacio/treal/analizadores/polygaz"
ANADIR="$HOME/genetic/"
GENETIC="$ANADIR/main"

#CONFIG_ACDC_IPET="scripts/gen_acdc_config_from_ipet_single.sh"
CONFIG_ACDC="$HOME/polygaz/scripts/gen_allconfigs_acdc.sh"

icache="locked"
IC_SETS=64
IC_WAYS=8
#ACDC_DC_LINES="4 8 16 32" # 256-2048 B netos
ACDC_DC_LINES=4 # 256-2048 B netos

#BENCHDIR="$HOME/investigacio/treal/benchmarks/xmls"
BENCHDIR="$HOME/data"

cd $ANADIR

######################## LAUNCH GENETIC ALGORITHM TO GENERATE ACDC CONFIGS
for fi in `find "$BENCHDIR" -name "*arm.xml"` ; do
    XML="$fi"
    #ACDC_DC_LINES=8
    for lines in $ACDC_DC_LINES ; do
        DRPFILE=`echo "$fi" | sed "s/\.xml/_acdc_${lines}\.xml/g"`
        DRPFILE_M=`echo "$fi" | sed "s/\.xml/_acdc_${lines}_m\.xml/g"`
        LOG=`echo "$DRPFILE" | sed "s/\.xml/\.log/g"`

        # MULTIPLE LOCKING POINTS
        if [ ! -f "$DRPFILE_M" ] ; then
            continue
        fi
        $CONFIG_ACDC $DRPFILE_M
        parallel_line=""
        for loop0 in $(seq 1 $(expr $lines - 1)) ; do
            LOG_L0=`echo "$DRPFILE" | sed "s/\.xml/_m_${loop0}\.log/g"`
            DRPFILE_M_L0=`echo "$DRPFILE_M" | sed "s/\.xml/_${loop0}\.xml/g"`
            # avoid already performed experiments
            BESTFILE_M="${DRPFILE_M_L0}_best_value"
            if [ -f "$BESTFILE_M" ] ; then
                continue
            fi
            # build the command string to launch with parallel
            if [ "$parallel_line" ] ; then
                parallel_line="${parallel_line} && $GENETIC $XML $DRPFILE_M_L0 $lines > $LOG_L0 2>&1"
            else
                parallel_line="$GENETIC $XML $DRPFILE_M_L0 $lines > $LOG_L0 2>&1"
            fi
        done
        echo "$parallel_line"
        # SINGLE LOCKING POINT
#        BESTFILE="${DRPFILE}_best_value"
#        # avoid already performed experiments
#        if [ -f "$BESTFILE" ] ; then
#            continue
#        fi
#        echo "$GENETIC $XML $DRPFILE $lines > $LOG 2>&1"
    done
#done # test what to be done
done | parallel -j $JOBS #--bar
