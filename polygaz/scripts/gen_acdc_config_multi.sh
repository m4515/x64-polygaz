#!/bin/bash

# Generates ACDC config file (DRP PCs) from estimations from no-cache IPET

source=$1
max=$2
lpfile=`echo "$source" | sed 's/\.solved$//g'`
declare -A drps
declare -A improv

while read addr value ; do
	#echo $addr
	#echo $value
	loop=`grep "^-IMPR_$addr" "$lpfile" | cut -f2 -d: | cut -f1 -d'*' | sed 's/ //g'`
	#echo $loop
	#echo improvement $value for $addr in $loop
	drps[$loop]="${drps[$loop]} $addr"
	#echo addrs in $loop: ${drps[$loop]}
	improv[$addr]=$value
done <<< `grep '^IMPR' "$source" | grep -v '-' | grep -v ' 0$' | sort -k 2gr | cut -f2 -d_`

#echo values: "${drps[@]}"
#echo keys: "${!drps[@]}"
#echo values: "${improv[@]}"
#echo keys: "${!improv[@]}"

echo '<?xml version="1.0"?>'
echo '<acdc_config>'

# print ilp model
( \
# print objective
echo 'max: improvement;'

# print: improvement = \sum IMPR*DRP
echo -n 'improvement ='
for addr in ${!improv[*]} ; do
	echo -n " + ${improv[$addr]} DRP_$addr"
done
echo ';'

# print: fixedDRPs = \sum DRPs outside loops
#echo -n 'fixedDRPs ='
#for addr in "${drps[0x0]}" ; do
#	echo -n " + ${improv[$addr]} DRP$addr"
#done
#echo ';'

# print, for each loop: DRPsLoopi = \sum DRPs in loop i
for loop in ${!drps[*]} ; do
	echo -n "DRPsLoop$loop ="
	for addr in ${drps[$loop]} ; do
		echo -n " + DRP_$addr"
	done
	echo ';'
done

# print FixedDRPs and DynamicDRPs constraints
echo "DRPsLoop0x0 = FixedDRPs;"
for loop in ${!drps[@]} ; do
	if [ $loop != '0x0' ] ; then
		echo "DRPsLoop$loop <= DynamicDRPs;"
	fi
done
echo "FixedDRPs + DynamicDRPs <= $max;"

# print binary variables
echo -n "binary"
for addr in ${!improv[*]} ; do
	echo -n " DRP_$addr,"
done
echo ' falsevar;' # to avoid special case for the , for first/last var

) | lp_solve |
#) ; exit
# call script gen_acdc_config.sh
grep '^DRP_.*1$' | cut -f2 -d_ | cut -f1 -d' ' | sort |
while read addr ; do
	echo "  <entry>"
	echo "    <pc>$addr</pc>"
	echo "  </entry>"
done

echo '</acdc_config>'
