#!/bin/bash

# Generates ACDC config file (DRP PCs) from estimations from no-cache IPET
# The  configuration is DYNAMIC (DRPs vary at specific loading points)

source=$1
max=$2
lpfile=`echo "$source" | sed 's/\.solved$//g'`
declare -A drps
declare -A improv

while read addr value ; do
	#echo $addr
	#echo $value
	loop=`grep "^-IMPR_$addr" "$lpfile" | cut -f2 -d: | cut -f1 -d'*' | sed 's/ //g'`
	#echo $loop
	#echo improvement $value for $addr in $loop
	drps[$loop]="${drps[$loop]} $addr"
	#echo addrs in $loop: ${drps[$loop]}
	improv[$addr]=$value
done <<< `grep '^IMPR' "$source" | grep -v '-' | grep -v ' 0$' | sort -k 2gr | cut -f2 -d_`

# sort addresses
for loop in ${!drps[*]} ; do
	drps[$loop]=`echo ${drps[$loop]} | tr ' ' '\n' | sort | tr '\n' ' '`
	#echo addrs in $loop: ${drps[$loop]}
done

#echo values: "${drps[@]}"
#echo keys: "${!drps[@]}"
#echo values: "${improv[@]}"
#echo keys: "${!improv[@]}"

drpvars=`\
# print ilp model
( \
# print objective
echo 'max: improvement;'

# print: improvement = \sum IMPR*DRP
echo -n 'improvement ='
for addr in ${!improv[*]} ; do
	echo -n " + ${improv[$addr]} DRP_$addr"
done
echo ';'

# print: fixedDRPs = \sum DRPs outside loops
#echo -n 'fixedDRPs ='
#for addr in "${drps[0x0]}" ; do
#	echo -n " + ${improv[$addr]} DRP$addr"
#done
#echo ';'

# print, for each loop: DRPsLoopi = \sum DRPs in loop i
for loop in ${!drps[*]} ; do
	echo -n "DRPsLoop$loop ="
	for addr in ${drps[$loop]} ; do
		echo -n " + DRP_$addr"
	done
	echo ';'
done

# print FixedDRPs and DynamicDRPs constraints
echo "DRPsLoop0x0 = FixedDRPs;"
for loop in ${!drps[@]} ; do
	if [ $loop != '0x0' ] ; then
		echo "DRPsLoop$loop <= DynamicDRPs;"
	fi
done
echo "FixedDRPs + DynamicDRPs <= $max;"

# print binary variables
echo -n "binary"
novars="true"
for addr in ${!improv[*]} ; do
	if [ "$novars" = "true" ] ; then
		echo -n " DRP_$addr"
		novars="false"
	else
		echo -n ", DRP_$addr"
	fi
done
echo ';'

) | lp_solve |
#) ; exit
grep '^DRP_.*1$' | cut -f2 -d_ | cut -f1 -d' ' | sort`

# print resulting XML
echo '<?xml version="1.0"?>'
echo '<acdc_config>'
# force loop 0x0 first
echo "  <loadpoint id=\"0\" pc=\"0x0\">"
for addr in ${drps[0x0]} ; do
	if [ "`echo $drpvars | grep $addr`" ] ; then
		echo "    <pc>$addr</pc>"
	fi
done
echo "  </loadpoint>"
point=1
for loop in ${!drps[*]} ; do
    if [ "$loop" != "0x0" ] ; then # discard loop 0x0
    	echo "  <loadpoint id=\"$point\" pc=\"$loop\">"
    	for addr in ${drps[$loop]} ; do
    		if [ "`echo $drpvars | grep $addr`" ] ; then
        		echo "    <pc>$addr</pc>"
    		fi
    	done
    	echo "  </loadpoint>"
	    point=`expr $point + 1`
    fi
done
echo '</acdc_config>'
