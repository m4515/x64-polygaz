#!angr/bin/python3
# coding=utf-8

import sys
#from ctypes import *
from ctypes import CDLL, c_int, c_char_p, c_double, c_void_p, byref

apron = CDLL("/home/alvaro/TFG/polygaz/libpolygaz.so")
#apron.getDifferenceSupInterval.argtypes = [c_char_p, c_char_p]
#apron.getDifferenceSupInterval.restype = c_double
#apron.getDifferenceInfInterval.argtypes = [c_char_p, c_char_p]
#apron.getDifferenceInfInterval.restype = c_double
apron.sameBoundsDifferenceInterval.argtypes = [c_char_p, c_char_p, c_void_p]
apron.sameBoundsDifferenceInterval.restype = c_int
apron.sameBoundsVariable.argtypes = [c_char_p, c_void_p]
apron.sameBoundsVariable.restype = c_int
apron.solveVarGetRegister.argtypes = [c_int]
apron.solveVarGetRegister.restype = c_int
apron.solveVarGetConstant.argtypes = [c_int]
apron.solveVarGetConstant.restype = c_int

class IndVar:
    def __init__(self, step, base, valid, constantBase, basereg, baseoffset):
        self.step = step
        self.base = base
        self.valid = valid
        self.constantBase = constantBase
#        self.basereg = basereg
#        self.baseoffset = baseoffset
#        self.baseregvalidated = False


loop = {}

#assignments = {}
#
#def setAssignments(loop, assign):
#    assignments[loop] = assign
#
#
#def isIndVar(addr, reg):
#    if (addr, reg) in loop:
#        if loop[(addr, reg)].valid == True: #and loop[(addr, reg)].step != 0:
#            return True
#        else:
#            del loop[(addr, reg)]
#            return False
#    else:
#        return False
#
#def collectBaseInfo(addr, reg):
#    regName = "r" + str(reg)
#    regNameEnc = regName.encode('utf-8')
#    bound = c_int()
#    if apron.sameBoundsVariable(regNameEnc, byref(bound)):
#        base = bound.value #% 0x100000000
#        constantBase = True
#        basereg = 0
#        baseoffset = 0
#    else:
#        base = 0
#        constantBase = False
#        basereg = apron.solveVarGetRegister(reg)
#        baseoffset = apron.solveVarGetConstant(reg)
#    loop[(addr, reg)] = IndVar(None, base, None, constantBase, basereg, baseoffset)
#
#
#
#def getBaseRegister(reg, registers, sRFG):
#    found = 0
#    shadowok = -1
#    offset = -1
#    regName = "r" + str(reg)
#    regNameEnc = regName.encode('utf-8')
#    bound = c_int()
#    for shadow in registers:
#        shadowName = "s" + str(shadow)
#        shadowNameEnc = shadowName.encode('utf-8')
##################################
##        supBound = apron.getDifferenceSupInterval(shadowNameEnc, regNameEnc)
##        infBound = apron.getDifferenceInfInterval(shadowNameEnc, regNameEnc)
##        if infBound == supBound:
##            if (reg in sRFG) and sRFG[reg]==shadow:
##                #print("POSSIBLE COPY: r"+str(reg), "<-- r"+str(shadow), "+", int(supBound), "confirmed")
##                offset = int(supBound)
##                shadowok = shadow
##                found += 1
##            #else:
##                #print("POSSIBLE COPY: r"+str(reg), "<-- r"+str(shadow), "+", int(supBound), "discarded")
##################################
#        if apron.sameBoundsDifferenceInterval(shadowNameEnc, regNameEnc, byref(bound)):
#            offset = bound.value
#            shadowok = shadow
#            found += 1
#
#    return (found, shadowok, offset)
#

def collectStepInfo(loopCFGNode, addr, registers): #, simpleRegFlowGraph):
    bound = c_int()
    for reg in registers:
        # calc step
        regName = "r" + str(reg)
        regNameEnc = regName.encode('utf-8')
        shadowName = "s" + str(reg)
        shadowNameEnc = shadowName.encode('utf-8')
        if apron.sameBoundsDifferenceInterval(regNameEnc, shadowNameEnc, byref(bound)):
            step = bound.value # signed
            valid = True
        else:
            step = 0
            valid = False
        # calc differences
        if (loopCFGNode, addr, reg) not in loop:
            if valid == True and apron.sameBoundsVariable(shadowNameEnc, byref(bound)):
                base = bound.value # unsigned?
                constantBase = True
            else:
                base = 0
                constantBase = False
            loop[(loopCFGNode, addr, reg)] = IndVar(step, base, valid, constantBase, None, None)
        elif loop[(loopCFGNode, addr, reg)].valid != valid or loop[(loopCFGNode, addr, reg)].step != step:
            loop[(loopCFGNode, addr, reg)].step = 0
            loop[(loopCFGNode, addr, reg)].valid = False


def printInfo():
    print("")
    print("   Induction Variables")
    print("-------------------------")
    last_cfg_node = None
    last_addr = None
    instance = 0
    for (loopCFGNode, addr, reg) in loop:
        if last_cfg_node != loopCFGNode:
            if last_addr == addr:
                instance += 1
            else:
                instance = 1
        if loop[(loopCFGNode, addr, reg)].valid and loop[(loopCFGNode, addr, reg)].step != 0:
            if loop[(loopCFGNode, addr, reg)].constantBase:
                print("Loop", hex(addr)+"("+str(instance)+") indvar:", loop[(loopCFGNode, addr, reg)].step, "r"+str(reg), "(R"+str(round(reg/4-2))+") +", loop[(loopCFGNode, addr, reg)].base)
            else:
                print("Loop", hex(addr)+"("+str(instance)+") indvar:", loop[(loopCFGNode, addr, reg)].step, "r"+str(reg), "(R"+str(round(reg/4-2))+") + unknown")
        last_cfg_node = loopCFGNode
        last_addr = addr
    print("")

#def validateAssignment(addr, reg, assigned):
#    if (addr, reg) in loop:
#        info = loop[addr, reg]
#        if info.basereg != assigned:
#            print("Unexpected assignment information found in loop", hex(addr), ":", reg, "<-", assigned, "(this is not necessarily an error)")
#            if info.constantBase:
#                print("Loop", hex(addr), "indvar:", loop[(addr, reg)].step, "r"+str(reg), "(R"+str(round(reg/4-2))+") +", loop[(addr, reg)].base)
#            else:
#                print("Loop", hex(addr), "indvar:", loop[(addr, reg)].step, "r"+str(reg), "(R"+str(round(reg/4-2))+") + (r"+str(loop[(addr, reg)].basereg), "+", loop[(addr, reg)].baseoffset, ")")
#        else:
#            loop[addr, reg].baseregvalidated = True
#    else:
#        print("Reg", reg, "apparently not assigned in loop", hex(addr))


def getInfo():
    return loop
