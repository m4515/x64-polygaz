#!angr/bin/python3
# coding=utf-8

""" Module with a set of functions to perform CFG/WCET analyses """


import sys
import resource # to set stack limit to unlimited (ulimit -s unlimited)
#from ctypes import *
from ctypes import CDLL, c_int, c_char_p, c_double, c_void_p, byref
from math import ceil
import time
import networkx
import angr # grafo
import claripy # estado de memoria
import pyvex # irVEX

import indvars
import flowfacts

#import logging

DEBUG_ASM = True
DEBUG_IRVEX = True
DEBUG_FIXPOINT = True
DEBUG_FIXPOINT_INDVARS = True
DEBUG_MEM = True
DEBUG_COMPARE = True
DEBUG_COST = True
#DEBUG_ASM = False
#DEBUG_IRVEX = False
#DEBUG_FIXPOINT = False
#DEBUG_FIXPOINT_INDVARS = False
DEBUG_MEM = True
DEBUG_COMPARE =True
#DEBUG_COST = False

###########################
DISREGARD_MEM_IN_ABSINT = False
# If False, abstract states include memory content, as registers.
# If True, memory is not included in abstract states.

IGNORE_MEM_STATE_FROM = None
# Initially, use state.mem[addr].<type>.resolved as valid content.
# If store to unknown address range, set to 0 and ignore state.mem.
# If store to range [x,+oo], set to min(x,IGNORE_MEM_STATE_FROM) and ignore state.mem from this address.


################################ INIT ################################
def init_apron(numnodes, domain):
    """Intializes apron interface"""
    # load apron interface
    #apron = CDLL("./apron_interface.so")

    # init apron_interface args
    apron.init.argtypes = [c_int, c_int]
    apron.addRegToExpr.argtypes = [c_int, c_char_p]
    apron.assignExprToReg.argtypes = [c_int]
    apron.assignExprToTemp.argtypes = [c_int]
    apron.assignExprToMem.argtypes = [c_int]
    apron.varExists.argtypes = [c_char_p]
    apron.varExists.restype = c_int
    apron.newNodeState.argtypes = [c_int, c_int]
    apron.setOutState.argtypes = [c_int]
    apron.addMemRef.argtypes = [c_int, c_int, c_int, c_char_p, c_int, c_int, c_int, c_int]
    apron.addMemConstRef.argtypes = [c_int, c_int, c_int, c_int, c_int, c_int, c_int]
    apron.addMemRefOffset.argtypes = [c_int, c_int, c_int, c_char_p, c_int, c_int]
    apron.getLastGR.argtypes = [c_int]
    apron.getLastGR.restype = c_int
    apron.setLastGR.argtypes = [c_int, c_int]
    apron.getIndexMemRefFirst.argtypes = [c_int]
    apron.getIndexMemRefFirst.restype = c_int
    apron.getIndexMemRefLast.argtypes = [c_int]
    apron.getIndexMemRefLast.restype = c_int
    apron.orderedMemRefs.argtypes = [c_int, c_int]
    apron.orderedMemRefs.restype = c_int
    apron.memRefIsConditional.argtypes = [c_int]
    apron.memRefIsConditional.restype = c_int
    apron.nodeExists.argtypes = [c_int, c_int]
    apron.nodeExists.restype = c_int
    apron.addNestInfo.argtypes = [c_int, c_int, c_int, c_int, c_int, c_int, c_int]
    apron.getVariableSupInterval.argtypes = [c_char_p]
    apron.getVariableSupInterval.restype = c_double
    apron.getVariableInfInterval.argtypes = [c_char_p]
    apron.getVariableInfInterval.restype = c_double
    apron.sameBoundsDifferenceInterval.argtypes = [c_char_p, c_char_p, c_void_p]
    apron.sameBoundsDifferenceInterval.restype = c_int
    apron.sameBoundsVariable.argtypes = [c_char_p, c_void_p]
    apron.sameBoundsVariable.restype = c_int
    apron.printXMLinfo.argtypes = [c_char_p, c_char_p]

    if domain == "oct":
        apron.init(2*numnodes, 1) # oct
    elif domain == "pk":
        apron.init(2*numnodes, 2) # pk
    elif domain == "ap_ppl":
        apron.init(2*numnodes, 3) # ap_ppl
    elif domain == "pkeq":
        apron.init(2*numnodes, 4) # pkeq
    else:
        print("Unknown domain", domain, "(valid: oct|pk|pkeq|ap_ppl)")
        sys.exit(-1)

# # init logger (after angr CFG/LoopFinder analysis)
# logger = logging.getLogger()
# #logger.setLevel(logging.CRITICAL)
# #logger.setLevel(logging.ERROR)
# #logger.setLevel(logging.WARNING)
# #logger.setLevel(logging.INFO)
# logger.setLevel(logging.DEBUG)
# ch = logging.StreamHandler()
# formatter = logging.Formatter('%(levelname)s: %(message)s')
# ch.setFormatter(formatter)
# logger.addHandler(ch)

def init_regs_arm():
    """ Intialize registers to explore for induction variables """
    # ARM gcc:
    # (r1): r12
    # (r2): r16
    # (r3): r20
    # (fp): r52 # Frame Pointer
    # (sp): r60=0x80000000 # Stack Pointer
    regs = []
    for reg in range(0, 16):
        if reg != 15: # exclude PC (15)
            regs.append((reg+2)*4) # ARM GCC

    apron.initStack(60, 0x80000000)
    # (pc): r68 # Program Counter
    # condition?: r72,r76,r80,r84,
    # condition?: r392 ==> appears with ARM THUMB: untested, compile with -marm
    return regs
def init_regs_x86_64():
    """ Initialize registers to explore for induction variables """
    # x64 :
    # (rax,0): 16
    # (rbx,3): 40
    # (rcx,1): 24
    # (rdx,2): 32
    # (rbp,5): 56
    # (rsp,4):r48=0x7FFFFFFFFFF0000
    # (rsi,6): 64
    # (rdi,7): 72
    # (r8,8): 80
    regs = []
    for reg in range (0,16):
       regs.append((reg+2)*8)
    apron.initStack(48, 0x8000000000000000)
    return regs
def no_back_edge_successors(cfg_node):
    """ Returns a list of successors not being back edges """
    result = []
    for dest_node in cfg_node.successors:
        is_back_edge = isBackEdge(dest_node, cfg_node)
#        is_back_edge = False
#        for loop in loop_info.loops:
#            if dest_node.addr == loop.entry.addr and cfg_node.to_codenode() in loop.body_nodes:
#                is_back_edge = True
#                break
        if not is_back_edge:
            # discard strange false single-node loop on fmref-O2 at 0x10528
            if dest_node == cfg_node:
                print("no_back_edge_successors warning: discarding a seemingly false single-node loop at 0x%x" % cfg_node.addr)
                is_back_edge = True
        if not is_back_edge:
            result.append(dest_node)
    return result


def init(filename, instance_functions):
    """initializes all"""
    global state, cfg, loop_info, fmain, apron, registers, main_node, cfg_nodes, loop_headers, IDOM, IDOMSTART, idom, start, return_addrs, ending_nodes
#    if len(sys.argv) != 2: # force domain
#        print("usage:", sys.argv[0], "<binaryfile>")
#        sys.exit()
#    filename = sys.argv[1]
    domain = "pkeq"

    print('Angr version', angr.__version__)
    # Set-up project from binary file
    proj = angr.Project(filename, auto_load_libs=False) # carga ejecutable
    main = proj.loader.main_object.get_symbol("main")
    #state = proj.factory.entry_state()
    state = proj.factory.blank_state(addr=main.rebased_addr)

    flowfacts.get_flowfacts(filename)
    #print("Flow facts:", flowfacts.maxLoopIter)

    # Build CFG and loop_info
    if instance_functions:
        # context_sensitivity_level=3 works except for md5, test3
        # md5_O0_arm (gcc-9.2.1) requires context_sensitivity_level=4
        # md5_O2_arm (gcc-9.2.1) requires context_sensitivity_level=6
        cfg = proj.analyses.CFGEmulated(normalize=True, context_sensitivity_level=3, starts=[main.rebased_addr], initial_state=state) # genera CFG
        print("CFG (emulated) has %d nodes and %d edges" % (len(cfg.graph.nodes()), len(cfg.graph.edges())))
    else:
        cfg = proj.analyses.CFG(normalize=True) # genera CFG (CFGFast)
        print("CFG (fast) has %d nodes and %d edges" % (len(cfg.graph.nodes()), len(cfg.graph.edges())))

    loop_info = proj.analyses.LoopFinder() # Set-up loop information
    # Locate main()
    fmain = cfg.kb.functions.function(name="main") # main function
    #if len(cfg.get_all_nodes(fmain.addr)) == 1: # main CFG node
    #    mainNode = cfg.get_any_node(fmain.addr) # main CFG node
    main_node = cfg.model.get_any_node(fmain.addr) # main CFG node
    mainBlock = main_node.block # main basic block

    # load apron interface
    apron = CDLL("/home/alvaro/TFG/polygaz/libpolygaz.so")
    #numcfgnodes = getNumNodes(fmain)
    #nodeset = get_node_set(fmain)
    #nodeset = cfg.graph.nodes()
    #print("nodeset:", nodeset)
    #numcfgnodes = len(nodeset)
    init_apron(len(cfg.graph.nodes()), domain)

    # set cfg_node indexes and headers for each loop
    # (a given loop may be represented by several emulated cfg node groups)
    #for loop in loop_info.loops:
    #    print("loop at", hex(loop.entry.addr))
    cfg_nodes = {}
    loop_headers = {}
    ending_nodes = []
    i = 0
    for node in cfg.model.nodes():
        cfg_nodes[node] = i
        for loop in loop_info.loops:
            if node.addr == loop.entry.addr:
                #print(hex(node.addr), "is header")
                loop_headers[node] = loop
                break
        #if not node.successors:
        if len(no_back_edge_successors(node)) == 0:
            ending_nodes.append(node) # CFG or loop ending nodes
        i += 1
    return_addrs = []
    for src, dst in cfg.graph.edges():
        if dst not in src.successors:
            return_addrs.append(dst.addr)


    # Init system registers
    registers = init_regs_x86_64()

    #### Stack limits
    resource.setrlimit(resource.RLIMIT_STACK, (resource.RLIM_INFINITY, resource.RLIM_INFINITY)) # set system stack limit to unlimited
    print("System stack limit:", resource.getrlimit(resource.RLIMIT_STACK))
    # Print max stack depth (default: 1000)
    #sys.setrecursionlimit(20000) # works with default ulimit stack size (8192 KiB)
    #sys.setrecursionlimit(1000000) # test: $ ulimit -s unlimited
    sys.setrecursionlimit(1000000)
    print("Python recursion limit (stack depth):", sys.getrecursionlimit())

    IDOMSTART = main_node
    IDOM = networkx.immediate_dominators(cfg.graph, IDOMSTART)
    start = IDOMSTART
    idom = IDOM


def generateOutState(CFGNode):
    """Process a CFG node. Assume global aistate for the initial state."""
    global aistate, ncomparisons,ncompsutiles
    #global anyStoreToUnknownAddr, state
    global state
    #global reginreg, regintemp
    basicBlock = CFGNode.block
    if __debug__ and DEBUG_ASM:
        print("-----instrucciones-----")
        basicBlock.pp()
        print("-----------------------")
    irsb = basicBlock.vex
    ncomparisons = 0
    ncompsutiles = 0
    for stmt in irsb.statements:
        #stmt.pp()
        if __debug__ and DEBUG_IRVEX:
            stmt.pp()
        if isinstance(stmt, pyvex.IRStmt.NoOp):
            # print(" No operation") # nada
            pass
        elif isinstance(stmt, pyvex.IRStmt.IMark):
            # print(" New instruction", stmt) # nueva instrucción hardware; nada
            currentPC = stmt.addr
        elif isinstance(stmt, pyvex.IRStmt.AbiHint):
            stmt.pp()
            #print(" ABI hint")  nada?
            #sys.exit(-1)
        elif isinstance(stmt, pyvex.IRStmt.Put):
            #print(" Escritura en registro:",)
            studyExpr(stmt.data, currentPC, CFGNode)
            #studyExpr(stmt.data, basicBlock.addr, currentPC)
            apron.assignExprToReg(stmt.offset)
            # nou
#            reg = apron.getRegInExpr()
#
#            if reg != -1:
#                if reg != stmt.offset:
#                    reginreg[stmt.offset] = reg
#                    #print("reg", stmt.offset, "actualizado con reg", reg)
#            else:
#                temp = apron.getTempInExpr()
#                if temp != -1:
#                    if temp in regintemp:
#                        if stmt.offset != regintemp[temp]:
#                            reginreg[stmt.offset] = regintemp[temp]
#                            #print("reg", stmt.offset, "actualizado con reg", reginreg[stmt.offset])
#                    else:
#                        if stmt.offset in reginreg:
#                            del reginreg[stmt.offset]
#                else:
#                    if stmt.offset in reginreg:
#                        del reginreg[stmt.offset]

        elif isinstance(stmt, pyvex.IRStmt.PutI):
            stmt.pp()
            print(" Escritura en bits de registro no implementado")
            sys.exit(-1)
        elif isinstance(stmt, pyvex.IRStmt.WrTmp):
            # print(" Escritura en registro virtual temporal")
            studyExpr(stmt.data, currentPC, CFGNode)
            #studyExpr(stmt.data, basicBlock.addr, currentPC)
            apron.assignExprToTemp(stmt.tmp)
            # nou
#            reg = apron.getRegInExpr()
#            if reg != -1:
#                regintemp[stmt.tmp] = reg
#                #print("temp", stmt.tmp, "actualizado con reg", reg)
#            else:
#                temp = apron.getTempInExpr()
#                if temp != -1:
#                    if temp in regintemp:
#                        regintemp[stmt.tmp] = regintemp[temp]
#                        #print("temp", stmt.tmp, "actualizado con reg", regintemp[temp])
#                    else:
#                        if stmt.tmp in regintemp:
#                            del regintemp[stmt.tmp]
#                else:
#                    if stmt.tmp in regintemp:
#                        del regintemp[stmt.tmp]
            #regName = "t"+str(stmt.tmp)
            #apron.assignExpr(regName.encode('utf-8'))
        elif isinstance(stmt, pyvex.IRStmt.Store):
            if __debug__ and DEBUG_MEM:
                print(" Escritura en memoria, PC=", hex(currentPC),
                    ", addr=", stmt.addr, "data=", stmt.data)
            data_type = stmt.data.result_type(irsb.tyenv)
            addr = processStore(stmt.addr, data_type, stmt.data)
            #addMemAccess(CFGNode, currentPC, stmt.addr, addr, 4, 0, 0)
            addMemAccess(CFGNode, currentPC, stmt.addr, addr, data_type, 0, 0)
        elif isinstance(stmt, pyvex.IRStmt.CAS):
            stmt.pp()
            print(" Atomic compare-and-swap no impl")
            sys.exit(-1)
        elif isinstance(stmt, pyvex.IRStmt.LLSC):
            stmt.pp()
            print(" Load-linked (STOREDATA=NULL) or Store-Conditional no impl")
            sys.exit(-1)
        elif isinstance(stmt, pyvex.IRStmt.MBE):
            stmt.pp()
            print(" Memory Bus Event? no impl")
            sys.exit(-1)
        elif isinstance(stmt, pyvex.IRStmt.Dirty):
            stmt.pp()
            print(" Dirty? no impl")
            sys.exit(-1)
        elif isinstance(stmt, pyvex.IRStmt.Exit):
            #print(" Conditional Exit from IRSB; no impl",)
            #stmt.guard.pp() # registro donde está el resultado de la condición
            #print(" Target:", stmt.dst.pp()) # destino de salto
            pass
        elif isinstance(stmt, pyvex.IRStmt.LoadG):
            #stmt.pp()
            if __debug__ and DEBUG_MEM:
                #print(" Lectura CONDICIONADA en memoria, PC=", hex(currentPC), ", addr=", stmt.addr, ", cond=", stmt.guard, "dest:", stmt.dst, "val:", constval)
                print(" Lectura CONDICIONADA en memoria, PC=", hex(currentPC),
                    ", addr=", stmt.addr, ", cond=", stmt.guard, "dest:", stmt.dst)
            #addr = processLoad(stmt.addr, 'Ity_I32')
            #addMemAccess(CFGNode, currentPC, stmt.addr, addr, 4, 1, 1)
            data_type = 'Ity_I32'
            # TODO: Do all conditioned loads have Ity_I32 type?
            print("Assuming Ity_I32 for the following predicated load:")
            stmt.pp()
            addr = processLoad(stmt.addr, data_type)
            addMemAccess(CFGNode, currentPC, stmt.addr, addr, data_type, 1, 1)
            #addMemAccess(basicBlock.addr, currentPC, stmt.addr, 4)
            if ncomparisons > 0:
                ncomparisons = ncomparisons - 1
                if __debug__ and DEBUG_COMPARE:
                    print("Warning: comparación perdida")
#            apron.initExpr()
#            if anyStoreToUnknownAddr:
#                apron.addTopToExpr()
#            else:
#                memval = state.mem[stmt.addr].int32_t.resolved
#                print("Warning: assuming int32_t (", memval, ") in load from", hex(stmt.addr))
#                apron.addConstToExpr(memval)
#            apron.assignExprToTemp(stmt.dst)
#            # # regName = "r"+str(stmt.offset)
#            #regName = "t"+str(stmt.dst)
#            #apron.assignExpr(regName.encode('utf-8'))
        elif isinstance(stmt, pyvex.IRStmt.StoreG):
            #stmt.pp()
            if __debug__ and (DEBUG_MEM or DEBUG_COMPARE):
                print(" Escritura CONDICIONADA en memoria, PC=", hex(currentPC),
                    ", addr=", stmt.addr, ", cond=", stmt.guard)
            data_type = stmt.data.result_type(irsb.tyenv)
            addr = processStore(stmt.addr, data_type, stmt.data)
            addMemAccess(CFGNode, currentPC, stmt.addr, addr, data_type, 0, 1)
            #addMemAccess(basicBlock.addr, currentPC, stmt.addr, 4)
            if ncomparisons > 0:
                ncomparisons = ncomparisons - 1
                if __debug__ and DEBUG_COMPARE:
                    print("Warning: comparación perdida")
        else:
            stmt.pp()
            print("generateOutState: Unknown statement:")
            sys.exit(-1)
    return ncomparisons,ncompsutiles


def processStore(target, ty, reg):
    """ Processes a store instruction """
    # get memory address
    if isinstance(target, pyvex.IRExpr.Const):
        addr = target.con.value
    else:
        if isinstance(target, pyvex.IRExpr.RdTmp):
            regName = "t" + str(target.tmp)
        elif isinstance(target, pyvex.IRExpr.Get):
            regName = "r" + str(target.offset)
        else:
            print("processStore: memRef type not implemented")
            target.pp()
            sys.exit(-1)
        regNameEnc = regName.encode('utf-8')
        bound = c_int()
        if apron.sameBoundsVariable(regNameEnc, byref(bound)):
            addr = bound.value #% 0x100000000
        else:
            addr = None
            supBound = apron.getVariableSupInterval(regNameEnc)
            infBound = apron.getVariableInfInterval(regNameEnc)

    # set memory content
    if addr is not None:
        if not DISREGARD_MEM_IN_ABSINT:
            apron.initExpr()
            if isinstance(reg, pyvex.IRExpr.Const):
                apron.addConstToExpr(reg.con.value)
                if __debug__ and DEBUG_MEM:
                    print("Addr:", hex(addr), "Const val:", reg.con.value)
            else:
                if isinstance(reg, pyvex.IRExpr.RdTmp):
                    regName = "t" + str(reg.tmp)
                elif isinstance(reg, pyvex.IRExpr.Get):
                    regName = "r" + str(reg.offset)
                else:
                    print("processStore: memRef type not implemented")
                    reg.pp()
                    sys.exit(-1)
                regNameEnc = regName.encode('utf-8')
                apron.addRegToExpr(1, regNameEnc)
                # TODO specify the size of the stored data
                if __debug__ and DEBUG_MEM:
                    print("Addr:", hex(addr), "Val:", regName)
            apron.assignExprToMem(addr)
    else:
        print("store to unknown address in", regName, "[", infBound, ",", supBound, "]")
        #apron.setMemToTop()
        if infBound == float("-inf"):
            apron.setMemToTop(0)
            IGNORE_MEM_STATE_FROM = 0
        else:
            #print("Top from", hex((int(infBound))))
            apron.setMemToTop(int(infBound))
            if IGNORE_MEM_STATE_FROM is None:
                IGNORE_MEM_STATE_FROM = int(infBound)
            else:
                IGNORE_MEM_STATE_FROM = min(IGNORE_MEM_STATE_FROM, int(infBound))
        # TODO: set top until supBound
    return addr


def processLoad(target, ty):
    """ Processes a load instruction """
    # get memory address
    if isinstance(target, pyvex.IRExpr.Const):
        addr = target.con.value
    else:
        if isinstance(target, pyvex.IRExpr.RdTmp):
            regName = "t" + str(target.tmp)
        elif isinstance(target, pyvex.IRExpr.Get):
            regName = "r" + str(target.offset)
        else:
            print("processLoad: memRef type not implemented")
            target.pp()
            sys.exit(-1)
        regNameEnc = regName.encode('utf-8')
        bound = c_int()
        if apron.sameBoundsVariable(regNameEnc, byref(bound)):
            addr = bound.value
            #print("Addr:",hex(addr))
        else:
            #print("None addr")
            addr = None
    # get memory content
    if addr is not None:
        floataccess = False
        memName = "m" + str(addr)
        memNameEnc = memName.encode('utf-8')
        #print("Memoria",memNameEnc)
        #if not apron.varExists(memNameEnc):
        if not apron.varExists(memNameEnc) and IGNORE_MEM_STATE_FROM is not None and addr < IGNORE_MEM_STATE_FROM:
            #print("In")
            if ty == 'Ity_I32':
                memval = state.mem[addr].int32_t.resolved
            elif ty == 'Ity_I64':
                memval = state.mem[addr].int64_t.resolved
            elif ty == 'Ity_I16':
                memval = state.mem[addr].int16_t.resolved
                #print("processLoad warning: assuming abstract value with 0 in the remaining 16 bits of addr", hex(addr))
            elif ty == 'Ity_I8':
                memval = state.mem[addr].int8_t.resolved
                #print("processLoad warning: assuming abstract value with 0 in the remaining 23 bits of addr", hex(addr))
            elif ty == 'Ity_F32' or ty == 'Ity_F64':
                #memval = state.mem[addr].float32_t.resolved
                print("processLoad warning: load of float at addr", hex(addr), "not considered")
                floataccess = True
            elif ty == 'Ity_V128':
                print("Vectorial access")
                
            else:
                print("unknown size in load to", addr)
                print(ty)
                sys.exit()
            #print("State.mem[", addr, "]:", memval)
            if not DISREGARD_MEM_IN_ABSINT:
                if floataccess or memval.uninitialized:
                    if __debug__ and DEBUG_MEM:
                        print("Addr:", hex(addr), "Init val: uninitialized or float")
                    apron.initExpr()
                    apron.addTopToExpr()
                    apron.assignExprToMem(addr)
                else:
                    constval = state.solver.eval(memval)
                    if __debug__ and DEBUG_MEM:
                        print("Addr:", hex(addr), "Init val:", constval)
                    apron.initExpr()
                    apron.addConstToExpr(constval)
                    apron.assignExprToMem(addr)
            elif not (floataccess or memval.uninitialized):
                apron.initExpr()
                apron.addConstToExpr(constval)
        else: # if DISREGARD_MEM_IN_ABSINT, assume Top
            #print("Out")
            apron.initExpr()
            apron.addTopToExpr()
            
        if not DISREGARD_MEM_IN_ABSINT:
            #print("Out2")
            # apron.initExpr() # not needed: addMemToExpr includes initExpr
            apron.addMemToExpr(addr)
    else:
        apron.initExpr()
        apron.addTopToExpr()
    return addr



def addMemAccess(CFGNode, PC, dest, addr, data_type, isload, isconditional):
    """Process a memory access statement"""
    global MemAccessAlreadySet
    global tested_loop

    # for main analysis, memory references are collected
    # for induction analysis, mem refs are discarded (they already exists)

    if data_type in ('Ity_I32', 'Ity_F32'):
        numbytes = 4
    elif data_type == 'Ity_I16':
        numbytes = 2
    elif data_type == 'Ity_I8':
        numbytes = 1
    elif data_type in ('Ity_F64,''Ity_I64'):
        numbytes = 8
    elif data_type in ('Ity_V128'):
        numbytes = 16
    else:
        print("unknown size in load to", hex(addr))
        print(data_type)
        sys.exit()

    if not MemAccessAlreadySet:
        #node = get_any_cfg_node(PC)
        node = CFGNode
        bbaddr = node.addr
        loopaddr = getDeepestLoop(node)
        if addr is None:
            addr = 0
        if isinstance(dest, pyvex.IRExpr.RdTmp):
            regName = "t"+str(dest.tmp)
            apron.addMemRef(set_node_id(CFGNode), bbaddr, PC, regName.encode('utf-8'), addr, numbytes,
                isload, isconditional, loopaddr)
        elif isinstance(dest, pyvex.IRExpr.Get):
            regName = "r"+str(dest.offset)
            apron.addMemRef(set_node_id(CFGNode), bbaddr, PC, regName.encode('utf-8'), addr, numbytes,
                isload, isconditional, loopaddr)
        elif isinstance(dest, pyvex.IRExpr.Const):
            apron.addMemConstRef(set_node_id(CFGNode), bbaddr, PC, dest.con.value, numbytes,
                isload, isconditional)
        else:
            print("addMemAccess: memRef type not implemented")
            dest.pp()
            sys.exit(-1)
    else:
        # induction analysis
        #1. localizar regName (descartar casos Const)
        if isinstance(dest, pyvex.IRExpr.RdTmp):
            regName = "t"+str(dest.tmp)
        elif isinstance(dest, pyvex.IRExpr.Get):
            regName = "r"+str(dest.offset)
        else:
            #print("addMemAccess: constant address")
            #dest.pp()
            return
        #2. para cada reg, evaluar constraint: regName - shadowReg
        bound = c_int()
        for indvar in registers:
            indvarName = "s" + str(indvar)
            if apron.sameBoundsDifferenceInterval(regName.encode('utf-8'), indvarName.encode('utf-8'), byref(bound)):
            #3. anotar los resultados constantes
                #print("Encontrado offset", bound.value, "para", indvarName, "en nodo", set_node_id(CFGNode), "@", hex(PC), regName)
                apron.addMemRefOffset(set_node_id(CFGNode), PC, tested_loop, regName.encode('utf-8'), indvar, bound.value)


############################ BINARY OPERATIONS ############################
def two_complement(value):
 #Funcion que devuelve el valor de una cadena hexadecimal en complemento a 2
 #Ha sido necesaria para controlar overflows en restas
 x = value
 if (value & 0x8000000000000000) == 0x8000000000000000 :
   x = -( (value ^ 0xFFFFFFFFFFFFFFFF) + 1 )
 return x
def is_arithmetic_bin_op(data):
    """ Returns whether data is an arithmetic binary operation """
    return bool(data.op in ('Iop_Add32', 'Iop_Sub32','Iop_Sub64','Iop_Add64','Iop_Add32x4','Iop_Sub32x4','Iop_Add64x2'))


def arithmeticBinOp(data):
    """Process an arithmetic (+,-) binary operation"""
    apron.initExpr()
    resta = 0
    # TODO: manage overflows!
    if data.op == 'Iop_Add32' or data.op == 'Iop_Add64':
        coef = 1
        
    elif data.op == 'Iop_Sub32' or data.op == 'Iop_Sub64':
        coef = -1
        resta = 1
    elif data.op == 'Iop_Add32x4' or data.op == 'Iop_Sub32x4'or data.op == 'Iop_Add64x2':
        apron.addTopToExpr()
        return
    else:
        print("Unknown arithmetic binary operation:")
        data.pp()
        sys.exit(-1)

    # proceso primer operando
    op0isconstant = False
    if isinstance(data.args[0], pyvex.IRExpr.RdTmp):
        # generar elem expresion "t%d", data.tmp
        regName = "t"+str(data.args[0].tmp)
        apron.addRegToExpr(1, regName.encode('utf-8'))
    elif isinstance(data.args[0], pyvex.IRExpr.Get):
        # generar elem expresion "r%d", data.offset
        regName = "r"+str(data.args[0].offset)
        apron.addRegToExpr(1, regName.encode('utf-8'))
    elif isinstance(data.args[0], pyvex.IRExpr.Const):
        # generar elem expresion
        apron.addConstToExpr(data.args[0].con.value)
        op0isconstant = True
    else:
        print("operando 0 en arithmetic binop no implementado:")
        print(type(data.args[0]))
        sys.exit(-1)

    # proceso segundo operando
    if isinstance(data.args[1], pyvex.IRExpr.RdTmp):
        # generar elem expresion "t%d", data.tmp
        regName = "t"+str(data.args[1].tmp)
        apron.addRegToExpr(coef, regName.encode('utf-8'))
    elif isinstance(data.args[1], pyvex.IRExpr.Get):
        # generar elem expresion "r%d", data.offset
        regName = "r"+str(data.offset)
        apron.addRegToExpr(coef, regName.encode('utf-8'))
    elif isinstance(data.args[1], pyvex.IRExpr.Const):
        # generar elem expresion
        #Aplicar complemento a 2 solo si se ha producido un overflow
        operand = data.args[1].con.value
        if resta == 1 and (operand * -1) < (-1 * 2**63):
         print("Warning Managed Overflaw: a 65 bits negative value was found!")
         data.pp()
         operand = two_complement(data.args[1].con.value)
        apron.addConstToExpr(coef*operand)
        if op0isconstant:
            print("Binary operation with two constants:")
            data.pp()
    else:
        print("operando 1 en arithmetic binop no implementado")
        print(type(data.args[1]))
        sys.exit(-1)


def is_geometric_bin_op(data):
    """ Returns whether data is a geometric binary operation """
    return bool(data.op in ('Iop_Shl32', 'Iop_Shr32','Iop_Shr64', 'Iop_Sar32', 'Iop_MullS32', 'Iop_Mul32','Iop_Mul64','Iop_MullU32','Iop_Sar64','Iop_DivModS64to32','Iop_Shl64','Iop_ShrN32x4','Iop_DivModU64to32'))


def geometricBinOp(data):
    """Process a geometric (*,/,sh,sa) binary operation"""
    apron.initExpr()

    if data.op == 'Iop_Shl32'or data.op == 'Iop_Shl64':
        # proceso segundo operando
        if isinstance(data.args[1], pyvex.IRExpr.Const):
            coef = 2**data.args[1].con.value
            #print("Shl: verify that shifted value is positive and does not overflow")
            print("Warning: ShiftLeft: assuming that shifted value is positive and does not overflow")
            # TODO:
            # 1. comprobar signo de primer operando
            #  - si es negativo: interpretar como natural
            #  - si desconocido: top
            # 2. comprobar magnitud
            #  - si <2^31: multiplicar por 2 como ahora
            #  - si >2^31: (x*2)-0x100000000
        else:
            print("operando 1 en geometric binop no implementado: Top")
            print(type(data.args[1]))
            apron.addTopToExpr()
            return
            #sys.exit(-1)

        #proceso primer operando
        if isinstance(data.args[0], pyvex.IRExpr.RdTmp):
            # generar elem expresion "t%d", data.tmp
            regName = "t"+str(data.args[0].tmp)
            apron.addRegToExpr(coef, regName.encode('utf-8'))
        elif isinstance(data.args[0], pyvex.IRExpr.Get):
            # generar elem expresion "r%d", data.offset
            regName = "r"+str(data.args[0].offset)
            apron.addRegToExpr(coef, regName.encode('utf-8'))
        elif isinstance(data.args[0], pyvex.IRExpr.Const):
            apron.addConstToExpr(coef*data.args[0].con.value)
        else:
            print("operando 0 en geometric binop no implementado")
            print(type(data.args[0]))
            sys.exit(-1)
    elif data.op == 'Iop_Mul32' or data.op == 'Iop_Mul64':
       # proceso segundo operando
       if isinstance(data.args[1],pyvex.IRExpr.Const):
            coef = data.args[1].con.value
       else:
            apron.addTopToExpr()
            return
       # proceso primer operando
       if isinstance(data.args[0],pyvex.IRExpr.RdTmp):
            #generar elem expresion "t%d", data.tmp
            regName = "t" + str(data.args[0].tmp)
            apron.addRegToExpr(coef,regName.encode('utf-8'))
       elif isinstance(data,args[0],pyvex.IRExpr.Get):
            #generar elem expresion "r%d", data offset
            regName = "r" + str(data.args[0].offset)
            apron.addRegToExpr(coef,regName.encode('utf-8'))
       else:
            print("operando 0 en mulop no implementado")
            print(type(data.args[0]))
            sys.exit(-1)

#    elif data.op == 'Iop_Shr32':
#        # proceso segundo operando
#        if isinstance(data.args[1], pyvex.IRExpr.Const):
#            num = 1
#            denom = 2**data.args[1].con.value
#            print("TODO: verify that shifted value is possitive")
#        else:
#            print("operando 1 en geometric binop no implementado")
#            print(type(data.args[1]))
#            sys.exit(-1)
#
#        #proceso primer operando
#        if isinstance(data.args[0], pyvex.IRExpr.RdTmp):
#            # generar elem expresion "t%d", data.tmp
#            regName = "t"+str(data.args[0].tmp)
#            apron.addRegToArray(num, denom, regName.encode('utf-8'))
#        elif isinstance(data.args[0], pyvex.IRExpr.Get):
#            # generar elem expresion "r%d", data.offset
#            regName = "r"+str(data.args[0].offset)
#            apron.addRegToArray(num, denom, regName.encode('utf-8'))
#        else:
#            print("operando 0 en geometric binop no implementado")
#            print(type(data.args[0]))
#            sys.exit(-1)

#    elif data.op == 'Iop_Sar32': # Arithemtic Shift Right
#        print("Arithmetic Shift Right not considered: Top")
#        apron.addTopToArray()
#    elif data.op == 'Iop_MullS32': # Widening multiply
#        print("MullS not considered: Top")
#        apron.addTopToArray()
#    elif data.op == 'Iop_Mul32':
#        print("Mul32 not considered: Top")
#        apron.addTopToArray()
    else:
        #data.pp()
        print("Geometric binary operation", data.op, "not considered: Top")
        apron.addTopToExpr()



def is_logic_bin_op(data):
    """ Returns whether data is a logic binary operation """
    return bool(data.op in ('Iop_Xor8','Iop_And8','Iop_Or8','Iop_Or32','Iop_Or64', 'Iop_And32', 'Iop_Xor32','Iop_Xor64','Iop_XorV128','Iop_And64','Iop_AndV128','Iop_OrV128'))
#    if data.op == 'Iop_Or32' or data.op == 'Iop_And32' or data.op == 'Iop_Xor32':
#        return True
#    else:
#        return False


def is_float_op(data):
    """ Returns whether data is a floating point operation """
    return bool(data.op in ('Iop_CmpF64', 'Iop_CmpF32', 'Iop_F64toI32S', 'Iop_F64toF32', 'Iop_SubF32', 'Iop_SubF64', 'Iop_MulF32', 'F32toF64', 'Iop_AddF32', 'Iop_F64toI32U', 'Iop_MulF64', 'Iop_AddF64', 'Iop_DivF64', 'Iop_DivF32','Iop_CmpLT32Fx4','Iop_Mul32F0x4','Iop_Mul32Fx4','Iop_Add32Fx4','Iop_Sub32Fx4','Iop_Div32Fx4','Iop_Sub32F0x4','Iop_Div32F0x4','Iop_CmpEQ32F0x4','Iop_CmpLE64F0x2','Iop_Add32F0x4','Iop_Add64F0x2','Iop_Sub64F0x2','Iop_Max64F0x2','Iop_Mul64F0x2','Iop_Div64F0x2','Iop_Div64Fx2','Iop_Mul64Fx2','Iop_Add64Fx2'))
    # unary: F32toF64
    # binary: Iop_CmpF64 Iop_CmpF32 Iop_F64toI32S Iop_F64toF32 Iop_F64toI32U
    # ternary: Iop_SubF32 Iop_SubF64 Iop_MulF32 Iop_AddF32 Iop_MulF64  Iop_AddF64 Iop_DivF64 Iop_DivF32


def logicBinOp(data):
    """Process a logic (and,or) binary operation"""
    apron.initExpr()
    apron.addTopToExpr()
    data.pp()
    print("Logic operations not considered: Top")


def is_comparison_bin_op(data):
    """ Returns whether data is a comparison binary operation """
    return bool(data.op in ('Iop_CmpNE8','Iop_CmpEQ8','Iop_CmpEQ32', 'Iop_CmpEQ64','Iop_CmpNE32', 'Iop_CmpLE32S', 'Iop_CmpLE32U','Iop_CmpLE64U', 'Iop_CmpLT32S', 'Iop_CmpLT32U','Iop_CmpLT64U','Iop_CmpLT64S','Iop_CmpNE64','Iop_CmpGT32Sx4'))


def comparisonBinOp(data):
    """Process a comparison (==,!=,<,>,<=,>=) binary operation"""
    # TODO: difference signed/unsigned
    resta = 0
    resta1 = 0
    apron.initExpr()
    # set type of constraint and coefficients
    if data.op == 'Iop_CmpEQ32'or data.op == 'Iop_CmpEQ64'or data.op == 'Iop_CmpEQ8':    # x1 == x2:
        apron.setConsEQ()           # x1 - x2 = 0; NEG: x1-x2!=0 or 0!=x2-x1
        coef1 = 1
        coef2 = -1
        resta = 1
    elif data.op == 'Iop_CmpNE8' or data.op == 'Iop_CmpNE32' or data.op =='Iop_CmpNE64':  # x1 != x2:
        apron.setConsDISEQ()        # x1 - x2 != 0; NEG: x1-x2==0 or 0==x2-x1
        coef1 = 1
        coef2 = -1
        resta = 1
    elif data.op == 'Iop_CmpLE32S' or data.op == 'Iop_CmpLE32U'or data.op == 'Iop_CmpLE64U':
        # x1 <= x2: - x1 + x2 >= 0; NEG: 0<x1-x2
        apron.setConsSUPEQ()
        coef1 = -1
        coef2 = 1
        resta1 = 1
    elif data.op == 'Iop_CmpLT32S' or data.op == 'Iop_CmpLT32U'or data.op == 'Iop_CmpLT64U'or data.op == 'Iop_CmpLT64S':
        # x1 < x2: - x1 + x2 > 0; NEG: 0<=x1-x2
        apron.setConsSUP()
        coef1 = -1
        coef2 = 1
        resta1 = 1
    elif data.op == 'Iop_CmpGT32Sx4':
        apron.addTopToExpr()
        return
    else:
        print("Unknown comparison binary operation:")
        data.pp()
        sys.exit(-1)

    # proceso primer operando
    if isinstance(data.args[0], pyvex.IRExpr.RdTmp):
        regName = "t"+str(data.args[0].tmp)
        apron.addRegToExpr(coef1, regName.encode('utf-8'))
    elif isinstance(data.args[0], pyvex.IRExpr.Get):
        regName = "r"+str(data.args[0].offset)
        apron.addRegToExpr(coef1, regName.encode('utf-8'))
    elif isinstance(data.args[0], pyvex.IRExpr.Const):
        operand = data.args[0].con.value
        if resta1 == 1 and (operand * -1) < (-1 * 2**63):
            print("Warning Managed Overflow: 65 bits value found in a compariosn")
            operand = two_complement(data.args[0].con.value)
        apron.addConstToExpr(coef1*operand)
    else:
        print("operando 0 en comparison binop no implementado:")
        print(type(data.args[0]))
        sys.exit(-1)

    # proceso segundo operando
    if isinstance(data.args[1], pyvex.IRExpr.RdTmp):
        regName = "t"+str(data.args[1].tmp)
        apron.addRegToExpr(coef2, regName.encode('utf-8'))
    elif isinstance(data.args[1], pyvex.IRExpr.Get):
        regName = "r"+str(data.offset)
        apron.addRegToExpr(coef2, regName.encode('utf-8'))
    elif isinstance(data.args[1], pyvex.IRExpr.Const):
        operand = data.args[1].con.value
        if resta == 1 and (operand * -1) < (-1 * 2**63):
            print("Warning Managed Overflow: 65 bits value found in a comparison")
            operand = two_complement(data.args[1].con.value)
        apron.addConstToExpr(coef2*operand)
    else:
        print("operando 1 en comparison binop no implementado")
        print(type(data.args[1]))
        sys.exit(-1)

    apron.completeCons() # preserves constraint


## Process an irvex expression
def studyExpr(data, currentPC, CFGNode):
    """ Process an irvex expression."""
    global state
    global ncomparisons
    global ncompsutiles
    #data.pp()
    if isinstance(data, pyvex.IRExpr.Binop):
        #print(data.op)
        if is_arithmetic_bin_op(data):
            arithmeticBinOp(data)
        elif is_comparison_bin_op(data):
            ncomparisons = ncomparisons + 1
            if ncomparisons == 1:
                ncompsutiles = ncompsutiles + 1
                comparisonBinOp(data)
            elif ncomparisons == 2:
                ncompsutiles = 0
                #No hay comparaciones utiles en el bloque basico
                print("WARNING: more than 1 comparison in basic block")
                print("Discarding previous of them")
                apron.discardCons()
                #La ultima comparacion encontrada en el bloque basico deberia ser aquella que corresponda al salto en x86_64
                #comparisonBinOp(data)
                #print("Normal Execution")
                #sys.exit(-1)
            # else: already discarded and reported
        elif is_geometric_bin_op(data):
            geometricBinOp(data)
        elif is_logic_bin_op(data):
            logicBinOp(data)
            if ncomparisons > 0:
                ncomparisons = 0
                if __debug__ and DEBUG_COMPARE:
                    print("Warning: comparaciones a 0")
        elif is_float_op(data):
            print("Floating point operations not considered: Top")
            apron.initExpr()
            apron.addTopToExpr()
        elif data.op == 'Iop_32HLto64':
            print("32HbLto64 operation not considered: Top")
            apron.initExpr()
            apron.addTopToExpr()
        elif data.op == 'Iop_64HLtoV128':
            print("64HLtoV128 operation not considered: Top")
            apron.initExpr()
            apron.addTopToExpr()
        elif data.op == 'Iop_F64toI64S'or 'Iop_I64StoF64':
            apron.initExpr()
            if isinstance(data.args[0], pyvex.IRExpr.RdTmp):
               # generar elem expresion "t%d", data.tmp
               regName = "t"+str(data.args[0].tmp)
               apron.addRegToExpr(1, regName.encode('utf-8'))
            elif isinstance(data.args[0], pyvex.IRExpr.Get):
               # generar elem expresion "r%d", data.offset
               regName = "r"+str(data.args[0].offset)
               apron.addRegToExpr(1, regName.encode('utf-8'))
        elif data.op == 'Iop_InterleaveLO32x4'or data.op == 'Iop_InterleaveLO64x2':
             print("Interleave Instruction not considered")
             apron.initExpr()
             apron.addTopToExpr()
        else:
            data.pp()
            print(data.op)
            print("operación en binop no implementada")
            sys.exit(-1)
    elif isinstance(data, pyvex.IRExpr.Unop):
        if data.op == 'Iop_Not32' or data.op == 'Iop_Not1':
            if ncomparisons > 0:
                ncomparisons = ncomparisons - 1
                if __debug__ and DEBUG_COMPARE:
                    print("Warning: comparación descartada")
        apron.initExpr()
        apron.addTopToExpr()
        if __debug__ and DEBUG_IRVEX:
            data.pp()
            print("  unary op: no impl")
#        # print("  unary op")
#        data.pp()
#        apron.initExpr()
#        if data.op == 'Iop_1Uto32' or data.op == 'Iop_32to1' or data.op == 'Iop_32to8':
#            # 1b to 32b, unsigned; 32b to 1b
#            if isinstance(data.args[0], pyvex.IRExpr.RdTmp):
#                regName = "t"+str(data.args[0].tmp)
#                apron.addRegToArray(1, regName.encode('utf-8'))
#            elif isinstance(data.args[0], pyvex.IRExpr.Get):
#                # generar elem expresion "r%d", data.offset
#                regName = "r"+str(data.args[0].offset)
#                apron.addRegToArray(1, regName.encode('utf-8'))
#            else:
#                data.pp()
#                print("unary operand: no impl")
#                sys.exit(-1)
#        elif data.op == 'Iop_Not32': # 2s complement: not x = -x - 1
#            print("Iop_Not32 not considered: Top")
#            print("MAY REVERSE A CONDITION!!!!")
#            if (ncomparisons > 0):
#                ncomparisons = ncomparisons - 1
#                print("Warning: comparación perdida")
#            apron.addTopToArray()
#        elif data.op == 'Iop_Not1': # not x = x - 1 = x + 1
#            print("Iop_Not1 not considered: Top")
#            print("MAY REVERSE A CONDITION!!!!")
#            if (ncomparisons > 0):
#                ncomparisons = ncomparisons - 1
#                print("Warning: comparación perdida")
#            apron.addTopToArray()
#        elif data.op == 'Iop_64HIto32': # I64 -> I32, high half
#            print("Iop_64HIto32 not considered: Top")
#            apron.addTopToArray()
#        else:
#            data.pp()
#            print("  unary op: no impl")
#            sys.exit(-1)
    elif isinstance(data, pyvex.IRExpr.ITE):
        print("Ternary if-then-else not considered: Top")
        #data.pp()
        #print("  ternary if-then-else: no impl")
#        print("  Ternary If-Then-Else (operación predicada). Si se cumple:")
#        data.cond.pp()
#        print("  entonces:")
#        data.iftrue.pp()
#        print("  Si no:")
#        data.iffalse.pp()
#        print("  Currently implemented as Top")
        apron.initExpr()
        apron.addTopToExpr()
        if ncomparisons > 0:
            ncomparisons = ncomparisons - 1
            if __debug__ and DEBUG_COMPARE:
                print("Warning: comparación perdida")
    elif isinstance(data, pyvex.IRExpr.Triop):
        if is_float_op(data):
            print("Floating point operations not considered")
            apron.initExpr()
            apron.addTopToExpr()
        else:
            print("  ternary op: no impl")
            data.pp()
            sys.exit(-1)
    elif isinstance(data, pyvex.IRExpr.Qop):
        print("  quaternary op: no impl")
        data.pp()
        sys.exit(-1)
    elif isinstance(data, pyvex.IRExpr.RdTmp):
        #print("  lectura reg temp: ", data.tmp)
        apron.initExpr()
        regName = "t"+str(data.tmp)
        apron.addRegToExpr(1, regName.encode('utf-8'))
    elif isinstance(data, pyvex.IRExpr.Const):
        if isinstance(data.con.value, float):
            print("  const: ",data.con)
            print("Floating point operations not considered")
            apron.initExpr()
            apron.addTopToExpr()
        else:
            apron.initExpr()
            if data.con.value < 2**64:
             apron.addConstToExpr(data.con.value)
            else:
             print("Warning:Const over 64 bits Top:",data.con.value)
             apron.addTopToExpr()
    elif isinstance(data, pyvex.IRExpr.Get):
        # print("  registro: ", " ; r"+str(data.offset))
        apron.initExpr()
        regName = "r"+str(data.offset)
        apron.addRegToExpr(1, regName.encode('utf-8'))
    elif isinstance(data, pyvex.IRExpr.GetI):
        print("  registro (bits específicos): no impl")
        sys.exit(-1)
    elif isinstance(data, pyvex.IRExpr.Load):
        #constval = processLoad(data.addr, data.ty)
        addr = processLoad(data.addr, data.ty)
        if __debug__ and DEBUG_MEM:
            #print(" Lectura de mem @", data.addr, ":", constval)
            print(" Lectura de mem @", data.addr)
        addMemAccess(CFGNode, currentPC, data.addr, addr, data.ty, 1, 0)
    elif isinstance(data, pyvex.IRExpr.CCall):
        if __debug__ and DEBUG_COMPARE:
            data.pp()
            print("  CCall (calculate_flag/condition) no implementada: Top")
        if ncomparisons != 0:
            if __debug__ and DEBUG_COMPARE:
                print("Number of comparisons in this basic block is", ncomparisons)
            ncomparisons = 0
        apron.initExpr()
        apron.addTopToExpr()
    else:
        data.pp()
        print("  expresión no implementada:", type(data))
        sys.exit(-1)


############################ LOOP/NODE OPERATIONS ############################
def getDeepestLoop(node):
    """get deepest loop including CFGnode node"""
    loopnode = None
    for loop in loop_info.loops:
        if node.to_codenode() in loop.body_nodes:
            if loopnode is None:
                loopnode = loop
            else:
                if loopnode not in loop.subloops:
                    loopnode = loop
    if loopnode is None:
        return 0
    else:
        return loopnode.entry.addr

def set_node_id(node):
    """must fit in 32 bits"""
    return cfg_nodes[node]

def set_alternative_id(node):
    """must fit in 32 bits"""
    return -cfg_nodes[node]

def get_cfgid_node(nodeid):
    """ Gets node from identifier """
    for node in cfg_nodes:
        if cfg_nodes[node] == nodeid:
            return node
    print("get_cfgid_node: nodeID %d not found." % nodeid)
    sys.exit(-1)

def get_any_cfg_node(instaddr):
    """ Gets any node including instaddr """
    while (cfg.model.get_any_node(instaddr) is None) and (instaddr > 0):
        instaddr -= 1
    return cfg.model.get_any_node(instaddr)


def getLoopSuccessor(CFGNode):
    for loop in loop_info.loops:
        if CFGNode.addr == loop.entry.addr:
            return cfg.model.get_any_node(loop.break_edges[0][1].addr)
    return False


def getLoop(nodeaddr):
    for loop in loop_info.loops:
        if nodeaddr == loop.entry.addr:
            return loop
    return None


## Test whether there is a back-edge from previous to CFGNode (loop header)
def isBackEdge(CFGNode, prevNode):
    """ Returns whether prevNode -> CFGNode is a back (continue) edge """
    for loop in loop_info.loops:
        if CFGNode.addr == loop.entry.addr:
            if (prevNode.to_codenode(), CFGNode.to_codenode()) in loop.continue_edges:
                return True
            # case of body basicblock ending with call, and header just in the next address
            node_above = get_any_cfg_node(CFGNode.addr - 1)
            if node_above.to_codenode() in loop.body_nodes:
                if len(node_above.successors) == 1 and node_above.successors[0] != CFGNode:
                    called_function = cfg.kb.functions.function(addr=node_above.successors[0].addr)
                    if called_function and prevNode.to_codenode() in set(called_function.nodes):
                        #print("isBackEdge warning: Revise whether node", node_above, "calls a function reaching", prevNode, "and returns to the loop header", CFGNode)
                        return True
    return False


############################ EXPLORE CFG ############################

## Process the CFG. Initially, call as exploreLoop2(first,last,first,first)
#  In-depth recursive CFG processing.
#
#  @param[in] first First CFG node to process
#  @param[in] last CFG node to stop (last not processed)
#  @param[in] CFGNode current CFG node to process
#  @param[in] previous previously processed CFG node
#def exploreLoop2(first, last, CFGNode, previous):
def exploreLoop2(first, CFGNode, previous):
    global visits

    if __debug__ and DEBUG_FIXPOINT:
        print("New CFG Node:", hex(CFGNode.addr), ", previous:", hex(previous.addr))
        #print("successors", CFGNode.successors)
    #if CFGNode == last:
    if CFGNode not in cfg.graph.nodes():
        if __debug__ and DEBUG_FIXPOINT:
            #print("last node reached")
            print("nuevo bloque fuera de bloques válidos: no se analiza")
        return
    if CFGNode in visits:
        if __debug__ and DEBUG_FIXPOINT:
            print(hex(CFGNode.addr), "had", visits[CFGNode], "visits")
        visits[CFGNode] += 1
        if isBackEdge(CFGNode, previous):
            # TODO recover limits / condition as array
            if __debug__ and DEBUG_FIXPOINT:
                print("widening: node.IN <- node.prev.IN \/ (node.prev.IN U node.IN)")
            if apron.wideningStatesNeedsUpdate(set_node_id(CFGNode)):
                pass
            else:
                if __debug__ and DEBUG_FIXPOINT:
                    print("widen: no changes")
                return # no changes; no further processing needed
        else:
            if __debug__ and DEBUG_FIXPOINT:
                print("join: node.in <- node.in JOIN prev.out")
            if apron.joinStatesNeedsUpdate(set_node_id(CFGNode)):
                pass
            else:
                if __debug__ and DEBUG_FIXPOINT:
                    print("join: no changes")
                return # no changes; no further processing needed
    else: # not yet visited
        if __debug__ and DEBUG_FIXPOINT:
            print("nuevo bloque, id", set_node_id(CFGNode))
            print(CFGNode)
        apron.newNodeState(set_node_id(CFGNode), CFGNode.addr) # set IN state
        if CFGNode != first:
            visits[CFGNode] = 1
        else:
            visits[CFGNode] = 99999
#       if "+0x" not in CFGNode.name:
#           numcfgnodes += len(cfg.kb.functions.function(name=CFGNode.name).nodes)
        print(len(visits), "("+hex(CFGNode.addr)+") of", len(cfg.graph.nodes()), "nodes visited")

    # analizar
    if __debug__ and DEBUG_FIXPOINT:
        print("analizar bloque (generar node.out):")
        print("")
    numcmps,usefulcmps = generateOutState(CFGNode)
    if __debug__ and DEBUG_FIXPOINT:
        print("")
    apron.setOutState(set_node_id(CFGNode))
    if len(CFGNode.successors) == 0: # exit: no more nodes
        if __debug__ and DEBUG_FIXPOINT:
            print("no more nodes")
        return
    else: # len(CFGNode.successors)>1
        for childNode in CFGNode.successors:
            unconditional = False
            if __debug__ and DEBUG_FIXPOINT:
                print("exploring successor", hex(childNode.addr), "from", hex(CFGNode.addr), set_node_id(CFGNode))
            apron.setDefaultState(set_node_id(CFGNode))
            # get branch condition, if any
            #irsb = proj.factory.block(CFGNode.addr).vex
            irsb = CFGNode.block.vex
            branch = irsb.statements[len(irsb.statements)-1]
            if isinstance(branch, pyvex.IRStmt.Exit):
                if __debug__ and DEBUG_FIXPOINT:
                    branch.pp()
                if branch.dst.value == childNode.addr:
                    if __debug__ and DEBUG_FIXPOINT:
                        print(" taken branch")
                    if numcmps > 0 and usefulcmps > 0:
                        notbottom = apron.addConstraint()
                    else:
                        notbottom = True
                        if __debug__ and DEBUG_COMPARE:
                            print("Warning: condition lacking!")
                else:
                    if __debug__ and DEBUG_FIXPOINT:
                        print(" not taken branch")
                    if numcmps > 0 and usefulcmps > 0:
                        notbottom = apron.addNegConstraint()
                        #print("Aqui")
                    else:
                        notbottom = True
                        if __debug__ and DEBUG_COMPARE:
                            print("Warning: condition lacking!")
            else:
                if __debug__ and DEBUG_FIXPOINT:
                    print(" no conditional branch")
                unconditional = True
                notbottom = True
            if notbottom:
                #exploreLoop2(first, last, childNode, CFGNode)
                exploreLoop2(first, childNode, CFGNode)
            else:
                if __debug__ and DEBUG_FIXPOINT:
                    print("unfeasible path")
        return


## Process loop CFG to test whether indreg is an induction variable
#
#  @param[in] loop Loop to explore for induction variable
#  @param[in] indreg register to test whether it is induction var.
#  @param[in] cfgnode current CFG node to process
def induction_explore(loop, cfgnode, previous, nodeset, loopCFGNode):
    global alreadyExplored

    if __debug__ and DEBUG_FIXPOINT_INDVARS:
        print("New CFG Node:", hex(cfgnode.addr), ", previous:", hex(previous.addr))
    if cfgnode.addr not in nodeset:
        print("Node", hex(cfgnode.addr), "outside nodeset of loop", hex(loop.entry.addr))
        return
    if cfgnode in alreadyExplored:
        if __debug__ and DEBUG_FIXPOINT_INDVARS:
            print("already explored")
        if isBackEdge(cfgnode, previous):
            if apron.wideningStatesNeedsUpdate(set_alternative_id(cfgnode)):
                pass
            else:
                if __debug__ and DEBUG_FIXPOINT_INDVARS:
                    print("return from widen")
                return
        else:
            if apron.joinStatesNeedsUpdate(set_alternative_id(cfgnode)):
                pass
            else:
                if __debug__ and DEBUG_FIXPOINT_INDVARS:
                    print("return from join")
                return
    else:
        if __debug__ and DEBUG_FIXPOINT_INDVARS:
            print("new node:", hex(cfgnode.addr))
        apron.newNodeState(set_alternative_id(cfgnode), -cfgnode.addr)
        alreadyExplored.append(cfgnode)

    numcmps,usefulcmps = generateOutState(cfgnode)
    apron.setOutState(set_alternative_id(cfgnode))

    if len(cfgnode.successors) == 0: # exit: no more nodes
        if __debug__ and DEBUG_FIXPOINT_INDVARS:
            print("no more nodes")
        return
    else: # len(CFGNode.successors)>1
        #print("for", cfgnode.successors)
        for childNode in cfgnode.successors:

            unconditional = True
            #if childNode.to_codenode() in loop.body_nodes:
            # WRONG! calls inside loops are not body nodes
            if (cfgnode.to_codenode(), childNode.to_codenode()) not in loop.break_edges:
                apron.setDefaultState(set_alternative_id(cfgnode))
                # get branch condition, if any
                irsb = cfgnode.block.vex
                branch = irsb.statements[len(irsb.statements)-1]
                if isinstance(branch, pyvex.IRStmt.Exit):
                    if __debug__ and DEBUG_FIXPOINT_INDVARS:
                        branch.pp()
                    if branch.dst.value == childNode.addr:
                        if __debug__ and DEBUG_FIXPOINT_INDVARS:
                            print(" taken branch")
                        if numcmps > 0 and usefulcmps > 0:
                            notbottom = apron.addConstraint()
                        else:
                            notbottom = True
                            if __debug__ and DEBUG_COMPARE:
                                print("Warning: condition lacking!")
                    else:
                        if __debug__ and DEBUG_FIXPOINT_INDVARS:
                            print(" not taken branch")
                        if numcmps > 0 and usefulcmps > 0:
                            notbottom = apron.addNegConstraint()
                        else:
                            notbottom = True
                            if __debug__ and DEBUG_COMPARE:
                                print("Warning: condition lacking!")
                else:
                    if __debug__ and DEBUG_FIXPOINT_INDVARS:
                        print(" no conditional branch")
                    notbottom = True
                    unconditional = True
                if notbottom:
                    if childNode.addr == loop.entry.addr:
                        if __debug__ and DEBUG_FIXPOINT_INDVARS:
                            print("header reached")
                        indvars.collectStepInfo(loopCFGNode, loop.entry.addr, registers) #, reginreg)
                    else:
                        induction_explore(loop, childNode, cfgnode, nodeset, loopCFGNode)
                else:
                    if __debug__ and DEBUG_FIXPOINT_INDVARS:
                        print("unfeasible path")
            # else is break edge
            else:
                if __debug__ and DEBUG_FIXPOINT_INDVARS:
                    print("break edge")
#            else:
#                if __debug__ and DEBUG_FIXPOINT_INDVARS:
#                    print("not analyzing node outside", nodeset)
        if __debug__ and DEBUG_FIXPOINT_INDVARS:
            print("no more nodes")
        return


def find_induction_vars():
    global loop_headers, alreadyExplored, tested_loop
    i = 1
    for headernode in loop_headers:
        loop = loop_headers[headernode]
        print("Analyzing loop at node", set_node_id(headernode), "at", hex(loop.entry.addr), "(" + str(i) + " of " + str(len(loop_headers)) + ")")
        headerset = False
        for pred in headernode.predecessors:
            if pred.to_codenode() not in loop.body_nodes:
                #print("Pred node:", hex(pred.addr))
                if apron.nodeExists(set_node_id(pred), pred.addr):
                    apron.setDefaultState(set_node_id(pred))
                    if not headerset:
                        apron.newNodeState(set_alternative_id(headernode), -headernode.addr)
                        headerset = True
                    else:
                        apron.joinStatesNeedsUpdate(set_alternative_id(headernode))
                    apron.setOutState(set_alternative_id(headernode)) # OUT = joined INs
                else:
                    print(" not analyzing loop from unreachable block", hex(pred.addr))
        #if apron.nodeExists(set_alternative_id(headernode), -headernode.addr):
        if headerset:
            apron.setDefaultState(set_alternative_id(headernode)) # OUT=joined IN(s)
            for reg in registers:
                apron.setShadowReg(reg)
            #----------------- especificar nodos, no addrs
            loop_addr_set = list(set(getLoopNodeSet(loop)))
            for addr in loop_addr_set:
                visited_node = False
                for node in visits:
                    if addr == node.addr:
                        visited_node = True
                if not visited_node:
                    loop_addr_set.remove(addr)

            #print("Loop addr set:", loop_addr_set)
            #tested_loop_id = set_node_id(headernode)
            tested_loop = headernode.addr
            alreadyExplored = [] # global var for induction_explore
            induction_explore(loop, headernode, headernode, loop_addr_set, headernode)
            #print("clearing negative nodes...")
            apron.clearNegativeNodes()
            #print("negative nodes cleared")
        else:
            print("not analyzing unreachable loop", hex(headernode.addr))
        i += 1


def dominates(idom, start, i1, i2):
    if apron.memRefIsConditional(i1):
        return False
    else:
        PC1 = apron.getMemRefPC(i1)
        PC2 = apron.getMemRefPC(i2)
        nodeid1 = apron.getMemRefCFGid(i1)
        nodeid2 = apron.getMemRefCFGid(i2)
        node1 = get_cfgid_node(nodeid1)
        node2 = get_cfgid_node(nodeid2)
        if node1 == node2:
            if PC1 == PC2:
                return bool(apron.orderedMemRefs(i1, i2))
            elif PC1 < PC2:
                return True
        else:
            return node_dominates(idom, start, node1, node2)

def node_dominates(idom, start, n1, n2): # n1 dominates n2?
    if idom[n2] == n1:
        return True
    elif idom[n2] == start:
        return False
    else:
        return node_dominates(idom, start, n1, idom[n2])

def calcGroupReuse(cfg, start, linesize):
    #idom = networkx.immediate_dominators(cfg.graph, start)

    i1 = 0
    PC1 = apron.getMemRefPC(i1)
    while PC1 != 0:
        i2 = i1 + 1
        PC2 = apron.getMemRefPC(i2)
        while PC2 != 0:
            if apron.haveGroupReuse(i1, i2, linesize):
                if dominates(idom, start, i1, i2): # i1 dominates i2
                    if (not apron.memRefIsConditional(i1) and
                        ((apron.getLastGR(i2) == 0) or
                        dominates(idom, start, apron.getIndexMemRefLast(i2), i1))):
                        # n2.prevdom doms n1
                        apron.setLastGR(i2, i1)
                elif dominates(idom, start, i2, i1): # if i2 dominates i1
                    if (not apron.memRefIsConditional(i2) and
                        ((apron.getLastGR(i1) == 0) or
                        dominates(idom, start, apron.getIndexMemRefLast(i1), i2))):
                        # n1.prevdom doms n2
                        apron.setLastGR(i1, i2)
            i2 += 1
            PC2 = apron.getMemRefPC(i2)
        i1 += 1
        PC1 = apron.getMemRefPC(i1)


def get_addr_set(func):
    return get_addr_set_recur(func, [])


def get_addr_set_recur(func, addrset):
    #print("nodes of", func.name, ":", func.nodes)
    for node in set(func.nodes):
        if node.addr not in addrset:
           addrset.append(node.addr)
    for call in set(func.get_call_sites()):
        calledf = func.get_call_target(call)
        if calledf != func.addr: # do not follow recursive calls
            get_addr_set_recur(cfg.kb.functions[calledf], addrset)
    return addrset


def get_node_set(func):
    #print("nodes of", func.name, ":", func.nodes)
    for node in set(func.nodes):
        if node not in get_node_set.nodeset:
            get_node_set.nodeset.append(node)
    for call in set(func.get_call_sites()):
        calledf = func.get_call_target(call)
        if calledf != func.addr: # do not follow recursive calls
            get_node_set(cfg.kb.functions[calledf])
    return get_node_set.nodeset
get_node_set.nodeset = []



def getLoopNodeSet(loop):
    loopnodeset = []
    for blocknode in loop.body_nodes:
        loopnodeset.append(blocknode.addr)
    if loop.has_calls:
        for blocknode in loop.body_nodes:
            #print(blocknode.addr)
            node = cfg.model.get_any_node(blocknode.addr)
            #for successor in blocknode.successors():
            # bug? blocknode.successors() fails sometimes
            for successor in node.successors:
                func = cfg.kb.functions.function(addr=successor.addr)
                if func:
                    #loopnodeset += getNodeSet(func)
                    for node2 in get_node_set(func):
                        loopnodeset.append(node2.addr)
    return loopnodeset



def classify_successors(cfg_node, loop):
    """ classifies, removing continue_edges to other loops and returns from body_nodes """
    internal_nodes = []
    break_nodes = []
    continue_nodes = []
    continue_to_other_loops = []
    #print(cfg_node)
    #print("Successors (and type of jump) of the entry point:", [ jumpkind + " to " + str(node.addr) for node,jumpkind in cfg.get_successors_and_jumpkind(cfg_node) ])
    #for dest_node in cfg_node.successors:
    for dest_node, jumpkind in cfg.model.get_successors_and_jumpkind(cfg_node):
        if dest_node.addr == loop.entry.addr: # continue_edge
            continue_nodes.append(dest_node)
        else: # no continue_edge
            is_break_node = False
            for (src, dst) in loop.break_edges:
                if dest_node.addr == dst.addr:
                    is_break_node = True
                    break
            if is_break_node:
                break_nodes.append(dest_node)
            else: # internal node or return node
                #print(dest_node, jumpkind)
                # discard continue_edges to other loops
                if not isBackEdge(dest_node, cfg_node):
                    # discard returns from body_nodes
                    #if not (cfg_node.to_codenode() in loop.body_nodes and jumpkind == "Ijk_Ret"):
                    if not (cfg_node.to_codenode() in loop.body_nodes and jumpkind == "Ijk_Ret"):
                        #print("-", dest_node, jumpkind)
                        internal_nodes.append(dest_node)
                    else: # return from body_nodes
                        break_nodes.append(dest_node)
                else:
                    continue_to_other_loops.append(dest_node)
    #if len(cfg.model.get_successors_and_jumpkind(cfg_node)) != (len(internal_nodes) + len(break_nodes) + len(continue_nodes)):
    if len(cfg.model.get_successors_and_jumpkind(cfg_node)) != (len(internal_nodes) + len(break_nodes) + len(continue_nodes) + len(continue_to_other_loops)):
        print("classify_successors: error: node lost")
        sys.exit(-1)
    return (internal_nodes, break_nodes, continue_nodes)


def get_successors_in_list(cfg_node, addr_list, node_list):
    for node in cfg_node.successors:
        if node.addr in addr_list and node not in node_list:
            node_list += get_successors_in_list(node, addr_list, node_list + [node])
    return list(set(node_list))


def compareNodes(visits, nodeset):
    for node in nodeset:
        if node not in visits:
            print(hex(node.addr), "counted but not visited")
    for node in visits: # this part cannot happen if nodeset is cfg.graph.nodes()
        if node not in nodeset:
            print(hex(node.addr), "visited but not counted")
            # several nodes may have the same addr
    visitedAddrs = []
    for node in visits:
        visitedAddrs.append(node.addr)
    return visitedAddrs


def get_enclosing_call_list(node, func): # returns list of nodes
    if node.to_codenode() in set(func.nodes):
        return [node]
    else:
        callerlist = []
        for call in set(func.get_call_sites()): # for calls in func
            calledf = func.get_call_target(call) # called function
            if func != cfg.kb.functions[calledf]: # if not recursive
                callerlistaux = []
                callerlistaux = get_enclosing_call_list(node, cfg.kb.functions[calledf]) # calls in called func
                if len(callerlistaux) != 0: # some call found
                    # callerlist.append(call)
                    #print("call:", call)
                    domnode = get_dominant_node(call, node)
                    if domnode:
                        callerlistaux.append(domnode)
                        callerlist += callerlistaux
    return callerlist

def get_dominant_node(addr, dominatedNode):
    while (cfg.model.get_any_node(addr) is None) and (addr > 0):
        addr -= 1 #Posible Cambio?
    for node in cfg.model.get_all_nodes(addr):
        if node_dominates(IDOM, IDOMSTART, node, dominatedNode):
            return node
    #print("get_dominant_node: No dominant node found in addr", hex(addr), "for node", dominatedNode)
    #print("candidates:", cfg.model.get_all_nodes(addr))

def enclosingLoop(loopaddr):
    encl = None
    deeperloop = getLoop(loopaddr)
    for loop in loop_info.loops: # for all loops
        if deeperloop in loop.subloops:
            if (encl is None) or (loop in encl.subloops):
                encl = loop
    if encl is None:
        return -1
    else:
        return encl.entry.addr


def genReuseTheoryVectors(idom, start, loopindvars):
    i = 0
    PC = apron.getMemRefPC(i)
    CFGid = apron.getMemRefCFGid(i)
    cfg_node = get_cfgid_node(CFGid)
    while PC != 0: # for all memory references
        #print("memRef:", i)
        apron.resetNestInfo()
        nodelist = get_enclosing_call_list(cfg_node, fmain)
        if len(nodelist) == 0:
            addrlist = [cfg_node]

        prev_node = None
        for (loopCFGNode, loopaddr, reg) in loopindvars: # for all induction variables
            # TODO: order loopindvars by loopCFGNode
            if loopCFGNode != prev_node:
                loop_addr_list = getLoopNodeSet(loop_headers[loopCFGNode])
                loop_node_list = get_successors_in_list(loopCFGNode, loop_addr_list, [loopCFGNode])
#                if len(loop_addr_list) != len(loop_node_list):
#                    # This is normal with CFGEmulated
#                    print("lists have different elements")
#                    print("addr list", loop_addr_list)
#                    print("node list", loop_node_list)
                prev_node = loopCFGNode
            if cfg_node in loop_node_list: # if memref is inside loop
                thisloopinfo = loopindvars[(loopCFGNode, loopaddr, reg)]
                if (thisloopinfo.valid == True) and (thisloopinfo.step != 0):
                    if thisloopinfo.constantBase == True:
                        apron.addNestInfo(CFGid, set_node_id(loopCFGNode), loopaddr, reg, thisloopinfo.step, thisloopinfo.base, 1)
                    else:
                        apron.addNestInfo(CFGid, set_node_id(loopCFGNode), loopaddr, reg, thisloopinfo.step, 0, 0)
        apron.genReuseTheoryMemRef(i)
        i += 1
        PC = apron.getMemRefPC(i)
        CFGid = apron.getMemRefCFGid(i)
        cfg_node = get_cfgid_node(CFGid)


def get_execs_in_function(node):
    # TODO: change function name
    execs = 1
    for loop in loop_info.loops: # for all loops
        if node.to_codenode() in loop.body_nodes: # if it is in loop
            execs *= flowfacts.maxLoopIter[loop.entry.addr]
    return execs


def get_recursive_execs(node, func):
    """
    Calculates the number of executions of the basic block associated to the node. If the basic block is associated to more than one node, it counts the executions considering all the nodes.
    """
    if node.to_codenode() in set(func.nodes):
        execs = get_execs_in_function(node)
        if __debug__ and DEBUG_COST:
            print("   ", execs, "executions in", func.name)
        return execs
    else: # node not in func
        maxexecs = 0
        for call in list(func.get_call_sites()): # for calls in func
            called_addr = func.get_call_target(call) # called address
            #if func != cfg.kb.functions[calledf]: # if not recursive
            called_func = cfg.kb.functions[called_addr]
            addr_list = get_addr_set(called_func)
            #print("  addrs in ", called_func.name, ":", addr_list)
            call_node_addr = call
            while (cfg.model.get_any_node(call_node_addr) is None) and (call_node_addr > 0):
                call_node_addr -= 1 #Posible Cambio?
            for call_node in cfg.model.get_all_nodes(call_node_addr):
                node_list = get_successors_in_list(call_node, addr_list, [call_node])
                if node in node_list:
                    in_call_execs = get_recursive_execs(node, called_func)
                    times_called = get_execs_in_function(call_node)
                    maxexecs += in_call_execs * times_called
                    if __debug__ and DEBUG_COST:
                        print("   ", times_called, "executions of", called_func.name, "from", hex(call), "(" + func.name + ")")
        if __debug__ and DEBUG_COST:
            if maxexecs != 0:
                print("   Total:", maxexecs, "executions in", func.name)
        return maxexecs


def setMaxExecs():
    i = 0
    PC = apron.getMemRefPC(i)
    while PC != 0: # for all memory references
        nodeid = apron.getMemRefCFGid(i)
        node = get_cfgid_node(nodeid)
        if __debug__ and DEBUG_COST:
            print("%d)" % i, hex(PC), node, nodeid)
        #execs = calcMaxExecs(node)
        execs = get_recursive_execs(node, fmain)
        #print(hex(PC), "/", PC, "execs in total:", execs)
        apron.addCount(i, execs)
        i += 1
        PC = apron.getMemRefPC(i)


#def calcGRDistance(idom, start, last):
#    dist = 0
#    lastPC = apron.getMemRefPC(last)
#    first = apron.getIndexMemRefLast(last)
#    firstPC = apron.getMemRefPC(first)
#    i = 0
#    PC = apron.getMemRefPC(i)
#    while PC != 0: # for all memory references
#        if i != last and i != first:
#            if dominates(idom, start, i, last) and dominates(idom, start, first, i):
#                dist += 1 # TODO: OJO, si está en un bucle, sumar tantas líneas como toque
#                # TODO: OJO, varios accesos a la misma dirección (GR) no incrementan
#                # TODO: OJO, accesos a otros conjuntos no incrementan
#        i += 1
#        PC = apron.getMemRefPC(i)
#    return dist
#
#def calcDistances(idom, start):
#    i = 0
#    PC = apron.getMemRefPC(i)
#    while PC != 0: # for all memory references
#        #if apron.hasGroupReuse(i): # LRU only, not needed for ACDC
#        if apron.getLastGR(i): # LRU only, not needed for ACDC
#            grdist = calcGRDistance(idom, start, i)
#            print("GR distance of", i, "is", grdist)
##        elif apron.hasSelfReuse(i):
##            calcTempDistance(i) # scalar and array
##            # TODO: ojo! podemos tener varios bucles internos con temporal, pero con distintas distancias. Para LRU podríamos tener hits/misses solo a veces. Asumir distancia máxima por simplificar?
##            calcSpaDistance(i) # array only
#        i += 1
#        PC = apron.getMemRefPC(i)


def main():
    if len(sys.argv) != 2:
        print("usage:", sys.argv[0], "<binaryfile>")
        sys.exit()
    filename = sys.argv[1]
    cache_line_size = 64 # bytes (default intel/arm L1: 64 B)
    reuse_analysis(filename, False, cache_line_size)

    setMaxExecs()
    apron.classify(cache_line_size)


def reuse_analysis(filename, instance_functions, cacheLineSize):
    global visits, MemAccessAlreadySet

    part_time = time.time()
    init(filename, instance_functions)
    print("--- Init time:", round(time.time() - part_time, 2), "seconds ---")
    part_time = time.time()

    apron.newNodeState(set_node_id(main_node), main_node.addr)
    visits = {} # dictionary of CFGNode:visits
    MemAccessAlreadySet = False
    exploreLoop2(main_node, main_node, main_node)
    MemAccessAlreadySet = True
    apron.clearInputStates();

    if len(visits) <= len(cfg.graph.nodes()):
        print("INFO: visited", len(visits), "of", len(cfg.graph.nodes()), "nodes")
    else:
        print("ERROR: visited", len(visits), "of", len(cfg.graph.nodes()), "nodes")
        print("ERROR: Continuing, but CFG may be incorrect")
    actualnodeset = compareNodes(visits, cfg.graph.nodes())

    print("")
    print("--- Memory Reference analysis time:", round(time.time() - part_time, 2), "seconds ---")
    print("")

    apron.printAbstractMemRefs()

    part_time = time.time()
    find_induction_vars()
    loopindvars = indvars.getInfo()
    print("")
    print("--- Induction variable analysis time:", round(time.time() - part_time, 2), "seconds ---")
    print("")

    indvars.printInfo()

    part_time = time.time()
    genReuseTheoryVectors(idom, start, loopindvars)
    print("")
    print("--- Reuse theory vector generation time:", round(time.time() - part_time, 2), "seconds ---")
    print("")
    apron.printReuseTheoryMemRefs()

    part_time = time.time()
    calcGroupReuse(idom, start, cacheLineSize)
    apron.calcFirstGR()
    if instance_functions:
        cfgtype = "emulated"
    else:
        cfgtype = "fast"
    #xmlfile = filename + "_" + cfgtype + ".xml"
    xmlfile = filename + ".xml"
    apron.printXMLinfo(xmlfile.encode("utf-8"), cfgtype.encode("utf-8"))
    print("")
    print("--- Group Reuse calculation time:", round(time.time() - part_time, 2), "seconds ---")
    print("")
    apron.printReuseTheoryMemRefs()

if __name__ == "__main__":
    sys.exit(main())
