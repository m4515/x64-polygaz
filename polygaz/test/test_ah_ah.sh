#!/bin/sh

BENCHMARK=test/ludcmp_O3_arm

IC=ah
ICstr=ah_0_0
DC=ah
DCstr=ah_0_0

venv/bin/python -u -O ./ipet.py $IC $DC "$BENCHMARK"
lp_solve -S1 "$BENCHMARK"_${ICstr}_${DCstr}.lp > "$BENCHMARK"_${ICstr}_${DCstr}.lp.result
cmp "$BENCHMARK"_${ICstr}_${DCstr}.lp.result "$BENCHMARK"_${ICstr}_${DCstr}.lp.solved_ok
