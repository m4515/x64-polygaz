
# coding=utf-8

""" Module for adding outest loop information to each access in a .xml file
"""

import sys
import xml.etree.ElementTree as ET

import angr # grafo
import pyvex # irVEX

import xmlparse
import flowfacts



INSTRUCTION_SIZE = 4 # bytes
CACHE_LINE_SIZE = 64 # bytes, INSTRUCTION AND DATA




################################ INIT ################################

def init(filename, instance_functions):
    """initializes all"""
    global cfg, loop_info, apron, registers, main_node, cfg_nodes, loop_headers, return_addrs, ending_nodes, reference, max_times, enclosing_loop_header, cfg_node_ids

    # Set-up project from binary file
    proj = angr.Project(filename, auto_load_libs=False) # carga ejecutable
    main = proj.loader.main_object.get_symbol("main")
    #state = proj.factory.entry_state()
    state = proj.factory.blank_state(addr=main.rebased_addr)

    flowfacts.get_flowfacts(filename)
    #print("Flow facts:", flowfacts.maxLoopIter)

    # Build CFG and loop_info
    if instance_functions:
        # context_sensitivity_level=3 works except for fmref O2, md5, test3
        # md5_O0_arm (gcc-9.2.1) requires context_sensitivity_level=4
        # md5_O2_arm (gcc-9.2.1) requires context_sensitivity_level=6
        cfg = proj.analyses.CFGEmulated(normalize=True, context_sensitivity_level=3, starts=[main.rebased_addr], initial_state=state) # genera CFG
        print("CFG (emulated) has %d nodes and %d edges" % (len(cfg.graph.nodes()), len(cfg.graph.edges())), file = sys.stderr)
    else:
        cfg = proj.analyses.CFG(normalize=True) # genera CFG (CFGFast)
        print("CFG (fast) has %d nodes and %d edges" % (len(cfg.graph.nodes()), len(cfg.graph.edges())), file = sys.stderr)

    loop_info = proj.analyses.LoopFinder() # Set-up loop information
    # Locate main()
    fmain = cfg.kb.functions.function(name="main") # main function
    main_node = cfg.model.get_any_node(fmain.addr) # main CFG node
    #mainBlock = main_node.block # main basic block

    xmlparse.parse(filename + ".xml", CACHE_LINE_SIZE)
    reference = xmlparse.get_references()

    # set cfg_node indexes and headers for each loop
    # (a given loop may be represented by several emulated cfg node groups)
    #for loop in loop_info.loops:
    #    print("loop at", hex(loop.entry.addr))
    cfg_nodes = {}
    cfg_node_ids = {}
    loop_headers = {}
    ending_nodes = []
    i = 0
    for node in cfg.model.nodes():
        cfg_nodes[node] = i
        if xmlparse.get_cfg_type() in ('emulated', None):
        # XML with CFGEmulated
            cfg_node_ids[i] = node
        else:
        #elif xmlparse.get_cfg_type() == 'fast':
            # XML with CFGFast
            print("IPET with CFGfast data patterns not implemented.", file = sys.stderr)
            sys.exit(-1)

        for loop in loop_info.loops:
            if node.addr == loop.entry.addr:
                #print(hex(node.addr), "is header")
                loop_headers[node] = loop
                break
        #if not node.successors:
        #if len(no_back_edge_successors(node)) == 0:
        #    ending_nodes.append(node) # CFG or loop ending nodes
        if len(node.successors) == 0:
            ending_nodes.append(node) # CFG ending nodes
        i += 1
    return_addrs = []
    for src, dst in cfg.graph.edges():
        if dst not in src.successors:
            return_addrs.append(dst.addr)

    enclosing_loop_header = {}
    max_times = {}
    recursive_tag_loop(main_node, 1, [])


def get_any_cfg_node(instaddr):
    """ Gets any node including instaddr """
    while (cfg.model.get_any_node(instaddr) is None) and (instaddr > 0):
        instaddr -= INSTRUCTION_SIZE
    return cfg.model.get_any_node(instaddr)


def no_back_edge_successors(cfg_node):
    """ Returns a list of successors not being back edges """
    result = []
    for dest_node in cfg_node.successors:
        is_back_edge = isBackEdge(dest_node, cfg_node)
        if not is_back_edge:
            # discard strange false single-node loop on fmref-O2 at 0x10528
            if dest_node == cfg_node:
                print("no_back_edge_successors warning: discarding a seemingly false single-node loop at 0x%x" % cfg_node.addr, file = sys.stderr)
                is_back_edge = True
        if not is_back_edge:
            result.append(dest_node)
    return result


def isBackEdge(CFGNode, prevNode):
    """ Returns whether prevNode -> CFGNode is a back (continue) edge """
    for loop in loop_info.loops:
        if CFGNode.addr == loop.entry.addr:
            if (prevNode.to_codenode(), CFGNode.to_codenode()) in loop.continue_edges:
                return True
            # case of body basicblock ending with call, and header just in the next address
            node_above = get_any_cfg_node(CFGNode.addr - INSTRUCTION_SIZE)
            if node_above.to_codenode() in loop.body_nodes:
                if len(node_above.successors) == 1 and node_above.successors[0] != CFGNode:
                    called_function = cfg.kb.functions.function(addr=node_above.successors[0].addr)
                    if called_function and prevNode.to_codenode() in set(called_function.nodes):
                        #print("isBackEdge warning: Revise whether node", node_above, "calls a function reaching", prevNode, "and returns to the loop header", CFGNode)
                        return True
    return False


def is_break_edge(pre_node, suc_node):
    """ Returns whether the edge pre->suc is break node """
    for loop in loop_info.loops:
        if pre_node.to_codenode() in loop.body_nodes:
            (internal_nodes, break_nodes, continue_nodes) = classify_successors(pre_node, loop)
            if suc_node in break_nodes:
                return True
    return False

def classify_successors(cfg_node, loop):
    """ classifies, removing continue_edges to other loops and returns from body_nodes """
    internal_nodes = []
    break_nodes = []
    continue_nodes = []
    continue_to_other_loops = []
    #print(cfg_node)
    #print("Successors (and type of jump) of the entry point:", [ jumpkind + " to " + str(node.addr) for node,jumpkind in cfg.get_successors_and_jumpkind(cfg_node) ])
    #for dest_node in cfg_node.successors:
    for dest_node, jumpkind in cfg.model.get_successors_and_jumpkind(cfg_node):
        if dest_node.addr == loop.entry.addr: # continue_edge
            continue_nodes.append(dest_node)
        else: # no continue_edge
            is_break_node = False
            for (src, dst) in loop.break_edges:
                if dest_node.addr == dst.addr:
                    is_break_node = True
                    break
            if is_break_node:
                break_nodes.append(dest_node)
            else: # internal node or return node
                #print(dest_node, jumpkind)
                # discard continue_edges to other loops
                if not isBackEdge(dest_node, cfg_node):
                    # discard returns from body_nodes
                    #if not (cfg_node.to_codenode() in loop.body_nodes and jumpkind == "Ijk_Ret"):
                    if not (cfg_node.to_codenode() in loop.body_nodes and jumpkind == "Ijk_Ret"):
                        #print("-", dest_node, jumpkind)
                        internal_nodes.append(dest_node)
                    else: # return from body_nodes
                        break_nodes.append(dest_node)
                else:
                    continue_to_other_loops.append(dest_node)
    #if len(cfg.model.get_successors_and_jumpkind(cfg_node)) != (len(internal_nodes) + len(break_nodes) + len(continue_nodes)):
    if len(cfg.model.get_successors_and_jumpkind(cfg_node)) != (len(internal_nodes) + len(break_nodes) + len(continue_nodes) + len(continue_to_other_loops)):
        print("classify_successors: error: node lost", file = sys.stderr)
        sys.exit(-1)
    return (internal_nodes, break_nodes, continue_nodes)


def recursive_tag_loop(cfg_node, max_execs, loop_nest_list):
    """ Explores CFG and builds ILP model """
    #global times, node_penalty
    #print(hex(cfg_node.addr), maxexecs)
    if cfg_node in enclosing_loop_header:
        # already explored
        return
    else:
        # set enclosing loop
        if len(loop_nest_list):
            enclosing_loop_header[cfg_node] = loop_nest_list[-1]
        else:
            enclosing_loop_header[cfg_node] = None
        max_times[cfg_node] = max_execs
        # explore successors
        successors = no_back_edge_successors(cfg_node)
        #print("No back edge successors of", cfg_node, ":", successors)
        if len(successors) == 0:
            return
        else:
            for child_node in successors:
                if child_node in loop_headers: # entering a loop
                #if child_node.addr in flowfacts.maxLoopIter: # entering a loop
                    if flowfacts.maxLoopIter[child_node.addr]:
                        #recursive_tag_loop(child_node, max_execs * flowfacts.maxLoopIter[child_node.addr], loop_nest_list + [loop_headers[child_node]])
                        recursive_tag_loop(child_node, max_execs * flowfacts.maxLoopIter[child_node.addr], loop_nest_list + [child_node])
                    else:
                        print("explore: reached loop 0x%x (0 iterations)" % child_node.addr, file = sys.stderr)
                elif is_break_edge(cfg_node, child_node):
                    #recursive_tag_loop(child_node, int(max_execs / flowfacts.maxLoopIter[enclosing_loop[cfg_node].entry.addr]), loop_nest_list[:-1])
                    recursive_tag_loop(child_node, int(max_execs / flowfacts.maxLoopIter[enclosing_loop_header[cfg_node].addr]), loop_nest_list[:-1])
                else:
                    recursive_tag_loop(child_node, max_execs, loop_nest_list)
            return





def get_all_nodes_containing_addr(addr):
    """
    Returns a list with all nodes containing the instruction address addr
    """
    node_list = cfg.model.get_all_nodes(addr)
    if len(node_list) == 0:
        for node in cfg.model.nodes():
            if addr in node.instruction_addrs:
                node_list.append(node)
    return node_list


def get_most_external_loop_header_from_addr(addr):
    """
    Gets the most external loop header address.
    If addr is outside loops, returns 0.
    If addr is inside different external loops (function called in them), return 0
    Required for multiple locking points based on external loops.
    """
    global_external_loop_addr = None
    for node in get_all_nodes_containing_addr(addr):
        external_loop_addr = get_most_external_loop_header_from_node(node)
        if global_external_loop_addr is None:
            global_external_loop_addr = external_loop_addr
        elif global_external_loop_addr != external_loop_addr:
            # multiple parent loops: function called from loops
            return 0
    return global_external_loop_addr

def get_most_external_loop_header_from_node(node):
    """
    Gets the most external loop header address.
    If node is outside loops, returns 0.
    If node is inside different external loops (function called in them), return 0
    Required for multiple locking points based on external loops.
    """
    if node in enclosing_loop_header:
        # node in loop
        global_parent_loop_addr = None
        loop_header = enclosing_loop_header[node]
        if loop_header is None:
            return 0
        else:
            for parent in loop_header.predecessors:
                if not isBackEdge(loop_header, parent):
                    parent_loop_addr = get_most_external_loop_header_from_node(parent)
                    if parent_loop_addr == 0: # already at the most external loop
                        parent_loop_addr = loop_header.addr
                    if global_parent_loop_addr is None:
                        global_parent_loop_addr = parent_loop_addr
                    elif global_parent_loop_addr != parent_loop_addr:
                        # multiple parent loops: function called from loops
                        return 0
            return global_parent_loop_addr
    else:
        return 0


def add_external_loop_info(filename):
    tree = ET.parse(filename)
    root = tree.getroot()
    source_version = root.attrib["polygaz"]
    print("XML generated by: polygaz", source_version, file = sys.stderr)
    cfg_type = root.attrib["cfg"]
    print("XML CFG type:", cfg_type, file = sys.stderr)

    for child in root:
        PC = int(child.find("pc").text, 0) # 0=guess base
        addr = get_most_external_loop_header_from_addr(PC)
        #print("The most external loop header from 0x%x is 0x%x" % (PC, addr))

        eloop = ET.SubElement(child, 'external_loop')
        eloop.text = str(hex(addr))
        #child.insert(2, eloop)

    tree.write(filename + '_eloop')





################################ MAIN ################################
def print_usage():
    print("usage:", sys.argv[0], "<binary_file>")


def main():
    num_args = len(sys.argv)
    if num_args != 2:
        print_usage()
        sys.exit()

    file_name_arg = 1
    file_name = sys.argv[file_name_arg]
    init(file_name, instance_functions=True)
    add_external_loop_info(file_name + '.xml')


if __name__ == "__main__":
    sys.exit(main())
