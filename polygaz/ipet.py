#!venv/bin/python3
# coding=utf-8

""" Module with a set of functions to generate an IPET model for WCET analysis """


import sys
from math import ceil
import time
import angr # grafo
import pyvex # irVEX

import flowfacts
import xmlparse
import acdcparse
import lockedicparse

impr_str = {}

############################# HARDWARE #############################
MEMORY_COST = 13
CACHE_HIT_COST = 1
CACHE_MISS_COST = CACHE_HIT_COST + MEMORY_COST
# Micron Technology, Inc., Automotive DDR SDRAM MT46V32M8, MT46V16M16, https://media-www.micron.com/-/media/client/global/documents/products/data-sheet/dram/mobile-dram/low-power-dram/lpddr/256mb_x8x16_at_ddr_t66a.pdf

######################### INSTRUCTION CACHE #########################
FETCH_HIT_COST = CACHE_HIT_COST
FETCH_MISS_COST = CACHE_MISS_COST
# Always-hit and always-miss: FETCH_HIT_COST=FETCH_MISS_COST
INSTRUCTION_SIZE = 4 # bytes
#INSTRUCTION_CACHE_TYPE = "locked"
#INSTRUCTION_CACHE_TYPE = "unlimited" # unlimited size, always first-miss
#INSTRUCTION_CACHE_SETS = 64 # intel/arm L1 instruction/data
#INSTRUCTION_CACHE_WAYS = 8 # intel/arm L1 instruction/data
CACHE_LINE_SIZE = 64 # bytes, INSTRUCTION AND DATA
IC_PRELOAD_CALL_COST = 47 # cycles. Call + execute preloading + return
# The actual cost of preloading cache lines is not included; it is calculated
# based on the number of cache lines to preload.

############################ DATA CACHE ############################
DATA_HIT_COST = CACHE_HIT_COST
DATA_MISS_COST = CACHE_MISS_COST
COPYBACK_COST = MEMORY_COST
ACDC_PRELOAD_CALL_COST = IC_PRELOAD_CALL_COST

################################ INIT ################################

def init(filename, instance_functions):
    """initializes all"""
    global cfg, loop_info, apron, registers, main_node, cfg_nodes, loop_headers, return_addrs, ending_nodes, reference, max_times, enclosing_loop_header, cfg_node_ids

    # Set-up project from binary file
    proj = angr.Project(filename, auto_load_libs=False) # carga ejecutable
    main = proj.loader.main_object.get_symbol("main")
    #state = proj.factory.entry_state()
    state = proj.factory.blank_state(addr=main.rebased_addr)

    flowfacts.get_flowfacts(filename)
    #print("Flow facts:", flowfacts.maxLoopIter)

    # Build CFG and loop_info
    if instance_functions:
        # context_sensitivity_level=3 works except for fmref O2, md5, test3
        # md5_O0_arm (gcc-9.2.1) requires context_sensitivity_level=4
        # md5_O2_arm (gcc-9.2.1) requires context_sensitivity_level=6
        cfg = proj.analyses.CFGEmulated(normalize=True, context_sensitivity_level=3, starts=[main.rebased_addr], initial_state=state) # genera CFG
        print("CFG (emulated) has %d nodes and %d edges" % (len(cfg.graph.nodes()), len(cfg.graph.edges())), file = sys.stderr)
    else:
        cfg = proj.analyses.CFG(normalize=True) # genera CFG (CFGFast)
        print("CFG (fast) has %d nodes and %d edges" % (len(cfg.graph.nodes()), len(cfg.graph.edges())), file = sys.stderr)

    loop_info = proj.analyses.LoopFinder() # Set-up loop information
    # Locate main()
    fmain = cfg.kb.functions.function(name="main") # main function
    main_node = cfg.model.get_any_node(fmain.addr) # main CFG node
    #mainBlock = main_node.block # main basic block

    xmlparse.parse(filename + ".xml", CACHE_LINE_SIZE)
    reference = xmlparse.get_references()

    # set cfg_node indexes and headers for each loop
    # (a given loop may be represented by several emulated cfg node groups)
    #for loop in loop_info.loops:
    #    print("loop at", hex(loop.entry.addr))
    cfg_nodes = {}
    cfg_node_ids = {}
    loop_headers = {}
    ending_nodes = []
    i = 0
    for node in cfg.model.nodes():
        cfg_nodes[node] = i
        if xmlparse.get_cfg_type() in ('emulated', None):
        # XML with CFGEmulated
            cfg_node_ids[i] = node
        else:
        #elif xmlparse.get_cfg_type() == 'fast':
            # XML with CFGFast
            print("IPET with CFGfast data patterns not implemented.", file = sys.stderr)
            sys.exit(-1)

        for loop in loop_info.loops:
            if node.addr == loop.entry.addr:
                #print(hex(node.addr), "is header")
                loop_headers[node] = loop
                break
        #if not node.successors:
        #if len(no_back_edge_successors(node)) == 0:
        #    ending_nodes.append(node) # CFG or loop ending nodes
        if len(node.successors) == 0:
            ending_nodes.append(node) # CFG ending nodes
        i += 1
    return_addrs = []
    for src, dst in cfg.graph.edges():
        if dst not in src.successors:
            return_addrs.append(dst.addr)

    enclosing_loop_header = {}
    max_times = {}
    recursive_tag_loop(main_node, 1, [])

    #### Stack limits
#    resource.setrlimit(resource.RLIMIT_STACK, (resource.RLIM_INFINITY, resource.RLIM_INFINITY)) # set system stack limit to unlimited
#    print("System stack limit:", resource.getrlimit(resource.RLIMIT_STACK))
#    # Print max stack depth (default: 1000)
#    #sys.setrecursionlimit(20000) # works with default ulimit stack size (8192 KiB)
#    sys.setrecursionlimit(1000000) # test: $ ulimit -s unlimited
#    print("Python recursion limit (stack depth):", sys.getrecursionlimit())



################################ IPET ################################
def generate_ipet_model(file_name):
    """
    Generates an IPET ILP model and writes the corresponding
    lp-solve .lp file.
    IPET requires instanced functions. Otherwise, call-return sequences may generate loops in the CFG and the generated model
    will be infeasible.
    """
    #lp_file = open("%s_%s_%d_%d_%s_%d_%d.lp" % (file_name, INSTRUCTION_CACHE_TYPE, INSTRUCTION_CACHE_SETS, INSTRUCTION_CACHE_WAYS, DATA_CACHE_TYPE, DATA_CACHE_SETS, DATA_CACHE_WAYS), 'w')
    lp_file = sys.stdout
    # Objective function of LP file
    generate_init_constraints(lp_file)
    # CFG flow control and loop constraints of LP file
    generate_structural_constraints(lp_file)
    # Instruction cache
    # Fetch stage costs: 1 mem. latency per miss
    if INSTRUCTION_CACHE_TYPE == "ah":
        generate_ah_ic_constraints(lp_file)
    #elif INSTRUCTION_CACHE_TYPE == "nc":
    #    generate_nc_ic_constraints(lp_file)
    elif INSTRUCTION_CACHE_TYPE == "ul":
        generate_unlimited_size_ic_constraints(lp_file)
    elif INSTRUCTION_CACHE_TYPE == "locked":
        generate_locked_ic_constraints(lp_file)
    else:
        print("generate_basicblock_constraint ERROR: unknown INSTRUCTION_CACHE_TYPE", file = sys.stderr)
        sys.exit(-1)
    # TODO conventional LRU IC
    # TODO locked IC
    # Execution stage costs: 1 cycle per instruction
    generate_execution_constraints(lp_file)
    # Memory stage cost: DATA_HIT_COST hit, DATA_MISS_COST per miss, MEMORY_COST per copyback
    if DATA_CACHE_TYPE == "ah":
        generate_ah_memory_constraints(lp_file)
    elif DATA_CACHE_TYPE == "am":
        generate_am_constraints(lp_file) # estimates DRP benefits
    elif DATA_CACHE_TYPE == "dm":
        generate_direct_memory_constraints(lp_file)
    elif DATA_CACHE_TYPE == "fm":
        generate_fm_memory_constraints(lp_file)
    elif DATA_CACHE_TYPE == "acdc":
        generate_acdc_memory_constraints(lp_file)
    elif DATA_CACHE_TYPE == "lru":
        generate_lru_memory_constraints(lp_file)
    elif DATA_CACHE_TYPE == "constlru":
        #generate_addronly_lru_memory_constraints(lp_file)
        generate_lru_memory_constraints(lp_file)
    else:
        print("generate_basicblock_constraint ERROR: unknown DATA_CACHE_TYPE", file = sys.stderr)
        sys.exit(-1)
    # Variable types for each part
    generate_variable_types_structural_constraints(lp_file)
    generate_variable_types_memory_constraints(lp_file)
    lp_file.close()


def var_name(pre, cfg_node):
    """ Generates a name for node: pre+nodeid_addr """
    return pre + str(cfg_nodes[cfg_node]).zfill(3) + "_" + str(hex(cfg_node.addr))

def edge_name(pre, cfg_node1, cfg_node2):
    """ Generates a name for node: pre+node1id_node2id """
    return pre + str(cfg_nodes[cfg_node1]).zfill(3) + "_" + str(cfg_nodes[cfg_node2]).zfill(3)


def generate_init_constraints(lp_file):
    """ Write objective for ILP """
    lp_file.write("\n/* Objective function */\n\n")
    lp_file.write("max: WCET;\n")
    lp_file.write("WCET = IC_load_cost + Exec_cost + Memory_cost;\n")
#    for node in cfg.graph.nodes():
#        try:
#            #assert(node.block.instructions)
#            node.block.instructions
#            lp_file.write(var_name(" +E", node))
#        except AttributeError as error:
#        #except AssertionError as error:
#            # Node from unresolvable jump target
#            # If this occurs, .lp model may be infeasible
#            print(error)
#            print("CFG node %s without instructions" % var_name("B", node))
#            lp_file.write("--- ERROR: Unresolvable jump target to %s ---\n" % (var_name("X", node)))
#
#    lp_file.write(";\n")


def generate_structural_constraints(lp_file):
    """ Generates structural (tree) constraints """
    """ DOI: ?? Cache_Modeling_for_Real-Time_Software\:_Beyond_Direct_Mapped_Instruction_Caches"""

    lp_file.write("\n/* Structural constraints */\n\n")
    lp_file.write("/* TimesNode = sum(TimesEnteringEdges) */\n")
    lp_file.write("/* TimesNode = sum(TimesExitingEdges) */\n")
    # lp_file.write("/* Starting node */\n")
    lp_file.write("Xstart = 1;\n")
    lp_file.write("Xstart = Dstart_%s;\n" % str(cfg_nodes[main_node]).zfill(3))
    lp_file.write("%s = Dstart_%s;\n" % (var_name("X", main_node), str(cfg_nodes[main_node]).zfill(3)))
    # xi = di
    for node in cfg.graph.nodes():
        # entering edges
        if len(node.predecessors) != 0:
            lp_file.write("%s =" % (var_name("X", node)))
            for pred in node.predecessors:
                lp_file.write(" + %s" % edge_name("D", pred, node))
            lp_file.write(";\n")
        # exiting edges
        if len(node.successors) != 0:
            lp_file.write("%s =" % (var_name("X", node)))
            for succ in node.successors:
                lp_file.write(" + %s" % edge_name("D", node, succ))
            lp_file.write(";\n")
    # lp_file.write("/* Ending nodes */\n")
    for cfg_node in ending_nodes:
        lp_file.write("%s = %s_end;\n" % (var_name("X", cfg_node), var_name("D", cfg_node)))
    lp_file.write("Xend = ")
    for cfg_node in ending_nodes:
        lp_file.write("+ %s_end" % var_name("D", cfg_node))
    lp_file.write(";\n")
    lp_file.write("Xend = 1;\n")

    lp_file.write("\n/* Loop header constraints (flow facts) */\n\n")
    for loop in loop_info.loops:
        for entry_node in cfg.model.get_all_nodes(loop.entry.addr):
            lp_file.write("%s <= %d;\n" % (var_name("X", entry_node), flowfacts.maxLoopIter[loop.entry.addr]))

    lp_file.write("\n/* Unreachable nodes */\n\n")
    for node in cfg.graph.nodes():
        if node not in enclosing_loop_header:
            lp_file.write("%s = 0;\n" % (var_name("X", node)))
            print("Unreachable node", node, file = sys.stderr)

def generate_variable_types_structural_constraints(lp_file):
    """
    Basic block times and transitions must be integer
    """
    lp_file.write("\n/* Structural variables */\n\n")
    lp_file.write("int ")
    max_nodes = len(cfg.graph.nodes())
    num_node = 0
    for node in cfg.graph.nodes():
        num_node += 1
        if num_node != max_nodes:
            lp_file.write("%s, " % (var_name("X", node)))
        else:
            lp_file.write("%s;\n" % (var_name("X", node)))
    lp_file.write("int ")
    max_edges = len(cfg.graph.edges())
    num_edge = 0
    for src, dst in cfg.graph.edges():
        num_edge += 1
        if num_edge != max_edges:
            lp_file.write("%s, " % (edge_name("D", src, dst)))
        else:
            lp_file.write("%s;\n" % (edge_name("X", src, dst)))
    # start/end nodes/edges should be integer


########################## INSTRUCTION CACHE ##########################
def cache_line_addr(addr):
    """ Returns the cache line address of a memory address """
    return addr - (addr % CACHE_LINE_SIZE)

#def print_equiv_nodes(addr):
#    """ Prints the sum of equivalent nodes X """
#    string = ""
#    #for equiv_node in get_all_cfg_nodes(addr):
#    for equiv_node in cfg.model.get_all_nodes(addr):
#        string += "+ %s " % (var_name("X", equiv_node))
#    return string

def instruction_addresses_in_line(addr):
    iaddr = cache_line_addr(addr)
    addrs = [iaddr]
    for _ in range(1, int(CACHE_LINE_SIZE/INSTRUCTION_SIZE)):
        iaddr += INSTRUCTION_SIZE
        addrs.append(iaddr)
    return addrs

def print_nodes_ocupying_line(addr):
    """ Prints the sum of nodes that (partially) occupy memory line <addr> """
    nodeset = set()
    instruction_addrs = instruction_addresses_in_line(addr)
    node_list = list(cfg.graph.nodes())
    for iaddr in instruction_addrs:
        #for node in cfg.graph.nodes():
        for node in node_list:
            if iaddr in node.instruction_addrs:
                nodeset.add(node)
                node_list.remove(node)
                break
    string = ""
    for node in list(nodeset):
        string += " + %s" % (var_name("X", node))
    return string

def generate_basicblock_ic_constraints(lp_file):
    """
    Prints the relation between nodes (basic blocks) and instruction
    cache lines (hits+misses), taking into account that:
    -  A cache line may be shared between several basic blocks.
    -  A basic block may correspond to several nodes (e.g. "virtualized" calls)
    Returns the set of processed instruction cache line addresses
    """
    lp_file.write("\n/* Basic block h/m instruction cache lines constraints */\n\n")
    processed_addrs = set()
    for node in cfg.graph.nodes():
        # instructions in the first cache line
        instr_in_first_line = CACHE_LINE_SIZE - (node.addr % CACHE_LINE_SIZE) % INSTRUCTION_SIZE
        if node.block.instructions < instr_in_first_line:
            instr_in_first_line = node.block.instructions
        # total touched instruction cache lines
        instruction_lines = 1 + ceil((node.block.instructions - instr_in_first_line) * INSTRUCTION_SIZE / CACHE_LINE_SIZE)
        # cost per traversal
        first_addr = cache_line_addr(node.addr)
        until_addr = first_addr + (CACHE_LINE_SIZE*instruction_lines)
        for line_addr in range(first_addr, until_addr, CACHE_LINE_SIZE):
            if line_addr not in processed_addrs:
                lp_file.write("IH_0x%x + IM_0x%x =%s;\n" % (line_addr, line_addr, print_nodes_ocupying_line(line_addr)))
                processed_addrs.add(line_addr)
    return processed_addrs


def generate_unlimited_size_ic_constraints(lp_file):
    """
    (Miss cost for first access, hit cost for others)
    Assuming an ideal pipeline hidding 1 cycle, MEMORY_COST for first access, 0 cost for others
    """
    ic_addresses = set()
    lp_file.write("\n/* Unlimited size instruction cache */")
    ic_addresses = generate_basicblock_ic_constraints(lp_file)
    lp_file.write("\n/* Cost of bringing instruction cache missing lines */\n")
    lp_file.write("/* Hit: %d; Miss %d */\n\n" % (0, MEMORY_COST))
    lp_file.write("IC_load_cost =")
    for line_addr in list(ic_addresses):
        lp_file.write(" + %d IH_0x%x + %d IM_0x%x" % (0, line_addr, MEMORY_COST, line_addr))
    lp_file.write(";\n")
    lp_file.write("\n/* Unlimited size instruction cache miss constraints */\n\n")
    for line_addr in list(ic_addresses):
        lp_file.write("IM_0x%x <= 1;\n" % (line_addr))

def generate_ah_ic_constraints(lp_file):
    """
        MEMORY_COST = 0
    """
    lp_file.write("\n/* Always hit instruction cache */\n")
    lp_file.write("IC_load_cost = 0;")

def generate_locked_ic_constraints(lp_file):
    """
    Line buffer + locked instruction cache
    """
    lp_file.write("\n/* Line buffer + instruction cache */")
    processed_addrs = generate_basicblock_ic_constraints(lp_file)

    lp_file.write("\n/* Cost of bringing instruction cache missing lines */\n")
    lp_file.write("/* Hit: %d; Miss %d */\n\n" % (0, MEMORY_COST))
    # TODO: currently assuming that, if a cache line is locked, it is only
    # reachable from its locking point and until another locking point.
    # That is, accesses to a given memory line always hit or always miss,
    # independently of the region.
    ic_locking_points = lockedicparse.ic_locking_points()
    if len(ic_locking_points) != 1:
        print("More than one locking points not implemented", file = sys.stderr)
        sys.exit(-1)
    ic_addresses = set() # list of locked addresses
    for point in lockedicparse.ic_locking_points():
        ic_addresses |= lockedicparse.locked_lines(point)
    # end TODO
    num_preload_calls = len(lockedicparse.ic_locking_points())
    num_locked_instr = len(ic_addresses)
    # calculate preload cost
    preload_cost = num_preload_calls * IC_PRELOAD_CALL_COST + num_locked_instr * MEMORY_COST
    lp_file.write("IC_load_cost = %d" % preload_cost)
    for line_addr in list(processed_addrs):
        # TODO: currently assuming a single locking point (start; 0)
        lp_file.write(" + %d IH_0x%x + %d IM_0x%x" % (0, line_addr, MEMORY_COST, line_addr))
    lp_file.write(";\n")
    lp_file.write("\n/* Locked instruction cache miss constraints */\n\n")
    for line_addr in list(processed_addrs):
        if lockedicparse.is_locked(line_addr, 0): # TODO: assumes a single locking point
            lp_file.write("IM_0x%x = 0; /* locked */\n" % (line_addr))
        else:
            lp_file.write("IH_0x%x = 0;\n" % (line_addr))


#def generate_nc_ic_constraints(lp_file):
#    """
#        MEMORY_COST = 0
#    """
#    lp_file.write("\n/* No instruction cache */")
#    lp_file.write("\n/* Basic block h/m cache lines constraints */\n\n")
#    ic_addresses = set()
#    for node in cfg.graph.nodes():
#        # instructions in the first cache line
#        instr_in_first_line = CACHE_LINE_SIZE - (node.addr % CACHE_LINE_SIZE) % INSTRUCTION_SIZE
#        if node.block.instructions < instr_in_first_line:
#            instr_in_first_line = node.block.instructions
#        # total touched instruction cache lines
#        instruction_lines = 1 + ceil((node.block.instructions - instr_in_first_line) * INSTRUCTION_SIZE / CACHE_LINE_SIZE)
#        # cost per traversal
#        first_addr = cache_line_addr(node.addr)
#        until_addr = first_addr + (CACHE_LINE_SIZE*instruction_lines)
#        for line_addr in range(first_addr, until_addr, CACHE_LINE_SIZE):
#            lp_file.write("%s <= IM_0x%x;\n" % (print_equiv_nodes(node.addr), line_addr))
#            ic_addresses.add(line_addr)
#    lp_file.write("\n/* Cost of bringing instruction cache missing lines */\n")
#    lp_file.write("/* Direct memory access %d */\n\n" % (0, MEMORY_COST-1))
#    lp_file.write("IC_load_cost =")
#    for line_addr in list(ic_addresses):
#        lp_file.write(" + %d IM_0x%x" % (MEMORY_COST-1, line_addr))
#    lp_file.write(";\n")


########################## EXECUTION ##########################
def generate_execution_constraints(lp_file):
    """
    Fixed cost: 1 cycle per instruction (hidden by ideal pipeline)
    """
    lp_file.write("\n/* Execution constraints */\n\n")
    # assuming execution cost hidden within ideal pipeline: 0
    lp_file.write("Exec_cost = 0;")
#    lp_file.write("/* = sum(InstructionsInNode * TimesNode) */\n")
#    lp_file.write("Exec_cost =")
#    for node in cfg.graph.nodes():
#        lp_file.write("+ %d %s" % (node.block.instructions, var_name("X", node)))
#    lp_file.write(";\n")


########################## DATA CACHE ##########################
def generate_ah_memory_constraints(lp_file):
    """
    Always hit
    Fixed cost: 1 cycle per memory access
    """
    lp_file.write("\n/* Load/store constraints for always hit */\n\n")
    generate_data_hitmiss_general_constraints(lp_file)
#    lp_file.write("/* Always Hit */")
#    for ref_id in reference:
#        node = cfg_node_ids[reference[ref_id].cfg_node_id]
#        lp_file.write("%s = %s;\n" % (var_name("X", node), ref_name("MH", ref_id)))
    lp_file.write("/* Misses = 0 */\n")
    for ref_id in reference:
        lp_file.write("%s = 0;\n" % (ref_name("MM", ref_id)))
    lp_file.write("/* Memory_cost = SUM(hit_cost * hits) */\n")
    lp_file.write("Memory_cost =")
    for ref_id in reference:
        lp_file.write(" + %d %s" % (DATA_HIT_COST, ref_name("MH", ref_id)))
    lp_file.write(";\n")

def generate_variable_types_memory_constraints(lp_file):
    """
    Hit/miss variables (MH/MM) must be integer
    """
    lp_file.write("\n/* Integer always hit variables */\n\n")
    lp_file.write("int ")
    max_refs = len(reference)
    num_ref = 0
    for ref_id in reference:
        num_ref += 1
        if num_ref != max_refs:
            lp_file.write("%s, %s, %s, " % (ref_name("MH", ref_id), ref_name("MM", ref_id), ref_name("MCB", ref_id)))
        else:
            lp_file.write("%s, %s, %s;\n" % (ref_name("MH", ref_id), ref_name("MM", ref_id), ref_name("MCB", ref_id)))


def generate_fm_memory_constraints(lp_file):
    """
    Worst-case of data cache with unlimited size (first miss)
    1 miss per data cache line accessed; no copybacks
    """
    lp_file.write("\n/* Load/store constraints for unlimited size data cache */\n\n")

    generate_data_hitmiss_general_constraints(lp_file)

    for ref_id in reference:
        print_ref_summary(lp_file, ref_id)
        node = cfg_node_ids[reference[ref_id].cfg_node_id]
        first_gr = reference[ref_id].first_group_reuse
        if first_gr is not None:
            lp_file.write("/* Reused (always hit) */\n")
            lp_file.write("%s = 0;\n" % (ref_name("MM", ref_id)))
        else:
            if is_constant(reference[ref_id]):
                lp_file.write("/* Constant (single miss) */\n")
                lp_file.write("%s <= 1;\n" % ref_name("MM", ref_id))
                lp_file.write("%s <= %s;\n" % (ref_name("MM", ref_id), var_name("X", node))) # por si es x=0
            elif is_nonlinear(reference[ref_id]):
                lp_file.write("/* Unknown (always miss) */\n")
                lp_file.write("%s <= %s;\n" % (ref_name("MM", ref_id), var_name("X", node)))
            else: # array
                misses = calculate_misses_in_enclosing_loop(node, reference[ref_id])
#                if is_precise_array(reference[ref_id]):
#                    acdc_misses = calculate_misses(node, reference[ref_id].term, reference[ref_id].c)
#                elif is_imprecise_array(reference[ref_id]):
#                    acdc_misses = calculate_misses(node, reference[ref_id].term, None)
                lp_file.write("/* Array (self-reuse in loop) */\n")
                #lp_file.write("%s <= %d;\n" % (ref_name("MM", ref_id), misses))
                #lp_file.write("%s <= %s;\n" % (ref_name("MM", ref_id), var_name("X", node)))
                lp_file.write("%s <=" % (ref_name("MM", ref_id)))
                if node in enclosing_loop_header:
                    loop_header_node = enclosing_loop_header[node]
                    for parent in loop_header_node.predecessors:
                        if not isBackEdge(node, parent):
                            lp_file.write(" + %s" % (edge_name("D", parent, node)))
                    lp_file.write(";\n")
                else:
                    # unreachable node
                    lp_file.write(" 0; /* unreachable */\n")
    # no writebacks

    lp_file.write("/* Memory_cost = SUM(hit_cost * hits + miss_cost * misses) */\n")
    lp_file.write("Memory_cost =")
    for ref_id in reference:
        lp_file.write(" + %d %s + %d %s" % (DATA_HIT_COST, ref_name("MH", ref_id), DATA_MISS_COST, ref_name("MM", ref_id)))
    lp_file.write(";\n")

#def generate_direct_memory_constraints(lp_file):
#    """
#    Direct memory access (no data cache)
#    Fixed cost: memory latency per memory access
#    """
#    lp_file.write("\n/* Load/store constraints */\n\n")
#    lp_file.write("/* Direct memory access (no cache) */")
#    for ref_id in reference:
#        node = cfg_node_ids[reference[ref_id].cfg_node_id]
#        lp_file.write("%s = %s;\n" % (var_name("X", node), ref_name("DM", ref_id)))
#    lp_file.write("Memory_cost =")
#    for ref_id in reference:
#        lp_file.write(" + %d %s" % (MEMORY_COST, ref_name("DM", ref_id)))
#    lp_file.write(";\n")

def generate_direct_memory_constraints(lp_file):
    """
    Direct memory access (no data cache)
    Fixed cost: memory latency per memory access
    Implemented as hits (0) + misses (latency cost) to add an
    ACDC improvement estimation
    """
    lp_file.write("\n/* Load/store constraints for no data cache */\n\n")
    generate_data_hitmiss_general_constraints(lp_file)
    lp_file.write("/* Hits = 0 */\n")
    for ref_id in reference:
        lp_file.write("%s = 0;\n" % (ref_name("MH", ref_id)))

    lp_file.write("/* Memory_cost = SUM(memory_latency * misses) */\n")
    lp_file.write("Memory_cost =")
    for ref_id in reference:
        lp_file.write(" + %d %s" % (MEMORY_COST, ref_name("MM", ref_id)))
    lp_file.write(";\n")

    # ACDC improvement estimation
#    for ref_id in reference:
#        set_drp_improvement_str(ref_id)
#    print_drp_improvements(lp_file)

def generate_am_constraints(lp_file):
    """
    Always miss access
    Fixed cost: miss cost per memory access
    ACDC improvement estimation
    """
    lp_file.write("\n/* Load/store constraints for no data cache */\n\n")
    generate_data_hitmiss_general_constraints(lp_file)
    lp_file.write("/* Hits = 0 */\n")
    for ref_id in reference:
        lp_file.write("%s = 0;\n" % (ref_name("MH", ref_id)))

    lp_file.write("/* Memory_cost = SUM(miss_cost * misses) */\n")
    lp_file.write("Memory_cost =")
    for ref_id in reference:
        lp_file.write(" + %d %s" % (DATA_MISS_COST, ref_name("MM", ref_id)))
    lp_file.write(";\n")

    # ACDC improvement estimation
    for ref_id in reference:
        set_drp_improvement_str(ref_id)
    print_drp_improvements(lp_file)


def all_loads_lru(last_gr):
    """ Returns whether all instructions reusing first_gr are loads """
    if reference[last_gr].is_load:
        if reference[last_gr].last_group_reuse:
            return all_loads_lru(reference[last_gr].last_group_reuse)
        else:
            return True
    else:
        return False

def all_loads_lru_accurate(ref_id, category):
    """ Returns whether all instructions reused by ref_id until a miss (included) are loads """
    if reference[ref_id].is_load:
        if category[ref_id] == 'AH':
            last_gr = reference[ref_id].last_group_reuse
            if last_gr is not None:
                return all_loads_lru_accurate(last_gr, category)
            else:
                return True
        else:
            return True
    else:
        return False

def all_loads_acdc(first_gr):
    """ Returns whether all instructions reusing first_gr are loads """
    for ref in reference:
        if reference[ref].first_group_reuse == first_gr and not reference[ref].is_load:
            return False
    return True



def generate_data_hitmiss_general_constraints(lp_file):
    """
    Generates X_nodeid = MH_refid + MM_refid;
    """
    lp_file.write("/* Times = Hits + Misses */\n")
    for ref_id in reference:
        node = cfg_node_ids[reference[ref_id].cfg_node_id]
        if reference[ref_id].is_predicated:
            lp_file.write("%s >= %s + %s;\n" % (var_name("X", node), ref_name("MH", ref_id), ref_name("MM", ref_id)))
        else:
            lp_file.write("%s = %s + %s;\n" % (var_name("X", node), ref_name("MH", ref_id), ref_name("MM", ref_id)))


def generate_lru_memory_constraints(lp_file):
    lp_file.write("\n/* Load/store constraints for LRU data cache */\n\n")
    generate_data_hitmiss_general_constraints(lp_file)

    category = {}
    array_misses = {}
    debug_str = {}
    for ref_id in reference:
        node = cfg_node_ids[reference[ref_id].cfg_node_id]
        #print_ref_summary(lp_file, ref_id)
        ################### BEGIN constlru patch ###################
        if DATA_CACHE_TYPE == "constlru" and not is_constant(reference[ref_id]):
            # Non-constant: always miss
            debug_str[ref_id] = "/* non-constant addr (always miss) */\n"
            category[ref_id] = 'AM'
            #lp_file.write("%s <= %s;\n" % (ref_name("MM", ref_id), var_name("X", node)))
#            if reference[ref_id].is_load:
#                lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))
#            else:
#                #lp_file.write("%s = %s;\n" % (ref_name("MCB", ref_id), var_name("X", node)))
#                lp_file.write("%s = %s;\n" % (ref_name("MCB", ref_id), ref_name("MM", ref_id)))
            continue
        #################### END constlru patch ####################
        #lp_file.flush()
        last_gr = reference[ref_id].last_group_reuse
        if node not in enclosing_loop_header:
            # unfeasible path; X var should be 0
            debug_str[ref_id] = "/* unfeasible path, %s should be 0 */\n" % var_name("X", node)
            category[ref_id] = 'AM' # any except FH should be ok
            #lp_file.write("%s = %s;\n" % (ref_name("MM", ref_id), var_name("X", node)))
#            lp_file.write("%s = %s;\n" % (ref_name("MCB", ref_id), ref_name("MM", ref_id)))
        elif enclosing_loop_header[node]:
            #enclosing_loop_addr = get_loop_addr(node)
            enclosing_loop_addr = enclosing_loop_header[node].addr
            if enclosing_loop_addr: # loop
            # TODO: get loop node set instead of addr set
                loop_addr_list = list(set(getLoopNodeSet(loop_headers[cfg.model.get_any_node(enclosing_loop_addr)])))
                node_list = get_successors_in_list(node, loop_addr_list, [node])
            else:
                print("ERROR", file = sys.stderr)
                sys.exit(-1)
            if last_gr is not None:
                ######## GROUP-REUSE in LOOP
                (distance, last_gr_node) = calc_distance_and_reused_node(node, ref_id, last_gr)
                if is_precise_array(reference[ref_id]) or is_imprecise_array(reference[ref_id]):
                    misses = calculate_misses_in_enclosing_loop(node, reference[ref_id])
#                    if is_precise_array(reference[ref_id]):
#                        offset = reference[ref_id].c
#                    else: # imprecise array
#                        offset = None
#                    misses = calculate_misses(node, reference[ref_id].term, offset)
                    if last_gr_node.addr in loop_addr_list:
                        # group reuse in loop
                        if distance > DATA_CACHE_WAYS: # always-miss
                            debug_str[ref_id] = "/* dist >=%d in loop (always miss) */\n" % distance
                            category[ref_id] = 'AM'
                            #lp_file.write("%s <= %s;\n" % (ref_name("MM", ref_id), var_name("X", node)))
                        else: # always hit
                            debug_str[ref_id] = "/* dist %d in loop (always hit) */\n" % (distance)
                            category[ref_id] = 'AH'
                            #lp_file.write("%s = 0;\n" % (ref_name("MM", ref_id)))
#                        if not reference[ref_id].is_load and all_loads_lru(last_gr):
#                            #copybacks = times[node]
#                            lp_file.write("%s = %s;\n" % (ref_name("MCB", ref_id), var_name("X", node)))
#                        else:
#                            lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))
                    else:
                        # group reuse outside loop
                        # This implies nested loops, such as:
                        # for i:
                        #   r += A[i]
                        #   for j:
                        #     s += A[i]
                        distance_before_loop = distance
                        # test self-reuse
                        distance_in_loop = max_conflicts_in_loop(node, loop_addr_list, ref_id, DATA_CACHE_WAYS)
                        if distance_in_loop > DATA_CACHE_WAYS:
                            if distance_before_loop > DATA_CACHE_WAYS: # always-miss
                                debug_str[ref_id] = "/* dist >=%d in/out (always miss) */" % (DATA_CACHE_WAYS)
                                category[ref_id] = 'AM'
                                #lp_file.write("%s <= %s;\n" % (ref_name("MM", ref_id), var_name("X", node)))
#                                if not reference[ref_id].is_load:
#                                    #copybacks = times[node]
#                                    lp_file.write("%s = %s;\n" % (ref_name("MCB", ref_id), var_name("X", node)))
#                                else:
#                                    lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))
                            else: # first-hit
                                debug_str[ref_id] = "/* dist >=%d in, %d before loop (first hit) */" % (distance_in_loop, distance_before_loop)
                                category[ref_id] = 'FH'
#                                if ENABLE_LRU_FIRST_HIT:
#                                    #lp_file.write("%s <= %s - 1;\n" % (ref_name("MM", ref_id), var_name("X", node)))
#                                else:
#                                    lp_file.write("%s <= %s; /* FH disabled */\n" % (ref_name("MM", ref_id), var_name("X", node)))
#                                if not reference[ref_id].is_load:
#                                    if all_loads_lru(last_gr):
#                                        #copybacks = times[node]
#                                        lp_file.write("%s <= %s;\n" % (ref_name("MCB", ref_id), var_name("X", node)))
#                                    else:
#                                        #copybacks = times[node] - 1
#                                        lp_file.write("%s <= %s - 1;\n" % (ref_name("MCB", ref_id), var_name("X", node)))
#                                else:
#                                    lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))
                        else:
                            if distance_before_loop > DATA_CACHE_WAYS: # (first-miss +) ratio
                                # first miss each time the loop is executed
                                debug_str[ref_id] = "/* dist %d in, >=%d before loop (first_miss*loop_execs, i.e. ratio) */" % (distance_in_loop, distance_before_loop)
                                category[ref_id] = 'XM'
                                array_misses[ref_id] = misses
                                #lp_file.write("%s <= %d;\n" % (ref_name("MM", ref_id), misses))
#                                if not reference[ref_id].is_load:
#                                    #copybacks = misses
#                                    lp_file.write("%s = %s;\n" % (ref_name("MCB", ref_id), ref_name("MM", ref_id)))
#                                else:
#                                    lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))
                            else: # always-hit
                                debug_str[ref_id] = "/* dist %d in, %d before loop (always hit) */" % (distance_in_loop, distance_before_loop)
                                category[ref_id] = 'AH'
                                #lp_file.write("%s = 0;\n" % (ref_name("MM", ref_id)))
#                                if not reference[ref_id].is_load:
#                                    if all_loads_lru(last_gr):
#                                        #copybacks = times[node]
#                                        lp_file.write("%s <= %s;\n" % (ref_name("MCB", ref_id), var_name("X", node)))
#                                    else:
#                                        lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))
#                                        #copybacks = misses accounted in the self-reuse
#                                        #lp_file.write("%s = %s;\n" % (ref_name("MCB", ref_id), ref_name("MM", ref_id)))
#                                else:
#                                    lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))
                elif is_constant(reference[ref_id]):
                    #lp_file.write("scalar self+group-reuse, ")
                    if last_gr_node.addr in loop_addr_list:
                        # group reuse in loop
                        if distance > DATA_CACHE_WAYS: # always-miss
                            debug_str[ref_id] = "/* dist >=%d in loop (always miss) */\n" % (distance)
                            category[ref_id] = 'AM'
                            #lp_file.write("%s <= %s;\n" % (ref_name("MM", ref_id), var_name("X", node)))
#                            if not reference[ref_id].is_load:
#                                #copybacks = times[node]
#                                lp_file.write("%s <= %s;\n" % (ref_name("MCB", ref_id), ref_name("MM", ref_id)))
#                            else:
#                                lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))
                        else: # always-hit
                            debug_str[ref_id] = "/* dist %d in loop (always hit) */\n" % (distance)
                            category[ref_id] = 'AH'
                            #lp_file.write("%s = 0;\n" % (ref_name("MM", ref_id)))
#                            if not reference[ref_id].is_load:
#                                #copybacks = times[node]
#                                if all_loads_lru(last_gr):
#                                    #copybacks = max(misses, 1)
#                                    lp_file.write("%s <= 1;\n" % (ref_name("MCB", ref_id)))
#                                else:
#                                    #copybacks = misses
#                                    lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))
#                            else:
#                                lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))
                    else:
                        #print("Reusing outside the loop")
                        distance_before_loop = distance
                        # test self-reuse
                        distance_in_loop = max_conflicts_in_loop(node, loop_addr_list, ref_id, DATA_CACHE_WAYS)
                        #print("Distance in loop:", distance_in_loop)
                        if distance_in_loop > DATA_CACHE_WAYS:
                            if distance_before_loop > DATA_CACHE_WAYS: # always-miss
                                debug_str[ref_id] = "/* dist >=%d in/out loop (always miss) */\n" % (DATA_CACHE_WAYS)
                                category[ref_id] = 'AM'
                                #lp_file.write("%s <= %s;\n" % (ref_name("MM", ref_id), var_name("X", node)))
#                                if not reference[ref_id].is_load:
#                                    #copybacks = misses
#                                    lp_file.write("%s = %s;\n" % (ref_name("MCB", ref_id), ref_name("MM", ref_id)))
#                                else:
#                                    lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))
                            else: # first-hit
                                debug_str[ref_id] = "/* dist >=%d in, %d out loop (first hit) */\n" % (distance_in_loop, distance_before_loop)
                                category[ref_id] = 'FH'
                                #lp_file.write("%s <= %s - 1;\n" % (ref_name("MM", ref_id), var_name("X", node)))
#                                if ENABLE_LRU_FIRST_HIT:
#                                    lp_file.write("%s <= %s - 1;\n" % (ref_name("MM", ref_id), var_name("X", node)))
#                                else:
#                                    lp_file.write("%s <= %s; /* FH disabled */\n" % (ref_name("MM", ref_id), var_name("X", node)))
#                                if not reference[ref_id].is_load:
#                                    if all_loads_lru(last_gr):
#                                        #copybacks = times[node]
#                                        lp_file.write("%s <= %s;\n" % (ref_name("MCB", ref_id), var_name("X", node)))
#                                    else:
#                                        #copybacks = misses
#                                        lp_file.write("%s = %s;\n" % (ref_name("MCB", ref_id), ref_name("MM", ref_id)))
#                                else:
#                                    lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))
                        else:
                            if distance_before_loop > DATA_CACHE_WAYS: #first-miss
                                debug_str[ref_id] = "/* dist %d in, >=%d out loop (first miss) */\n" % (distance_in_loop, distance_before_loop)
                                category[ref_id] = 'FM'
                                #lp_file.write("%s <= 1;\n" % (ref_name("MM", ref_id)))
#                                if not reference[ref_id].is_load:
#                                    #copybacks = times[node]
#                                    lp_file.write("%s <= %s;\n" % (ref_name("MCB", ref_id), var_name("X", node)))
#                                else:
#                                    lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))
                            else: # always-hit
                                debug_str[ref_id] = "/* dist %d in, %d out loop (always hit) */\n" % (distance_in_loop, distance_before_loop)
                                category[ref_id] = 'AH'
                                #lp_file.write("%s = 0;\n" % (ref_name("MM", ref_id)))
#                                if not reference[ref_id].is_load:
#                                    if all_loads_lru(last_gr):
#                                        #copybacks = times[node]
#                                        lp_file.write("%s <= %s;\n" % (ref_name("MCB", ref_id), var_name("X", node)))
#                                    else:
#                                        #copybacks = times[node] - 1
#                                        lp_file.write("%s <= %s - 1;\n" % (ref_name("MCB", ref_id), var_name("X", node)))
#                                else:
#                                    lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))
                elif is_nonlinear(reference[ref_id]):
                    #lp_file.write("non-linear group-reuse, ")
                    if last_gr_node.addr in loop_addr_list:
                        if distance > DATA_CACHE_WAYS: # always-miss
                            debug_str[ref_id] = "/* dist >=%d in loop (always miss) */\n" % (distance)
                            category[ref_id] = 'AM'
                            #lp_file.write("%s <= %s;\n" % (ref_name("MM", ref_id), var_name("X", node)))
#                            if not reference[ref_id].is_load:
#                                #copybacks = misses
#                                lp_file.write("%s = %s;\n" % (ref_name("MCB", ref_id), ref_name("MM", ref_id)))
#                            else:
#                                lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))
                        else: # always-hit
                            debug_str[ref_id] = "/* dist %d in loop (always hit) */\n" % (distance)
                            category[ref_id] = 'AH'
                            #lp_file.write("%s = 0;\n" % (ref_name("MM", ref_id)))
#                            if not reference[ref_id].is_load and all_loads_lru(last_gr):
#                                #copybacks = times[node]
#                                lp_file.write("%s = %s;\n" % (ref_name("MCB", ref_id), var_name("X", node)))
#                            else:
#                                lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))
                    else:
                        #print("Reusing outside the loop")
                        if distance > DATA_CACHE_WAYS: # always-miss
                            debug_str[ref_id] = "/* dist %d before loop (always miss) */\n" % (distance)
                            category[ref_id] = 'AM'
                            #lp_file.write("%s <= %s;\n" % (ref_name("MM", ref_id), var_name("X", node)))
#                            if not reference[ref_id].is_load:
#                                #copybacks = misses
#                                lp_file.write("%s = %s;\n" % (ref_name("MCB", ref_id), ref_name("MM", ref_id)))
#                            else:
#                                lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))
                        else: # first-hit
                            debug_str[ref_id] = "/* dist %d before loop (first hit) */\n" % (distance)
                            category[ref_id] = 'FH'
                            #lp_file.write("%s <= %s - 1;\n" % (ref_name("MM", ref_id), var_name("X", node)))
#                            if ENABLE_LRU_FIRST_HIT:
#                                lp_file.write("%s <= %s - 1;\n" % (ref_name("MM", ref_id), var_name("X", node)))
#                            else:
#                                lp_file.write("%s <= %s; /* FH disabled */\n" % (ref_name("MM", ref_id), var_name("X", node)))
#                            if not reference[ref_id].is_load and all_loads_lru(last_gr):
#                                #copybacks = times[node]
#                                lp_file.write("%s <= %s;\n" % (ref_name("MCB", ref_id), var_name("X", node)))
#                            else:
#                                lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))
                else:
                    print("ERROR: unknown reuse type", file = sys.stderr)
                    sys.exit(-1)

            #elif enclosing_loop_addr and last_gr is None:
            else:
                # NO GROUP-REUSE in loop
                debug_str[ref_id] = "/* no group reuse in loop /*\n"
                if is_precise_array(reference[ref_id]) or is_imprecise_array(reference[ref_id]):
                    # get distance and sequential misses
                    distance = max_conflicts_in_loop(node, loop_addr_list, ref_id, DATA_CACHE_WAYS)
                    misses = calculate_misses_in_enclosing_loop(node, reference[ref_id])
#                    if is_precise_array(reference[ref_id]):
#                        #lp_file.write("precise array self-reuse, ")
#                        misses = calculate_misses(node, reference[ref_id].term, reference[ref_id].c)
#                    else: # imprecise array
#                        #lp_file.write("imprecise array self-reuse, ")
#                        misses = calculate_misses(node, reference[ref_id].term, None)
                    # get stride
#                    stride = 0
#                    for (i, h) in reference[ref_id].term:
#                        if i == enclosing_loop_addr:
#                            stride = h
#                            break
                    # if stride==0, we have temporal (not spatial) reuse
                    #lp_file.write("/* stride %d /*\n" % (stride))

                    if distance > DATA_CACHE_WAYS:
                        debug_str[ref_id] = "/* dist >=%d (always miss) */\n" % (distance)
                        category[ref_id] = 'AM'
                    else:
                        debug_str[ref_id] = "/* dist %d (hit if same cache line) */\n" % (distance)
                        category[ref_id] = 'XM'
                        array_misses[ref_id] = misses

#                    ## TODO: the following if is not needed, since misses already account for the strike
#                    if stride == 0 or int(CACHE_LINE_SIZE / stride) > 1:
#                        # short self spatial reuse
#                        if distance > DATA_CACHE_WAYS:
#                            debug_str[ref_id] = "/* dist >=%d, dim %d (always miss) */\n" % (distance, len(reference[ref_id].term))
#                            category[ref_id] = 'AM'
#                            #lp_file.write("%s <= %s;\n" % (ref_name("MM", ref_id), var_name("X", node)))
#                            #if not reference[ref_id].is_load:
##                            if not reference[ref_id].is_load or not all_loads_lru(ref_id):
##                                #copybacks = misses
##                                lp_file.write("%s = %s;\n" % (ref_name("MCB", ref_id), ref_name("MM", ref_id)))
##                            else:
##                                lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))
#                        else:
#                            debug_str[ref_id] = "/* dist %d (hit if same cache line) */\n" % (distance)
#                            category[ref_id] = 'XM'
#                            array_misses[ref_id] = misses
#                            #lp_file.write("%s <= %s;\n" % (ref_name("MM", ref_id), var_name("X", node)))
#                            #lp_file.write("%s <= %d;\n" % (ref_name("MM", ref_id), misses))
#                            #if not reference[ref_id].is_load:
##                            if not reference[ref_id].is_load or not all_loads_lru(ref_id):
##                                #copybacks = misses
##                                lp_file.write("%s = %s;\n" % (ref_name("MCB", ref_id), ref_name("MM", ref_id)))
##                            else:
##                                lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))
#                    else:
#                        # long self spatial reuse
#                        if distance > DATA_CACHE_WAYS:
#                            debug_str[ref_id] = "/* dist >=%d, stride %d dim %d (always miss) */\n" % (distance, stride, len(reference[ref_id].term))
#                            category[ref_id] = 'AM'
#                            #lp_file.write("%s <= %s;\n" % (ref_name("MM", ref_id), var_name("X", node)))
#                            #if not reference[ref_id].is_load:
##                            if not reference[ref_id].is_load or not all_loads_lru(ref_id):
##                                #copybacks = misses
##                                lp_file.write("%s = %s;\n" % (ref_name("MCB", ref_id), ref_name("MM", ref_id)))
##                            else:
##                                lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))
#                        else:
#                            debug_str[ref_id] = "/* dist %d, stride %d dim %d (always miss) */\n" % (distance, stride, len(reference[ref_id].term))
#                            category[ref_id] = 'AM'
#                            #lp_file.write("%s <= %s;\n" % (ref_name("MM", ref_id), var_name("X", node)))
#                            #if not reference[ref_id].is_load:
##                            if not reference[ref_id].is_load or not all_loads_lru(ref_id):
##                                #copybacks = misses
##                                lp_file.write("%s = %s;\n" % (ref_name("MCB", ref_id), ref_name("MM", ref_id)))
##                            else:
##                                lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))
                elif is_constant(reference[ref_id]):
                    #lp_file.write("scalar self-reuse, ")
                    distance = max_conflicts_in_loop(node, loop_addr_list, ref_id, DATA_CACHE_WAYS)
                    if distance > DATA_CACHE_WAYS: # always-miss
                        debug_str[ref_id] = "/* dist >=%d (always miss) */\n" % (distance)
                        category[ref_id] = 'AM'
                        #lp_file.write("%s <= %s;\n" % (ref_name("MM", ref_id), var_name("X", node)))
#                        if not reference[ref_id].is_load:
#                            #copybacks = misses
#                            lp_file.write("%s = %s;\n" % (ref_name("MCB", ref_id), ref_name("MM", ref_id)))
#                        else:
#                            lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))
                    else: # first-miss
                        debug_str[ref_id] = "/* dist %d (first miss) */\n" % (distance)
                        category[ref_id] = 'FM'
                        #lp_file.write("%s <= 1;\n" % (ref_name("MM", ref_id)))
#                        if not reference[ref_id].is_load:
#                            #copybacks = misses
#                            lp_file.write("%s = %s;\n" % (ref_name("MCB", ref_id), ref_name("MM", ref_id)))
#                        else:
#                            lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))
                elif is_nonlinear(reference[ref_id]):
                    debug_str[ref_id] = "/* non-linear in loop (always miss) */\n"
                    category[ref_id] = 'AM'
                    #lp_file.write("%s <= %s;\n" % (ref_name("MM", ref_id), var_name("X", node)))
#                    if not reference[ref_id].is_load:
#                        #copybacks = misses
#                        lp_file.write("%s = %s;\n" % (ref_name("MCB", ref_id), ref_name("MM", ref_id)))
#                    else:
#                        lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))
                else:
                    print("ERROR: unknown reuse type", file = sys.stderr)
                    sys.exit(-1)

        else: # outside loop
            if last_gr is not None:
                # GROUP-REUSE outside loop
                (distance, last_gr_node) = calc_distance_and_reused_node(node, ref_id, last_gr)
#                    if is_constant(reference[ref_id]):
#                        lp_file.write("scalar group-reuse, ")
#                    elif is_nonlinear(reference[ref_id]):
#                        lp_file.write("non-linear group-reuse, ")
#                    else:
#                        print("ERROR: unknown reuse type at 0x%x" % reference[ref_id].pc)
#                        sys.exit(-1)
                if distance > DATA_CACHE_WAYS: # no locality
                    debug_str[ref_id] = "/* dist >=%d (always miss) */\n" % (distance)
                    category[ref_id] = 'AM'
                    #misses = times[node] # = 0 or 1
                    #lp_file.write("%s <= %s;\n" % (ref_name("MM", ref_id), var_name("X", node)))
#                    if not reference[ref_id].is_load:
#                        #copybacks = misses
#                        lp_file.write("%s = %s;\n" % (ref_name("MCB", ref_id), ref_name("MM", ref_id)))
#                    else:
#                        lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))
                else: # locality
                    debug_str[ref_id] = "/* dist %d (always hit) */\n" % (distance)
                    category[ref_id] = 'AH'
                    #lp_file.write("%s = 0;\n" % (ref_name("MM", ref_id)))
#                    if not reference[ref_id].is_load and all_loads_lru(last_gr):
#                        #copybacks = times[node] # = 0 or 1
#                        lp_file.write("%s <= %s;\n" % (ref_name("MCB", ref_id), ref_name("MM", ref_id)))
#                    else:
#                        lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))
            else:
                # NO GROUP-REUSE outside loop (scalar first-use, non-linear): MISS
#                    if is_constant(reference[ref_id]):
#                        lp_file.write("scalar first-use")
#                    else:
#                        lp_file.write("non-linear first-use")
                #misses = times[node]
                debug_str[ref_id] = "/* first use (miss) */\n"
                category[ref_id] = 'AM'
                #lp_file.write("%s <= %s;\n" % (ref_name("MM", ref_id), var_name("X", node)))
                #lp_file.write("%s = 0; /* first use */\n" % (ref_name("MH", ref_id)))
#                if not reference[ref_id].is_load:
#                    #copybacks = times[node]
#                    lp_file.write("%s = %s;\n" % (ref_name("MCB", ref_id), ref_name("MM", ref_id)))
#                else:
#                    lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))

    for ref_id in reference:
        print_ref_summary(lp_file, ref_id)
        lp_file.write(debug_str[ref_id])
        ########## Print constraints depending on category
        generate_miss_constraints_by_category(lp_file, ref_id, category, array_misses)
        ########## Print writeback constraint
        if reference[ref_id].is_load:
            # no writeback for this access
            lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))
        else:
            # store
            if category[ref_id] in ('AM', 'FH', 'FM', 'XM'):
                # store with misses (to eventually write back):
                # count as writebacks as misses
                lp_file.write("%s = %s;\n" % (ref_name("MCB", ref_id), ref_name("MM", ref_id)))
            else: # AH
                last_gr = reference[ref_id].last_group_reuse
                if all_loads_lru_accurate(last_gr, category):
                    if is_constant(reference[ref_id]):
                        # store hit (AH) to single address and no previous store hits sets dirty flag:
                        # single writeback
                        # (special case)
                        lp_file.write("%s = 1;\n" % (ref_name("MCB", ref_id)))
                    else:
                        # store hit (AH) to constant/variable address and no previous store hits:
                        # count as writebacks as misses OF THE MISSING REF
                        missing_ref = last_gr
                        while category[missing_ref] == 'AH':
                            missing_ref = reference[missing_ref].last_group_reuse
                        lp_file.write("%s = %s;\n" % (ref_name("MCB", ref_id), ref_name("MM", missing_ref)))
                else:
                    # store hit with previous store hit:
                    # writeback already counted
                    lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))

# VERSION 2
#            last_gr = reference[ref_id].last_group_reuse
#            if last_gr is None:
#                # store miss:
#                # count as writebacks as misses
#                lp_file.write("%s = %s;\n" % (ref_name("MCB", ref_id), ref_name("MM", ref_id)))
#            else:
#                if all_loads_lru_accurate(last_gr, category):
#                    # store hit (AH) and no previous store hits:
#                    # single writeback
#                    lp_file.write("%s <= 1;\n" % (ref_name("MCB", ref_id)))
#                else:
#                    # store hit with previous store hit:
#                    # writeback already counted
#                    lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))

# VERSION 1
#            if category[ref_id] == 'AH' and not all_loads_lru_accurate(last_gr, category):
#                # store hit with previous store hit:
#                # writeback already counted
#                lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))
#            else:
#                if category[ref_id] == 'AH':
#                    # store hit and no previous store hits:
#                    # single writeback
#                    lp_file.write("%s <= 1;\n" % (ref_name("MCB", ref_id)))
#                else:
#                    # store miss and no previous store hits:
#                    # count as writebacks as misses
#                    lp_file.write("%s = %s;\n" % (ref_name("MCB", ref_id), ref_name("MM", ref_id)))


    ########## Print global memory cost constraint
    lp_file.write("/* Memory_cost = SUM(hit_cost*hits + miss_cost*misses + wb_cost*writebacks) */\n")
    lp_file.write("Memory_cost =")
    for ref_id in reference:
        lp_file.write(" + %d %s + %d %s + %d %s" % (DATA_HIT_COST, ref_name("MH", ref_id), DATA_MISS_COST, ref_name("MM", ref_id), MEMORY_COST, ref_name("MCB", ref_id)))
    lp_file.write(";\n")


def generate_miss_constraints_by_category(lp_file, ref_id, category, array_misses):
    """
    Print miss constraints depending on its category and array_misses.
    Valid for LRU and ACDC.
    TODO: for FH/FM, set in relation to loop i, not to last loop
    """
    node = cfg_node_ids[reference[ref_id].cfg_node_id]
    if category[ref_id] == 'AH':
        # MemMisses = 0
        lp_file.write("%s = 0;\n" % (ref_name("MM", ref_id)))
    elif category[ref_id] == 'AM':
        # MemMisses = X
        lp_file.write("%s <= %s;\n" % (ref_name("MM", ref_id), var_name("X", node)))
    elif category[ref_id] == 'FH':
        # LRU-ONLY (impossible in ACDC)
        # The localtion of the dominant ld/st is not tested, so it
        # may be outside several nested loops
        if ENABLE_LRU_FIRST_HIT:
            lp_file.write("%s <= %s - 1;\n" % (ref_name("MM", ref_id), var_name("X", node)))
        else:
            lp_file.write("%s <= %s;\n /* FH disabled -> AM */" % (ref_name("MM", ref_id), var_name("X", node)))
#        if ENABLE_LRU_FIRST_HIT:
#            # MemMisses = X - \sum(non-back-edges_to_loop-header)
#            #TODO: buscar nodo con first stride
#            loop_header_node = enclosing_loop_header[node]
#            lp_file.write("%s <= %s" % (ref_name("MM", ref_id), var_name("X", node)))
#            for parent in loop_header_node.predecessors:
#                if not isBackEdge(node, parent):
#                    lp_file.write(" - %s" % (edge_name("D", parent, node)))
#            lp_file.write(";\n")
#        else:
#            # MemMisses = X
#            lp_file.write("%s <= %s; /* FH disabled */\n" % (ref_name("MM", ref_id), var_name("X", node)))
    elif category[ref_id] == 'FM':
        if DATA_CACHE_TYPE in ("lru", "constlru"):
            # single miss each time the enclosing loop is reached
            # MemMisses = \sum(non-back-edges to loop-header)
            #TODO: buscar nodo con first stride
            loop_header_node = enclosing_loop_header[node]
            lp_file.write("%s <=" % (ref_name("MM", ref_id)))
            for parent in loop_header_node.predecessors:
                if not isBackEdge(node, parent):
                    lp_file.write(" + %s" % (edge_name("D", parent, node)))
            lp_file.write(";\n")
        elif DATA_CACHE_TYPE == "acdc":
            # single miss
            lp_file.write("%s <= 1;\n" % (ref_name("MM", ref_id)))
        else:
            print("generate_miss_constraints_by_category ERROR: unknown DATA_CACHE_TYPE", file = sys.stderr)
            sys.exit(-1)
    elif category[ref_id] == 'XM':
        # MemMisses = \sum(misses * non-back-edges_to_loop-header)
        loop_header_node = enclosing_loop_header[node]
        #lp_file.write("%s <= %d;\n" % (ref_name("MM", ref_id), array_misses[ref_id]))
        lp_file.write("%s <=" % (ref_name("MM", ref_id)))
        for parent in loop_header_node.predecessors:
            if not isBackEdge(node, parent):
                lp_file.write(" + %s" % (edge_name("D", parent, node)))
        lp_file.write(";\n")


# Make list of unique accesses between reusing accesses
def max_conflicts_in_loop(node, addr_list, target_ref, abort_at):
    """
    Obtain the number of unique accesses (except subloops) between a given load/store in different iterations, considering any alternative path between them.
    If the returned value is larger than <abort_at>, it is not exact.

    Returns:
        The number described above.
    """
    ref_list = recursive_access_list_in_loop(node, addr_list, [], [], target_ref, abort_at + 1)
    #print("Conflicts for ld/st at", hex(reference[target_ref].pc), ref_list)
    num_refs = len(ref_list)
    if is_array(reference[target_ref]):
        num_refs -= 1 # discard conflict with itself
    #print("Total: %d" % num_refs)
    return num_refs

def recursive_access_list_in_loop(node, addr_list, explored, ref_list, target_ref, abort_at):
    """
    Recursivelly explore successors of <node> belonging to <addr_list> to obtain a list of unique accesses (except subloops) between a given load/store in different iterations, considering any alternative path between them.
    Abort if the list gets larger than <abort_at>.

    Returns:
        A list of unique addresses (except for subloops and unknown address marks)
    """
    if node.addr not in explored and node.addr in addr_list:
        accesses = ref_list_in(ref_list, node, target_ref)
        explored.append(node.addr)
#        if len(accesses) > abort_at:
#            return accesses + [-1,-1,-1]
        #print("explored", explored)
        max_accesses = accesses
        for child in node.successors:
            if len(max_accesses) > abort_at:
                return max_accesses
            # OPTION 1: Consider the path with target_ref and all alternative paths. For each ref, add it
            path_accesses = recursive_access_list_in_loop(child, addr_list, explored, max_accesses, target_ref, abort_at)
            if len(path_accesses) > abort_at:
                return path_accesses
            #if max_times[child] > max_times[node] and max_times[node]:
            if child in max_times and node in max_times and max_times[child] > max_times[node]:
                # if child is a new loop, calculate extra pollution
                path_accesses = calc_pollution(child, int(max_times[child]/max_times[node]), path_accesses, target_ref, abort_at)
            max_accesses += path_accesses
            # OPTION 2: Consider just the path with target_ref and the worst alternative path. UNSAFE.
            # path_accesses = recursive_access_list_in_loop(child, addr_list, explored, accesses, target_ref, abort_at)
            # if times[child] > times[node] and times[node]:
            #     # if child is a new loop, calculate extra pollution
            #     path_accesses = calc_pollution(node, int(times[child]/times[node]), path_accesses, target_ref, abort_at)
            # if len(path_accesses) > len(max_accesses):
            #     max_accesses = path_accesses
            # OPTION 3: Consider just the path with target_ref. UNSAFE.
            # not implemented
        #print(added_accesses)
        return max_accesses
    else:
        return ref_list

def get_successors_in_list(cfg_node, addr_list, node_list):
    for node in cfg_node.successors:
        if node.addr in addr_list and node not in node_list:
            node_list += get_successors_in_list(node, addr_list, node_list + [node])
    return list(set(node_list))

def calc_distance_and_reused_node(node, ref_id, last_gr):
    """
    Calculates the reuse distance and the reused node
    """
    distance = None
    last_gr_node = None
    for reused_node in get_all_cfg_nodes(reference[last_gr].pc):
        this_distance = max_conflicts_between(reused_node, last_gr, node, ref_id, ref_id, DATA_CACHE_WAYS)
        if distance is None:
            distance = this_distance
            last_gr_node = reused_node
        elif this_distance < distance:
            distance = this_distance
            last_gr_node = reused_node
        if distance > DATA_CACHE_WAYS:
            # abort with current distance
            return (distance, last_gr_node)
    #print("distance %d to" % distance, last_gr_node)
    return (distance, last_gr_node)

def max_conflicts_between(ini_node, ini_ref, fin_node, fin_ref, target_ref, abort_at):
    """
    Returns the number of loads/stores between two loads/stores.
    If constant_addr, does not count references accessing other sets.
    If node_list (nodes in a loop), do not explore outside.
    """
    #print("Conflicts between 0x%x and 0x%x" % (reference[ini_ref].pc, reference[fin_ref].pc))
    ref_list = back_explore_conflicts(ini_node, ini_ref, fin_node, fin_ref, fin_node, [], target_ref, abort_at + 1)
    #print_pcs(target_ref, ref_list)
    num_refs = len(ref_list)
    if is_array(reference[target_ref]) and num_refs > 0:
        num_refs -= 1 # discard conflict with itself
    #print("Calculated conflicts: %d" % num_refs)
    return num_refs

def back_explore_conflicts(ini, ini_id, fin, fin_id, node, ref_list, target_ref, abort_at):
    """
    Performs a backwards exploration (from nodes fin to ini) to count memory references from ini_id to fin_id conflicting with target_ref.

    Returns:
        The longest list of references found between ini_id in ini and fin_id in fin, ending with a mark if aborted at abort_at
    """
    #print(var_name("B", node), ref_list)
    if ini == fin: # Same basic block, no CFG analysis needed
        return ref_list_from_to(ini, ini_id, fin_id, target_ref)
    elif ini == node: # First (ending) node reached, end CFG analysis
        return ref_list_from(ref_list, ini, ini_id, target_ref)
    elif fin == node: # Last (starting) node, start CFG analysis
        accesses = ref_list_to([], fin, fin_id, target_ref)
    else:
        accesses = ref_list_in_reverse(ref_list, node, target_ref)
    if len(accesses) > abort_at:
        #return accesses + [-1, -1, -1]
        return accesses
    max_accesses = accesses
    #print("accesos:", len(max_accesses))
    #print(node.predecessors)
    for parent in node.predecessors:
        if len(max_accesses) > abort_at:
            return max_accesses
        if not isBackEdge(node, parent): # may traverse loops
            path_accesses = back_explore_conflicts(ini, ini_id, fin, fin_id, parent, accesses, target_ref, abort_at)
            if node in max_times and parent in max_times and max_times[parent] > max_times[node]:
                path_accesses = calc_pollution(parent, int(max_times[parent]/max_times[node]), path_accesses, target_ref, abort_at)
            if len(path_accesses) > len(max_accesses):
                max_accesses = path_accesses
                #print("+pollution:", len(max_accesses))
    return max_accesses


def ref_list_in_reverse(ini_list, node, target_ref):
    """
    Returns a list of unique loads/stores in node conflicting with <target_ref>.
    If <target_ref> is None, assume conflict always.

    Returns:
        List of accessed reference ids
    """
    ref_list = ini_list.copy()
    refs = get_refs_in_node(node)
    for ref in reversed(refs):
        added = False
        for confl_ref in ref_list:
            if reference[confl_ref].last_group_reuse and reference[confl_ref].last_group_reuse == ref:
                # if ref reuses anoter ref in ref_list):
                # update for the newer last group reuse reference
                ref_list.remove(confl_ref)
                ref_list.append(ref)
#                    print("  + 0x%x (%d) - 0x%x (%d)" % (reference[ref].pc, ref, reference[confl_ref].pc, confl_ref))
                added = True
                break
        if not added and conflicts(ref, target_ref):
            ref_list.append(ref)
#                print("  + 0x%x (%d)" % (reference[ref].pc, ref))
#            else:
#                print("  discarded 0x%x" % (reference[ref].pc))
    return ref_list


def ref_list_from(ini_list, node, ref_id, target_ref):
    ref_list = ini_list
    refs = get_refs_in_node(node)
    counting = False
    for ref in refs:
        if ref == ref_id:
            counting = True
        if counting:
            if reference[ref].last_group_reuse and reference[ref].last_group_reuse in ref_list:
                # if ref reuses anoter ref in ref_list):
                # update for the newer last group reuse reference
                ref_list.remove(reference[ref].last_group_reuse)
                ref_list.append(ref)
#                    print("  + 0x%x - 0x%x" % (reference[ref].pc,reference[reference[ref].last_group_reuse].pc))
            elif conflicts(ref, target_ref):
                ref_list.append(ref)
#                    print("  + 0x%x " % (reference[ref].pc))
#                else:
#                    print("  discarded 0x%x" % (reference[ref].pc))
    return ref_list

def ref_list_from_to(node, ini, fin, target_ref):
    """
    Returns a list of unique loads/stores in node, from <ini> to <fin> conflicting with <target_ref>.

    Returns:
        List of accessed reference ids
    """
    if ini < fin:
        ini_id = ini
        fin_id = fin
    else:
        fin_id = ini
        ini_id = fin
    ref_list = []
    counting = False
    refs = get_refs_in_node(node)
    for ref in refs:
        if ref == ini_id:
            counting = True
        if ref == fin_id and counting:
#            print(ref_list)
            return ref_list
        if counting:
            if reference[ref].last_group_reuse and reference[ref].last_group_reuse in ref_list:
                # if ref reuses anoter ref in ref_list):
                # update for the newer last group reuse reference
                ref_list.remove(reference[ref].last_group_reuse)
                ref_list.append(ref)
#                print("  + 0x%x - 0x%x" % (reference[ref].pc,reference[reference[ref].last_group_reuse].pc))
            elif conflicts(ref, target_ref):
                ref_list.append(ref)
#                print("  + 0x%x (%d)" % (reference[ref].pc, ref))
#            else:
#                print("  discarded 0x%x" % (reference[ref].pc))
    print("ref_list_from_to ERROR: delimiters", ini_id, fin_id, "not found", file = sys.stderr)
    print(refs, file = sys.stderr)
    sys.exit(-1)


#def get_loop_addr(cfg_node):
#    """
#    Returns the addr of the loop enclosing cfg_node.
#    IMPORTANT: if cfg_node is inside no loop, the return value will be invalid.
#    """
#    # first try: look in loop body nodes
#    #loop_addr = getDeepestLoop(cfg_node)
#    loop_addr = enclosing_loop[cfg_node]
#    if loop_addr != 0:
#        return loop_addr
#    #print("second try for", cfg_node)
#    # second try (functions called from loops): look for predecessor loop header
#    for parent in cfg_node.predecessors:
#        if parent in loop_headers and not is_break_edge(parent, cfg_node):
#            return parent.addr
#    for parent in cfg_node.predecessors:
#        if not is_break_edge(parent, cfg_node):
#            result = get_loop_addr(parent)
#            if result:
#                return result
#        else: # loop above, jump it and continue testing
#            loop_addr = get_loop_addr(parent)
#            #print("found loop 0x%x above node" % (loop_addr))
#            if loop_addr:
#                loop = getLoop(loop_addr)
#                for entry_node in cfg.model.get_all_nodes(loop_addr):
#                    #print("entry node of loop 0x%x:" % (loop_addr), entry_node)
#                    for parent2 in entry_node.predecessors:
#                        result = get_loop_addr(parent2)
#                        if result:
#                            return result
#    # no loop found
#    #print("get_loop_addr: Error,", var_name("B", cfg_node), "is inside no loop")
#    return None
#    #sys.exit(-1)

def getLoop(nodeaddr):
    for loop in loop_info.loops:
        if nodeaddr == loop.entry.addr:
            return loop
    return None

def getLoopNodeSet(loop):
    loopnodeset = []
    for blocknode in loop.body_nodes:
        loopnodeset.append(blocknode.addr)
    if loop.has_calls:
        for blocknode in loop.body_nodes:
            #print(blocknode.addr)
            node = cfg.model.get_any_node(blocknode.addr)
            #for successor in blocknode.successors():
            # bug? blocknode.successors() fails sometimes
            for successor in node.successors:
                func = cfg.kb.functions.function(addr=successor.addr)
                if func:
                    #loopnodeset += getNodeSet(func)
                    for node2 in get_node_set(func):
                        loopnodeset.append(node2.addr)
    return loopnodeset

def get_node_set(func):
    #print("nodes of", func.name, ":", func.nodes)
    for node in set(func.nodes):
        if node not in get_node_set.nodeset:
            get_node_set.nodeset.append(node)
    for call in set(func.get_call_sites()):
        calledf = func.get_call_target(call)
        if calledf != func.addr: # do not follow recursive calls
            get_node_set(cfg.kb.functions[calledf])
    return get_node_set.nodeset
get_node_set.nodeset = []

def get_all_cfg_nodes(instaddr):
    """ Gets all nodes including instaddr """
    while (cfg.model.get_any_node(instaddr) is None) and (instaddr > 0):
        instaddr -= INSTRUCTION_SIZE
    return cfg.model.get_all_nodes(instaddr)

def conflicts(ref1, ref2):
    """
    Returns whether two references conflict in cache.
    If they 1) present group reuse, or 2) are the same constant address, or 3) are constants mapped to different sets, they are assumed to conflict.
    """
    if reference[ref1].last_group_reuse == ref2 or reference[ref2].last_group_reuse == ref1:
        return False
    if is_constant(reference[ref1]) and is_constant(reference[ref2]):
        addr1 = reference[ref1].c
        addr2 = reference[ref2].c
        if addr1 == addr2 or data_cache_set(addr1) != data_cache_set(addr2):
            return False
    return True

def data_cache_set(addr):
    return int(addr / CACHE_LINE_SIZE) % DATA_CACHE_SETS

def ref_list_to(ini_list, node, ref_id, target_ref):
    """
    Returns a list of unique loads/stores in node, until <fin_id> conflicting with <target_ref>.

    Returns:
        List of accessed reference ids
    """
    ref_list = ini_list
    refs = get_refs_in_node(node)
    for ref in refs:
        if ref == ref_id:
            break
        if reference[ref].last_group_reuse and reference[ref].last_group_reuse in ref_list:
            # if ref reuses anoter ref in ref_list):
            # update for the newer last group reuse reference
            ref_list.remove(reference[ref].last_group_reuse)
            ref_list.append(ref)
        elif conflicts(ref, target_ref):
            ref_list.append(ref)
#                print("  added", hex(reference[ref].pc))
#            else:
#                print("  discarded", hex(reference[ref].pc))
    return ref_list

def ref_list_in(ini_list, node, target_ref):
    """
    Returns a list of unique loads/stores in node conflicting with <target_ref>.
    If <target_ref> is None, assume conflict always.

    Returns:
        List of accessed reference ids
    """
    ref_list = ini_list
    refs = get_refs_in_node(node)
    for ref in refs:
        if reference[ref].last_group_reuse and reference[ref].last_group_reuse in ref_list:
            # if ref reuses anoter ref in ref_list):
            # update for the newer last group reuse reference
            ref_list.remove(reference[ref].last_group_reuse)
            ref_list.append(ref)
        elif conflicts(ref, target_ref):
            ref_list.append(ref)
    return ref_list


def calc_pollution(node, iterations, ref_list, target_ref, abort_at):
    """
    Given a list of references, for each array reference in the list, this function test how many ways in each set it might touch. Then, it replicates the reference as many times as ways it can touch in a set.

    Returns:
    The new list of references, with the described replications.
    """
    new_ref_list = ref_list.copy()
    #loop_addr = get_loop_addr(node)
    if enclosing_loop_header[node]:
        loop_addr = enclosing_loop_header[node].addr
    else:
        print("Node %s is inside no loop", node, file = sys.stderr)
    for ref in ref_list:
        if ref != -1 and is_array(reference[ref]):
            access_stride = 0
            #print("Node", node, "in loop 0x%x" % loop_addr)
            for (addr, stride) in reference[ref].term:
                #print("%d i0x%x" % (stride, addr))
                if loop_addr == addr:
                    access_stride = stride
                    break
            if access_stride: # self-spatial reuse
                if reference[target_ref].c:
                    loop_misses = calculate_sequential_misses(iterations, access_stride, reference[target_ref].c)
                else:
                    loop_misses = calculate_sequential_misses(iterations, access_stride, None)
                polluted_ways = ceil(loop_misses / DATA_CACHE_SETS)
                if polluted_ways > 1:
                    print("%d polluted ways in %s" % (polluted_ways, var_name("M", node)), file = sys.stderr)
                for way in range(1, polluted_ways):
                    new_ref_list.append(ref)
                    if len(new_ref_list) > abort_at:
                        return new_ref_list + [-1,-1,-1]
    return new_ref_list



def is_array(ref):
    return bool(ref.term)

def is_precise_array(ref):
    return bool(ref.c and ref.term)

def is_imprecise_array(ref):
    return bool(not ref.c and ref.term)

def is_constant(ref):
    return bool(ref.c and not ref.term)

def is_nonlinear(ref):
    return bool(not ref.c and not ref.term)

def print_ref_summary(lp_file, ref_id): #, has_drp):
    lp_file.write("/* 0x%x " % reference[ref_id].pc)
#    if has_drp:
#        lp_file.write("/* 0x%x DRP " % reference[ref_id].pc)
#    else:
#        lp_file.write("/* 0x%x NO DRP " % reference[ref_id].pc)
    if reference[ref_id].is_load:
        lp_file.write("ld ")
    else:
        lp_file.write("st ")
    if is_precise_array(reference[ref_id]):
        lp_file.write("precise array ")
    elif is_imprecise_array(reference[ref_id]):
        lp_file.write("imprecise array ")
    elif is_constant(reference[ref_id]):
        if reference[ref_id].max_number:
            if reference[ref_id].is_multiline:
                lp_file.write("%d/%d MULTILINE " % (reference[ref_id].number+1, reference[ref_id].max_number+1))
            else:
                lp_file.write("%d/%d single line " % (reference[ref_id].number+1, reference[ref_id].max_number+1))
        lp_file.write("scalar ")
    elif is_nonlinear(reference[ref_id]):
        lp_file.write("non-linear ")
    else:
        print("ERROR: unknown reuse type", file = sys.stderr)
        sys.exit(-1)
    if reference[ref_id].first_group_reuse is None:
        lp_file.write("without group reuse to ")
    else:
        lp_file.write("with group-reuse to ")
    if is_constant(reference[ref_id]):
        lp_file.write("0x%x " % reference[ref_id].c)
    elif is_array(reference[ref_id]):
        for (i, h) in reference[ref_id].term:
            lp_file.write("%d 0x%x + " % (h, i))
        if is_precise_array(reference[ref_id]):
            lp_file.write("0x%x " % reference[ref_id].c)
        else:
            lp_file.write("? ")
    elif is_nonlinear(reference[ref_id]):
        lp_file.write("non-linear ")
    lp_file.write("*/\n")

def ref_name(pre, ref_id):
    #return "%s_0x%x_%d" % (pre, reference[ref_id].pc, reference[ref_id].number)
    return pre + str(reference[ref_id].cfg_node_id).zfill(3) + "_0x%x_%d" % (reference[ref_id].pc, reference[ref_id].number)

def generate_acdc_memory_constraints(lp_file):
    lp_file.write("\n/* Load/store constraints for ACDC */\n\n")
    # general constraints
    generate_data_hitmiss_general_constraints(lp_file)

    # reference specific constraints
    category = {}
    array_misses = {}
    debug_str = {}
    for ref_id in reference:
        node = cfg_node_ids[reference[ref_id].cfg_node_id]
        print_ref_summary(lp_file, ref_id)
        # DRP set estimation, not required
        #set_drp_improvement_str(ref_id)
        # end of DRP set estimation, not required
        first_gr = reference[ref_id].first_group_reuse
        if first_gr is not None:
            # GROUP-REUSE
            has_drp = acdcparse.has_drp(reference[first_gr].pc)
            if has_drp:
                # always hit: MM=0, MCB=0
                #lp_file.write("/* DRP (always hit) */\n")
                debug_str[ref_id] = "/* DRP (always hit) */\n"
                category[ref_id] = 'AH'
                #lp_file.write("%s = 0;\n" % (ref_name("MM", ref_id)))
            else:
                # always miss: MH=0, MCB=0
                #lp_file.write("/* No DRP (always miss) */\n")
                debug_str[ref_id] = "/* No DRP (always miss) */\n"
                category[ref_id] = 'AM'
                #lp_file.write("%s <= %s;\n" % (ref_name("MM", ref_id), var_name("X", node)))
            #lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))
        else:
            # NO GROUP-REUSE
            if node not in enclosing_loop_header:
                # unfeasible path; X var should be 0
                debug_str[ref_id] = "/* unfeasible path, %s should be 0 */\n" % var_name("X", node)
                category[ref_id] = 'AM' # any should be ok
            elif enclosing_loop_header[node] and acdcparse.has_drp(reference[ref_id].pc):
                # SELF REUSE (in loop)
                if is_nonlinear(reference[ref_id]): # always miss
                    # always miss
                    #lp_file.write("/* DRP (always miss to be reused) */\n")
                    debug_str[ref_id] = "/* DRP (always miss to be reused) */\n"
                    category[ref_id] = 'AM'
                    #lp_file.write("%s <= %s;\n" % (ref_name("MM", ref_id), var_name("X", node)))
                elif is_array(reference[ref_id]):
                    acdc_misses = calculate_misses_in_enclosing_loop(node, reference[ref_id])
                    debug_str[ref_id] = "/* DRP (array self-reuse in loop) */\n"
                    category[ref_id] = 'XM'
                    array_misses[ref_id] = acdc_misses
                elif is_constant(reference[ref_id]):
                    debug_str[ref_id] = "/* DRP (scalar self-reuse in loop) */\n"
                    category[ref_id] = 'FM'
                else:
                    print("ERROR: unknown reuse type", file = sys.stderr)
                    sys.exit(-1)

#                else: # array or constant
#                    acdc_misses = calculate_misses_in_enclosing_loop(node, reference[ref_id])
#                    debug_str[ref_id] = "/* DRP (self-reuse in loop) */\n"
#                    category[ref_id] = 'XM'
#                    array_misses[ref_id] = acdc_misses
            else:
                # No reuse
                if enclosing_loop_header[node]:
                    #lp_file.write("/* No DRP (always miss) */\n")
                    debug_str[ref_id] = "/* No DRP (always miss) */\n"
                elif acdcparse.has_drp(reference[ref_id].pc):
                    #lp_file.write("/* DRP (miss, first use to be reused) */\n")
                    debug_str[ref_id] = "/* DRP (miss, first use to be reused) */\n"
                else:
                    #lp_file.write("/* No DRP (miss) */\n")
                    debug_str[ref_id] = "/* No DRP (miss) */\n"
                category[ref_id] = 'AM'
                #lp_file.write("%s <= %s;\n" % (ref_name("MM", ref_id), var_name("X", node)))
                # if no drp, no copyback; if drp, it is constant outside loop: no copyback
                #lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))

    for ref_id in reference:
        print_ref_summary(lp_file, ref_id)
        lp_file.write(debug_str[ref_id])
        ########## Print constraints depending on category
        generate_miss_constraints_by_category(lp_file, ref_id, category, array_misses)
        ########## Print writeback constraint
        if not acdcparse.has_drp(reference[ref_id].pc):
            # always hit in DC or bypass DC:
            # no writeback
            lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))
        else:
            # has DRP (first_gr is None): sometimes hit / miss
            if is_constant(reference[ref_id]):
                # no replacements: no writebacks
                lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))
            elif reference[ref_id].is_load and all_loads_acdc(ref_id):
                # all loads: no writebacks
                lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))
            else:
                # not all loads: count as writebacks as misses
                lp_file.write("%s = %s;\n" % (ref_name("MCB", ref_id), ref_name("MM", ref_id)))

#            # account writebacks on the ref with DRP
#            first_gr = reference[ref_id].first_group_reuse
#            #if first_gr is not None and all_loads_acdc(ref_id):
#            if first_gr is not None:
#                lp_file.write("impossible situation\n")
#                sys.exit(-1)
#                # all loads: no writeback
#                lp_file.write("%s = 0;\n" % (ref_name("MCB", ref_id)))
#            else:
#                # not all loads: count as writebacks as misses
#                lp_file.write("%s = %s;\n" % (ref_name("MCB", ref_id), ref_name("MM", ref_id)))

    # Grouped cost
    lp_file.write("/* Memory_cost = AC_load_cost + SUM(hit_cost*hits + miss_cost*misses + wb_cost*writebacks) */\n")
    num_loadpoints = len(acdcparse.load_points())
    lp_file.write("AC_load_cost = %d; /* %d DRPs (%dc) in %d load points * %d cycles (%dc)*/\n" % (MEMORY_COST*len(acdcparse.drps()) + num_loadpoints*ACDC_PRELOAD_CALL_COST, len(acdcparse.drps()), MEMORY_COST*len(acdcparse.drps()), num_loadpoints, ACDC_PRELOAD_CALL_COST, num_loadpoints*ACDC_PRELOAD_CALL_COST))
    lp_file.write("Memory_cost = AC_load_cost")
    for ref_id in reference:
         lp_file.write(" + %d %s + %d %s + %d %s" % (DATA_HIT_COST, ref_name("MH", ref_id), DATA_MISS_COST, ref_name("MM", ref_id), MEMORY_COST, ref_name("MCB", ref_id)))
    lp_file.write(";\n")

    # print of DRP estimation, not required
    #print_drp_improvements(lp_file)


def print_drp_improvements(lp_file):
    """
    Prints the estimation string.
    lp_solve should have IMPR_0xXXXX variables with the estimated
    improving when granting data replacement permissions to each PC.
    This estimation may not correlate to an improvement in the WCET.
    """
    lp_file.write("\n/* DRP estimation constraints */\n\n")
    for pc in impr_str:
        #lp_file.write("%s;\n" % impr_str[pc])
        addr = get_most_external_loop_header_from_addr(pc)
        lp_file.write("%s; /* most external loop: 0x%x */\n" % (impr_str[pc], addr))

    lp_file.write("free ")
    for pc in impr_str:
        lp_file.write("IMPR_0x%x, " % pc)
    lp_file.write("IMPR_0x0;\n")


def get_all_nodes_containing_addr(addr):
    """
    Returns a list with all nodes containing the instruction address addr
    """
    node_list = cfg.model.get_all_nodes(addr)
    if len(node_list) == 0:
        for node in cfg.model.nodes():
            if addr in node.instruction_addrs:
                node_list.append(node)
    return node_list


def get_most_external_loop_header_from_addr(addr):
    """
    Gets the most external loop header address.
    If addr is outside loops, returns 0.
    If addr is inside different external loops (function called in them), return 0
    Required for multiple locking points based on external loops.
    """
    global_external_loop_addr = None
    for node in get_all_nodes_containing_addr(addr):
        external_loop_addr = get_most_external_loop_header_from_node(node)
        if global_external_loop_addr is None:
            global_external_loop_addr = external_loop_addr
        elif global_external_loop_addr != external_loop_addr:
            # multiple parent loops: function called from loops
            return 0
    return global_external_loop_addr

def get_most_external_loop_header_from_node(node):
    """
    Gets the most external loop header address.
    If node is outside loops, returns 0.
    If node is inside different external loops (function called in them), return 0
    Required for multiple locking points based on external loops.
    """
    if node in enclosing_loop_header:
        # node in loop
        global_parent_loop_addr = None
        loop_header = enclosing_loop_header[node]
        if loop_header is None:
            return 0
        else:
            for parent in loop_header.predecessors:
                if not isBackEdge(loop_header, parent):
                    parent_loop_addr = get_most_external_loop_header_from_node(parent)
                    if parent_loop_addr == 0: # already at the most external loop
                        parent_loop_addr = loop_header.addr
                    if global_parent_loop_addr is None:
                        global_parent_loop_addr = parent_loop_addr
                    elif global_parent_loop_addr != parent_loop_addr:
                        # multiple parent loops: function called from loops
                        return 0
            return global_parent_loop_addr
    else:
        return 0


def set_drp_improvement_str(ref_id):
    """
    Sets a string for an estimation of improvement of DRPs.
    Improvements consider in respect of all access having MEMORY_COST
    """
    first_gr = reference[ref_id].first_group_reuse
    if first_gr is not None:
        pc = reference[first_gr].pc
    else:
        pc = reference[ref_id].pc
    if pc not in impr_str:
        # cost of preloading AC (negative improvement)
        impr_str[pc] = "-IMPR_0x%x = %d" % (pc, MEMORY_COST)

    if first_gr is not None:
        # always hit
        # DATA_HIT_COST instead of DATA_MISS_COST for all accesses
        impr_str[pc] += " + %d %s" % (DATA_HIT_COST-DATA_MISS_COST, ref_name("MM", ref_id))
    else:
        if is_nonlinear(reference[ref_id]):
            # DATA_MISS_COST instead of DATA_MISS_COST for all accesses
            #impr_str[pc] += " + %d %s" % (DATA_MISS_COST-MEMORY_COST, ref_name("MM", ref_id))
            if not reference[ref_id].is_load or not all_loads_acdc(ref_id):
                # writebacks: as many as misses
                impr_str[pc] += " + %d %s" % (MEMORY_COST, ref_name("MM", ref_id))
        else:
            node = cfg_node_ids[reference[ref_id].cfg_node_id]
            if is_constant(reference[ref_id]):
                # constant in loop with self-reuse
                # no writebacks; never will be replaced
                # ponemos todo a hits menos uno, que dejamos en miss
                impr_str[pc] += " + %d %s + %d" % (DATA_HIT_COST-DATA_MISS_COST, ref_name("MM", ref_id), DATA_MISS_COST-DATA_HIT_COST)
            elif is_array(reference[ref_id]):
                acdc_misses = calculate_misses_in_enclosing_loop(node, reference[ref_id])
                has_writebacks = not reference[ref_id].is_load or not all_loads_acdc(ref_id)
                if node in enclosing_loop_header:
                    loop_header_node = enclosing_loop_header[node]
                    # ponemos todo a hits
                    impr_str[pc] += " + %d %s" % (DATA_HIT_COST-DATA_MISS_COST, ref_name("MM", ref_id))
                    for parent in loop_header_node.predecessors:
                        if not isBackEdge(node, parent):
                            # acdc_misses por entrada en bucle
                            impr_str[pc] += " + %d %s" % ((DATA_MISS_COST-DATA_HIT_COST)*acdc_misses, edge_name("D", parent, node))
                            if has_writebacks:
                                impr_str[pc] += " + %d %s" % (MEMORY_COST*acdc_misses, edge_name("D", parent, node))
            else:
                print("unknown ref type", file = sys.stderr)
                sys.exit(-1)

def set_drp_improvement_str_old(ref_id):
    """
    Sets a string for an estimation of improvement of DRPs.
    Improvements consider in respect of all access having MEMORY_COST
    """
    first_gr = reference[ref_id].first_group_reuse
    if first_gr is not None:
        pc = reference[first_gr].pc
    else:
        pc = reference[ref_id].pc
    if pc not in impr_str:
        # cost of preloading AC (negative improvement)
        impr_str[pc] = "IMPR_0x%x = -%d" % (pc, MEMORY_COST)

    if first_gr is not None:
        # always hit
        # DATA_HIT_COST instead of MEMORY_COST for all accesses
        impr_str[pc] += " + %d %s" % (MEMORY_COST-DATA_HIT_COST, ref_name("MM", ref_id))
    else:
        if is_constant(reference[ref_id]):
            # constant in loop with self-reuse
            # DATA_HIT_COST instead of MEMORY_COST for all accesses
            # minus the first one, which has a DATA_HIT_COST penalty.
            # (MC-DH)(n-1) - DH = (MC-DH)(n-1) + (MC-DH)-MC =
            # (MC-DH)n - MC
            impr_str[pc] += " + %d %s - %d" % (MEMORY_COST-DATA_HIT_COST, ref_name("MM", ref_id), MEMORY_COST)
            # no writebacks; never will be replaced
        elif is_array(reference[ref_id]):
            node = cfg_node_ids[reference[ref_id].cfg_node_id]
            acdc_misses = calculate_misses_in_enclosing_loop(node, reference[ref_id])
#            if is_precise_array(reference[ref_id]):
#                acdc_misses = calculate_misses(node, reference[ref_id].term, reference[ref_id].c)
#            elif is_imprecise_array(reference[ref_id]):
#                acdc_misses = calculate_misses(node, reference[ref_id].term, None)
            impr_str[pc] += " + %d %s - %d" % (MEMORY_COST-DATA_HIT_COST, ref_name("MM", ref_id), DATA_HIT_COST * acdc_misses)
            if not reference[ref_id].is_load or not all_loads_acdc(ref_id):
                # writebacks
                impr_str[pc] += " - %d" % (acdc_misses * MEMORY_COST)
        elif is_nonlinear(reference[ref_id]):
            # DATA_MISS_COST instead of MEMORY_COST for all accesses
            impr_str[pc] += " - %d %s" % (DATA_HIT_COST, ref_name("MM", ref_id))
            if not reference[ref_id].is_load or not all_loads_acdc(ref_id):
                # writebacks: as many as misses
                impr_str[pc] += " - %d %s" % (MEMORY_COST, ref_name("MM", ref_id))
        else:
            print("unknown ref type", file = sys.stderr)
            sys.exit(-1)



#    if is_constant(reference[ref_id]):
#        if first_gr is not None:
#            impr_str[first_gr] += " + %d %s" % (DATA_MISS_COST, ref_name("MM", ref_id))
#        else:
#            impr_str[ref_id] += " + %d %s - 1" % (DATA_MISS_COST, ref_name("MM", ref_id))
#
#    elif is_array(reference[ref_id]):
#        if first_gr is not None:
#            impr_str[first_gr] += " + %d %s" % (DATA_MISS_COST, ref_name("MM", ref_id))
#        else:
#            node = cfg_node_ids[reference[ref_id].cfg_node_id]
#            if is_precise_array(reference[ref_id]):
#                acdc_misses = calculate_misses(node, reference[ref_id].term, reference[ref_id].c)
#            elif is_imprecise_array(reference[ref_id]):
#                acdc_misses = calculate_misses(node, reference[ref_id].term, None)
#            impr_str[ref_id] += " + %s - %d" % (ref_name("MM", ref_id), acdc_misses)
#            if not all_loads_acdc(first_gr):
#                # writebacks
#                impr_str[ref_id] += " + %s - %d" % (ref_name("MM", ref_id), acdc_misses * (MEMORY_COST / DATA_MISS_COST))
#
#    elif is_nonlinear(reference[ref_id]): # always miss
#        if first_gr is not None:
#            impr_str[first_gr] += ref_name(" + MM", ref_id)
#    else:
#        print("unknown ref type")
#        sys.exit(-1)

def calculate_misses_in_enclosing_loop(node, ref):

    if node not in enclosing_loop_header:
        print("Node", node, "in unfeasible path", file = sys.stderr)
        return 0
    # get number of executions of this node
#    if max_times[node] == 0:
#        # unfeasible path
#        # print("calculate_misses WARNING: loop 0x%x has no iterations" % (loop_addr))
#        return 0

    loop_addr = enclosing_loop_header[node].addr
    loop_iterations = flowfacts.maxLoopIter[loop_addr]

    # get stride for this loop
    access_stride = 0
    for (addr, stride) in ref.term:
        #print("%d i0x%x" % (stride, addr))
        if loop_addr == addr:
            access_stride = stride
            break

    # calculate misses just considering this loop
    if access_stride:
        # stride found
        if is_precise_array(ref) and len(ref.term) == 1:
            # non-nested loop with known base address
            loop_misses = calculate_sequential_misses(loop_iterations, access_stride, ref.c)
        else:
            # array with unknown base address or nested loop
            loop_misses = calculate_sequential_misses(loop_iterations, access_stride, None)
    else:
        # no stride found for this loop: temporal reuse
        # the same array position is accessed in each iteration
#        print("calculate_misses: temporal (not spatial) reuse at loop 0x%x" % loop_addr)
#        print(node)
#        for term in terms:
#            print("%d i0x%x" % (term[1], term[0]))
        loop_misses = 1

    return loop_misses


def calculate_misses(node, terms, base):
    # get number of iterations in loop
    #loop_addr = get_loop_addr(node)
    if node not in enclosing_loop_header:
        print("Node", node, "in unfeasible path", file = sys.stderr)
        return 0
    loop_addr = enclosing_loop_header[node].addr
    loop_iterations = flowfacts.maxLoopIter[loop_addr]

    # get number of executions of this node
    if max_times[node] == 0:
        # unfeasible path
        # print("calculate_misses WARNING: loop 0x%x has no iterations" % (loop_addr))
        # outer_loop_iterations = 0
        return 0

    # calculate stride for this loop
    access_stride = 0
    for (addr, stride) in terms:
        #print("%d i0x%x" % (stride, addr))
        if loop_addr == addr:
            access_stride = stride
            break
    # calculate misses just considering this loop depending on stride
    if access_stride:
        # stride found
        loop_misses = calculate_sequential_misses(loop_iterations, access_stride, base)
    else:
        # no stride found for this loop;
        # the same array position is accessed in each iteration
        print("calculate_misses: temporal (not spatial) reuse at loop 0x%x" % loop_addr, file = sys.stderr)
        #print(node)
        for term in terms:
            print("%d i0x%x" % (term[1], term[0]), file = sys.stderr)
        #print(terms)
        loop_misses = 1

    return loop_misses




def old_calculate_misses(node, terms, base):
    """ Calculates safe number of misses considering nested loops """
    """
        TODO: Depending on the loop pattern, iterations reported in the
        source code may be one less than the actual number of executions
        of a basic block. Studying patterns and adding +1 is not implemented.
        See DOI: 10.1371/journal.pone.0229980
    """

    # get number of iterations in loop
    #loop_addr = get_loop_addr(node)
    if node not in enclosing_loop_header:
        print("Node", node, "in unfeasible path", file = sys.stderr)
        return 0
    loop_addr = enclosing_loop_header[node].addr
    loop_iterations = flowfacts.maxLoopIter[loop_addr]

    # get number of executions of this node
    if max_times[node] == 0:
        # unfeasible path
        # print("calculate_misses WARNING: loop 0x%x has no iterations" % (loop_addr))
        # outer_loop_iterations = 0
        return 0
    elif max_times[node] == loop_iterations:
        # no nesting: simple computation of misses
        outer_loop_iterations = 1
    else:
        # nested loop: look for outside loop
        outer_loop_iterations = int(max_times[node] / loop_iterations)
        if outer_loop_iterations != (max_times[node] / loop_iterations):
            print("calculate_misses ERROR: loop 0x%x iterates a non integer times." % loop_addr, file = sys.stderr)
            sys.exit(-1)

    # calculate stride for this loop
    access_stride = 0
    for (addr, stride) in terms:
        #print("%d i0x%x" % (stride, addr))
        if loop_addr == addr:
            access_stride = stride
            break
    # calculate misses just considering this loop depending on stride
    if access_stride:
        # stride found
        loop_misses = calculate_sequential_misses(loop_iterations, access_stride, base)
    else:
        # no stride found for this loop;
        # the same array position is accessed in each iteration
        print("calculate_misses: temporal (not spatial) reuse at loop 0x%x" % loop_addr, file = sys.stderr)
        #print(node)
        for term in terms:
            print("%d i0x%x" % (term[1], term[0]), file = sys.stderr)
        #print(terms)
        loop_misses = 1

    total_misses = loop_misses * outer_loop_iterations
    return total_misses

def calculate_sequential_misses(times, stride, base):
    """ Calculates maximum misses in the sequence """
    elems_in_line = CACHE_LINE_SIZE / abs(stride)
    if elems_in_line <= 1:
        misses = times # always miss
    else:
        if base:
            if stride > 0:
                first_line_elems = (CACHE_LINE_SIZE - (base % CACHE_LINE_SIZE)) / stride
            else:
                first_line_elems = (base % CACHE_LINE_SIZE) / (-stride)
        else:
            first_line_elems = 1
        min_touched_lines = 1 + ceil((times-first_line_elems) / elems_in_line)
        misses = min_touched_lines
    return misses

# TODO: calculate misses for a ref just in its enclosing loop
#def calculate_misses_in_loop(ref_id):



def get_refs_in_node(node):
    """
    Returns the list of memory references (from XML) in the specified node.
    For XML generated by CFGEmulated (virtual instances), node ids should coincide.
    For XML generated by CFGFast (no virtual instances), node address is used.
    """
    if xmlparse.get_cfg_type() in ('emulated', None):
        # XML with CFGEmulated
        refs_in_node = xmlparse.get_references_by_node()
        if cfg_nodes[node] in refs_in_node:
            return refs_in_node[cfg_nodes[node]]
        else:
            return []
    elif xmlparse.get_cfg_type() == 'fast':
        # XML with CFGFast
        refs_by_addr = xmlparse.get_references_by_addr()
        refs_in_codeblock = []
        for addr in node.block.instruction_addrs:
            if addr in refs_by_addr:
                refs_in_codeblock += refs_by_addr[addr]
        return refs_in_codeblock
    else:
        print("Unknown CFG type in XML:", xmlparse.get_cfg_type(), file = sys.stderr)
        sys.exit(-1)


###################### LOOP FUNCTIONS

def no_back_edge_successors(cfg_node):
    """ Returns a list of successors not being back edges """
    result = []
    for dest_node in cfg_node.successors:
        is_back_edge = isBackEdge(dest_node, cfg_node)
        if not is_back_edge:
            # discard strange false single-node loop on fmref-O2 at 0x10528
            if dest_node == cfg_node:
                print("no_back_edge_successors warning: discarding a seemingly false single-node loop at 0x%x" % cfg_node.addr, file = sys.stderr)
                is_back_edge = True
        if not is_back_edge:
            result.append(dest_node)
    return result

def isBackEdge(CFGNode, prevNode):
    """ Returns whether prevNode -> CFGNode is a back (continue) edge """
    for loop in loop_info.loops:
        if CFGNode.addr == loop.entry.addr:
            if (prevNode.to_codenode(), CFGNode.to_codenode()) in loop.continue_edges:
                return True
            # case of body basicblock ending with call, and header just in the next address
            node_above = get_any_cfg_node(CFGNode.addr - INSTRUCTION_SIZE)
            if node_above.to_codenode() in loop.body_nodes:
                if len(node_above.successors) == 1 and node_above.successors[0] != CFGNode:
                    called_function = cfg.kb.functions.function(addr=node_above.successors[0].addr)
                    if called_function and prevNode.to_codenode() in set(called_function.nodes):
                        #print("isBackEdge warning: Revise whether node", node_above, "calls a function reaching", prevNode, "and returns to the loop header", CFGNode)
                        return True
    return False

def is_break_edge(pre_node, suc_node):
    """ Returns whether the edge pre->suc is break node """
    for loop in loop_info.loops:
        if pre_node.to_codenode() in loop.body_nodes:
            (internal_nodes, break_nodes, continue_nodes) = classify_successors(pre_node, loop)
            if suc_node in break_nodes:
                return True
    return False

def classify_successors(cfg_node, loop):
    """ classifies, removing continue_edges to other loops and returns from body_nodes """
    internal_nodes = []
    break_nodes = []
    continue_nodes = []
    continue_to_other_loops = []
    #print(cfg_node)
    #print("Successors (and type of jump) of the entry point:", [ jumpkind + " to " + str(node.addr) for node,jumpkind in cfg.get_successors_and_jumpkind(cfg_node) ])
    #for dest_node in cfg_node.successors:
    for dest_node, jumpkind in cfg.model.get_successors_and_jumpkind(cfg_node):
        if dest_node.addr == loop.entry.addr: # continue_edge
            continue_nodes.append(dest_node)
        else: # no continue_edge
            is_break_node = False
            for (src, dst) in loop.break_edges:
                if dest_node.addr == dst.addr:
                    is_break_node = True
                    break
            if is_break_node:
                break_nodes.append(dest_node)
            else: # internal node or return node
                #print(dest_node, jumpkind)
                # discard continue_edges to other loops
                if not isBackEdge(dest_node, cfg_node):
                    # discard returns from body_nodes
                    #if not (cfg_node.to_codenode() in loop.body_nodes and jumpkind == "Ijk_Ret"):
                    if not (cfg_node.to_codenode() in loop.body_nodes and jumpkind == "Ijk_Ret"):
                        #print("-", dest_node, jumpkind)
                        internal_nodes.append(dest_node)
                    else: # return from body_nodes
                        break_nodes.append(dest_node)
                else:
                    continue_to_other_loops.append(dest_node)
    #if len(cfg.model.get_successors_and_jumpkind(cfg_node)) != (len(internal_nodes) + len(break_nodes) + len(continue_nodes)):
    if len(cfg.model.get_successors_and_jumpkind(cfg_node)) != (len(internal_nodes) + len(break_nodes) + len(continue_nodes) + len(continue_to_other_loops)):
        print("classify_successors: error: node lost", file = sys.stderr)
        sys.exit(-1)
    return (internal_nodes, break_nodes, continue_nodes)

def get_any_cfg_node(instaddr):
    """ Gets any node including instaddr """
    while (cfg.model.get_any_node(instaddr) is None) and (instaddr > 0):
        instaddr -= INSTRUCTION_SIZE
    return cfg.model.get_any_node(instaddr)

def recursive_tag_loop(cfg_node, max_execs, loop_nest_list):
    """ Explores CFG and builds ILP model """
    #global times, node_penalty
    #print(hex(cfg_node.addr), maxexecs)
    if cfg_node in enclosing_loop_header:
        # already explored
        return
    else:
        # set enclosing loop
        if len(loop_nest_list):
            enclosing_loop_header[cfg_node] = loop_nest_list[-1]
        else:
            enclosing_loop_header[cfg_node] = None
        max_times[cfg_node] = max_execs
        # explore successors
        successors = no_back_edge_successors(cfg_node)
        #print("No back edge successors of", cfg_node, ":", successors)
        if len(successors) == 0:
            return
        else:
            for child_node in successors:
                if child_node in loop_headers: # entering a loop
                #if child_node.addr in flowfacts.maxLoopIter: # entering a loop
                    if flowfacts.maxLoopIter[child_node.addr]:
                        #recursive_tag_loop(child_node, max_execs * flowfacts.maxLoopIter[child_node.addr], loop_nest_list + [loop_headers[child_node]])
                        recursive_tag_loop(child_node, max_execs * flowfacts.maxLoopIter[child_node.addr], loop_nest_list + [child_node])
                    else:
                        print("explore: reached loop 0x%x (0 iterations)" % child_node.addr, file = sys.stderr)
                elif is_break_edge(cfg_node, child_node):
                    #recursive_tag_loop(child_node, int(max_execs / flowfacts.maxLoopIter[enclosing_loop[cfg_node].entry.addr]), loop_nest_list[:-1])
                    recursive_tag_loop(child_node, int(max_execs / flowfacts.maxLoopIter[enclosing_loop_header[cfg_node].addr]), loop_nest_list[:-1])
                else:
                    recursive_tag_loop(child_node, max_execs, loop_nest_list)
            return


################################ MAIN ################################
def print_usage():
    print("usage:", sys.argv[0], "<instruction-cache-spec> <data-cache-spec> <binaryfile>")
    print("  Instruction cache specification: { ah | am | locked <sets> <ways> | lru <sets> <ways> | nc | ul }")
    print("    ah: always hit (1c penalty per access)")
    print("    am: always miss")
    print("    locked: requires xml with addresses")
    print("    lru: set-associative LRU")
    print("    nc: no cache (mem. latency penalty per access)")
    print("    ul: unlimited size (one miss per cache line at most)")
    print("  Data cache specification: { acdc <ways> | ah | am | constlru <sets> <ways> [NoFH] | dm | fm | lru <sets> <ways> [NoFH] }")
    print("    acdc: ACDC (requires xml with Data Replacement Permissions)")
    print("    ah: always hit (1c penalty per access)")
    print("    am: always miss without writebacks (estimates DRP benefits)")
    print("    constlru: persistence-based LRU (constants only)")
    print("    dm: direct memory / no cache (mem. latency penalty per access)")
    print("    fm: unlimited size (first miss)")
    print("    lru: LRU (disable FirsHit with NoFH if result is infeasible)")
    sys.exit()


def main():
    global INSTRUCTION_CACHE_TYPE, INSTRUCTION_CACHE_SETS, INSTRUCTION_CACHE_WAYS, DATA_CACHE_TYPE, DATA_CACHE_SETS, DATA_CACHE_WAYS, ACDC_DC_LINES, ENABLE_LRU_FIRST_HIT

    num_args = len(sys.argv)
    if num_args < 4:
        print_usage()
    instr_first_arg = 1

    # Instruction cache specification
    instr_cache = sys.argv[instr_first_arg]
    if instr_cache == "lru":
        # lru in instr_cache
        # sets in instr_cache + 1
        # ways in instr_cache + 2
        # data from instr_cache + 3
        if num_args < instr_first_arg + 4:
            print_usage()
        isets = int(sys.argv[instr_first_arg + 1])
        iways = int(sys.argv[instr_first_arg + 2])
        data_first_arg = instr_first_arg + 3
        print("not implemented", file = sys.stderr)
        print_usage()
    elif instr_cache == "locked":
        isets = int(sys.argv[instr_first_arg + 1])
        iways = int(sys.argv[instr_first_arg + 2])
        data_first_arg = instr_first_arg + 3
    elif instr_cache in ("am"):
        isets = 0
        iways = 0
        print("not implemented", file = sys.stderr)
        print_usage()
    elif instr_cache in ('ah', 'nc', "ul"):
        isets = 0
        iways = 0
        data_first_arg = instr_first_arg + 1
    else:
        print_usage()

    # Data cache specification
    data_cache = sys.argv[data_first_arg]
    if data_cache == "acdc":
        # acdc in data_cache
        # ways in data_cache + 1
        # filename in data_cache + 2
        if num_args < data_first_arg + 2:
            print_usage()
        dsets = 1
        dways = int(sys.argv[data_first_arg + 1])
        file_name_arg = data_first_arg + 2
    elif data_cache in ("lru", "constlru"):
        # lru in data_cache
        # sets in data_cache + 1
        # ways in data_cache + 2
        # filename or NoFH in data_cache + 3
        if num_args < data_first_arg + 3:
            print_usage()
        dsets = int(sys.argv[data_first_arg + 1])
        dways = int(sys.argv[data_first_arg + 2])
        if sys.argv[data_first_arg + 3] == "NoFH":
            ENABLE_LRU_FIRST_HIT = True
            file_name_arg = data_first_arg + 4
        else:
            ENABLE_LRU_FIRST_HIT = False
            file_name_arg = data_first_arg + 3
    elif data_cache in ("ah", "am", "dm", "fm"):
        # type in data_cache
        # filename in data_cache + 1
        if num_args < data_first_arg + 1:
            print_usage()
        dsets = 0
        dways = 0
        file_name_arg = data_first_arg + 1
    else:
        print_usage()

    # File name
    if num_args != file_name_arg + 1:
        print_usage()
    file_name = sys.argv[file_name_arg]

    # Initialize
    part_time = time.process_time()
    INSTRUCTION_CACHE_TYPE = instr_cache
    INSTRUCTION_CACHE_SETS = isets
    INSTRUCTION_CACHE_WAYS = iways
    DATA_CACHE_TYPE = data_cache
    DATA_CACHE_SETS = dsets
    DATA_CACHE_WAYS = dways
    ACDC_DC_LINES = dways

    init(file_name, instance_functions=True)
    if INSTRUCTION_CACHE_TYPE == "locked":
        lockedicparse.parse("%s_lockedic_%d_%d.xml" % (file_name, INSTRUCTION_CACHE_SETS, INSTRUCTION_CACHE_WAYS))
    if DATA_CACHE_TYPE == "acdc":
        acdcparse.parse("%s_acdc_%d.xml" % (file_name, ACDC_DC_LINES))
    print("--- Init time:", round(time.process_time() - part_time, 2), "seconds ---", file = sys.stderr)

    # Call model generation
    part_time = time.process_time()
    generate_ipet_model(file_name)
    print("--- ILP model generation time:", round(time.process_time() - part_time, 2), "seconds ---", file = sys.stderr)


if __name__ == "__main__":
    sys.exit(main())
