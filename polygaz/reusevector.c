#include "reusevector.h"
#include <limits.h>

reuseth_vector_t* rtvectorInitMemRef(int base) {
    reuseth_vector_t *vector=malloc(sizeof(reuseth_vector_t));
    vector->coeff=base;
    vector->loopaddr=0;
    vector->tag=VECTOR_END;
    return vector;
}

reuseth_vector_t* rtvectorCopy(reuseth_vector_t *vector) {
    int len=rtvectorGetLen(vector);
    reuseth_vector_t *res=malloc(len*sizeof(reuseth_vector_t));
    for (int i=0; i<len; i++) {
        res[i]=vector[i];
    }
    return res;
}

int rtvectorGetLen(reuseth_vector_t *vector) {
    int elems=0;
    while (vector[elems].tag!=VECTOR_END) {
        elems++;
    }
    elems++;
    return elems;
}

reuseth_vector_t* rtvectorAddUnkLoop(reuseth_vector_t *vector, int loopaddr) {
    int len=rtvectorGetLen(vector);
    reuseth_vector_t *res=malloc((len+1)*sizeof(reuseth_vector_t));
    res[0].coeff=0;
    res[0].loopaddr=loopaddr;
    res[0].tag=VECTOR_UNK_COEF;
    for (int i=0; i<len; i++) {
        res[i+1]=vector[i];
    }
    free(vector);
    return res;
}

reuseth_vector_t* rtvectorAddLoop(reuseth_vector_t *vector, int coef, int loopaddr) {
    int len=rtvectorGetLen(vector);
    reuseth_vector_t *res=malloc((len+1)*sizeof(reuseth_vector_t));
    res[0].coeff=coef;
    res[0].loopaddr=loopaddr;
    res[0].tag=VECTOR_KNOWN_COEF;
    for (int i=0; i<len; i++) {
        if (vector[i].loopaddr!=loopaddr) {
            res[i+1]=vector[i];
        } else {
            printf("rtvectorAddLoop warning: trying to add the already added loop 0x%x\n",loopaddr);
            if (vector[i].coeff!=coef) {
                printf("previously added loop coef was %d and now is %d\n",vector[i].coeff,coef);
                //exit(-1);
            }
            vector[i].coeff=coef;
            free(res);
            return vector;
        }
    }
    free(vector);
    return res;
}

int rtvectorIsBaseKnown(reuseth_vector_t *vector) {
    int len=rtvectorGetLen(vector);
    if (len>1 && vector[len-2].tag!=VECTOR_UNK_BASE) {
        return 1;
    } else if (len==1) {
        return 1;
    } else {
        return 0;
    }
}

reuseth_vector_t* rtvectorAddBase(reuseth_vector_t *vector, int base) {
    int len=rtvectorGetLen(vector);
    vector[len-1].coeff+=base;
    return vector;
}

reuseth_vector_t* rtvectorAddUnkBase(reuseth_vector_t *vector) {
    if (rtvectorIsBaseKnown(vector)) {
        int len=rtvectorGetLen(vector);
        reuseth_vector_t *res=malloc((len+1)*sizeof(reuseth_vector_t));
        for (int i=0; i<(len-1); i++) {
            res[i]=vector[i];
        }
        res[len-1].coeff=0;
        res[len-1].loopaddr=0;
        res[len-1].tag=VECTOR_UNK_BASE;
        res[len]=vector[len-1];
        free(vector);
        return res;
    } else {
        return vector;
    }
}

void rtvectorPrint(reuseth_vector_t *vector) {
    int len=rtvectorGetLen(vector);
    for (int i=0; i<len; i++) {
        if (i!=0 && i!=(len-1)) {
            printf("+ ");
        }
        switch (vector[i].tag) {
            case VECTOR_KNOWN_COEF:
                printf("%d i0x%08x ", vector[i].coeff, vector[i].loopaddr); break;
            case VECTOR_UNK_COEF:
                printf("? i0x%08x ", vector[i].loopaddr); break;
            case VECTOR_UNK_BASE:
                printf("unknown "); break;
            case VECTOR_END:
                if (len==1) {
                    printf("0x%08x", vector[i].coeff);
                } else if (vector[i].coeff) {
                    printf("+ 0x%x", vector[i].coeff);
/*                    if (vector[i].coeff>0) {
                        printf("+ %d", vector[i].coeff);
                    } else {
                        printf("- %d", -vector[i].coeff);
                    }*/
                }
                break;
            default:
                printf("rtvectorPrint error\n");
                exit(-1);
        }
    }
}

int rtvectorEquals(reuseth_vector_t *v1, reuseth_vector_t *v2) {
    int len=rtvectorGetLen(v1);
    if (len!=rtvectorGetLen(v2)) {
        return 0;
    } else { // same length
        for (int i=0; i<len; i++) {
            if (v1[i].coeff!=v2[i].coeff || v1[i].loopaddr!=v2[i].loopaddr || v1[i].tag!=v2[i].tag) {
                return 0;
            }
        }
        return 1;
    }
}

int rtvectorAllKnown(reuseth_vector_t *v) {
    int len=rtvectorGetLen(v);
    for (int i=0; i<len; i++) {
        if (v[i].tag==VECTOR_UNK_COEF || v[i].tag==VECTOR_UNK_BASE)
            return 0;
    }
    return 1;
}

int rtvectorGetBase(reuseth_vector_t *v) {
    int len=rtvectorGetLen(v);
    return v[len-1].coeff;
}

int rtvectorIsConstant(reuseth_vector_t *v) {
    int len=rtvectorGetLen(v);
    if (len==1) {
        return 1;
    } else {
        if (len==2 && v[0].tag==VECTOR_UNK_BASE) {
            return 1;
        } else {
            return 0;
        }
    }
}

int rtvectorGetCoef(reuseth_vector_t *vector, int i) {
    int len=rtvectorGetLen(vector);
    if (i>len) {
        printf("rtvectorGetCoef error: invalid position\n");
        exit(-1);
        //return 0;
    } else {
        if (vector[i].tag==VECTOR_KNOWN_COEF) {
            return vector[i].coeff;
        } else {
            return 0;
            //printf("rtvectorGetCoef error\n");
            //exit(-1);
        }
    }
}

int rtvectorGetLoopAddr(reuseth_vector_t *vector, int i) {
    int len=rtvectorGetLen(vector);
    if (i>len) {
        printf("rtvectorGetLoopAddr error: invalid position\n");
        exit(-1);
        //return 0;
    } else {
        if (vector[i].tag==VECTOR_KNOWN_COEF || vector[i].tag==VECTOR_UNK_COEF) {
            return vector[i].loopaddr;
        } else {
            return 0;
            //printf("rtvectorGetCoef error\n");
            //exit(-1);
        }
    }
}

int rtvectorGetDeepestCoef(reuseth_vector_t *vector) {
    int len=rtvectorGetLen(vector);
    for (int i=len-1; i>=0; i--) {
        if (vector[i].tag==VECTOR_KNOWN_COEF) {
            return vector[i].coeff;
        } else if (vector[i].tag==VECTOR_UNK_COEF){
            return INT_MAX;
        }
    }
    return 0; // no loop coeff
}

int rtvectorGetCoefSum(reuseth_vector_t *vector) {
    int sum=0;
    int len=rtvectorGetLen(vector);
    for (int i=len-1; i>=0; i--) {
        if (vector[i].tag==VECTOR_KNOWN_COEF) {
            sum+=vector[i].coeff;
        } else if (vector[i].tag==VECTOR_UNK_COEF){
            return INT_MAX;
        }
    }
    return sum;
}
