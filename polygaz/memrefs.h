//#include "ap_global1.h"
//#include "pk.h"

#include "reusevector.h"
#include "apron_interface.h"

// comment to disable debug info
//#define DEBUG_ABSTRACT_STATE // prints the abstract state after each operation
//#define DEBUG_EXPRESSION // prints the expression after assignment
//#define DEBUG_CONSTRAINT // prints the constraint after set/used
//#define DEBUG_MEM


//#define MAXREFS 5000 // tamaño del vector de referencias a memoria
#define MAXREFS 50000 // tamaño del vector de referencias a memoria
// CFGFast: all <5000, except test3_O0 <20000
// CFGEmulated: test3_O2,O3 >30000, <50000?
// CFGEmulated: mpeg2_O0 >50000
#define MAXINDVARS 300 // tamaño del vector de variables de inducción
// test3_O1 <300
#define MAXNESTEDLOOPS 10 // max next depth

/**
 * Structure for storing loops and induction variables
 *
 * Each loop (at addr) may have several registers (reg) acting as induction variables.
 * The ind. var. (reg) is incrementet by (step) at each iteration.
 * The loop begins with a value of base at reg.
 * If base is unknown, the base value is probably taken from an ind. var. of an
 * enclosing loop. If so, encladdr holds the address of the enclosing loop, and
 * enclreg holds its corresponding ind. var. register.
 *
 * known step, known base
 * encladdr == 0: initial value is base+known
 * encladdr == -1: initial value is base+unknown
 * encladdr != -1 AND enclreg != -1: loop pair [encladdr,encladdr] must be explored
 *
 * OLD OLD OLD OLD
 * encladdr == -1 AND enclreg == -1: initial value is base+unknown
 * encladdr == -1 AND enclreg != -1: initial value is base
 * encladdr != -1 AND enclreg != -1: loop pair [encladdr,encladdr] must be explored
 * OLD OLD OLD OLD
 */
typedef struct {
    int cfgnodeid; /**< CFG node id */
    int loopid; /**< Loop header CFG node id */
    int addr; /**< Loop address */
    int reg; /**< Register acting as induction variable */
    int step; /**< Increment of induction variable for each iteration */
    int base; /**< Initial value of induction variable (at loop entry) */
    int baseknown; /**< 0 if base is unknown */
    //int encladdr; /**< -1 if base is unknown */
    //int enclreg; /**< No longer used */
    //int enclregconfirmed; /**< No longer used */
    // OLD
    //int encladdr; /**< Address of loop enclosing this (addr) loop */
    //int enclreg; /**< Induction var. of enclosing loop */
    //int enclregconfirmed; /**< enclreg has been assigned to reg */
} loopIndVar_t;

/*
typedef enum {
    KNOWN_STEP_KNOWN_BASE,   // all info known, stop
    KNOWN_STEP_UNKNOWN_BASE, // base depends of enclosing loop, dig further
    UNKNOWN_STEP_KNOWN_BASE, // copied register
    UNKNOWN_STEP_UNKNOWN_BASE,
} loopIndVar_tag_t;
*/

#define MAXNESTDEPTH 100

/**
 * Struct with information about loop induction variables
 *
 * Prepared by setting addr and regnum to specific values.
 */
typedef struct {
    int addr; /**< Memory location of a memory instruction */
    //int addr; /**< Memory address of the deepest loop containing the load/store */
    int upaddr; /**< Memory address of a loop containing the loop at addr */
    int regnum; /**< Register number to test whether it is induction var */
    int step; /**< Increment of the induction variable (i+=step) */
    int base; /**< Initial value of the induction variable */
    //induction_info_t info;
} induction_t;


/**
 * Struct for a memory references
 *
 * For load/store to an immediate address (not relative to any register), such
 * address is stored in 'dest'. Otherwise, regname holds the name of the (temporal)
 * register with the target memory address. Temporal registers are assumed to be
 * written a single time for each basic block, so accesses to a given temporal register
 * (e.g. t11) in the same basic block (bbaddr) are assumed to access the same
 * memory address.
 */
typedef struct memref_t {
    int cfgnodeid; /**< CFG node id */
    int addr; /**< Memory location of this memory instruction */
    int bbaddr; /**< Basic block address */
    int loopaddr;
    int dest; /**< Target memory address (if constant) or 0 */
    char regname[MAX_VAR_CHARS+1]; /**< Register name holding the target address */
    int firsttarget; /**< for nonconstant, first target touched */
    int indvar[MAXINDVARS]; /**< possible induction vars, until -1 */
    int loop[MAXINDVARS]; /**< loop address of indunction var */
    int offset[MAXINDVARS]; /**< offset in respect of ind. var */
    int isload;
    int isconditional; /**< 1 if conditional memory access, 0 if always performed */
    int maxcount; /** maximum number of times that may be executed */
    //int maxtouchedlines; /** maximum number of cache lines touched */
    int bytes; /**< Size of the accessed element (usually 4), in bytes */
    ap_abstract1_t aistate; /**< Abstract state when performing the mem. access */
    reuseth_vector_t *loopcoef;
    struct memref_t *firstGR;
    struct memref_t *lastGR;
} memref_t;


////////////////////////// INDUCTION VARS ///////////////////////////
/*
//void inductionSetResult(int addr, int regnum);
void setNumLoops(int numloops);
void extractIndVars(ap_abstract1_t *aistate, char * regname, int bbaddr);
void prepareInduction(int addr, int reg);
//int getLoopAddr(int index);
//int getLoopReg(int index);
int getInductionPC(int index);
int getInductionReg(int index);
void setInductionLoop(int addr);
//void setShadowReg(int regnum);
void inductionTest(int regnum);
*/

////////////////////////// NEW INDUCTION VARS ///////////////////////////
//void resetNestInfo();
//void addNestInfo(int addr, int reg, int step, int base);
//void addNestInfo(int addr, int reg, int step, int base, int encladdr, int enclreg);
//reuseth_vector_t * genReuseTheoryMemRefs(reuseth_vector_t *rv,ap_abstract1_t *aistate,char* regname);
//reuseth_vector_t * genReuseTheoryMemRefs(reuseth_vector_t *rv,ap_abstract1_t *aistate,int loopaddr, char* regname);


////////////////////////// MEMORY REFERENCES ///////////////////////////
/**
 * Adds/updates current abstract state to array of memory references, for a CONSTANT memory address
 *
 * @param[in] bbaddr addr of deepest loop containing this load/store instruction
 * @param[in] addr PC of current load/store instruction
 * @param[in] dest Referenced memory address
 * @param[in] bytes Number of read/written bytes
 */
//void addMemConstRef(int addr, int dest, int bytes, int isload);
void addMemConstRef(int cfgnodeid, int bbaddr, int addr, int dest, int bytes, int isload, int isconditional);

/**
 * Adds/updates current abstract state to array of memory references, for a memory address hold in a register
 *
 * @param[in] bbaddr addr of deepest loop containing this load/store instruction
 * @param[in] addr PC of current load/store instruction
 * @param[in] registerName Name of the register holding the memory address
 * @param[in] bytes Number of read/written bytes
 */
//void addMemRef(int addr, char* registerName, int bytes, int isload);
//void addMemRef(int bbaddr, int addr, char* registerName, int bytes, int isload, int isconditional);
void addMemRef(int cfgnodeid, int bbaddr, int addr, char* registerName, int ftarget, int bytes, int isload, int isconditional, int loopaddr);

/**
 * adds information for calculating offsets
 *
 * @param[in] addr Address of the memory reference
 * @param[in] loop Address of the loop
 * @param[in] registerName Register holding the target address
 * @param[in] reg Register holding the induction variable
 * @param[in] offset Offset (registerName - reg_at_loop_entry)
 */
void addMemRefOffset(int cfgnodeid, int addr, int loop, char* registerName, int reg, int offset);

///////////////////// ANALYSIS OF INDUCTION VARIABLES //////////////////////
/**
 * Gets the variable (register) associated to register (reg) in the
 * abstract state aistate, and sets its coefficient to the global variable
 * intcoef.
 * Assuming an abstract state (aistate) with constraints such as r8=r12+5,
 * solveVarGetRegister(8) returns 12, and solveVarGetConstant(8) returns 5.
 *
 * @param[in] reg number of register (angr id) to find
 * @return register (angr id) associated to reg, or -1
 */
int solveVarGetRegister(int reg);
/**
 * Returns the global variable intcoef, set by solveVarGetRegister
 */
int solveVarGetConstant(int reg);


///////////////////// PRINTING OF MEMORY REFERENCES //////////////////////
/**
 * Solve the variable regname WITHOUT using induction info and print it
 * @param[in] aistate Abstract state to solve from
 * @param[in] regname Name of the variable to solve
 * @param[in] bbaddr addr of deepest loop containing the load/store instruction
 */
void solveAndPrintVar(ap_abstract1_t *aistate, char * regname, int bbaddr);

/**
 * Prints a memory reference WITHOUT induction variables information
 */
void printMemRef(memref_t * memref); // calls extractIndVars()

/**
 * Prepares the list of possible induction variables from a memory reference
 */
//void analyzeMemRef(memref_t * memref); // calls solveAndPrintVar()

/**
 * Prints list of collected memory references WITHOUT induction variables information
 */
void printAbstractMemRefs(); // calls printMemRef()

/**
 * Prepares the list of possible induction variables from the collected memory references
 */
//void analyzeMemRefs(); // calls analyzeMemRef()


///////////////////// GENERATION OF MEMORY PATTERNS //////////////////////

/**
 * Prints a memory reference WITH induction variables information
 */
void printMemRef(memref_t * memref); // calls extractIndVars()

void resetNestInfo();
//void addNestInfo(int addr, int reg, int step, int base, int encladdr, int enclreg);
//void addNestInfo(int addr, int reg, int step, int base, int encladdr, int enclreg, int enclregconfirmed);
void addNestInfo(int cfgnodeid, int loopid, int addr, int reg, int step, int base, int baseknown);
//int addIndVar(reuseth_vector_t **rv, int addr, int reg, double coeffRegname);
//int addIndVar(reuseth_vector_t **rv, int addr, int reg, double coeffRegname, int basehint);

/**
 * Generates the reuse theory vector for reference i
 * If mem. ref. i is not constant, calls genReuseTheoryMemRefs and updates
 * memrefs[i].loopcoef
 */
void genReuseTheoryMemRef(int i);

/**
 * Calculates the reuse theory vector recursively, from a loop and register name.
 * @param[in,out] rv Initial reuse vector, may be empty with just constant 0
 * @param[in,out] aistate Abstract state to study, its environment may grow
 * @param[in] loopaddr Loop to test for induction variable steps
 * @param[in] regname Register name (induction variable) to study for loopaddr
 */
//reuseth_vector_t * genReuseTheoryMemRefs(reuseth_vector_t *rv,ap_abstract1_t *aistate,int loopaddr, char* regname);
//reuseth_vector_t * genReuseTheoryMemRefs(reuseth_vector_t *rv,ap_abstract1_t *aistate,int loopaddr, char* regname, int basehint);

/**
 * Prints list of collected memory references WITH induction variables information
 */
void printInductionMemRefs(); // calls printInductionMemRef()

///////////////////// REUSE ANALYSIS //////////////////////

/**
 * Returns whether memref i is a predicated/conditioned load/store
 */
int memRefIsConditional(int index);

/**
 * Gets the PC of memref i
 */
int getMemRefPC(int index);
int getMemRefCFGid(int index);

/**
 * Gets the memref index of the first data access that memref i is reusing.
 * Such memref should be preloaded and fixed in AC (ACDC) for maximum hits.
 * Currently not implemented.
 */
int getIndexMemRefFirst(int i);

/**
 * Gets the memref index of the last data access that memref i is reusing.
 * Such memref should be tested for persistence in LRU to predict hits
 */
int getIndexMemRefLast(int i);

/**
 * Returns whether memref i1 occurs before i1 in a multiregister load/store
 */
int orderedMemRefs(int i1, int i2);

/**
 * Returns the base address of memref i
 */
int getMemRefBase(int i);

/**
 * Gets the PC of the last data access that memref i is reusing.
 * Its accessed data should be tested for persistence in LRU to predict hits
 */
int getLastGR(int i);

/**
 * Gets the PC of the first data access that memref i is reusing.
 * Such PC should be preloaded and fixed in AC (ACDC) for maximum hits.
 * Currently not implemented.
 */
int getFirstGR(int i);

/**
 * Sets the index (i2) of the last data access that memref i1 is reusing.
 * Its accessed data should be tested for persistence in LRU to predict hits
 */
void setLastGR(int i1, int i2);

/**
 * Sets the index (i2) of the first data access that memref i1 is reusing.
 * Its PC should be preloaded and fixed in AC (ACDC) for maximum hits.
 */
void setFirstGR(int i1, int i2);

/**
 * Returns whether memref i1 and i2 have group reuse assuming certain cache linesize
 */
int haveGroupReuse(int index1, int index2, int linesize);


/**
 * Generates a XML file with information on data reuse
 */
void printXMLinfo(char* filename, char* cfgtype);
