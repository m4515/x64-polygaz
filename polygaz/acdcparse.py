
# coding=utf-8

"""
Module for reading and parsing a dynamic locking configuration .xml file
for an ACDC.
PCs at load point at pc=0 (static) are loaded when the program starts and
mantained during the whole execution.
PCs at other load points (dynamic) are loaded one at a time, at the specified
point, overwriting the previous one (if any) and flushing its data contents.
IMPORTANT: WCET analysis assumes that data used by loads/stores at dynamic
are not used before the loading point nor after a dynamic ACDC update. Data
used in several dynamic points should be associated to pc 0 (static).

Example of xml file:
<?xml version="1.0"?>
<acdc_config>
  <loadpoint id="0" pc="0x0">
    <pc>0x102e0</pc>
    <pc>0x10eac</pc>
  </loadpoint>
  <loadpoint id="1" pc="0x107b8">
    <pc>0x107dc</pc>
    <pc>0x10b00</pc>
    <pc>0x10bd8</pc>
    <pc>0x10c44</pc>
    <pc>0x10d80</pc>
    <pc>0x10d8c</pc>
  </loadpoint>
  <loadpoint id="2" pc="0x11008">
    <pc>0x10fac</pc>
    <pc>0x11060</pc>
    <pc>0x11098</pc>
    <pc>0x110bc</pc>
    <pc>0x110f0</pc>
    <pc>0x11128</pc>
  </loadpoint>
</acdc_config>
"""

import xml.etree.ElementTree as ET
import sys

drp_load_points = {} # list of load points (PCs)
drp_set = set() # Set of all DRPs

def parse(filename):
    """ Parsing function """
    tree = ET.parse(filename)
    root = tree.getroot()
    for load_point in root.findall('loadpoint'):
        load_pc = int(load_point.attrib["pc"], 0)
        drp_load_points[load_pc] = set() # set of DRPs in this point
        for line in load_point.findall('pc'):
            #print("  %s" % line.text)
            if int(line.text, 0) == 0: # PC is 0x0
                print("Discarding load of PC %d at 0x%x" % (int(line.text, 0), load_pc), file = sys.stderr)
            else:
                drp_load_points[load_pc].add(int(line.text, 0))
        if len(drp_load_points[load_pc]): # There are PCs to load
            drp_set.update(drp_load_points[load_pc]) # union
            print("ACDC DRPs to load at 0x%x (%d):" % (load_pc, len(drp_set)), drp_load_points[load_pc], file = sys.stderr)
        else: # No PCs to load
            print("Discarding ACDC load at 0x%x (no PCs to load)" % (load_pc), file = sys.stderr)
            drp_load_points.pop(load_pc)
    print("PCs with DRP in ACDC (%d): %s" % (len(drp_set), drp_set), file = sys.stderr)


def has_drp(pc):
    """ Returns whether PC has DRP """
    return bool(pc in drp_set)

def drps():
    """ Returns all DRPs """
    return list(drp_set)

def load_points():
    """ Returns the list of DRP loading points """
    return drp_load_points.keys()

def main():
    """ Simply parses, for testing purposes """
    if len(sys.argv) != 2:
        print("usage:", sys.argv[0], "<acdc_config.xml>", file = sys.stderr)
        sys.exit()
    filename = sys.argv[1]

    parse(filename)

if __name__ == "__main__":
    sys.exit(main())
