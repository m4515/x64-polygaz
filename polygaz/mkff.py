#!angr/bin/python3
# coding=utf-8

import sys
import angr # grafo
import re

INSTRUCTION_SIZE = 1 # bytes

def no_back_edge_successors(cfg_node):
    """ Returns a list of successors not being back edges """
    result = []
    for dest_node in cfg_node.successors:
        is_back_edge = isBackEdge(dest_node, cfg_node)
        if not is_back_edge:
            # discard strange false single-node loop on fmref-O2 at 0x10528
            if dest_node == cfg_node:
                print("no_back_edge_successors warning: discarding a seemingly false single-node loop at 0x%x" % cfg_node.addr)
                is_back_edge = True
        if not is_back_edge:
            result.append(dest_node)
    return result

def get_any_cfg_node(instaddr):
    """ Gets any node including instaddr """
    while (cfg.model.get_any_node(instaddr) is None) and (instaddr > 0):
        instaddr -= INSTRUCTION_SIZE
    return cfg.model.get_any_node(instaddr)

def isBackEdge(CFGNode, prevNode):
    """ Returns whether prevNode -> CFGNode is a back (continue) edge """
    for loop in loop_info.loops:
        if CFGNode.addr == loop.entry.addr:
            if (prevNode.to_codenode(), CFGNode.to_codenode()) in loop.continue_edges:
                return True
            # case of body basicblock ending with call, and header just in the next address
            node_above = get_any_cfg_node(CFGNode.addr - INSTRUCTION_SIZE)
            if node_above.to_codenode() in loop.body_nodes:
                if len(node_above.successors) == 1 and node_above.successors[0] != CFGNode:
                    called_function = cfg.kb.functions.function(addr=node_above.successors[0].addr)
                    if called_function and prevNode.to_codenode() in set(called_function.nodes):
                        #print("isBackEdge warning: Revise whether node", node_above, "calls a function reaching", prevNode, "and returns to the loop header", CFGNode)
                        return True
    return False

def test_reachability(cfg_node):
    if cfg_node in reachable:
        # already explored
        return
    else:
        reachable.add(cfg_node)
        # explore successors
        successors = no_back_edge_successors(cfg_node)
        if len(successors) == 0:
            return
        else:
            for child_node in successors:
                test_reachability(child_node)
            return

def test_recursion():
    for func_addr in cfg.kb.functions:
        func = cfg.kb.functions[func_addr]
        for call in list(func.get_call_sites()):
            calledf = func.get_call_target(call)
            if func == cfg.kb.functions[calledf]:
                return func
    return None

def get_function_from2(addr):
    for func_addr in cfg.kb.functions:
        func = cfg.kb.functions[func_addr]
        if addr in func.block_addrs_set:
            return func
    return None

def get_function_from(addr, start):
    """
    Gets any function containing addr (basicblock first addr),
    reachable from function start
    BUG IN ANGR?: DOES NOT DETECT CALL IF IT IS THE LAST INSTRUCTION
    Happens in st_O2_arm
    """
    if addr in start.block_addrs:
        return start
    else:
        for call in list(start.get_call_sites()):
            calledf = start.get_call_target(call)
            if start != cfg.kb.functions[calledf]:
                print("test for 0x%x in" % addr, cfg.kb.functions[calledf])
                function = get_function_from(addr, cfg.kb.functions[calledf])
                if function:
                    return function
            else:
                print("recursive call (not supported) detected:", start.name)
                sys.exit(-1)
        return None

## Prints flowfacts, with a format similat to otawa .ff
def print_ff(filename, start):
    prevfunction = None
    parent = {}
    with open(filename, "w") as f:
        #prevloop = None
        for loop in loop_info.loops:
#            function = get_function_from(loop.entry.addr, start)
#            if function:

            loop_reachable = False
            for node in cfg.model.get_all_nodes(loop.entry.addr):
                if node in reachable:
                    loop_reachable = True
                    break
            if loop_reachable:
                function = get_function_from2(loop.entry.addr)

                #print("loop at", hex(loop.entry.addr), "in", function.name, "is reachable")
                # Separate functions with newline
                if function != prevfunction:
                    print(file=f)
                    print("// Function", function.name, file=f)
                    prevfunction = function
                # get calls info
                if loop.has_calls:
                    callstr = ", has calls"
                else:
                    callstr = ", no calls"
                # get subloops info
                if loop.subloops:
                    subloopstr = ", subloops:"
                    for subloop in loop.subloops:
                        subloopstr += " " + hex(subloop.entry.addr)
                        parent[subloop.entry.addr] = loop.entry.addr
                else:
                    subloopstr = ", no subloops"
                # set nesting spaces (assumes parents are listed above)
                nesting = ""
                thisloop = loop.entry.addr
                while thisloop in parent:
                    nesting += "    "
                    thisloop = parent[thisloop]
                print(nesting + "loop \"" + function.name + "\" +", hex(loop.entry.addr - function.addr), "?; //", hex(loop.entry.addr) + callstr + subloopstr, file=f)
            else:
                print(file=f)
                print("loop \"nonreachable\" + 0x0 0; //", hex(loop.entry.addr), file=f)
                print("loop at", hex(loop.entry.addr), "not reachable from", start.name)


## Main
def main():
    global cfg, loop_info, reachable

    if len(sys.argv) == 1:
        print("usage:", sys.argv[0], "<binaryfile>")
        sys.exit()
    filename = sys.argv[1]

    # Set max stack depth (default: 1000)
    sys.setrecursionlimit(20000)
    #print("Max stack depth:", sys.getrecursionlimit())

    name = re.match(".+\.elf$", sys.argv[1])
    if name:
        #print("match")
        #print(name[0])
        ffname = filename[:-3] + "ff"
    else:
        #print("not match")
        ffname = filename + ".ff"

    # Set-up project from binary file
    proj = angr.Project(sys.argv[1], auto_load_libs=False) # carga ejecutable
    main = proj.loader.main_object.get_symbol("main")
    state = proj.factory.blank_state(addr=main.rebased_addr)

    # Build CFG and loopInfo
    #cfg = proj.analyses.CFG(normalize=True) # genera CFG (CFGFast)
    cfg = proj.analyses.CFGEmulated(normalize=True, context_sensitivity_level=3, starts=[main.rebased_addr], initial_state=state) # genera CFG
    #cfg = proj.analyses.CFGEmulated(normalize=True,
    #                                context_sensitivity_level=3,
    #                                starts=[main.rebased_addr],
    #                                initial_state=state) # genera CFG
    #cfg.normalize() # genera correctamente bloques básicos, si no hay bloques
    print("CFG ok")

    loop_info = proj.analyses.LoopFinder() # Set-up loop information

    fmain = cfg.kb.functions.function(name="main") # main function
    main_node = cfg.model.get_any_node(fmain.addr) # main CFG node
    print("Loops ok")
    func = test_recursion()
    if func:
        print("recursive call (not supported) detected:", func.name)
        sys.exit(-1)
    print("Recursion test ok")
    reachable = set()
    test_reachability(main_node)
    print("Reachability test ok")

    print_ff(ffname, fmain)

if __name__ == "__main__":
    sys.exit(main())
