#include "apron_interface.h"


/*
-------------------- EJEMPLO DE CODIGO apron_interface_test.c ---------------------
gcc -Wall -o apron_interface_test apron_interface.c apron_interface_test.c -lapron -lpolkaMPQ
-----------------------------------------------------------------------------------
#include "apron_interface.h"

// global vars
ap_manager_t* man; // manager
ap_environment_t* env; // environment
ap_linexpr1_t expr; // global var
ap_abstract1_t aistate; // global aistate

nodestates_t * nodestates;

int main() {

    init(10); // 10 nodes
    fprintf(stdout,"init ok\n");

    initExpr();
    addConstToExpr(11);
    assignExpr("r10"); // añade "r10=11"

    initExpr();
    addTopToExpr();
    assignExpr("r10");

    initExpr();
    addRegToExpr(1,"r12");
    //addConstToArray(14);
    // r12>=14
    ap_lincons1_t cons = ap_lincons1_make(AP_CONS_SUPEQ,&expr,NULL);
    addConstToExpr(14);

    ap_lincons1_fprint(stdout,&cons);
}
*/


/**
 * Global manager. All  functions use this manager.
 */
ap_manager_t* man;

/**
 * Global environment WITHOUT memory variables, only regs,temps,shadow
 * Environment is saved and restored it setOutState(), setDefaultState()
 *
 * All functions use this environment.
 */
ap_environment_t* env; // environment

/**
 * Global expression. All functions to operate on expression use this.
 * NO MORE THAN 1 EXPRESSION CAN BE USED AT THE SAME TIME
 */
ap_linexpr1_t expr;

/**
 * Global abstract state.
 *
 * It maintains the current working state and is always valid.
 * It is REPLICATED to node.OUT in setOutState().
 * It is freed and re-set (REPLICATED) in setDefaultState()
 * All functions to operate on states use this.
 * Join and Widening use this and another state.
 * NO MORE THAN 1 STATE CAN BE USED AT THE SAME TIME
 */
ap_abstract1_t aistate;

ap_lincons1_array_t array; // array for the widen threshold

/**
 * Global constraint
 *
 * It is set when a comparison is processed. It must not be freed.
 * It is copied (NOT REPLICATED) in setOutState().
 */
ap_lincons1_t cons;

/**
 * Array of node states
 */
nodestates_t * nodestates;
unsigned int nextemptynode=0;
unsigned int MAX_NODE_STATES;

void init(int numcfgnodes, int domain) {
    if (domain==DOMAIN_PK)
        man = pk_manager_alloc(true); // polyhedron pk
    else if (domain==DOMAIN_OCT)
        man = oct_manager_alloc(); // octagon oct
//    else if (domain==DOMAIN_AP_PPL)
//        man = ap_ppl_poly_manager_alloc(true); // polyhedron ap_ppl
    else if (domain==DOMAIN_PKEQ)
        man = pkeq_manager_alloc(); // polyhedron pkeq
    else {
        printf("Domain not defined.\n");
        exit(-1);
    }

    printf("******************************\n");
    //printf("apron_interface init\n");
#ifdef POLYGAZ_VERSION
    printf("Polygaz version %s\n", POLYGAZ_VERSION);
#endif
    printf("Apron library %s, version %s\n",man->library,man->version);
    printf("%d CFG nodes\n", numcfgnodes/2);
    printf("******************************\n");

    // set variable names
    //ap_var_t name_of_dim[MAX_REGS + MAX_SHADOW_REGS + MAX_TEMP_REGS];
    //ap_var_t name_of_dim[MAX_REGS + MAX_REGS + MAX_TEMP_REGS];
    /*
    ap_var_t name_of_dim[MAX_REGS];
    char *varname;

    int varpos=0;
    for (int numreg=0; numreg<MAX_REGS; numreg++) {
        varname=malloc((MAX_VAR_CHARS+1)*sizeof(char));
        // ARM r1 --> angr r12; ARM r2 --> angr r20
        // (r)12 = ((r)1+2)*4; (r)20 = ((r)3+2)*4
        //sprintf(varname,"r%d",numreg);
        sprintf(varname,"r%d",(numreg+2)*4);
        name_of_dim[varpos]=varname;
        varpos++;
    }
    for (int numreg=0; numreg<MAX_REGS; numreg++) {
        varname=malloc((MAX_VAR_CHARS+1)*sizeof(char));
        //sprintf(varname,"s%d",numreg);
        sprintf(varname,"s%d",(numreg+2)*4);
        name_of_dim[varpos]=varname;
        varpos++;
    }
    for (int numreg=0; numreg<MAX_TEMP_REGS; numreg++) {
        varname=malloc((MAX_VAR_CHARS+1)*sizeof(char));
        sprintf(varname,"t%d",numreg);
        name_of_dim[varpos]=varname;
        varpos++;
    }
    */
    //TODO Free variable names

    // set environment
    //env = ap_environment_alloc(name_of_dim,MAX_REGS+MAX_SHADOW_REGS+MAX_TEMP_REGS,NULL,0);
    //env = ap_environment_alloc(name_of_dim,MAX_REGS+MAX_REGS+MAX_TEMP_REGS,NULL,0);
    //env = ap_environment_alloc(name_of_dim,MAX_REGS,NULL,0);
    env = ap_environment_alloc(NULL,0,NULL,0);
    //ap_environment_fdump(stdout,env);

    // set initial abstract interpretation state
    aistate=ap_abstract1_top(man,env);
    // global aistate now holds the initial (empty) state
    // all operations are performed on (global) aistate
#ifdef DEBUG_ABSTRACT_STATE
    ap_abstract1_fprint(stdout,man,&aistate);
#endif

    // init array of nodestates
    nodestates=malloc(numcfgnodes*sizeof(nodestates_t));
    MAX_NODE_STATES=numcfgnodes;

//    induction=calloc(2*numloops,sizeof(induction_t));
    //cons = ap_lincons1_make(AP_CONS_SUPEQ, ap_linexpr1_alloc(env,AP_LINEXPR_SPARSE,2), NULL);
    //ap_lincons1_clear(&cons);
}


////////////////////////// CONSTRAINTS ///////////////////////////
void setConsEQ() {
    //ap_lincons1_clear(&cons);
    cons = ap_lincons1_make(AP_CONS_EQ,&expr,NULL);
    //consneg = ap_lincons1_make(AP_CONS_DISEQ,&expr,NULL);
}

void setConsSUPEQ() {
    //ap_lincons1_clear(&cons);
    cons = ap_lincons1_make(AP_CONS_SUPEQ,&expr,NULL);
    //consneg = ap_lincons1_make(AP_CONS_SUP,&expr,NULL);
}

void setConsSUP() {
    //ap_lincons1_clear(&cons);
    cons = ap_lincons1_make(AP_CONS_SUP,&expr,NULL);
    //consneg = ap_lincons1_make(AP_CONS_SUPEQ,&expr,NULL);
}

void setConsDISEQ() {
    //ap_lincons1_clear(&cons);
    cons = ap_lincons1_make(AP_CONS_DISEQ,&expr,NULL);
    //consneg = ap_lincons1_make(AP_CONS_EQ,&expr,NULL);
}

// completeCons required to preserve expr
// After completing a constraint, other expressions may be set before this
// constraint (which contains expressions) is assigned.
void completeCons() {
    ap_lincons1_t aux;
    aux=cons;
    cons = ap_lincons1_copy(&aux);
#ifdef DEBUG_CONSTRAINT
    ap_lincons1_fprint(stdout,&cons);
#endif
}

void discardCons() {
    ap_lincons1_clear(&cons);
}


////////////////////////// ENVIRONMENT ///////////////////////////
int varExists(char* var) {
    if (ap_environment_dim_of_var(env,var)!=AP_DIM_MAX) {
        return 1;
    } else {
        return 0;
    }
}

void varAdd(char* var) {
    ap_environment_t *new;
    new=ap_environment_add(env,(void**)&var,1,NULL,0);
    if (new!=NULL) {
        //printf("Added variable %s\n",var);
        //ap_environment_fdump(stdout,env);
        aistate=ap_abstract1_change_environment(man,true,&aistate,new,false);
        env=new;
        if (env->intdim %100==0) {
            printf("%zd variables reached\n",env->intdim);
        }
    } else {
        printf("varAdd: error adding variable %s\n",var);
    }
}

////////////////////////// EXPRESSIONS ///////////////////////////
void initExpr() {
    ap_linexpr1_clear(&expr);
    expr = ap_linexpr1_make(env,AP_LINEXPR_SPARSE,0);
}

void addRegToExpr(int coef, char* registerName) {
    if (!varExists(registerName)) {
        varAdd(registerName);
        ap_linexpr1_extend_environment_with(&expr,env);
    }
    ap_linexpr1_set_coeff_scalar_int(&expr, registerName, coef);
}

void addConstToExpr(int num) {
    ap_linexpr1_set_cst_scalar_int(&expr, num);
}

void addTopToExpr() {
    ap_interval_t* interval=ap_interval_alloc();
    ap_interval_set_top(interval);
    ap_linexpr1_set_cst_interval(&expr, interval);
    ap_interval_free(interval);
}

void addMemToExpr(int addr) {
    ap_environment_t *new;
    //char varname[MAX_VAR_CHARS+1]; // TODO apron bug?: array causes segfault
    char* varname=malloc((MAX_VAR_CHARS+1)*sizeof(char));

    sprintf(varname,"m%x",addr);
    if (!varExists(varname)) {
        //ap_environment_fdump(stdout,env);
        new=ap_environment_add(env,(void**)&varname,1,NULL,0);
        if (new==NULL) {
            printf("addMemToExpr error\n");
        }
        env=new;
    }
    //expr = ap_linexpr1_make(new,AP_LINEXPR_SPARSE,0);
    expr = ap_linexpr1_make(env,AP_LINEXPR_SPARSE,0);
    ap_linexpr1_set_coeff_scalar_int(&expr, varname, 1);
    //ap_linexpr1_fprint(stdout,&expr);
    free(varname); // do not free before ap_linexpr1_set_coeff_scalar_int
}

void setMemToTop(int from) {
    int addr;
    //printf("setMemToTop\n");
    for (int indvar=0; indvar<env->intdim; indvar++) {
        char* var=env->var_of_dim[indvar];
        if (var[0]=='m') {
            addr=strtoul(&var[1],NULL,16);
            if (from<addr) {
                initExpr();
                addTopToExpr();
                assignExpr(var);
            }
        } else if (var[0]>'m') {
            break;
        }
    }
}

inline void assignExpr(char* registerName) {
#ifdef DEBUG_EXPRESSION
    fprintf(stdout," %s <-- ",registerName);
    ap_linexpr1_fprint(stdout,&expr);
    //fprintf(stdout,"\n");
#endif
    if (!varExists(registerName)) {
        varAdd(registerName);
    }
    homogenExprEnv(&expr,&aistate);
    aistate = ap_abstract1_assign_linexpr(man,true,&aistate,registerName,&expr,NULL); // true indica destructivo
#ifdef DEBUG_EXPRESSION
    printf(" ; ");
    ap_interval_t* varinterval=ap_abstract1_bound_variable(man,&aistate,registerName);
    ap_interval_fprint(stdout,varinterval);
    printf("\n");
#endif
#ifdef DEBUG_ABSTRACT_STATE
    ap_abstract1_fprint(stdout,man,&aistate);
#endif
}

void assignExprToReg(int num) {
    char varname[MAX_VAR_CHARS+1];
    sprintf(varname,"r%d",num);
    assignExpr(varname);
}

void assignExprToTemp(int num) {
    char varname[MAX_VAR_CHARS+1];
    sprintf(varname,"t%d",num);
    assignExpr(varname);
}

void assignExprToShadow(int num) {
    char varname[MAX_VAR_CHARS+1];
    sprintf(varname,"s%d",num);
    assignExpr(varname);
}

void assignExprToMem(int num) {
    char varname[MAX_VAR_CHARS+1];
    sprintf(varname,"m%x",num);
    assignExpr(varname);
}

void setShadowReg(int regnum) {
    char regname[MAX_VAR_CHARS+1];
    sprintf(regname,"r%d",regnum);
    if (!varExists(regname)) {
        varAdd(regname);
    }
    initExpr();
    addRegToExpr(1, regname);
    assignExprToShadow(regnum);
}

int getRegInExpr() {
    ap_var_t var;
    ap_coeff_t *coeffvar;
    size_t j;
    int numvars=0;
    char* varname;
    //ap_linexpr1_fprint(stdout,&expr); fprintf(stdout,"\n");
    ap_linexpr1_ForeachLinterm1(&expr, j, var, coeffvar) {
        if (!ap_coeff_zero(coeffvar)) {
            numvars++;
            varname=var;
        }
    }
    if (numvars==1 && varname[0]=='r') {
        return atoi(&varname[1]);
    } else {
        return -1;
    }
}

int getTempInExpr() {
    ap_var_t var;
    ap_coeff_t *coeffvar;
    size_t j;
    int numvars=0;
    char* varname;
    //ap_linexpr1_fprint(stdout,&expr); fprintf(stdout,"\n");
    ap_linexpr1_ForeachLinterm1(&expr, j, var, coeffvar) {
        if (!ap_coeff_zero(coeffvar)) {
            numvars++;
            varname=var;
        }
    }
    if (numvars==1 && varname[0]=='t') {
        return atoi(&varname[1]);
    } else {
        return -1;
    }
}

void initStack(int reg, unsigned int value) {
    initExpr();
    ap_linexpr1_set_cst_scalar_double(&expr, (double) value);
    assignExprToReg(reg);
}

/////////////// correctOverflow not yet tested
/*
void correctOverflow(char *v) {
    double dblbound;
    unsigned int bound;
    ap_interval_t* varinterval=ap_abstract1_bound_variable(man,&aistate,v);
    if (ap_scalar_equal(varinterval->inf,varinterval->sup)) {
        // ap_scalar_get_double() in documentation
        ap_double_set_scalar(&dblbound,varinterval->sup,0);
        if (dblbound >0x7FFFFFFF) {
            bound=(unsigned int)dblbound;
            initExpr();
            addConstToExpr((int)bound);
            assignExpr(v);
        } else if (dblbound < -0xFFFFFFFF) {
            bound=(unsigned int)dblbound;
            initExpr();
            addConstToExpr((int)bound);
            assignExpr(v);
        }
    }
}
*/
/////////////// correctOverflow not yet tested



////////////////////////// ABSTRACT STATES ///////////////////////////
void newNodeState(int id, int addr) {
    if (nextemptynode<MAX_NODE_STATES) {
        nodestates[nextemptynode].id=id;
        nodestates[nextemptynode].addr=addr;
        nodestates[nextemptynode].in=ap_abstract1_copy(man,&aistate);
        //ap_lincons1_clear(&nodestates[nextemptynode].cons);
        // TODO: set node.cons to zero to know it is invalid
        nextemptynode++;
        //printf("created node %d of %d with addr %d\n", nextemptynode, MAX_NODE_STATES, addr);
    } else {
        printf("newNodeState error: more than %d nodes needed\n",MAX_NODE_STATES);
        exit(-1);
    }
}

void setOutState(int id) {
    // TODO: optimize node search: its .in value has been just touched
    for (int node=nextemptynode-1; node>=0; node--) {
        if (nodestates[node].id==id) {
            nodestates[node].out=ap_abstract1_copy(man,&aistate);
            nodestates[node].cons=cons;
            nodestates[node].env=env;
            return;
        }
    }
    printf("setOutState: node not found\n");
    exit(-1);
}

void setDefaultState(int id) {
    ap_abstract1_clear(man,&aistate);
    for (int node=nextemptynode-1; node>=0; node--) {
        if (nodestates[node].id==id) {
            aistate=ap_abstract1_copy(man,&nodestates[node].out);
            cons=nodestates[node].cons;
            env=nodestates[node].env;
            return;
        }
    }
    printf("setDefaultState: node not found\n");
    exit(-1);
}

int nodeExists(int id, int addr) {
    for (int node=nextemptynode-1; node>=0; node--) {
        if (nodestates[node].id==id) {
            if (nodestates[node].addr==addr) {
                return 1;
            } else {
                printf("Error: node id exists with addr %x instead of %x\n",nodestates[node].addr,addr);
                exit(-1);
            }
        }
    }
    return 0;
}

void clearInputStates() {
    for (int node=0; node<nextemptynode; node++) {
        ap_abstract1_clear(man,&nodestates[node].in);
    }
}

void clearNegativeNodes() {
    while (nextemptynode>0 && nodestates[nextemptynode-1].addr<0) {
        //printf("clearing node.in with addr -0x%x\n",-nodestates[nextemptynode-1].addr);
        ap_abstract1_clear(man,&nodestates[nextemptynode-1].in);
        //printf("clearing node.out with addr -0x%x\n",-nodestates[nextemptynode-1].addr);
        ap_abstract1_clear(man,&nodestates[nextemptynode-1].out);
        nextemptynode--;
    }
    // this second while shoult not be needed
    int node=nextemptynode-1;
    while (node>=0) {
        if (nodestates[node].addr<0) {
            printf("clearNegativeNodes warning: found negative interleaved nodes\n");
            ap_abstract1_clear(man,&nodestates[node].in);
            ap_abstract1_clear(man,&nodestates[node].out);
            nodestates[node].addr=0;
        }
        node--;
    }
}


////////////////////////// ENVIRONMENTS ///////////////////////////

void homogenExprEnv(ap_linexpr1_t *expr, ap_abstract1_t *a2) {
    ap_environment_t *e1,*e2,*e3;
    int comp;
    ap_dimchange_t *d1, *d2;

    e1=ap_linexpr1_envref(expr);
    //printf("\ne1\n");
    //ap_environment_fdump(stdout,e1);
    e2=ap_abstract1_environment(man,a2);
    //printf("\ne2\n");
    //ap_environment_fdump(stdout,e2);
    comp=ap_environment_compare(e1,e2);
    switch (comp) {
        case 0: break; // equals
        case -1: // e1 included in e2
            ap_linexpr1_extend_environment_with(expr,e2);
            env=e2;
            break;
        case 1: // e2 included in e1
            *a2=ap_abstract1_change_environment(man,true,a2,e1,false);
            env=e1;
            break;
        case 2: // least common environment exists and is a strict superset
            e3=ap_environment_lce(e1,e2,&d1,&d2);
            ap_linexpr1_extend_environment_with(expr,e3);
            *a2=ap_abstract1_change_environment(man,true,a2,e3,false);
            env=e3;
            break;
        default: // (-2) not compatible
            printf("homogenEnv: trying to homogenize non compatible envs\n");
            exit(-1);
    }
}
/*
void homogenConsEnv(ap_lincons1_t *cons, ap_abstract1_t *a2) {
    ap_environment_t *e1,*e2,*e3;
    int comp;
    ap_dimchange_t *d1, *d2;

    e1=env;
    e2=ap_abstract1_environment(man,a2);
    comp=ap_environment_compare(e1,e2);
    switch (comp) {
        case 0: break; // equals
        case -1: // e1 included in e2
            ap_lincons1_extend_environment_with(cons,e2);
            env=e2;
            break;
        case 1: // e2 included in e1
            *a2=ap_abstract1_change_environment(man,true,a2,e1,false);
            env=e1;
            break;
        case 2: // least common environment exists and is a strict superset
            e3=ap_environment_lce(e1,e2,&d1,&d2);
            ap_lincons1_extend_environment_with(cons,e3);
            *a2=ap_abstract1_change_environment(man,true,a2,e3,false);
            env=e3;
            break;
        default: // (-2) not compatible
            printf("homogenEnv: trying to homogenize non compatible envs\n");
            exit(-1);
    }
}
*/
void homogenArrayEnv(ap_lincons1_array_t *array, ap_abstract1_t *a2) {
    ap_environment_t *e1,*e2,*e3;
    int comp;
    ap_dimchange_t *d1, *d2;

    e1=ap_lincons1_array_envref(array);
    e2=ap_abstract1_environment(man,a2);
    comp=ap_environment_compare(e1,e2);
    switch (comp) {
        case 0: break; // equals
        case -1: // e1 included in e2
            ap_lincons1_array_extend_environment_with(array,e2);
            env=e2;
            break;
        case 1: // e2 included in e1
            *a2=ap_abstract1_change_environment(man,true,a2,e1,false);
            env=e1;
            break;
        case 2: // least common environment exists and is a strict superset
            e3=ap_environment_lce(e1,e2,&d1,&d2);
            ap_lincons1_array_extend_environment_with(array,e3);
            *a2=ap_abstract1_change_environment(man,true,a2,e3,false);
            env=e3;
            break;
        default: // (-2) not compatible
            printf("homogenEnv: trying to homogenize non compatible envs\n");
            exit(-1);
    }
}

void homogenEnv(ap_abstract1_t *a1, ap_abstract1_t *a2) {
    ap_environment_t *e1,*e2,*e3;
    int comp;
    ap_dimchange_t *d1, *d2;

    e1=ap_abstract1_environment(man,a1);
    e2=ap_abstract1_environment(man,a2);
    comp=ap_environment_compare(e1,e2);
    switch (comp) {
        case 0: break; // equals
        case -1: // e1 included in e2
            *a1=ap_abstract1_change_environment(man,true,a1,e2,false);
            env=e2;
            break;
        case 1: // e2 included in e1
            *a2=ap_abstract1_change_environment(man,true,a2,e1,false);
            env=e1;
            break;
        case 2: // least common environment exists and is a strict superset
            e3=ap_environment_lce(e1,e2,&d1,&d2);
            *a1=ap_abstract1_change_environment(man,true,a1,e3,false);
            *a2=ap_abstract1_change_environment(man,true,a2,e3,false);
            env=e3;
            break;
        default: // (-2) not compatible
            printf("homogenEnv: trying to homogenize non compatible envs\n");
            exit(-1);
    }
}


////////////////////////// CONSTRAINTS ///////////////////////////
int addConstraint() {
    ap_lincons1_t auxcons=ap_lincons1_copy(&cons); // TODO: copy is not required?
    ap_lincons1_extend_environment_with(&auxcons,env);
#ifdef DEBUG_ABSTRACT_STATE
    ap_abstract1_fprint(stdout,man,&aistate);
#endif
#ifdef DEBUG_CONSTRAINT
    ap_lincons1_fprint(stdout,&cons); printf("\n");
#endif
    //ap_environment_fdump(stdout,env);
    ap_lincons1_array_t array=ap_lincons1_array_make(env,1);
    ap_lincons1_array_set(&array,0,&auxcons);
    //ap_lincons1_array_fprint(stdout,&array); printf("\n");
    homogenArrayEnv(&array,&aistate);
    aistate=ap_abstract1_meet_lincons_array(man,true,&aistate,&array);
#ifdef DEBUG_ABSTRACT_STATE
    ap_abstract1_fprint(stdout,man,&aistate);
#endif
    if (ap_abstract1_is_bottom(man, &aistate)) {
        printf("bottom after adding constraint\n");
        return 0;
    } //else if (ap_abstract1_is_top(man, &aistate)) {
      //  printf("is top\n");
    //}
    return 1;
}

int addNegConstraint() {
    ap_lincons1_t consneg;
#ifdef DEBUG_CONSTRAINT
    ap_lincons1_fprint(stdout,&cons); printf("  -->  ");
#endif
    ap_linexpr1_t eaux=ap_lincons1_linexpr1ref(&cons);
    ap_linexpr1_t e=ap_linexpr1_copy(&eaux); // copy required!
    // negar coeficientes
    size_t i;
    ap_var_t var;
    ap_coeff_t *coeff;
    ap_coeff_t *newcoeff=ap_coeff_alloc(AP_COEFF_SCALAR);
    ap_linexpr1_ForeachLinterm1(&e, i, var, coeff) {
        ap_coeff_neg(newcoeff,coeff);
        ap_linexpr1_set_coeff(&e, var, newcoeff);
    };
    // negar constante
    ap_linexpr1_get_cst(newcoeff, &e);
    ap_coeff_neg(newcoeff,newcoeff);
    ap_linexpr1_set_cst(&e,newcoeff);
    ap_coeff_free(newcoeff);
    // negar tipo de restricción
    ap_constyp_t* constype=ap_lincons1_constypref(&cons);
    switch (*constype) {
        case(AP_CONS_EQ): consneg=ap_lincons1_make(AP_CONS_DISEQ,&e,NULL); break;
        case(AP_CONS_DISEQ): consneg=ap_lincons1_make(AP_CONS_EQ,&e,NULL); break;
        case(AP_CONS_SUPEQ): consneg=ap_lincons1_make(AP_CONS_SUP,&e,NULL); break;
        case(AP_CONS_SUP): consneg=ap_lincons1_make(AP_CONS_SUPEQ,&e,NULL); break;
        default:
            printf("addNegConstraint: unknown cons type\n");
            exit(-1);
    }
#ifdef DEBUG_CONSTRAINT
    ap_lincons1_fprint(stdout,&consneg); printf("\n");
#endif

    ap_lincons1_extend_environment_with(&consneg,env);
    ap_lincons1_array_t array=ap_lincons1_array_make(env,1);
    ap_lincons1_array_set(&array,0,&consneg);
    // ap_lincons1_array_fprint(stdout,&array); printf("\n");
    homogenArrayEnv(&array,&aistate);
    aistate=ap_abstract1_meet_lincons_array(man,true,&aistate,&array);
#ifdef DEBUG_ABSTRACT_STATE
    ap_abstract1_fprint(stdout,man,&aistate);
#endif
    if (ap_abstract1_is_bottom(man, &aistate)) {
        printf("bottom after adding constraint\n");
        return 0;
    } //else if (ap_abstract1_is_top(man, &aistate)) {
      //  printf("is top\n");
    //}
    return 1;
}


////////////////////////// JOIN / WIDENING ///////////////////////////
int wideningStatesNeedsUpdate(int id) {
    for (int node=nextemptynode-1; node>=0; node--) {
        if (nodestates[node].id==id) {

#ifdef DEBUG_ABSTRACT_STATE
            printf("  node.prevIterIN JOIN node.currIterIN:\n");
            printf(" ------------ \n");
            ap_abstract1_fprint(stdout,man,&nodestates[node].in);
            printf(" join \n");
            ap_abstract1_fprint(stdout,man,&aistate);
            printf(" = \n");
#endif
            homogenEnv(&nodestates[node].in,&aistate);
            ap_abstract1_t join=ap_abstract1_join(man,false,&nodestates[node].in,&aistate);
#ifdef DEBUG_ABSTRACT_STATE
            ap_abstract1_fprint(stdout,man,&join);
            printf(" ------------ \n");
#endif

#ifdef DEBUG_ABSTRACT_STATE
            printf("  node.prevIterIN WIDEN joined:\n");
            printf(" ------------ \n");
            ap_abstract1_fprint(stdout,man,&nodestates[node].in);
            printf(" WIDEN \n");
            ap_abstract1_fprint(stdout,man,&join);
            printf(" = \n");
#endif
            // a1 is supposed to be included in a2
            ap_abstract1_t widen=ap_abstract1_widening(man,&nodestates[node].in,&join);
#ifdef DEBUG_ABSTRACT_STATE
            ap_abstract1_fprint(stdout,man,&widen);
            printf(" ------------ \n");
#endif
            ap_abstract1_clear(man,&join);

            if (ap_abstract1_is_eq(man,&widen,&nodestates[node].in)) {
                ap_abstract1_clear(man,&widen);
                return 0;
            } else {
                ap_abstract1_clear(man,&nodestates[node].in);
                // minimize environment
                widen=ap_abstract1_minimize_environment(man,true,&widen);
                env=ap_abstract1_environment(man,&widen);
                nodestates[node].in=ap_abstract1_copy(man,&widen);
                ap_abstract1_clear(man,&aistate);
                aistate=widen;
                ap_abstract1_clear(man,&nodestates[node].out);
                // TODO: free node.cons if needed
                return 1;
            }
        }
    }
    printf("widenStatesNeedsUpdate: node not found\n");
    exit(-1);
}

int joinStatesNeedsUpdate(int id) {
    for (int node=nextemptynode-1; node>=0; node--) {
        if (nodestates[node].id==id) {
#ifdef DEBUG_ABSTRACT_STATE
            ap_abstract1_fprint(stdout,man,&nodestates[node].in);
            printf(" join \n");
            ap_abstract1_fprint(stdout,man,&aistate);
            printf(" = \n");
#endif
            homogenEnv(&nodestates[node].in,&aistate);
            ap_abstract1_t join=ap_abstract1_join(man,false,&nodestates[node].in,&aistate);
#ifdef DEBUG_ABSTRACT_STATE
            ap_abstract1_fprint(stdout,man,&join);
#endif
            if (ap_abstract1_is_eq(man,&join,&nodestates[node].in)) { // no changes
                ap_abstract1_clear(man,&join);
                return 0;
            } else { // changes; out cleared; must be recalculated
                ap_abstract1_clear(man,&nodestates[node].in);
                // minimize environment
                join=ap_abstract1_minimize_environment(man,true,&join);
                env=ap_abstract1_environment(man,&join);
                nodestates[node].in=ap_abstract1_copy(man,&join);
                ap_abstract1_clear(man,&aistate);
                aistate=join;
                // warning, if joining all end-of-program states, clear will fail
                ap_abstract1_clear(man,&nodestates[node].out);
                // TODO: free node.cons if needed
                return 1;
            }
        }
    }
    printf("joinStatesNeedsUpdate: node not found\n");
    exit(-1);
}


////////////////////////// SATISFACTION / INTERVALS ///////////////////////////
int satisfiesEquality2(char *v1, char* v2) {
    if (!strcmp(v1,v2)) {
        printf("Warning: satisfiesEquality called with the same variable name\n");
    }
    env=ap_abstract1_environment(man,&aistate);
    initExpr();
    addRegToExpr(1, v1);
    addRegToExpr(-1, v2);
    setConsEQ();
    completeCons();
    //ap_lincons1_fprint(stdout,&cons); //printf("\n");
    //ap_abstract1_fprint(stdout,man,a);
    //homogenConsEnv(&cons,&aistate);
    int res=ap_abstract1_sat_lincons(man,&aistate,&cons);
//    if (res==0) {
//        printf(" --> False\n");
//    } else {
//        printf(" --> True\n");
//    }
    return res;
}
/* FUNCIONA
int satisfiesEquality(ap_abstract1_t *a, char *v1, char* v2) {
    if (!strcmp(v1,v2)) {
        printf("Warning: satisfiesEquality called with the same variable name\n");
    }
    env=ap_abstract1_environment(man,a);
    initExpr();
    addRegToExpr(1, v1);
    addRegToExpr(-1, v2);
    setConsEQ();
    completeCons();
    //ap_lincons1_fprint(stdout,&cons); //printf("\n");
    //ap_abstract1_fprint(stdout,man,a);
    //homogenConsEnv(&cons,a);
    *a=ap_abstract1_change_environment(man,true,a,env,false);
    int res=ap_abstract1_sat_lincons(man,a,&cons);
//    if (res==0) {
//        printf(" --> False\n");
//    } else {
//        printf(" --> True\n");
//    }
    return res;
}*/
int satisfiesEquality(ap_abstract1_t *a, char *v1, char* v2) {
    if (!strcmp(v1,v2)) {
        printf("Warning: satisfiesEquality called with the same variable name\n");
    }
    env=ap_abstract1_environment(man,a);
    if (varExists(v1) && varExists(v2)) {
        initExpr();
        addRegToExpr(1, v1);
        addRegToExpr(-1, v2);
        setConsEQ();
        completeCons();
        //*a=ap_abstract1_change_environment(man,true,a,env,false);
        int res=ap_abstract1_sat_lincons(man,a,&cons);
//    if (res==0) {
//        printf(" --> False\n");
//    } else {
//        printf(" --> True\n");
//    }
        return res;
    } else {
        return 0;
    }
}

/*
double getDifferenceSupInterval(char *v1, char* v2) {
    double stepdbl;
    //long long int aux;

    initExpr();
    addRegToExpr(1, v1);
    addRegToExpr(-1, v2);
    ap_interval_t* varinterval=ap_abstract1_bound_linexpr(man,&aistate,&expr);
    ap_double_set_scalar(&stepdbl,varinterval->sup,0);
    //printf("  %s-%s interval is ",v1,v2);
    //ap_interval_fprint(stdout,varinterval); printf("\n");
    return stepdbl;
//    aux=stepdbl;
//    return (int)(aux%(0x100000000));
}

double getDifferenceInfInterval(char *v1, char* v2) {
    double stepdbl;
    //long long int aux;

    initExpr();
    addRegToExpr(1, v1);
    addRegToExpr(-1, v2);
    ap_interval_t* varinterval=ap_abstract1_bound_linexpr(man,&aistate,&expr);
    ap_double_set_scalar(&stepdbl,varinterval->inf,0);
    return stepdbl;
//    aux=stepdbl;
//    return (int)(aux%(0x100000000));
}*/

int sameBoundsDifferenceInterval(char *v1, char* v2, int* bound) {
    double dblbound;

    //printf("  %s-%s interval is ",v1,v2);
    initExpr();
    addRegToExpr(1, v1);
    addRegToExpr(-1, v2);
    aistate=ap_abstract1_change_environment(man,true,&aistate,env,false);
    ap_interval_t* varinterval=ap_abstract1_bound_linexpr(man,&aistate,&expr);
    //ap_interval_fprint(stdout,varinterval); printf("\n");
    if (ap_scalar_equal(varinterval->inf,varinterval->sup)) {
        // ap_scalar_get_double() in documentation
        ap_double_set_scalar(&dblbound,varinterval->sup,0);
        *bound=(int)dblbound; //% 0x100000000;
        return 1;
    }
    return 0;
}

double getVariableSupInterval(char *v) {
    double addrdbl;
    //long long int aux;
    if (!varExists(v)) {
        //varAdd(v);
        return INFINITY;
    }
    aistate=ap_abstract1_change_environment(man,true,&aistate,env,false);
    ap_interval_t* varinterval=ap_abstract1_bound_variable(man,&aistate,v);
    ap_double_set_scalar(&addrdbl,varinterval->sup,0);
    //printf("  %s interval is ",v);
    //ap_interval_fprint(stdout,varinterval); printf("\n");
    return addrdbl;
}
double getVariableInfInterval(char *v) {
    double addrdbl;
    //long long int aux;
    if (!varExists(v)) {
        return -INFINITY;
    }
    ap_interval_t* varinterval=ap_abstract1_bound_variable(man,&aistate,v);
    ap_double_set_scalar(&addrdbl,varinterval->inf,0);
    return addrdbl;
}

int sameBoundsVariable(char *v, int* bound) {
    double dblbound;
    if (!varExists(v)) {
        return 0;
    }
    aistate=ap_abstract1_change_environment(man,true,&aistate,env,false);
    ap_interval_t* varinterval=ap_abstract1_bound_variable(man,&aistate,v);
    if (ap_scalar_equal(varinterval->inf,varinterval->sup)) {
        // ap_scalar_get_double() in documentation
        ap_double_set_scalar(&dblbound,varinterval->sup,0);
        // For registers/variables holding addresses, negative values are actually stack addresses
        //*bound=(unsigned int)dblbound;
        // No longer needed unsigned, since stack is initialized as double
        *bound=(int)dblbound;
        return 1;
    }
    return 0;
}
