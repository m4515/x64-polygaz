#!venv/bin/python3
# coding=utf-8

import sys
import analysis1 as reuse_detection

def main():
    """ Launches the data reuse analysis. Should generate .xml """
    if len(sys.argv) != 3:
        print("usage:", sys.argv[0], "fast|emulated <binaryfile>")
        sys.exit()
    cfg_type = sys.argv[1]
    filename = sys.argv[2]
    cache_line_size = 64 # bytes (default intel/arm L1: 64 B)
    if cfg_type == "emulated":
        reuse_detection.reuse_analysis(filename, True, cache_line_size)
    elif cfg_type == "fast":
        reuse_detection.reuse_analysis(filename, False, cache_line_size)
    else:
        print("Unknown CFG type")

main()
