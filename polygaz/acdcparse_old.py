
# coding=utf-8

""" Module for reading and parsing a locking configuration .xml file """


import xml.etree.ElementTree as ET
import sys


drp_set = set()

def parse(filename):
    """ Parsing function """

    tree = ET.parse(filename)
    root = tree.getroot()

    for child in root:
        #drps.add(int(child.attrib["pc"], 0)) # 0=guess base
        drp_set.add(int(child.find("pc").text, 0)) # 0=guess base
    print("PCs with DRP in ACDC (%d):" % len(drp_set), drp_set)

def has_drp(pc):
    if pc in drp_set:
        return True
    else:
        return False

def drps():
    return list(drp_set)

def main():
    """ Simply parses, for testing purposes """
    if len(sys.argv) != 2:
        print("usage:", sys.argv[0], "<acdc_config.xml>")
        sys.exit()
    filename = sys.argv[1]

    parse(filename)

if __name__ == "__main__":
    sys.exit(main())
