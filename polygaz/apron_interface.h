#include "ap_global1.h" // include always
#include "oct.h" // octagon
#include "pk.h" // polyhedron newpolka
#include "pkeq.h" // polyhedron pkeq
#include "ap_ppl.h" // polyhedron ppl

#define DOMAIN_OCT 1    // oct    // fftO1 430.00 ludcmpO3
#define DOMAIN_PK 2     // pk     // fftO1 111.01 ludcmpO3 456.46
#define DOMAIN_AP_PPL 3 // ap_ppl // fftO1  87.76 ludcmpO3
#define DOMAIN_PKEQ 4   // pkeq   // fftO1  75.43 ludcmpO3 412.71

// comment to disable debug info
#define DEBUG_ABSTRACT_STATE // prints the abstract state after each operation
#define DEBUG_EXPRESSION // prints the expression after assignment
#define DEBUG_CONSTRAINT // prints the constraint after set/used
#define DEBUG_MEM

// number of registers to consider
//#define MAX_REGS 0 //40 /**< Max registers to consider, addressed as "r0" to "r99" */
// ARM: r0-r15 -> angr r8-r68 + r72-r80 (sort of comparison input registers)
//#define MAX_SHADOW_REGS 100 /**< Max shadow registers to consider, addressed as "s0" to "s99" */
//#define MAX_TEMP_REGS 0 //200 /**< Max temporal registers to consider, addressed as "t0 to "t99" */
#define MAX_VAR_CHARS 9 /**< Max ASCII chars for the variable names above (not including ending 0) */


/**
 * Struct for a node abstract state
 *
 * addr holds the address of its first instruction
 * in holds its input abstract state
 * out holds its output abstract state (may be empty at first)
 * cons holds the brach output constraint, if conditional-branch
 * When first set, addr and in are set, out and cons are not initialized
 */
typedef struct {
    int id; /**< Identifier of basic block */
    int addr; /**< Address of first instruction of the basic block */
    ap_abstract1_t in; /**< INPUT abstract state */
    ap_abstract1_t out; /**< OUTPUT abstract state */
    ap_lincons1_t cons; /**< condition (conditional constraint) for conditional branch, if any */
    ap_environment_t *env;
} nodestates_t;

/**
 * Initializes global vars and allocates states for numcfgnodes
 */
//void init(int numcfgnodes);
void init(int numcfgnodes, int domain);
//void init(int numcfgnodes, int numloops);


////////////////////////// CONSTRAINTS ///////////////////////////
/**
 * Creates a new equality constraint from expr
 */
void setConsEQ();

/**
 * Creates a new >= constraint from expr
 */
void setConsSUPEQ();

/**
 * Creates a new > constraint from expr
 */
void setConsSUP();

/**
 * Creates a new != constraint from expr
 */
void setConsDISEQ();

/**
 * Completes a constraint
 * Required when completing it: copies structure to preserve expr
 */
void completeCons();

/**
 * Discards a constraint
 * If more than one comparison is detected (complex boolean expression)
 * the previous constraint (from the first comparison) is discarded.
 */
void discardCons();

////////////////////////// ENVIRONMENT ///////////////////////////
/**
 * Checks whether a variable name exists in the environment
 * @param[in] var string of variable name
 * @return 1 if exists; 0 otherwise
 */
int varExists(char* var);

/**
 * Adds a variable name to the environment, as integer
 * @param[in] var string of variable name
 */
void varAdd(char* var);


////////////////////////// EXPRESSIONS ///////////////////////////
/**
 * Initializes linear expression
 */
void initExpr();

/**
 * Adds register to expr: coef*registerName
 */
void addRegToExpr(int coef, char* registerName);

/**
 * Adds constant to expr
 */
void addConstToExpr(int num);

/**
 * Adds Top to expr
 */
void addTopToExpr();

/**
 * Adds memory to expr
 */
void addMemToExpr(int addr);

/**
 * Sets memory to Top from the specified address
 */
void setMemToTop(int from);

/**
 * Assigns expr to registername and updates aistate
 */
void assignExpr(char* registerName);

/**
 * Assigns expr to register and updates aistate
 *
 * @param[in] num Number of the register to update
 */
void assignExprToReg(int num);

/**
 * Assigns expr to temporal/virtual storage and updates aistate
 *
 * @param[in] num Number of the temporal storage to update
 */
void assignExprToTemp(int num);

/**
 * Assigns expr to shadow variable to test for induction var and updates aistate
 *
 * @param[in] num Number of the shadow var to update
 */
void assignExprToShadow(int num);

void assignExprToMem(int num);
void setShadowReg(int regnum);
int getRegInExpr();
int getTempInExpr();

/**
 * Initializes reg as the stack pointer with value
 */
void initStack(int reg, unsigned int value);


////////////////////////// ABSTRACT STATES ///////////////////////////
/**
 * Copy aistate as the IN state of node addr. OUT state not yet set.
 * Copy is completely disjoined; both can be used without affecting each other.
 *
 * @param[in] addr Starting address of the node to add
 */
void newNodeState(int id, int addr);

/**
 * Set a new OUT state (copy aistate) for node addr
 * Copy is completely disjoined; both can be used without affecting each other.
 */
void setOutState(int id);

/**
 * Recover the OUT state of node addr and set it as default (aistate)
 * Copy is completely disjoined; both can be used without affecting each other.
 * Global cons is copied (reusing pointers, must not be freed!) from node output constraint
 */
void setDefaultState(int id);

/**
 * Tests whether a node exists
 *
 * @return 1 if node exists, 0 otherwise
 */
int nodeExists(int id, int addr);

/**
 * Clears input states from nodes.
 * May free much memory after memory reference analysis, and input states
 * are not needed for induction variable analysis.
 */
void clearInputStates();

/**
 * Clears nodes with negative addrs (used for induction variable analysis)
 */
void clearNegativeNodes();


////////////////////////// ENVIRONMENTS ///////////////////////////
/**
 * Homogenizes the environment of expression and abstract value,
 * and updates env and their corresponding environments accordingly
 */
void homogenExprEnv(ap_linexpr1_t *expr, ap_abstract1_t *a2);

/**
 * Homogenizes the environment of constraint and abstract value,
 * and updates env and their corresponding environments accordingly
 */
//void homogenConsEnv(ap_lincons1_t *cons, ap_abstract1_t *a2);

/**
 * Homogenizes the environment of array and abstract value,
 * and updates env and their corresponding environments accordingly
 */
void homogenArrayEnv(ap_lincons1_array_t *array, ap_abstract1_t *a2);

/**
 * Homogenizes the environment of two abstract values,
 * and updates env and their corresponding environments accordingly
 */
void homogenEnv(ap_abstract1_t *a1, ap_abstract1_t *a2);


////////////////////////// CONSTRAINTS ///////////////////////////
/**
 * Adds constraint (if condition) to default state (aistate)
 *
 * @return 0 if state is bottom after adding constraint
 */
int addConstraint();

/**
 * Adds constraint (else condition) to default state (aistate)
 *
 * @return 0 if state is bottom after adding constraint
 */
int addNegConstraint();


////////////////////////// JOIN / WIDENING ///////////////////////////
/**
 * Joins default state and IN state of node addr.
 * Returns whether OUT state must be recalculated (if so, clears OUT state)
 *
 * @return 1 if OUT state must be recalculated; 0 if IN==join(IN,prev.out)
 */
int joinStatesNeedsUpdate(int id);

/**
 * Widens default state and IN state of node addr, with threshold expr.
 * Returns whether OUT state must be recalculated (if so, clears OUT state)
 *
 * @return 1 if OUT state must be recalculated; 0 if IN==join(IN,prev.out)
 */
int wideningStatesNeedsUpdate(int id);


////////////////////////// SATISFACTION / INTERVALS ///////////////////////////
/**
 * Tests whether constraint v1=v2 is satisfied in abstract state
 *
 * @return bool ap_abstract1_sat_lincons()
 */
int satisfiesEquality(ap_abstract1_t *a, char *v1, char* v2);

int sameBoundsDifferenceInterval(char *v1, char* v2, int* bound);
//double getDifferenceSupInterval(char *v1, char* v2);
//double getDifferenceInfInterval(char *v1, char* v2);
double getVariableSupInterval(char *v);
double getVariableInfInterval(char *v);
