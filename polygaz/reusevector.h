#include <stdlib.h>
#include <stdio.h>

typedef enum {
    VECTOR_KNOWN_COEF,
    VECTOR_UNK_COEF,
    VECTOR_UNK_BASE, // JUST BEFORE LAST TAG, with loopaddr 0
    VECTOR_END, // LAST TAG, with valid coeff (may be 0) and loopaddr 0
} vector_tag_t;

typedef struct {
    int coeff;
    int loopaddr;
    vector_tag_t tag;
} reuseth_vector_t;

/**
 * Initializes reuse theory vector with a contant base address
 */
reuseth_vector_t* rtvectorInitMemRef(int base);

/**
 * Generates a copy of a reuse theory vector
 */
reuseth_vector_t* rtvectorCopy(reuseth_vector_t *vector);

/**
 * Gets the length of the vector (MUST EXIST)
 */
int rtvectorGetLen(reuseth_vector_t *vector);

/**
 * Adds an unknown loop induction variable
 */
reuseth_vector_t* rtvectorAddUnkLoop(reuseth_vector_t *vector, int loopaddr);

/**
 * Adds a known loop induction variable
 */
reuseth_vector_t* rtvectorAddLoop(reuseth_vector_t *vector, int coef, int loopaddr);

/**
 * Tests whether vector has an unknown base address
 */
int rtvectorIsBaseKnown(reuseth_vector_t *vector);

/**
 * Adds (updates) a base address
 */
reuseth_vector_t* rtvectorAddBase(reuseth_vector_t *vector, int base);

/**
 * Adds an unknown base address
 */
reuseth_vector_t* rtvectorAddUnkBase(reuseth_vector_t *vector);

/**
 * Prints a reuse theory vector
 */
void rtvectorPrint(reuseth_vector_t *vector);

/**
 * Compares two vectors; returns 1 if they are equals, 0 otherwise
 */
int rtvectorEquals(reuseth_vector_t *v1, reuseth_vector_t *v2);

/**
 * Tests whether all elements are known; returns 1 if so, 0 otherwise
 */
int rtvectorAllKnown(reuseth_vector_t *v);

/**
 * Tests whether v is constant (known or unknown)
 */
int rtvectorIsConstant(reuseth_vector_t *v);

/**
 * Returns the vector base
 */
int rtvectorGetBase(reuseth_vector_t *v);

/**
 * Returns the vector coefficient at position n
 */
int rtvectorGetCoef(reuseth_vector_t *vector, int i);

/**
 * Returns the vector loop-address at position n
 */
int rtvectorGetLoopAddr(reuseth_vector_t *vector, int i);

/**
 * Returns the coefficient of the deepest loop
 * If unknown, returns INT_MAX
 */
int rtvectorGetDeepestCoef(reuseth_vector_t *vector);
int rtvectorGetCoefSum(reuseth_vector_t *vector);
