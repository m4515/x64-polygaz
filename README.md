# x64 polygaz

Adaptación para habilitar el análisis de binarios x86 de 64 bits de las herramientas desarrolladas por el GAZ (Grupo de arquitectura de computadores de Zaragoza) para el análisis de reuso de datos en aplicaciones de tiempo real. 

## Name
x64 polygaz.

## Description
Este proyecto pretende ampliar la funcionalidad de la herramienta polygaz desarrollada en al grupo de arquitectura de computadores de Zaragoza gaZ para análisis de reuso de datos en sistemas de tiempo real con memorias cache.

## Installation
Se pueden seguir los mismos pasos que en el Readme de referencia de la herramienta original que se ha mantenido en el directorio polygaz.

## Usage
Principalmente se han modificado mkff.py y analysis1.py. 


Se ha creado reuseparse.py a partir de xmlparse.py que permite obtener datos de reuso temporal y espacial con un nuevo modelo de representación por tuplas. También se ha creado reuse_boxplot.r para crear gráficos a partir de las tuplas de reuso.


Para generar plantillas usar mkff.sh


Para analisis de reuso usar reuse_analysis.sh


Para obtención de tuplas usar reuseparse.py 


Para crear gráficos a partir de las tuplas usar reuse_boxplot.r

Para crear gráficos en base a la clasificación de reúso obtenida usar reuse_charac.r

## Authors and acknowledgment
Tutorizado por Juan Segarra y Rubén Gran miembros del gaZ.

## Project status
En pausa por el momento
